def ms_timefactor(time):
#    return 1
    if time < 900:
        return 1.2
    elif time < 1300:
        return 1.2 - 0.05 * ((time - 600) // 60)
    else:
        return 1.2

#intervals must be a list of pairs
def merge_intervals(intervals0, ln = 2):
    intervals = [(x[0], x[1]) for x in intervals0]

    while True:
        intervals = sorted(intervals)
        merged = False

        for i in range(len(intervals) - 1):
            for j in range(i + 1, len(intervals)):
                if intervals[j][0] <= intervals[i][1] <= intervals[j][1]:
#                    print "MERGING:", i, j, intervals[i], intervals[j]
                    merged = True
                    intervals[i] = (intervals[i][0], 
                        max(intervals[i][1], intervals[j][1]))
                    del intervals[j]
                    break
            if merged:
                break

        if not merged:
            break

#    print intervals
    return intervals

def intervals_intersect(i1, i2):
    if i1[0] <= i2[0] < i1[1]:
        return True
    if i1[0] < i2[1] <= i1[1]:
        return True

    if i2[0] <= i1[0] < i2[1]:
        return True
    if i2[0] < i1[1] <= i2[1]:
        return True
    return False


class Constrainter:
    def __init__(self, initdict):
        self.max_concurrent = initdict['max_concurrent']
        self.showing_increments = initdict['showing_increments']
        self.min_spreads = initdict['min_spreads']
        self.max_spreads = initdict['max_spreads']
#        self.min_gap
        self.playtimes = {}
        self.basic_intervals = {} #intervals need to be merged!!
        self.merged_intervals = {}
        self.spread_intervals = {}
        self.showings = {}
        self.aud_occupancies = {}
        self.total_perf_count = initdict['total_perf_count']
        self.perf_count = {}
        for k in self.total_perf_count.keys():
            self.perf_count[k] = 0

    #return the next valid showtime
    def next_showtime(self, showing, aud, time, verbose = True):
        #find valid times and then check concurrency
        t = time

        while True:
            rr = self.can_add_showing(showing, aud, t)
            if verbose:
                print "\tfor time {0} rr was".format(t), rr
            if not rr['status']:
                reason = rr['reason']
                if reason in [1,3,4,7,9]:
                    return -1
                elif reason == 2:
                    t += self.showing_increments
                    continue
                elif reason == 5 or reason == 6 or reason == 8:
                    dv = rr['retry']
                    if dv < 0:
                        return -1
                    if dv % self.showing_increments != 0:
                        dv = self.showing_increments * \
                        (1 + dv // self.showing_increments)
                    t = dv
                    continue
                else:
                    assert(0)

            return t


    def add_intervals(self, aud, sh, time):
        lngt = sh.length #FIXME adjust for first / last performance

        title = sh.title
#        interval = (time, 
#            time + 
        d = int(ms_timefactor(time)*self.min_spreads[title])
        d2 = int(ms_timefactor(time + self.max_spreads[title]) * self.max_spreads[title])
        interval = (time - d, time + d)
        assert(interval[1] > interval[0])

        if title not in self.basic_intervals:
            self.basic_intervals[title] = []

        self.basic_intervals[title].append((interval[0], interval[1], 
            aud.ext_id))

        self.merged_intervals[title] = merge_intervals(
            self.basic_intervals[title])

        if aud.ext_id not in self.aud_occupancies:
            self.aud_occupancies[aud.ext_id] = []
        self.aud_occupancies[aud.ext_id].append((time, time + sh.length))

        if title not in self.spread_intervals:
            self.spread_intervals[title] = []
        self.spread_intervals[title].append((time + d, time + d2))
        if title not in self.perf_count:
            self.perf_count[title] = 0
        self.perf_count[title] += 1
#        self.spread_intervals[title].append(

        print "\t\tADDINT", interval


    def can_add_showing(self, showing, aud, time):
        #1 showing known?
        if showing.id in self.showings:
            return {'status': False, 'reason': 1}

        #2 concurrency constraint
        if time in self.playtimes:
            if self.playtimes[time] >= self.max_concurrent:
                return {'status': False, 'reason': 2}
        
        #3 can be hosted in aud
        if not aud.canHostShowing(showing, ignore_freezes = True):
            return {'status': False, 'reason': 3}

        #4 is within the open hours constraint
        if time < aud.open_at or time + showing.length > aud.close_at:
#            print time, aud.open_at, aud.close_at, showing.length, time < aud.open_at, time + showing.length > aud.close_at
            return {'status': False, 'reason': 4}

        #5 violates spread
        if showing.title in self.merged_intervals:
            min_error = 9999999
            diff = 0

            good = True
            for e in self.merged_intervals[showing.title]:
                start = time
                end = time + int(ms_timefactor(time)*
                    self.min_spreads[showing.title])
                
                if (e[0] <= time < e[1]):
#                if intervals_intersect((start, end), e):
                    good = False
                    diff = e[1] - time
                    if diff > 0:
                        if diff < min_error:
                            min_error = diff
#                    print "AAA", diff, time, e
            if not good:
                if min_error > 100000:
                    min_error = -1
                return {'status': False, 'reason': 5, 'retry': time + min_error}

        #6 auditorium occupied: intersects with any interval
        if aud.ext_id in self.aud_occupancies:
            end = time + showing.length

            for e in self.aud_occupancies[aud.ext_id]:
                if (e[0] <= time < e[1]) or \
                    (e[0] <= end < e[1]) or \
                    ((time <= e[0]) and (end >= e[1])):
                        return {'status': False, 'reason': 6, 
                            'retry': e[1]}
        #7 showing's own time constraint
        if len(showing.time_constraint) > 0:
            if time >= showing.time_constraint[0][1]:
                return {'status': False, 'reason': 7} 

            elif time < showing.time_constraint[0][0]:
                return {'status': False, 'reason': 8, 
                    'retry': showing.time_constraint[0][0]}

        #8 too large a spread
        if 0:
#        if self.perf_count[showing.title] >= 0.8 * \
#                self.total_perf_count[showing.title]:
            if showing.title in self.spread_intervals:
                good = False
                print "\t\tSPREADS", self.spread_intervals[showing.title]
                for e in self.spread_intervals[showing.title]:
                    if e[0] <= time < e[1]:
                        good = True
                        break
#                    print "AAA", diff, time, e
                if not good:
                    return {'status': False, 'reason': 9}



        return {'status': True}

    def add_showing(self, showing, aud, time, force = False):
        if not force:
            rr = self.can_add_showing(showing, aud, time)
            if not rr['status']:
                return rr

        #now add
        self.add_intervals(aud, showing, time)
        
#        .title, (time,
#            time + self.min_spreads[showing.title]))
        print "ADDING", showing, time, self.merged_intervals

        if time not in self.playtimes:
            self.playtimes[time] = 0
        self.playtimes[time] += 1

        return {'status': True}

    def fix_auditorium(self, aud):
        pass

    def measure_stress(self, aud):
        if len(aud.showings) == 0:
            return {}

        stress = {}
        for e in aud.showings.values():
#            print "\tFF", e.id
            #time constraints
            st = self.next_showtime(e, aud, aud.open_at, verbose = False)
            if st == -1:
                stress[e.id] = 1
                continue

            if st - aud.open_at > aud.available_time:
                stress[e.id] = 1
                continue
            if st - aud.open_at == aud.available_time:
                stress[e.id] = 0.5
                continue
            stress[e.id] = 0

        return stress

    def drop_auditorium(self, aud):
        pass
