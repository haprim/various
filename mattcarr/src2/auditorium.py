import itertools
import copy

def default_weightf(t):
    if t >= 540 and t < 720:
        return 1
    if t >= 720 and t < 900:
        return 2
    if t >= 900 and t < 1080:
        return 4
    if t >= 1080 and t < 1275:
        return 6
    if t >= 1275 and t < 1455:
        return 5
    if t >= 1455:
        return 3
    assert(0)


class Auditorium:
    aud_id_ctr = 1
    def __init__(self, aud):
        self.capacity = aud['capacity']
        self.formats = aud['parameters']['formats']
        self.ext_id = aud['id']
        self.id = Auditorium.aud_id_ctr
        self.has_custom_showing = (aud['hasCust'] != -1)
        if 'partial' in aud:
            self.is_partial = True
        else:
            self.is_partial = False

        Auditorium.aud_id_ctr += 1

        self.open_at = aud['schedule'][0]
        self.close_at= aud['schedule'][1]
        assert(self.close_at > self.open_at)
        self.available_time = self.close_at - self.open_at
        self.load = 0
        self.slots = [(self.open_at, self.close_at)]

        self.can_addremove = True

        self.showings = {}
        self.prio_showings = []
        self.schedule = {}
        self.hasCustomShowing = False

        self.is_sticky = False

    def canHostShowing(self, showing, ignore_freezes = False):
        #if already frozen, skip
        if not ignore_freezes:
            if not self.can_addremove:
                return False
    
        shcap = showing.aud_constraint['capacity']
    
        #showing requires some other auditorium size
        if self.capacity < shcap[0] or self.capacity > shcap[1]:
            return False

        #showing requires different technology / scheme
        if showing.aud_constraint['scheme']:
            if showing.aud_constraint['scheme'] not in self.formats:
                return False

        #showing requires different format
        if showing.format not in self.formats:
            return False

        #showing requires a particular auditorium
        if 'id' in showing.aud_constraint:
            if showing.aud_constraint['id'] != self.ext_id:
                return False

        #otherwise it's good
        return True

    def hasTimingConflict(self, sh, diff):
#        print "TIMING"
        if len(sh.time_constraint) == 0:
            return False

        for e in self.showings.values():
            if len(e.time_constraint) > 0:
                if abs(e.time_constraint[0][0] - 
                    sh.time_constraint[0][0]) < diff:
                    print "AAAAA CANNOT", sh, e, sh.id, e.id
                    return True
        return False

    def hasTitleClash(self, sh):
        for e in self.showings.values():
            if e.title == sh.title:
                return True
        return False

    def prioritizeShowings(self):
        sorter = [(-x.demand, x) for x in self.showings.values()]
        self.prio_showings = [x[1] for x in sorter]

    def assignShowing(self, showing):
        self.showings[showing.id] = showing
        self.available_time -= showing.length
        self.load += showing.length

    def getNFreezes(self):
        return sum([int(len(x.possible_auditoriums)==1) for x in self.showings.values()]), len(self.showings)

    def findReassignableShowings(self):
        return [x for x in self.showings.values() if 
            len(x.possible_auditoriums) > 1]

    def blockTime(self, begin, end):
        newints = []

        for e in self.slots:
            int1 = max(e[0], begin)
            int2 = min(e[1], end)
#            print "beg/end/e[0], e[1]/int1/int2", begin, end, e[0], e[1],int1, int2
            if int1 > int2:
                newints.append((e[0], e[1]))
                continue
            if e[0] < int1:
                newints.append((e[0], int1))
            if e[1] > int2:
                newints.append((int2, e[1]))
        self.slots = newints
        self.available_time = sum([x[1] - x[0] for x in self.slots])

    def assignShowing(self, showing):
        assert(showing.id not in self.showings)
        self.showings[showing.id] = showing
        self.available_time -= showing.length
        self.load += showing.length

    def next_showtime_in_aud(self, csr, showing):
        #need 3 things: fit into a valid interval, be valid for movie 
        #constraints, do not violate csr constraints
        ll = showing.length

        ct = self.slots[:]
        t = ct[0][0]
        ll = showing.length

        while True:
            if len(ct) == 0:
                print "EXIi1"
                return -1

            #find first valid interval
            if (t > ct[0][1]) or ((ct[0][1] - t) < ll):
                ct = ct[1:]
                if len(ct) == 0:
                    print "EXIi2"
                    return -1

                t = ct[0][0]
                continue

            #else: does it meet film's constraints
            if len(showing.time_constraint) == 1:
                if t < showing.time_constraint[0][0]:
                    t = showing.time_constraint[0][0]
                    continue

                elif t >= showing.time_constraint[0][1]:

                    print "EXIi3", t, showing.time_constraint
                    return -1

            #now system constraints
            t2 = csr.next_showtime(showing, self, t)
#FIXME the line above is called twice after it returns a valid result. should be accepted upon the first. probably the elif t2>t -> continue should be switched to "break"
            print "t2=", t2
            if t2 == -1:
                print "X"
                return -1
            elif t2 > t:
                print "Y"
                t = t2
                continue
            elif t2 < t:
                assert(0) #just for safety FIXME

            #t == t2 case
            return t2


    def arrangeShowings(self, csr0, weightf = default_weightf):
        shindices = self.showings.keys()
        rls = range(len(shindices))
        max_score = 0
        best_loc = None
        best_times = []
        tot_time = sum([e[1] - e[0] for e in self.slots])
        verlen = sum([e.length for e in self.showings.values()])
        """
        if verlen > tot_time:
            self.slots.append((self.slots[-1][1], 
                self.slots[-1][1] + 10000))
            self.available_time += 10000
            print "EXTENDING!", self.slots
        """
        if verlen > tot_time:
            return False

#        assert(verlen <= tot_time)
        #FIXME how about chopped up schedules e.g. custom?

        for perm in itertools.permutations(rls):
            print "STARTING WITH PERM", perm
            ct = self.slots[:]

            score = -1
            times = []
            csr = copy.deepcopy(csr0)

            for i in perm:
                sh = self.showings[shindices[i]]
                tm = self.next_showtime_in_aud(csr, sh)
                if tm == -1:
                    break
#                if ii == 0:
#                    if tm - self.open_at > self.available_time:
#                        print "IMPOSS!"
#                        break
                print "\telt", i, tm
                times.append(tm)
                csr.add_showing(sh, self, tm, force = True)
            
            print perm, times
            if len(times) < len(rls):
                continue

            #now measure weight
            score = 0
            for j in rls:
                score += weightf(times[j]) * \
                    self.showings[shindices[perm[j]]].demand

            if score > max_score:
                print "NEW MAX", score, perm
                max_score = score
                best_loc = [shindices[x] for x in perm]
                best_times = times[:]

        if not best_loc:
            return False

        print "NOW SCHEDULING"
        for i in range(len(best_loc)):
            print "i=", i, best_times, best_loc
            self.scheduleShowing(best_loc[i], best_times[i], csr0)

        return True

    def scheduleShowing(self, sh_index, t, csr):
        sh = self.showings[sh_index]
        print "\tasked to schedule for {0}, time {1}".format(sh, t)

        #verify it's valid for the intervals
        found_slot = False
        for e in self.slots:
            if e[0] <= t < e[1]:
                found_slot = True
                break
        assert(found_slot)

        self.available_time -= sh.length
        self.load += sh.length
        self.blockTime(t, t + sh.length)
        sh.assigned_time = t
        self.assigned_aud = self
#        csr.add_intervals(self, sh, t)
        csr.add_showing(sh, self, t, force=True)

    def unassignShowing(self, showing):
        del self.showings[showing.id]
        self.available_time += showing.length
        self.load -= showing.length

    def adjustOpenHours(self, optime, closetime):
        assert(closetime > optime)
        l1 = self.close_at - self.open_at
        l2 = closetime - optime
        self.available_time += l2 - l1
        self.open_at = optime
        self.close_at = closetime
        if self.slots:
            self.slots[0] = (optime, self.slots[0][1])
            self.slots[-1] = (self.slots[-1][0], closetime)

    def __str__(self):
        return "Aud {0}: shows: {1}, load: {2}, oi: {3}".format(self.ext_id,
            [x.id for x in self.showings.values()], self.load, self.slots)




if __name__ == "__main__":
    from infra.auds_1_18 import *
    from utilities.utils import *
    rectify_input(globs, auditoriums, bookings)
    print globs

    a1 = Auditorium(auditoriums[0])
    a2 = Auditorium(auditoriums[1])
