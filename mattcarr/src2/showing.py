from auditorium import *

class Showing:
    showing_id_ctr = 1
    def __init__(self, initdct):
        self.type = initdct['playing']
        self.time_constraint = initdct['time_constraint']
        self.aud_constraint = initdct['auditorium_constraint']
        self.format = initdct['format']
        self.demand = initdct['demand']
        self.length = initdct['length']
        self.raw_length = initdct['raw_length']
        self.title = initdct['title']

        self.assigned_aud = None
        self.assigned_time = -1
        self.id = Showing.showing_id_ctr
        Showing.showing_id_ctr += 1

        self.auditoriums = {}
        self.possible_auditoriums = []
        self.statevar1 = 0 #can use for greedy algos
        self.blocked_auditoriums = set([])


    def calculatePossibleAuditoriums(self, auds):
        result = []

        for e in auds:
            if e.canHostShowing(self):
                self.auditoriums[e.ext_id] = e
                result.append((-e.capacity, e.ext_id))
        result = [x[1] for x in sorted(result)]
        self.possible_auditoriums = result
        return result


    def forceAssignAuditorium(self, aud):
        tasks = [('unlink', self.id, self.assigned_aud),
            ('link', self.id, aud.ext_id)]
        self.assigned_aud = aud

        return tasks

    def __str__(self):
        xid = ''
        if self.assigned_aud:
            xid = 'aud ' + str(self.assigned_aud.ext_id)
        return "Showing {0}, '{1}', fmt {2} {3}, {4} {5} {6}".format(self.id, 
            self.title, self.aud_constraint['scheme'],
            self.format, xid, self.assigned_time, self.length) + \
            str(self.possible_auditoriums) + " / " + str(self.time_constraint)



if __name__ == "__main__":
    from infra.auds_1_18 import *
    from utilities.utils import *
    rectify_input(globs, auditoriums, bookings)

    sg1 = Showing(bookings[0])
    sg2 = Showing(bookings[4])
    print sg1
    print sg2
    auds = []
    for e in auditoriums:
        auds.append(Auditorium(e, globs['schedule']))

    aa=sg2.calculatePossibleAuditoriums(auds)
#    if len(aa) == 1:
#        aa.forceAssignAuditorium
    print aa
