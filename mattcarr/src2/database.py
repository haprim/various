from src2.auditorium import *
from src2.showing import *
from infra.enforcer import *
from constrainter import *
import numpy as np
import copy

class SDB:
    def __init__(self, auds, showings, extras):
        #1 load auds in prio order
        self.auds = {}
        self.auds_by_prio = []
        self.pre_solution = []
        self.min_dist_splits = extras['min_dist_splits']

        for i, e in enumerate(auds):
            self.auds[e.ext_id] = e

        sorter = sorted([(-v.capacity, v.ext_id) for v in self.auds.values()])

        self.auds_by_prio = [x[1] for x in sorter]

        self.showings = {}
        for e in showings:
            vassert(len(e.time_constraint) <= 1, 
                "Time constraint must have length <= 1 ({0})".format(e.id))

            if e.type == 'custom':
                x = copy.deepcopy(e)
                audi = self.auds[x.aud_constraint['id']]
                x.assigned_aud = audi
                x.assigned_time = x.time_constraint[0][0]

                self.pre_solution.append(x)
#                print "AUDI BEFOR BLOCK", audi, x.time_constraint
                audi.blockTime(x.time_constraint[0][0], 
                    x.time_constraint[0][0] + x.length)
                audi.hasCustomShowing = True
#                audi.load += 1.5 * x.length
#                audi.available_time -= int(0.3 * x.length)
#                print "AUDI AFTER BLOCK", audi
            else:
                self.showings[e.id] = e
#                self.showings.append(e)
            e.calculatePossibleAuditoriums(self.auds.values())

    #call this to observe any freezes
    def calculateFreezes(self):
        for k, v in self.auds.items():
            fr1, tot1 = v.getNFreezes()
            if tot1 == fr1:
                v.can_addremove = False
                print "FREEZE", k
        
        print "BEF", self.auds_by_prio
        sorter = sorted([(v.can_addremove, v.available_time, -v.capacity, 
            v.ext_id) for v in self.auds.values()])
        print sorter

        self.auds_by_prio = [x[3] for x in sorter]
        print "AFT", self.auds_by_prio
        return

    def doInitialAssignments(self):
        #in buckets
        for e in self.showings.values():
            aud2assign = self.auds[e.possible_auditoriums[-1]]
            e.forceAssignAuditorium(aud2assign)
            aud2assign.assignShowing(e)

        #prioritize
        for e in self.auds.values():
            e.prioritizeShowings()

        print "BEFORE"
        for e in self.auds.values():
            print e, e.available_time

        self.smoothenLoads_Initial()


    def smoothenLoads_Initial(self): 

        while True:
            loads = [x.load for x in self.auds.values()]

            lds = sorted([(v.load, k) for k, v in self.auds.items()])
            lds = sorted([(v.load, k) for k, v in self.auds.items()])
#            lds = [(self.auds[x].load, x) for x in self.auds_by_prio]
#            print "LDSDIFF", lds[-1][0] - lds[0][0]
            if lds[-1][0] - lds[0][0] < 100:
                break

            done_something = False
            for cyc in lds:
#                print "\tdecr", cyc
                #if the auditorium is not fully booked, leave it
#                if self.auds[cyc[1]].available_time > 0:
#                    continue

                ret = self.decreaseLoads(self.auds[cyc[1]], 2,
                    avoid_title_clashes = True)
                if not ret:
                    ret = self.decreaseLoads(self.auds[cyc[1]], 2,
                        avoid_title_clashes = False)

                if ret:
                    done_something = True
                    break

            if not done_something:
                break
#            break

        print "AFTER"
        for e in self.auds.values():
            print e, e.available_time


    def decreaseLoads(self, aud, op_type, avoid_title_clashes = False):
        assert(op_type in [1,2])
        #1 find showings that can be reassigned
        shcands = aud.findReassignableShowings()

        #2 iterate on these
        found = False
        for e in shcands:
            if found:
                break

#            print "Trying to move {0} from {1}".format(e.id, e.assigned_aud.id)
#            print "POSS", [x for x in e.possible_auditoriums]
            for k in e.possible_auditoriums:
#                print "\tAUD", k
                v = e.auditoriums[k]
                if v.id == aud.id:
#                    print "\t1"
                    continue
                if v.id in e.blocked_auditoriums:
#                    print "\t2", e2.id
                    continue

                if v.hasTimingConflict(e, self.min_dist_splits):
                    continue

                if avoid_title_clashes:
                    if v.hasTitleClash(e):
                        continue

                if op_type == 2:
                    error_before = abs(aud.available_time) + \
                        abs(v.available_time)
                    error_after = abs(aud.available_time + e.length) + \
                        abs(v.available_time - e.length)

                    if error_after > error_before:
#                        print "\t3", aud.available_time, v.available_time, e.length, error_before, error_after
                        continue
                    elif error_after == error_before:
                        if aud.load - e.length <= v.load + e.length:
#                            print "\t3a", aud.available_time, v.available_time, e.length, error_before, error_after, aud.load, v.load
                            continue
                elif op_type == 1:
                    if not v.canHostShowing(e):
#                        print "\t4"
                        continue

                    if v.available_time - e.length < 0:
#                        print "\t5"
                        continue

                #I guess this is okay?
                found = True
                if op_type == 1:
                    print "Moved {0} from {1} to {2}".format(e.id,
                        e.assigned_aud.id, v.id)
                elif op_type == 2:
                    print "Wiggled {0} from {1} to {2}".format(e.id,
                        e.assigned_aud.id, v.id)

                if e.assigned_aud != e.possible_auditoriums[-1]:
#                if True:
                    e.blocked_auditoriums.add(e.assigned_aud)
                e.assigned_aud.unassignShowing(e)
                e.forceAssignAuditorium(v)
                v.assignShowing(e)
                break
                
        return found


    def smoothenLoads_SingleSwaps(self): 
        result = True
        al = [(x.available_time, x.ext_id) for x in self.auds.values()]
        al = [x[1] for x in sorted(al)]
#        for e0 in reversed(self.auds_by_prio):
        for e0 in al:
            v = self.auds[e0]
            print "TRY", e0, v.available_time
            if v.available_time >= 0:
                continue

            fixed = False
            
            #greedily try to fix at the expense of the next
#            for k2 in self.auds_by_prio:
            for k2 in reversed(al):
                while True:
                    print "\tvs", k2, v.available_time, self.auds[k2].available_time
                    if k2 == e0:
                        break

                    ret = self.doSingleSwaps(e0, k2)
#                    self.reassignHours()
                    if ret:
                        if v.available_time >= 0:
                            print "\t\tFIXED"
                            print "STEP"
                            for ee in self.auds.values():
                                print "\t", ee, ee.available_time
                            fixed = True
                            break

                    #not fixed, but something happened:
                    if ret:
                        continue

                    break
                if fixed:
                    break

            if not fixed:
                result = False

        return result

    def doSingleSwaps(self, src, targ):
        print "\t\t", self.auds[src].available_time, self.auds[targ].available_time
        if self.auds[src].available_time > self.auds[targ].available_time:
            print "\t\texit1"
            return False

        srclens = sorted([(v.length, v) for k, v in self.auds[src].showings.items()])
        targlens = sorted([(v.length, v) for k, v in self.auds[targ].showings.items()])
        print "\t\t", [(x[0], x[1].id) for x in srclens], [(x[0], x[1].id) for x in targlens]


        #now find movies that can be swapped!!!
        found = False
        swapout_cands = []
        for e in reversed(srclens):
            if self.auds[targ].canHostShowing(e[1]):
                if not self.auds[targ].hasTimingConflict(e[1], 
                    self.min_dist_splits):
                    swapout_cands.append(e[1])

        if len(swapout_cands) == 0:
            print "\t false 1"
            return False

        #case 1: large time difference, can swap out of src
        for srcsh in swapout_cands:
            if self.auds[targ].available_time - srcsh.length > 0:
                self.auds[src].unassignShowing(srcsh)
                self.showings[srcsh.id].forceAssignAuditorium(self.auds[targ])
                self.auds[targ].assignShowing(srcsh)
                return True

        #case 2: swap against something
        srcsh = swapout_cands[0]

        found = False
        for e in targlens:
            if self.auds[src].canHostShowing(e[1]):
                if not self.auds[src].hasTimingConflict(e[1], 
                    self.min_dist_splits):
                    found = True
                    break
        if not found:
            print "\t false 2"
            return False
        targsh = e[1]

        #when do we swap?
        #well, either if the movie we swap out is longer than the one we'd swap in or if by swapping it out, we fix the src without ruining the target.
#        if srclens[-1][0] > targlens[0][0]:
        if srcsh.length > targsh.length:
#            srcsh = srclens[-1][1]
#            targsh = targlens[0][1]

#            if self.auds[targ].available_time - targsh.length + srcsh.length < 0:
#                return False

            self.auds[src].unassignShowing(srcsh)
            self.showings[srcsh.id].forceAssignAuditorium(self.auds[targ])
            self.auds[targ].assignShowing(srcsh)

            self.auds[targ].unassignShowing(targsh)
            print "\t", targsh.id, src, len(self.showings)

            print self.showings[targsh.id]
            self.showings[targsh.id].forceAssignAuditorium(self.auds[src])
            self.auds[src].assignShowing(targsh)

            print "Swapped: {0}({4}) {1}->{2}, {3}({5}) {2}->{1}".format(
                srcsh.id, src, targ, targsh.id, srclens[0][0], targlens[-1][0])
            print "\tAVTS now: {0}: {1}, {2}: {3}".format(
                src, self.auds[src].available_time,
                targ, self.auds[targ].available_time,
                )

            return True
                
        return False

    #leave out partials! e.g. yyy
    def reassignHours(self):
        scheds = list(reversed(sorted([(x.close_at - x.open_at, x.open_at, 
            x.close_at, x.ext_id) for x in self.auds.values() if not x.is_partial])))
        avs = sorted([(x.available_time, x.ext_id) 
            for x in self.auds.values() if not x.is_partial])
#        print "REASS"
#        print scheds
#        print avs
        for i in range(len(avs)):
            self.auds[avs[i][1]].adjustOpenHours(scheds[i][1], scheds[i][2])
#        print scheds
#        print avs

################
    def smoothenLoads_DoubleSwaps(self): 
        result = True
        al = [(x.available_time, x.ext_id) for x in self.auds.values()]
        al = [x[1] for x in sorted(al)]
#        for e0 in reversed(self.auds_by_prio):
        for e0 in al:
            fixed = False
            v = self.auds[e0]
            print "TRY", e0, v.available_time
            if v.available_time > 0:
                continue

            out = False
            #greedily try to fix at the expense of the next
#            for k2 in self.auds_by_prio:
            for k2 in reversed(al):
                print "\tvs", k2, v.available_time, self.auds[k2].available_time
                if k2 == e0:
                    break

                ret = self.doDoubleSwaps(k2, e0)
                if ret:
                    if v.available_time >= 0:
                        fixed = True
                        break
            if fixed:
                continue

        return out

    def doDoubleSwaps(self, src, targ):
        print "\t\t", self.auds[src].available_time, self.auds[targ].available_time
        if self.auds[src].available_time > self.auds[targ].available_time:
            return False

        srclens = sorted([(v.length, v) for k, v in self.auds[src].showings.items() if self.auds[targ].canHostShowing(v)])
        targlens = sorted([(v.length, v) for k, v in self.auds[targ].showings.items() if self.auds[src].canHostShowing(v)])
        print "\t\t", [(x[0], x[1].id) for x in srclens], [(x[0], x[1].id) for x in targlens]
        if len(srclens) < 1 or len(targlens) < 2:
            return False


        found = False
        for e in reversed(srclens):
            for i2, e2 in enumerate(targlens[:-1]):
                if e[1].length < e2[1].length + targlens[i2 + 1][1].length:
                    if (e[1].length > e2[1].length) and (e[1].length > targlens[i2 + 1][1].length):
                        if self.auds[src].available_time >= 0:
                            found = True
                            srcsh = e[1]
                            targsh1 = e2[1]
                            targsh2 = targlens[i2 + 1][1]

        if found:
            self.auds[src].unassignShowing(srcsh)
            self.showings[srcsh.id].forceAssignAuditorium(self.auds[targ])
            self.auds[targ].assignShowing(srcsh)

            self.auds[targ].unassignShowing(targsh1)
            self.auds[targ].unassignShowing(targsh2)

            self.showings[targsh1.id].forceAssignAuditorium(self.auds[src])
            self.showings[targsh1.id].forceAssignAuditorium(self.auds[src])

            self.auds[src].assignShowing(targsh1)
            self.auds[src].assignShowing(targsh2)

            print "Bi-swapped: {0}({4}) {1}->{2}, {3}({5}) and {6}({7}) {2}->{1}".format(
                srcsh.id, src, targ, targsh1.id, srcsh.length, targsh1.length,
                targsh2.id, targsh2.length)
            print "\tAVTS now: {0}: {1}, {2}: {3}".format(
                src, self.auds[src].available_time,
                targ, self.auds[targ].available_time, 
                )

            return True
                
        return False

    #try to swap constraints with other showings of the same movie
    def swapConstrainedPairs(self, audid, aud_cands):
        for e in self.auds[audid].showings.values():
            if len(e.time_constraint) == 0:
                print "\tMATCHING AUD {0}, {1}".format(audid, e)
                found = False
                for k1 in aud_cands:
                    for k2 in self.auds[k1].showings.values():
                        if k2.title == e.title:
                            if len(k2.time_constraint) > 0:
                                if k2.time_constraint[0][0] <= e.assigned_time < k2.time_constraint[0][1]:
                                    print "PROMISING", k2
                                    if self.auds[audid].canHostShowing(k2):
                                        print "\t\tswapping:", k2, "in aud {0}".format(k1)
                                        e.time_constraint = k2.time_constraint
                                        k2.time_constraint = []
                                        found = True
                                        break
                        if found:
                            break

    def mst(self, prio_index):
        strs = {}
        me = self.auds_by_prio[prio_index]
        print "\tSTRESS LEVELS {0} (avt {1}".format(me, self.auds[me].available_time)
        for i, e2 in enumerate(self.auds_by_prio):
            stress = csr.measure_stress(sdb.auds[e2])
            if len(stress) == 0:
                strfrac = 0
            else:
                strfrac = sum(stress.values()) / float(len(stress))
            if i > prio_index:
                strs[e2] = stress
            elif i == prio_index:
                base = stress
                baseval = strfrac

            print "\t\taud {0}:, stress={1}".format(e2, strfrac)
        return strs, base, baseval


    def swapShowings(self, src, srcsh, targ, targsh):
        src.unassignShowing(srcsh)
        self.showings[srcsh.id].forceAssignAuditorium(targ)
        targ.assignShowing(srcsh)

        targ.unassignShowing(targsh)
        self.showings[targsh.id].forceAssignAuditorium(src)
        src.assignShowing(targsh)

    def decreaseStress(self, prio_index):

        strs, base, baseval = self.mst(prio_index)
        me = self.auds_by_prio[prio_index]

        sorter = [(sum(v.values())/float(len(v)), k) for k, v in strs.items()]
        sorter = sorted(sorter)

#        sorter = [(self.auds[k].available_time, k) for k in strs.keys()]
#        sorter = list(reversed(sorted(sorter)))
        print sorter
        found = False

        src = self.auds[me]
        for cands in sorter:
            if cands[0] >= baseval:
                break

            out = []
            for k, v in base.items():
                if v > 0:
                    if self.auds[cands[1]].canHostShowing(self.showings[k]):
                        out.append((len(self.showings[k].time_constraint), k,v))
            out = list(reversed(sorted(out)))                        
            print "\tRegarding move from {0} to {1}: {2}".format(
                me, cands[1], out)
            inc = []
            for k, v in strs[cands[1]].items():
                if v == 0:
                    if self.auds[me].canHostShowing(self.showings[k]):
                        inc.append((len(self.showings[k].time_constraint),k,v))
            inc = sorted(inc)                        
            print "\t\tConverse dir: {0}".format(inc)

            mindiff = 999999
            minval = None
            targ = self.auds[cands[1]]

            for e1 in out:
                if found:
                    break

                srcsh = self.showings[e1[1]]
                for e2 in inc:
                    print "\tTry: {0} vs {1}".format(e1[1], e2[1])
                    if e2[0] > 0:
                        print "\t\tConstrained"
                        break
                                            
                    targsh = self.showings[e2[1]]

                    diff = targsh.length - srcsh.length
                    print "\t\tDiff={0}, AT1={1}, AT2={2}".format(diff,
                        src.available_time, targ.available_time)
                    if src.available_time - diff < 0:
                        print "\t\t\tLeft"
                        continue
                    if targ.available_time + diff < 0:
                        print "\t\t\tRight"
                        continue

                    #otherwise we found it!!
                    print "COOL, we will swap {0} out and {1} in from {2}".format(srcsh.id, targsh.id, targ.ext_id)
                    self.swapShowings(src, srcsh, targ, targsh)

                    stress = csr.measure_stress(src)
                    if sum(stress.values()) / float(len(stress)) < baseval - 0.01:
                        found = True
                        print "\tand it was okay!"
                        break
                    else:
                        print "\tbut alas, a delibabe"
                        self.swapShowings(src, targsh, targ, srcsh)

            if found:
                break

        print "SEE AGAIN THE STRESS"
        print "SHOWINGS", [x.id for x in self.auds[me].showings.values()]
        a,b,c=self.mst(prio_index)
        print b,c 

        return found           


    def decreaseStress2(self, prio_index):

        strs, base, baseval = self.mst(prio_index)
        me = self.auds_by_prio[prio_index]

        sorter = [(self.auds[k].available_time, k) for k in strs.keys()]
        sorter = list(reversed(sorted(sorter)))
        print sorter
        found = False

        src = self.auds[me]
        diffl = []

        for cands in sorter:

            out = []
            for k, v in base.items():
                if v > 0:
                    if self.auds[cands[1]].canHostShowing(self.showings[k]):
                        out.append((len(self.showings[k].time_constraint), k,v))
            out = list(reversed(sorted(out)))                        
            print "\tRegarding move from {0} to {1}: {2}".format(
                me, cands[1], out)
            inc = []
            for k, v in strs[cands[1]].items():
                if v == 0:
                    if self.auds[me].canHostShowing(self.showings[k]):
                        inc.append((len(self.showings[k].time_constraint),k,v))
            inc = sorted(inc)                        
            print "\t\tConverse dir: {0}".format(inc)

            mindiff = 999999
            minval = None
            targ = self.auds[cands[1]]

            for e1 in out:
                if found:
                    break

                srcsh = self.showings[e1[1]]
                for e2 in inc:
                    print "\tTry: {0} vs {1}".format(e1[1], e2[1])
#                    if e2[0] > 0:
#                        print "\t\tConstrained"
#                        break
                                            
                    targsh = self.showings[e2[1]]

                    diff = targsh.length - srcsh.length
                    print "\t\tDiff={0}, AT1={1}, AT2={2}".format(diff,
                        src.available_time, targ.available_time)
                    if src.available_time - diff < 0:
                        continue
                    if targ.available_time + diff < 0:
                        continue
                    diffl.append((diff, srcsh.id, targ.ext_id, targsh.id))

        diffl = sorted(diffl)
        print "diffl", diffl

        for e in diffl:
            srcsh = self.showings[e[1]]
            targ = self.auds[e[2]]
            targsh = self.showings[e[3]]
            print "\tMay swap: {0}({1}) <-> {2}({3})".format(srcsh.id,
                src.ext_id,
                targsh.id, targ.ext_id)

            self.swapShowings(src, srcsh, targ, targsh)

            stress = csr.measure_stress(src)
            if sum(stress.values()) / float(len(stress)) < baseval - 0.01:
                found = True
                print "\tand it was okay!"
                break
            else:
                print "\tbut alas, a delibabe"
                self.swapShowings(src, targsh, targ, srcsh)

            if found:
                break

        print "SEE AGAIN THE STRESS"
        print "SHOWINGS", [x.id for x in self.auds[me].showings.values()]
        a,b,c=self.mst(prio_index)
        print b,c 

        return found           


def dhelper(csr):
    print "\n\n===============\nSCHEDULE STATE:"
    for k in sorted(csr.basic_intervals.keys()):
        v = csr.basic_intervals[k]
        print "\tMovie {0}".format(k)
        for e in sorted(v):
            print "\t\tPlaying at {4}:{5} - {0} ({1}-{2}) in aud {3}".format(
                (e[0] + e[1]) // 2, e[0], e[1], e[2],
                str((e[0] + e[1]) // 2 // 60).zfill(2),
                str((e[0] + e[1]) // 2 % 60).zfill(2))
#    for e in sdb.auds_by_prio:


if __name__ == "__main__":
#    from infra.auds_1_18 import *
    from infra.a3_18 import *
    from utilities.utils import *

    for e in auditoriums:
        validate_auditorium(e)
    for e in bookings:
        validate_movie(e)

    rectify_input(globs, auditoriums, bookings)
    sl = []
    for e in bookings:
        if e['playing'] == 'split':
            for e2 in e['time_constraint']:
                cpd = copy.deepcopy(e)
                cpd['time_constraint'] = [e2]
                sl.append(Showing(cpd))
        else:
            for i in range(e['min_perf_count']):
                sl.append(Showing(e))

    tempauds = []
    maxid = max([x['id'] for x in auditoriums])
    for e in auditoriums:
        if e['hasCust'] == -1:
            tempauds.append(e)
        else:
            funk = bookings[e['hasCust']]['time_constraint'][0][0]
            lgt = bookings[e['hasCust']]['length']

            e2 = copy.deepcopy(e)
            e3 = copy.deepcopy(e)

            e['schedule'] = (funk, funk + lgt)
#            e['partial'] = True

            e2['schedule'] = (e2['schedule'][0], funk)
            e2['id'] = maxid + 1
#            e2['partial'] = True
            e2['hasCust'] = -1
            e3['schedule'] = (funk + lgt, e3['schedule'][1])
            e3['id'] = maxid + 2
#            e3['partial'] = True
            e3['hasCust'] = -1

            tempauds.append(e)
            tempauds.append(e2)
            tempauds.append(e3)
    #FIXME support max 1 custom per auditorium
#    for e in auditoriums
    sdb = SDB([Auditorium(x) for x in tempauds], sl, globs)

    for e in sdb.showings.values():
        print e
    print sdb.auds_by_prio
#    print len(sl)
    sdb.doInitialAssignments()


    ###
    for i in range(5):
        sdb.smoothenLoads_SingleSwaps()

        print "POST_SWAP", i
        for e in sdb.auds.values():
            print e, e.available_time
            sdb.reassignHours()

    print "POST REASSIGN", i
    for e in sdb.auds.values():
        print e, e.available_time

    print sdb.auds_by_prio

    """
    if any([x.available_time < 0 for x in sdb.auds.values()]):
        print "DOUBLE SWAP!!!"
        sdb.smoothenLoads_DoubleSwaps()
#        sdb.smoothenLoads_SingleSwaps()
        print "POST DOUBLE SWAP"
        for e in sdb.auds.values():
            print e, e.available_time
    ###
    """
#    quit() #xxx

    sdb.calculateFreezes()

    #now arrange:
    cinit = assemble_constrainter_init(globs, bookings)
    csr = Constrainter(cinit)

    """
    r = csr.add_showing(sdb.showings[1], sdb.auds[1], 540)
    print r
    r = csr.add_showing(sdb.showings[1], sdb.auds[1], 540)
    print "NEXT", csr.next_showtime(sdb.showings[1], sdb.auds[1], 540)
    print r, sdb.showings[1].title, csr.min_spreads
    r = csr.add_showing(sdb.showings[40], sdb.auds[1], 540)
    print r
    """


#    print sdb.pre_solution[0]
#    print sdb.auds[15]

    """
    for e in sdb.auds[1].showings.values():
        print e
    sdb.auds[1].arrangeShowings(csr)
    quit()
    """

#    for k, v in sdb.auds.items():
    for i, k in enumerate(sdb.auds_by_prio):
        v = sdb.auds[k]
        dhelper(csr)

#        sdb.decreaseStress(i)
        print "ARRANGING", v
        print "MOVIES:"
        for ee in v.showings.values():
            print "\t", ee

        for ii in range(5):
            v.arrangeShowings(csr)
            if len(v.showings) == 0:
                break
            if v.showings.values()[0].assigned_time != -1:
                break
            pp = sdb.decreaseStress(i)
            if not pp:
                sdb.decreaseStress2(i)
            print "REARRANGING", v
            print "MOVIES:"
            for ee in v.showings.values():
                print "\t", ee



        print "NOW SWAP out of {0}, with {1}".format(k, sdb.auds_by_prio[i+1:])
        sdb.swapConstrainedPairs(k, sdb.auds_by_prio[i + 1:])

    """
    for k in sorted(sdb.auds.keys()):
        print "FINAL -- AUD ", k
        v = sdb.auds[k]
        sorter = [(x.assigned_time, x) for x in v.showings.values()]
        sorter = [x[1] for x in sorted(sorter)]
        for e in sorter:
            print "\t", e
    """           
    import random

    bad_auds = []
    for e in sdb.auds_by_prio:
        if len(sdb.auds[e].showings) == 0:
            continue
        sh1 = sdb.auds[e].showings.values()[0]
        if sh1.assigned_time == -1:
            bad_auds.append(e)

    for e in bad_auds:
        print "BAD AUD", e, sdb.auds[e].available_time
        for e2 in sdb.auds[e].showings:
            print sdb.showings[e2]

    for e in bad_auds:
        sdb.swapConstrainedPairs(e, [x for x in sdb.auds_by_prio if x != e])
    
    print "CSR"
    for k, v in csr.basic_intervals.items():
        print k, sorted(v)

    print "#aud_id, movie, tech, time"
    for k in sorted(sdb.auds.keys()):
        v = sdb.auds[k]
        shl = [(x.assigned_time, x) for x in v.showings.values()]
        shl = [x[1] for x in sorted(shl)]

        for e2 in shl:
            ext = ''
            if e2.aud_constraint['scheme']:
                ext += ' / {0}'.format(e2.aud_constraint['scheme'])

            print "{0},{1},{2},{3}:{4}".format(v.ext_id,
                e2.title, (e2.format + ext).upper(), 
                str((e2.assigned_time // 60 ) % 24).zfill(2),
                str((e2.assigned_time % 60)).zfill(2))

    print "FINAL"
    dhelper(csr)
