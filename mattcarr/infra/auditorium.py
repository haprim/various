import itertools

def default_weightf(t):
    if t >= 540 and t < 720:
        return 1
    if t >= 720 and t < 900:
        return 2
    if t >= 900 and t < 1080:
        return 4
    if t >= 1080 and t < 1275:
        return 6
    if t >= 1275 and t < 1455:
        return 5
    if t > 1455:
        return 3
    assert(0)

class Auditorium:
    aud_id_ctr = 1
    def __init__(self, aud):
        self.capacity = aud['capacity']
        self.formats = aud['parameters']['formats']

        self.ext_id = aud['id']

        self.id = Auditorium.aud_id_ctr

        Auditorium.aud_id_ctr += 1

        self.open_at = aud['schedule'][0]
        self.close_at = aud['schedule'][1]
        self.available_time = self.close_at - self.open_at
        self.load = 0
        self.intervals = [(self.open_at, self.close_at)]

        self.showings = []

    def canShow(self, showing):
        shcap = showing.aud_constraint['capacity']
        if self.capacity < shcap[0] or self.capacity > shcap[1]:
#            print "1", shcap, self.capacity
            return False
        if showing.aud_constraint['scheme']:
            if showing.aud_constraint['scheme'] not in self.formats:
#                print "2", showing.aud_constraint, self.formats
                return False
        if showing.format not in self.formats:            
#            print "3", showing.format, self.formats
            return False
        if 'id' in showing.aud_constraint:
            if showing.aud_constraint['id'] != self.ext_id:
#                print "4"
                return False
        return True

    def canShowWithTime(self, showing):
        if not self.canShow(showing):
            return False
        if self.available_time - showing.length < 0:
            return False
        return True

    def assignShowing(self, showing):
        self.showings.append(showing)
        self.available_time -= showing.length
        self.load += showing.length

    def unassignShowing(self, showing):
        self.showings.remove(showing)
        self.available_time += showing.length
        self.load -= showing.length

    def prioritizeShowings(self):
        sorter = ((-x.demand, x) for x in self.showings)
        self.showings = [x[1] for x in sorted(sorter)]

    def scheduleShowing(self, sh_index, t):
        sh = self.showings[sh_index]
        print "\tasked to schedule for {0}, time {1}".format(sh, t)

        ok = False
        iv = 0
        while True:
            siv = self.intervals[iv]
            if siv[1] - siv[0] < sh.length:
                self.available_time -= siv[1] - siv[0]
                #FIXME update load?

                self.intervals = self.intervals[1:]
            else:
                ok = True
                break

        assert(ok)
        sh.assigned_time = t
        self.intervals[0] = (self.intervals[0][0] + sh.length, 
            self.intervals[0][1])
#        self.load += sh.length
#        self.available_time -= sh.length

    def blockTime(self, begin, end):
        newints = []

        for e in self.intervals:
            """
            if e[1] <= begin:
                newints.append(e)
                continue
            if e[0] >= end:
                newints.append(e)
                continue

            else: #overlap
            """
            int1 = max(e[0], begin)
            int2 = min(e[1], end)
#            print "beg/end/e[0], e[1]/int1/int2", begin, end, e[0], e[1],int1, int2
            if int1 > int2:
                newints.append((e[0], e[1]))
                continue
            if e[0] < int1:
                newints.append((e[0], int1))
            if e[1] > int2:
                newints.append((int2, e[1]))
        self.intervals = newints


    def findReassignableShowings(self):
        return [x for x in self.showings if len(x.possible_auditoriums) > 1]


    def arrangeShowings(self, weightf = default_weightf):
        rls = range(len(self.showings))
        max_score = 0
        max_loc = None
        max_times = []
        tot_time = sum([e[1] - e[0] for e in self.intervals])
        verlen = sum([e.length for e in self.showings])
        if verlen > tot_time:
            self.intervals.append((self.intervals[-1][1], 
                self.intervals[-1][1] + 10000))
            self.available_time += 10000
            print "EXTENDING!", self.intervals
#        assert(verlen <= tot_time)
        #FIXME how about chopped up schedules e.g. custom?

        for perm in itertools.permutations(rls):
            ct = self.intervals[:]

            score = -1
            times = []

            for i in perm:
                ll = self.showings[i].length
                ok = True
                while True:
                    if len(ct) == 0:
                        ok = False
                        break
                    if ct[0][1] - ct[0][0] < ll:
                        ct = ct[1:]
                    else:
                        ok = True
                        break

                if not ok:
                    break

                if len(self.showings[i].time_constraint) == 1:
                    if ct[0][0] < self.showings[i].time_constraint[0][0]  or \
                        ct[0][0] >= self.showings[i].time_constraint[0][1]:
                        
                        print "\tFAIL", ct[0][0], self.showings[i], self.showings[i].time_constraint, i
                        print ct[0][0] < self.showings[i].time_constraint[0][0], ct[0][0] >= self.showings[i].time_constraint[0][1]
                        ok = False
                        break

                times.append(ct[0][0])
                ct[0] = (ct[0][0] + ll, ct[0][1])

            if not ok:
                continue
            #now measure weight
            score = 0
            for j in rls:
                score += weightf(times[j]) * self.showings[j].demand

            if score > max_score:
                print "NEW MAX", score, perm
                max_score = score
                max_loc = perm[:]
                max_times = times[:]

        if not max_loc:
            return False

        print "NOW SCHEDULING"
        for i in range(len(max_loc)):
            print "i=", i, max_times
            self.scheduleShowing(max_loc[i], max_times[i])
        sorter = [(x.assigned_time, x) for x in self.showings]
        self.showings = [x[1] for x in sorted(sorter)]

        return True

    def __str__(self):
        return "Aud {0}: shows: {1}, load: {2}, oi: {3}".format(self.ext_id,
            [x.id for x in self.showings], self.load, self.intervals)

    def verbose_print(self):
        s = "\n".join(["\t {0} at {1}:{2}".format(self.showings[i].title,
            str((self.showings[i].assigned_time // 60) % 24).zfill(2),
            str(self.showings[i].assigned_time % 60).zfill(2))
            for i in range(len(self.showings))])
        return self.__str__() + "\n" + s

if __name__ == "__main__":
    from auds_1_18 import *
    from utilities.utils import *
    rectify_input(globs, auditoriums, bookings)
    print globs

    a1 = Auditorium(auditoriums[0], globs['schedule'])
    a2 = Auditorium(auditoriums[1], globs['schedule'])
