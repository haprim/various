globs = {
    'schedule': ('10:00', '01:40'),
    'cleaning': 10,
    'trailer': 20,
    'adtime': 25,
    'min_shows': 4,
    'film_length_rounding': 5,
    'min_dist_splits': 240, #for constrained-time showing conflicts

    'max_concurrent_starts': 2,
    'showing_increments': 5,

    'timedefs': {'early': (600, 960),
                'matinee': (720, 1080), 'afternoon': (780, 1260),
                'evening': (960, 1380), 'late': (1180, 1560)}

}

auditoriums = [
    {
        'id': 1,
        'capacity': 250, 
        'parameters': {'formats': ['2D', '3D']},
    },

    {
        'id': 2,
        'capacity': 225, 
        'parameters': {'formats': ['2D']},
    },

    {
        'id': 3,
        'capacity': 160, 
        'parameters': {'formats': ['2D', '3D']},
    },
    {
        'id': 4,
        'capacity': 120, 
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 5,
        'capacity': 120, 
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 6,
        'capacity': 162, 
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 7,
        'capacity': 225, 
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 8,
        'capacity': 300, 
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 9,
        'capacity': 400, 
        'parameters': {'formats': ['2D', 'RPX']},
    },
    {
        'id': 10,
        'capacity': 350, 
        'parameters': {'formats': ['3D', 'IMAX']},
    },
    {
        'id': 11,
        'capacity': 285,
        'parameters': {'formats': ['2D', '3D']},
    },
    {
        'id': 12,
        'capacity': 225, 
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 13,
        'capacity': 154, 
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 14,
        'capacity': 120, 
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 15,
        'capacity': 120, 
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 16,
        'capacity': 154, 
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 17,
        'capacity': 225, 
        'parameters': {'formats': ['SCREENX']},
    },
    {
        'id': 18,
        'capacity': 295, 
        'parameters': {'formats': ['2D']},
    }


]

bookings = [
    {
        'id': 'bk01a',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': None, 'capacity': (1, 10000)},
        'format': '2D',
        'min_perf_count': 4,
        'demand': 5,
        'raw_length': 114,
        'title': 'Bumble Bee'
    },

    {
        'id': 'bk01b',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': None, 'capacity': (1, 10000)},
        'format': '3D',
        'min_perf_count': 1,
        'demand': 5,
        'raw_length': 114,
        'title': 'Bumble Bee'
    },


    {
        'id': 'bk02a',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': 'RPX', 'capacity': (0, 10000)},
        'format': '2D',
        'min_perf_count': 5,
        'demand': 5,
        'raw_length': 114,
        'title': 'Bumble Bee'
    },

    {
        'id': 'bk03a',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': None, 'capacity': (0, 10000)},
        'format': '2D',
        'min_perf_count': 5,
        'demand': 5,
        'raw_length': 132,
        'title': 'Vice'
    },

    {
        'id': 'bk04a',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': None, 'capacity': (1, 300)},
        'format': '2D',
        'min_perf_count': 4,
        'demand': 2,
        'raw_length': 117,
        'title': 'SpidermanU'
    },


    {
        'id': 'bk04b',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': None, 'capacity': (1, 300)},
        'format': '3D',
        'min_perf_count': 1,
        'demand': 2,
        'raw_length': 117,
        'title': 'SpidermanU'
    },

    {
        'id': 'bk05a',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': None, 'capacity': (1, 10000)},
        'format': '2D',
        'min_perf_count': 4,
        'demand': 8,
        'raw_length': 143,
        'title': 'Aquaman'
    },

    {
        'id': 'bk06a',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': 'IMAX', 'capacity': (0, 10000)},
        'format': 'IMAX',
        'min_perf_count': 3,
        'demand': 8,
        'raw_length': 143,
        'title': 'Aquaman'
    },

    {
        'id': 'bk06b',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': 'IMAX', 'capacity': (0, 10000)},
        'format': '3D',
        'min_perf_count': 1,
        'demand': 8,
        'raw_length': 143,
        'title': 'Aquaman'
    },

    {
        'id': 'bk07a',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': 'SCREENX', 'capacity': (0, 10000)},
        'format': 'SCREENX',
        'min_perf_count': 4,
        'demand': 8,
        'raw_length': 143,
        'title': 'Aquaman'
    },

    {
        'id': 'bk08a',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': None, 'capacity': (100, 280)},
        'format': '2D',
        'min_perf_count': 4,
        'demand': 8,
        'raw_length': 143,
        'title': 'Aquaman'
    },

    {
        'id': 'bk08b',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': None, 'capacity': (1, 280)},
        'format': '3D',
        'min_perf_count': 1,
        'demand': 8,
        'raw_length': 143,
        'title': 'Aquaman'
    },

    {
        'id': 'bk09a',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': None, 'capacity': (1, 10000)},
        'format': '2D',
        'min_perf_count': 5,
        'demand': 8,
        'raw_length': 130,
        'title': 'Mary Poppins'
    },

    {
        'id': 'bk10a',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': None, 'capacity': (1, 10000)},
        'format': '2D',
        'min_perf_count': 5,
        'demand': 8,
        'raw_length': 130,
        'title': 'Mary Poppins'
    },

    {
        'id': 'bk11a',
        'playing': 'split',
        'time_constraint': ['matinee', 'evening'],
        'auditorium_constraint': {'scheme': None, 'capacity': (0, 10000)},
        'format': '2D',
        'min_perf_count': 2,
        'demand': 8,
        'raw_length': 130,
        'title': 'Mary Poppins'
    },

    {
        'id': 'bk12a',
        'playing': 'split',
        'time_constraint': ['matinee', 'evening'],
        'auditorium_constraint': {'scheme': None, 'capacity': (0, 10000)},
        'format': '2D',
        'min_perf_count': 2,
        'demand': 2,
        'raw_length': 119,
        'title': 'Insta Family'
    },

    {
        'id': 'bk13a',
        'playing': 'split',
#        'time_constraint': [(540, 720), (720, 900), (900, 1080)],
        'time_constraint': ['early', 'matinee', 'afternoon'],
        'auditorium_constraint': {'scheme': None, 'capacity': (0, 10000)},
        'format': '2D',
        'min_perf_count': 3,
        'demand': 2,
        'raw_length': 112,
        'title': 'Ralph Breaks'
    },

    {
        'id': 'bk14a',
        'playing': 'split',
#        'time_constraint': [(1080, 1275), (1275, 1455)],
        'time_constraint': ['evening', 'late'],
        'auditorium_constraint': {'scheme': None, 'capacity': (0, 10000)},
        'format': '2D',
        'min_perf_count': 2,
        'demand': 2,
        'raw_length': 128,
        'title': 'Mortal Engines'
    },

    {
        'id': 'bk15a',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': None, 'capacity': (0, 10000)},
        'format': '2D',
        'min_perf_count': 5,
        'demand': 5,
        'raw_length': 116,
        'title': 'The Mule'
    },

    {
        'id': 'bk16a',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': None, 'capacity': (0, 10000)},
        'format': '2D',
        'min_perf_count': 5,
        'demand': 2,
        'raw_length': 90,
        'title': 'Holmes Watson'
    },

    {
        'id': 'bk17a',
        'playing': 'split',
#        'time_constraint': [(540, 720), (900, 1080), (1275, 1455)],
        'time_constraint': ['early', 'afternoon', 'late'],
        'auditorium_constraint': {'scheme': None, 'capacity': (0, 10000)},
        'format': '2D',
        'min_perf_count': 3,
        'demand': 2,
        'raw_length': 90,
        'title': 'Grinch'
    },

    {
        'id': 'bk18a',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': None, 'capacity': (0, 10000)},
        'format': '2D',
        'min_perf_count': 5,
        'demand': 2,
        'raw_length': 116,
        'title': 'Marwel'
    },

    {
        'id': 'bk19a',
        'playing': 'split',
#        'time_constraint': [(900, 1080), (1275, 1455)],
        'time_constraint': ['afternoon', 'late'],
        'auditorium_constraint': {'scheme': None, 'capacity': (0, 10000)},
        'format': '2D',
        'min_perf_count': 2,
        'demand': 2,
        'raw_length': 134,
        'title': 'Fant Beasts'
    },

    {
        'id': 'bk20a',
        'playing': 'split',
#        'time_constraint': [(900, 1080), (1275, 1455)],
        'time_constraint': ['afternoon', 'late'],
        'auditorium_constraint': {'scheme': None, 'capacity': (0, 10000)},
        'format': '2D',
        'min_perf_count': 2,
        'demand': 2,
        'raw_length': 130,
        'title': 'Creed 2'
    },

    {
        'id': 'bk21a',
        'playing': 'split',
#        'time_constraint': [(540, 900), (1080, 1275)],
        'time_constraint': ['early'],
        'auditorium_constraint': {'scheme': None, 'capacity': (0, 10000)},
        'format': '2D',
        'min_perf_count': 1,
        'demand': 2,
        'raw_length': 135,
        'title': 'Bohemian'
    },

    {
        'id': 'bk22a',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': None, 'capacity': (0, 10000)},
        'format': '2D',
        'min_perf_count': 5,
        'demand': 2,
        'raw_length': 104,
        'title': 'Second Act'
    },

    {
        'id': 'bk23a',
        'playing': 'custom',
        'time_constraint': [(1110, 1111)],
        'auditorium_constraint': {'scheme': None, 'capacity': (0, 10000),
            'id': 15},

        'format': '2D',
        'min_perf_count': 1,
        'demand': 2,
        'raw_length': 135,
        'title': 'MET Opera'
    },







]


for e in auditoriums:
    e['schedule'] = globs['schedule']
