from utilities.utils import *

globs_mand_fields = ['schedule', 'cleaning', 'trailer', 'adtime',
    'film_length_rounding', 'max_concurrent_starts', 'showing_increments',
    'clean_spacing', 'split_spacing', 'earliest_closing_time']

aud_mand_fields = ['capacity', 'schedule', 'parameters', 'id', 'scheme', 
    'size_label']
movie_mand_fields = ['demand', 'raw_length', 'title']
booking_mand_fields = ['auditorium_constraint', 'formats', 'movie']

general_mand_fields = ['version', 'hourly_gap_constants']


def validate_time(t):
    vassert(t.find(':') != -1, "Need colon in time field")
    tl = t.split(':')
    vassert(len(tl[0]) in [1,2], "Invalid syntax: {0}".format(tl[0]))
    vassert(is_int(tl[0]), "Hour must be int ({0})".format(tl[0]))
    vassert(is_int(tl[1]), "Minute must be int ({0})".format(tl[1]))
    return True

def validate_globs(dct):
    for e in globs_mand_fields:
        vassert(e in dct, "need field '{0}' in globs".format(e))

    for e in globs_mand_fields:
        if e == 'schedule':
            continue
        elif e in ['clean_spacing', 'split_spacing']:
            vassert(type(dct[e]) == float, 
                "Globs field {0} must be float".format(e))
        else:
            vassert(type(dct[e]) == int, "Globs field {0} must be int".format(
                e))
    validate_time(dct['schedule'][0])
    validate_time(dct['schedule'][1])


def validate_auditorium(dct):
    vassert(type(dct) == dict, "need a dictionary")
    vassert('id' in dct, "Need 'id' field for all auditoriums")

    for e in aud_mand_fields:
        vassert(e in dct, "need field '{0}' in aud id {1}".format(e,
            dct['id']))

    vassert(type(dct['capacity']) == int, 
        "capacity not int in id {0}".format(dct['id']))
    vassert(dct['capacity'] > 0, "capacity not positive in id {0}".format(
        dct['id']))

    vassert(type(dct['size_label']) in [str, unicode], 
        "size_label must be string in id {0}".format(dct['id']))
    vassert(len(dct['size_label']) > 0, 
        "size_label cannot be empty in id {0}".format(dct['id']))

#    e = dct['schedule']
#    vassert(type(e) == tuple, "Schedule not a tuple")
#    vassert(len(e) == 2, "Schedule has length != 2")
    validate_time(dct['schedule'][0])
    validate_time(dct['schedule'][1])

    vassert(type(dct['parameters']) == dict, "Parameters not a dict")
    vassert('formats' in dct['parameters'], "No formats in 'parameters'")

def validate_movie(dct):
#    print dct
    vassert(type(dct) == dict, "need a dictionary")
    vassert('id' in dct, "Need 'id' field in all movies")

    for e in movie_mand_fields:
        vassert(e in dct, "need field '{0}' in movie id {1}".format(e,
            dct['id']))

    vassert(type(dct['demand']) == int, "Demand not int")
    vassert((dct['demand'] > 0) and dct['demand'] <= 10, 
        "Demand not positive")
    vassert(type(dct['raw_length']) == int, "Raw_length not an int")
    ll = dct['raw_length']
    vassert(ll > 0 and ll < 240, "Raw_length not in 1-240")



def validate_booking(dct):
#    print dct
    vassert(type(dct) == dict, "need a dictionary")
    vassert('id' in dct, "need field 'id' in all bookings")

    for e in booking_mand_fields:
        vassert(e in dct, "need field '{0}' in booking {1}".format(e, 
            dct['id']))

    vassert('playing' in dct, 'Need a "playing" field')
    vassert(dct['playing'] in ['clean', 'split', 'custom'], 
        "Invalid playing type: {0}".format(dct['playing']))

    if dct['playing'] in  ['split', 'custom']:
        vassert(len(dct['time_constraint']) == len(dct['formats']),
            "Need as many time slots as the min performance count ({0})".format(
                dct['id']))

    vassert('scheme' in dct['auditorium_constraint'], 
        "need field 'auditorium_constraint.scheme' in booking {0}".format(
            dct['id']))
    vassert('capacity' in dct['auditorium_constraint'], 
        "need field 'auditorium_constraint.capacity' in booking {0}".format(
            dct['id']))
    vassert(type(dct['auditorium_constraint']['scheme']) in [str, unicode], 
        "auditorium_constraint.scheme must be string in booking {0}".format(
            dct['id']))
    vassert(type(dct['auditorium_constraint']['capacity']) == list, 
        "auditorium_constraint.capacity must be list in booking {0}".format(
            dct['id']))
    for e in dct['auditorium_constraint']['capacity']:
        if type(e) not in [str, unicode]:
            vassert(False, "all entries in auditorium_constraint.capacity must be str in booking {0}".format(dct['id']))


def mba_cross_validate(movies, bookings, auditoriums):
    seen_movies = set([])
    for e in movies:
        seen_movies.add(e['id'])

    for e in bookings:
        vassert(e['movie'] in seen_movies, 
            "Booking {0} refers to unknown movie ID {1}".format(
                e['id'], e['movie']))

    for e in bookings:
        ll = e['auditorium_constraint']['capacity']
        if len(ll) == 0:
            continue
        ls = set(ll)
        vall = any([x['size_label'] in ls for x in auditoriums])
        vassert(vall, 
            "Booking {0} seems to have no suitable auditorium size-wise".format(
            e['id']))

        if len(e['auditorium_constraint']['scheme']) > 0:
            s = e['auditorium_constraint']['scheme']
            vall = any([x['scheme'] == s for x in auditoriums])
            vassert(vall, "Booking {0} seems to have no suitable auditorium scheme-wise".format(e['id']))


"""
validate_auditorium(
    {
        'id': 11111,
        'capacity': 25,
        'schedule': ("9:30", "20:45"),
        'parameters': {'formats': ['2D']}
        
        }
)
"""

if __name__ == "__main__":
    from auds_1_18 import *

    for e in auditoriums:
        validate_auditorium(e)
