from auditorium import *
from showing import *
from enforcer import *
import numpy as np
import copy

class SDB:
    def __init__(self, auds, showings):
        #1 load auds in prio order
        self.auds = []
        self.auds_by_id = {}
        self.auds_by_prio = []
        self.pre_solution = []

        for i, e in enumerate(auds):
            self.auds.append(e)
            self.auds_by_id[e.ext_id] = i

        sorter = sorted([(-auds[i].capacity, auds[i].id) for i in 
            range(len(auds))])

        self.auds_by_prio = [x[1] for x in sorter]

        self.showings = []
        for e in showings:
            vassert(len(e.time_constraint) <= 1, 
                "Time constraint must have length <= 1 ({0})".format(e.id))

            if e.type == 'custom':
                x = copy.deepcopy(e)
                audi = self.auds[self.auds_by_id[
                    x.aud_constraint['id']]]
                x.assigned_aud = audi
                x.assigned_time = x.time_constraint[0][0]

                self.pre_solution.append(x)
#                print "AUDI BEFOR BLOCK", audi, x.time_constraint
                audi.blockTime(x.time_constraint[0][0], 
                    x.time_constraint[0][0] + x.length)
#                print "AUDI AFTER BLOCK", audi
            else:
                self.showings.append(e)

    def returnPriorityOrder(self, auds):
        return [x for x in self.auds_by_prio if x in set(auds)]

    def doInitialAssignments(self):
        #in buckets
        for e in self.showings:
            al = e.calculatePossibleAuditoriums(self.auds)
            e.prioritizeAuditoriums(self.auds_by_prio)
            prios = [x.id for x in e.possible_auditoriums]

            aud2assign = self.auds[self.auds_by_id[prios[-1]]]
            e.forceAssignAuditorium(aud2assign)
            aud2assign.assignShowing(e)

        #prioritize
        for e in self.auds:
            e.prioritizeShowings()

        print "BEFORE"
        for e in self.auds:
            print "X", e

        lim = 130000
        while True:
            lim -= 1
            if lim <= 0:
                break
            loads = [x.load for x in self.auds]
            lds = sorted([(self.auds[i].load, i) for i in range(len(self.auds))])

            if lds[-1][0] - lds[0][0] < 150:
                break

#            minload = np.argmin(loads)
#            maxload = np.argmax(loads)
            done_something = False
            for cyc in lds:
                if self.auds[cyc[1]].available_time > 0:
                    continue

                ret = self.decreaseLoad(self.auds[cyc[1]])
                if ret:
                    done_something = True
                    break

#            ret = self.decreaseLoad(self.auds[maxload])
            if not done_something:
                break
#            break

        print "AFTER"
        for e in self.auds:
            print e

        print self.showings[10].possible_auditoriums
#        print minload, maxload

    def decreaseLoad(self, aud):
        #1 find showings that can be reassigned
        shcands = aud.findReassignableShowings()

        #2 iterate on these
        found = False
        for e in shcands:
            if found:
                break

#            print "Trying to move {0} from {1}".format(e.id, e.assigned_aud.id)
#            print "POSS", [x.id for x in e.possible_auditoriums]
            for e2 in e.possible_auditoriums:
                if e2.id == aud.id:
#                    print "\t1"
                    continue
                if e2.id in e.blocked_auditoriums:
#                    print "\t2", e2.id
                    continue
                if not e2.canShowWithTime(e):
#                    print "\t3", e2.id
                    continue

                #I guess this is okay?
                found = True
                print "Moved {0} from {1} to {2}".format(e.id,
                    e.assigned_aud.id, e2.id)

                if e.assigned_aud.id != e.possible_auditoriums[-1].id:
#                if True:
                    e.blocked_auditoriums.add(e.assigned_aud.id)
                e.assigned_aud.unassignShowing(e)
                e.forceAssignAuditorium(e2)
                e2.assignShowing(e)
                break
                
        return found
#            e.forceAssignAuditorium(



if __name__ == "__main__":
    from auds_1_18 import *
    from utilities.utils import *

    for e in auditoriums:
        validate_auditorium(e)
    for e in bookings:
        validate_movie(e)

    rectify_input(globs, auditoriums, bookings)
    sl = []
    for e in bookings:
        if e['playing'] == 'split':
            for e2 in e['time_constraint']:
                cpd = copy.deepcopy(e)
                cpd['time_constraint'] = [e2]
                sl.append(Showing(cpd))
        else:
            for i in range(e['min_perf_count']):
                sl.append(Showing(e))

    sdb = SDB([Auditorium(x) for x in auditoriums], sl)

    for e in sdb.showings:
        print e
    print sdb.auds_by_prio
#    print len(sl)
    sdb.doInitialAssignments()

    for e in sdb.auds:
        print "ARRANGING", e
        e.arrangeShowings()

    for e in sdb.auds:
        print e.verbose_print()
    print sum([x.load for x in sdb.auds])

    print "#aud_id, movie, tech, time"
    for e in sdb.auds:
        for e2 in e.showings:
            ext = ''
            if e2.aud_constraint['scheme']:
                ext += ' / {0}'.format(e2.aud_constraint['scheme'])

            print "{0},{1},{2},{3}:{4}".format(e.ext_id,
                e2.title, (e2.format + ext).upper(), 
                str((e2.assigned_time // 60 ) % 24).zfill(2),
                str((e2.assigned_time % 60)).zfill(2))

    for e in sdb.showings:
        print e
