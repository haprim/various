from utilities.utils import *
import copy

#hard constraints only
def select_auditoriums(auds, bk):
    result = []
    bks = bk['auditorium_constraint']['capacity']
    bkf = bk['auditorium_constraint']['scheme']

    for i in range(len(auds)):
        e = auds[i]
        #1 must have enough capacity
        if e['capacity'] < bks[0] or e['capacity'] >= bks[1]:
            print "BK ID {0} fails on Aud ID {1} due to capacity".format(
                bk['id'], e['id'])
            continue

        if bkf:
            if bkf not in e['parameters']['formats']:
                print "BK ID {0} fails on Aud ID {1} due to scheme".format(
                    bk['id'], e['id'])
                print bkf, e['parameters']['formats']
                continue
        if bk['format'] not in e['parameters']['formats']:
            print "BK ID {0} fails on Aud ID {1} due to format".format(
                bk['id'], e['id'])
            continue

        if 'id' in bk['auditorium_constraint']:
            if e['id'] != bk['auditorium_constraint']['id']:
                print "BK ID {0} fails on Aud ID {1} (restricted ID)".format(
                    bk['id'], e['id'])
                continue

        print "ADDING", i
        result.append(i)

    return result

def primitive_matching(bks, selections, auds):
    aud_times = {i: auditoriums[i]['schedule'][1] - 
        auditoriums[i]['schedule'][0] for i in range(len(auditoriums))}
    skip_bk = [False for _ in bks]
    print "INITIAL AUD TIMES", sum(aud_times.values())

    for i, e in enumerate(bks):
        if len(selections[i]) == 1:
            aud_times[selections[i][0]] -= bks[i]['length'] * \
                bks[i]['min_perf_count']
            print "\tallocating: {0} mins to aud {1}".format(
                bks[i]['length'] * bks[i]['min_perf_count'],
                selections[i][0])
            skip_bk[i] = True

    bksorter = [(bks[i]['demand'], i) for i in range(len(bks)) if \
        skip_bk[i] == False]
    bksorter = list(reversed(sorted(bksorter)))

    audsorter = [(auds[i]['capacity'], i, auds[i]['id']) for i in 
        range(len(auds))]
    audsorter = list(reversed(sorted(audsorter)))

    impossible = False
    print "AS=", audsorter
    print aud_times

    for e in bksorter:
        audcursor = 0
        elt = bks[e[1]]
        unused_time = elt['length'] * elt['min_perf_count']
        audelt = audsorter[audcursor][1]
        print "\n====\nPLACING: {0}, {1} * {2} = {3} to {4}".format(elt['id'], 
            elt['length'], elt['min_perf_count'], unused_time, audelt)
        
        while unused_time > 0:
            if aud_times[audelt] < elt['length']:
                audcursor += 1
                if audcursor >= len(audsorter):
                    impossible = True
                    break

                audelt = audsorter[audcursor][1]
                print "\tskip aud to", audelt, "({0})".format(unused_time)
                continue

            print "\tallocating: {0} mins".format(elt['length'])
            aud_times[audelt] -= elt['length']
            unused_time -= elt['length']
            print "\n",aud_times, sum(aud_times.values())
        if impossible:
            print "IMPOSSIBLE!!!"
            break

    print aud_times
    print "SUM", sum(aud_times.values())

def aud_totals(auds):
    result = {}
    types = {}
    for e in auditoriums:
        playtime = e['schedule'][1] - e['schedule'][0]
        nfts = len(e['parameters']['formats'])
        for e2 in e['paramters']['formats']:
            if e2 not in types:
                types[e2] = []
            types[e2].append(None)


def stat1(bks, auds):
    xxx

def plain_aud_stats(auds):
    result = {}
    types = {}
    for e in auds:
        playtime = e['schedule'][1] - e['schedule'][0]
        tp = '/' + '/'.join(e['parameters']['formats'])
        if tp not in types:
            types[tp] = 0
        types[tp] += playtime

    result['pertype'] = types
    return result



if __name__ == "__main__":
    from infra.auds_1_18 import *
    rectify_input(globs, auditoriums, bookings)

    for e in auditoriums:
        t1 = time_to_mins(e['schedule'][0])
        t2 = time_to_mins(e['schedule'][1])
        if t1 > t2:
            t2 += 1440
        e['schedule'] = (t1, t2)

    plain_aud_stats(auditoriums)        
    print bookings

    maybes = {}
    for i, e in enumerate(bookings):
        maybes[i] = select_auditoriums(auditoriums, e)

    for k in sorted(maybes.keys()):
        print k, bookings[k]['id'], maybes[k]

    primitive_matching(bookings, maybes, auditoriums)        
