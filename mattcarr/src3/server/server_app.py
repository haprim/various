from flask import Flask, request, jsonify
from database import *
from sdbhelper import *
import sys
import syslog
import traceback
#import MySQLdb
app = Flask(__name__)

def log_err():
    tp__, vl__, tb__ = sys.exc_info()
    s1 = ';'.join(traceback.format_tb(tb__)).replace('\n', ';')
    s2 = vl__
    syslog.syslog(s1 + " ||| " + str(s2))


@app.route("/")
def hello():
    return "<h1 style='color:blue'>Hello There2!</h1>"

@app.route("/echo", methods=['POST'])
def echofn():
    try:
        dct = request.get_json(force=True)
    except:
        pass
        return jsonify({"Success": "False"})

    return jsonify(dct)


@app.route("/schedule1", methods=['POST'])
def sched1():
    try:
        dct = request.get_json(force=True)
        totres = perform_scheduling(dct)
        if len(totres) == 0:
            return jsonify({"Success": "False",
                "reason": "Impossible to schedule under the input conditions"})
        add_optionals(totres[-1][3], totres[-1][-1], dct)

        text = results_as_csv(totres[-1][3])
        return jsonify({"Success": "True", "result": text})

    except Exception as e:
        log_err()
        return jsonify({"Success": "False", "reason": str(e)})

    return jsonify(dct)

 
@app.route("/schedule2_old", methods=['POST'])
def sched2_old():
    try:
#        conn = MySQLdb.connect('localhost', 'root', 'azx', 'serverlog')
#        cursor = conn.cursor()

        dct = request.get_json(force=True)
        print dct
        totres = perform_scheduling(dct)
        if len(totres) == 0:
            return jsonify({"Success": "False",
                "reason": "Impossible to schedule under the input conditions"})

#            query = "insert into serverlog(time, input, output) values (now(), {0}, {1})".format(
#                conn.escape_string(str(dct)), 
#                conn.escape_string(str(result)))
#            cursor.execute(query)
#            conn.commit()

        add_optionals(totres[-1][3], totres[-1][-1], dct)

        retl = results_as_json(totres[-1][3])
        return jsonify({"Success": "True", "result": retl})

#        query = "insert into serverlog(time, input, output) values (now(), {0}, {1})".format(
#            conn.escape_string(str(dct)), 
#            conn.escape_string(str(result)))
#        cursor.execute(query)
#        conn.commit()


    except Exception as e:
        log_err()
        return jsonify({"Success": "False", "reason": str(e)})

    return jsonify(dct)



@app.route("/schedule2", methods=['POST'])
def sched2():
    try:
#        conn = MySQLdb.connect('localhost', 'root', 'azx', 'serverlog')
#        cursor = conn.cursor()

        dct = request.get_json(force=True)
        print dct
        preliminary_prepare_input(dct)
        todo = generate_viper_tasks(dct, {'spacing': (0.5, 30)})
        results = []

        for e in todo:
            rd = full_sched(dct, e)
            if rd['status'] > 0:
                results.append(rd)

        if len(results) == 0:
            return jsonify({"Success": "False",
                "reason": "Impossible to schedule under the input conditions"})

        minval = 10000000
        minloc = -1
        for i, e in enumerate(results):
            assert(e['status'] > 0)
            if e['best_time'] < minval:
                minval = e['best_time']
                minloc = i

        retl = results_as_json(results[minloc]['sdb'])
        return jsonify({"Success": "True", "result": retl})

#        query = "insert into serverlog(time, input, output) values (now(), {0}, {1})".format(
#            conn.escape_string(str(dct)), 
#            conn.escape_string(str(result)))
#        cursor.execute(query)
#        conn.commit()


    except Exception as e:
        log_err()
        return jsonify({"Success": "False", "reason": str(e)})

    return jsonify(dct)




if __name__ == '__main__':
    app.run(host='0.0.0.0')

