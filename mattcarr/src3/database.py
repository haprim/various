from auditorium import *
from showing import *
from infra.enforcer import *
from constrainter import *
import numpy as np
import copy
from sdbhelper import *
import random

class SDB:
    def __init__(self, auds, showings, maps, extras):
        #1 load auds in prio order
        self.auds = {}
        self.auds_by_prio = []
        self.pre_solution = []
        self.min_dist_splits = extras['min_dist_splits']
        #FIXME only until Zs responds
        self.min_dist_splits = 20
        self.clean_time = extras['cleaning']
        self.trailer_time = extras['trailer']
        self.ad_time = extras['adtime']
        self.timedefs = extras['timedefs']
        self.audmappings = copy.copy(maps)

        for i, e in enumerate(auds):
            self.auds[e.ext_id] = e

        sorter = sorted([(-v.capacity, v.ext_id) for v in self.auds.values() 
            if v.has_custom_showing == False])

        self.auds_by_prio = [x[1] for x in sorter]

        self.showings = {}
        for e in showings:
            vassert(len(e.time_constraint) <= 1, 
                "Time constraint must have length <= 1 ({0})".format(e.id))

            if e.type == 'custom':
                x = copy.deepcopy(e)
                audi = self.auds[x.aud_constraint['id']]
                x.assigned_aud = audi
                x.assigned_time = x.time_constraint[0][0] - self.ad_time

                audi.hasCustomShowing = True
                self.pre_solution.append((audi, x))
                e.calculatePossibleAuditoriums([audi])
            else:
                self.showings[e.id] = e
                e.calculatePossibleAuditoriums([x for x in self.auds.values() 
                    if not x.has_custom_showing])

            vassert(len(e.possible_auditoriums) > 0, 
                'Showing with booking ID {0} / title "{1}" has no auditorium that can play it'.format(
                    e.bkid, e.title))

        for e in self.auds.values():
            if e.has_custom_showing:
                del self.auds[e.ext_id]


    #call this to observe any freezes
    def calculateFreezes(self):
        for k, v in self.auds.items():
            fr1, tot1 = v.getNFreezes()
            if tot1 == fr1:
                v.can_addremove = False
        
        sorter = sorted([(v.can_addremove, v.available_time, -v.capacity, 
            v.ext_id) for v in self.auds.values()])

        self.auds_by_prio = [x[3] for x in sorter]
        return


    def doInitialAssignments(self, congregate_cleans=True, n_combs = 0):
        prehandled_showings = set([])
        if congregate_cleans:
            cleans, used_auds = congregateCleanShowings(self.showings, 
                self.auds, self.clean_time, self.ad_time)

            #HACK: never, under any circumstance treat leftovers of a custom
            #auditorium as clean
            #cleans = {k:v for k, v in cleans.items() if (type(v) == int) or ((type(v) == str) and (v.find('temp') != 0))}

            prehandled_showings = self.assignCleanBookings(cleans, used_auds)
            self.combAuditoriums(cleans, n_combs)

            self.freezeAuditoriumAssignments(used_auds)


        splist = [(x.time_constraint[0][0], x.time_constraint[0][1], x) for x
            in self.showings.values() if x.type == 'split']

        available_auds = {k: v for k, v in self.auds.items() if k not in used_auds}

        self.distributeSplitShowings(splist, available_auds)
#        print available_auds

        #prioritize
        for e in self.auds.values():
            e.prioritizeShowings()

        msg(256, "AFTER INITIAL ASSIGNMENTS")
        for e in self.auds.values():
            msg(256, "{0} - {1}".format(e, e.available_time))

        self.smoothenLoads_Initial()


    def smoothenLoads_Initial(self): 

        while True:
            loads = [x.load for x in self.auds.values()]

            lds = sorted([(v.load, k) for k, v in self.auds.items()])
            if lds[-1][0] - lds[0][0] < 100: #FIXME should be a constant
                break

            done_something = False
            for cyc in lds:
                #if the auditorium is not fully booked, leave it
#                if self.auds[cyc[1]].available_time > 0:
#                    continue

                ret = self.decreaseLoads(self.auds[cyc[1]], 2,
                    avoid_title_clashes = True)
                if not ret:
                    ret = self.decreaseLoads(self.auds[cyc[1]], 2,
                        avoid_title_clashes = False)

                if ret:
                    done_something = True
                    break

            if not done_something:
                break
#            break

        msg(256, "AFTER LOAD BALANCING")
        for e in self.auds.values():
            msg(256, "{0} - {1}".format(e, e.available_time))


    def decreaseLoads(self, aud, op_type, avoid_title_clashes = False):
        assert(op_type in [1,2])
        #1 find showings that can be reassigned
        shcands = aud.findReassignableShowings()

        #2 iterate on these
        found = False
        for e in shcands:
            if found:
                break

            msg(1024, "Trying to move {0} from {1} (can go to: {2})".format(
                e.id, e.assigned_aud.id, e.possible_auditoriums))

            for k in e.possible_auditoriums:
                v = e.auditoriums[k]
                if v.id == aud.id:
                    continue
                if v.id in e.blocked_auditoriums:
                    continue

                if v.hasTimingConflict(e, self.min_dist_splits):
                    continue

                if avoid_title_clashes:
                    if v.hasTitleClash(e):
                        continue

                if op_type == 2:
                    error_before = abs(aud.available_time) + \
                        abs(v.available_time)
                    error_after = abs(aud.available_time + e.length) + \
                        abs(v.available_time - e.length)

                    if error_after > error_before:
                        continue
                    elif error_after == error_before:
                        if aud.load - e.length <= v.load + e.length:
                            continue
                elif op_type == 1:
                    if not v.canHostShowing(e):
                        continue

                    if v.available_time - e.length < 0:
                        continue

                #I guess this is okay?
                found = True
                if op_type == 1:
                    msg(512, "Moved {0} from {1} to {2}".format(e.id,
                        e.assigned_aud.id, v.id))
                    #FIXME is this branch ever called?

                elif op_type == 2:
                    msg(512, "Wiggled {0} from {1} to {2}".format(e.id,
                        e.assigned_aud.id, v.id))

                if e.assigned_aud != e.possible_auditoriums[-1]:
                    e.blocked_auditoriums.add(e.assigned_aud)

                e.assigned_aud.unassignShowing(e)
                e.forceAssignAuditorium(v)
                v.assignShowing(e)
                break
                
        return found


    def smoothenLoads_SingleSwaps(self): 
        result = True
        al = [(x.available_time, x.ext_id) for x in self.auds.values()]
        al = [x[1] for x in sorted(al)]
#        for e0 in reversed(self.auds_by_prio):
        for e0 in al:
            v = self.auds[e0]
            msg(1024, "TRY SWAP: audid={0}, avtm={1}, others={2}".format(
                e0, v.available_time, al))
#            if v.available_time >= 0:
#                continue
            if not v.can_addremove:
                continue

            fixed = False
            
            #greedily try to fix at the expense of the next
#            for k2 in self.auds_by_prio:
            for k2 in reversed(al):
                while True:
                    msg(1024, "\tvs audid={0}, avtm={1}".format(k2, 
                        self.auds[k2].available_time))
                    if k2 == e0:
                        break

                    ret = self.doSingleSwaps(e0, k2)
#                    self.reassignHours()
                    if ret:
                        if v.available_time >= 0:
                            msg(1024,"\t\tFIXED")
                            for ee in self.auds.values():
                                msg(2048, "\t {0} - {1}".format(ee, 
                                    ee.available_time))
                            fixed = True
                            break

                    #not fixed, but something happened:
                    if ret:
                        continue

                    break
                if fixed:
                    break

            if not fixed:
                result = False

        return result

    def doSingleSwaps(self, src, targ):
        msg(2048, "\t\t audid={0}, avtm={1} <--> audid={2}, avtm={3}".format(
            self.auds[src].ext_id, self.auds[src].available_time, 
            self.auds[targ].ext_id, self.auds[targ].available_time))

        if self.auds[src].available_time > self.auds[targ].available_time:
            msg(2048, "\t\t\tSwap would not increase src.available_time, break")
            return False

        srclens = sorted([(v.length, v) for k, v in self.auds[src].showings.items()])
        targlens = sorted([(v.length, v) for k, v in self.auds[targ].showings.items()])
        msg(2048, "\t\t srclist: {0}, targlist: {1}".format(
            [(x[0], x[1].id) for x in srclens], 
            [(x[0], x[1].id) for x in targlens]))

        #now find movies that can be swapped!!!
        found = False
        swapout_cands = []
        for e in reversed(srclens):
            if self.auds[targ].canHostShowing(e[1]):
                if not self.auds[targ].hasTimingConflict(e[1], 
                    self.min_dist_splits):
                    swapout_cands.append(e[1])

        if len(swapout_cands) == 0:
            msg(2048, "\t\t\tNo showings to swap from src to targ, break")
            return False

        #case 1: large time difference, can swap out of src
        for srcsh in swapout_cands:
            if self.auds[targ].available_time - srcsh.length > 0:
                self.auds[src].unassignShowing(srcsh)
                self.showings[srcsh.id].forceAssignAuditorium(self.auds[targ])
                self.auds[targ].assignShowing(srcsh)
                return True

        #case 2: swap against something
        srcsh = swapout_cands[0]

        found = False
        for e in targlens:
            if self.auds[src].canHostShowing(e[1]):
                if not self.auds[src].hasTimingConflict(e[1], 
                    self.min_dist_splits):
                    found = True
                    break
        if not found:
            msg(2048, "\t\t\tNo showings to swap from targ to src, break")
            return False
        targsh = e[1]

        #when do we swap?
        #well, either if the movie we swap out is longer than the one we'd swap in or if by swapping it out, we fix the src without ruining the target.
#        if srclens[-1][0] > targlens[0][0]:
        if srcsh.length > targsh.length:
#            srcsh = srclens[-1][1]
#            targsh = targlens[0][1]

#            if self.auds[targ].available_time - targsh.length + srcsh.length < 0:
#                return False

            self.auds[src].unassignShowing(srcsh)
            self.showings[srcsh.id].forceAssignAuditorium(self.auds[targ])
            self.auds[targ].assignShowing(srcsh)

            self.auds[targ].unassignShowing(targsh)
#            print "\t", targsh.id, src, len(self.showings)

#            print self.showings[targsh.id]
            self.showings[targsh.id].forceAssignAuditorium(self.auds[src])
            self.auds[src].assignShowing(targsh)

            msg(1024, "Swapped: {0}({4}) {1}->{2}, {3}({5}) {2}->{1}".format(
                srcsh.id, src, targ, targsh.id, srclens[0][0], 
                targlens[-1][0]))
            msg(2048, "\tAVTS now: {0}: {1}, {2}: {3}".format(
                src, self.auds[src].available_time,
                targ, self.auds[targ].available_time))

            return True
                
        return False

    #leave out partials! e.g. yyy
    def reassignHours(self):
        scheds = list(reversed(sorted([(x.close_at - x.open_at, x.open_at, 
            x.close_at, x.ext_id) for x in self.auds.values() if not x.is_partial])))
        avs = sorted([(x.available_time, x.ext_id) 
            for x in self.auds.values() if not x.is_partial])
        for i in range(len(avs)):
            self.auds[avs[i][1]].adjustOpenHours(scheds[i][1], scheds[i][2])

################

    #try to swap constraints with other showings of the same movie
    def swapConstrainedPairs(self, audid, aud_cands):
        for e in self.auds[audid].showings.values():
            if len(e.time_constraint) == 0:
                msg(1024, "\tMATCHING AUD {0}, {1}".format(audid, e))
                found = False
                for k1 in aud_cands:
                    for k2 in self.auds[k1].showings.values():
                        if k2.title == e.title:
                            if len(k2.time_constraint) > 0:
                                if k2.time_constraint[0][0] <= e.assigned_time < k2.time_constraint[0][1]:
                                    msg(1024, "PROMISING: {0}".format(k2))
                                    if self.auds[audid].canHostShowing(k2):
                                        msg(1024, "\t\tswapping: {0} in aud {1}".format(k2, k1))
                                        e.time_constraint = k2.time_constraint
                                        k2.time_constraint = []
                                        found = True
                                        break
                        if found:
                            break

    def mst(self, prio_index, csr):
        strs = {}
        me = self.auds_by_prio[prio_index]
        msg(1024, "\tSTRESS LEVELS {0} (avt {1}".format(
            me, self.auds[me].available_time))

        for i, e2 in enumerate(self.auds_by_prio):
            if (len(self.auds[e2].showings) == 0) and \
                    (self.auds[e2].can_addremove == False):
                continue
            stress = csr.measure_stress(self.auds[e2])
            if len(stress) == 0:
                strfrac = 0
            else:
                strfrac = sum(stress.values()) / float(len(stress))
            if i > prio_index:
                strs[e2] = stress
            elif i == prio_index:
                base = stress
                baseval = strfrac

            msg(1024, "\t\taud {0}:, stress={1}".format(e2, strfrac))
        return strs, base, baseval


    def swapShowings(self, src, srcsh, targ, targsh):
        src.unassignShowing(srcsh)
        self.showings[srcsh.id].forceAssignAuditorium(targ)
        targ.assignShowing(srcsh)

        targ.unassignShowing(targsh)
        self.showings[targsh.id].forceAssignAuditorium(src)
        src.assignShowing(targsh)

    def decreaseStress(self, prio_index, csr):

        strs, base, baseval = self.mst(prio_index, csr)
        me = self.auds_by_prio[prio_index]
#        print "strs=", strs
        sorter = [(sum(v.values())/float(len(v)), k) for k, v in strs.items()]
        sorter = sorted(sorter)

        found = False

        src = self.auds[me]
        for cands in sorter:
            if cands[0] >= baseval:
                break

            out = []
            for k, v in base.items():
                if v > 0:
                    if self.auds[cands[1]].canHostShowing(self.showings[k]):
                        out.append((len(self.showings[k].time_constraint), k,v))
            out = list(reversed(sorted(out)))                        
            msg(1024, "\tRegarding move from {0} to {1}: {2}".format(
                me, cands[1], out))
            inc = []
            for k, v in strs[cands[1]].items():
                if v == 0:
                    if self.auds[me].canHostShowing(self.showings[k]):
                        inc.append((len(self.showings[k].time_constraint),k,v))
            inc = sorted(inc)                        
            msg(1024, "\t\tConverse dir: {0}".format(inc))

            mindiff = 999999
            minval = None
            targ = self.auds[cands[1]]

            for e1 in out:
                if found:
                    break

                srcsh = self.showings[e1[1]]
                for e2 in inc:
                    msg(2048, "\tTry: {0} vs {1}".format(e1[1], e2[1]))
                    if e2[0] > 0:
                        msg(2048, "\t\tConstrained")
                        break
                                            
                    targsh = self.showings[e2[1]]

                    diff = targsh.length - srcsh.length
                    msg(2048, "\t\tDiff={0}, AT1={1}, AT2={2}".format(diff,
                        src.available_time, targ.available_time))
                    if src.available_time - diff < 0:
                        msg(2048, "\t\t\tLeft")
                        continue
                    if targ.available_time + diff < 0:
                        msg(2048, "\t\t\tRight")
                        continue

                    #otherwise we found it!!
                    msg(1024,  "COOL, we will swap {0} out and {1} in from {2}".format(srcsh.id, targsh.id, targ.ext_id))
                    self.swapShowings(src, srcsh, targ, targsh)

                    stress = csr.measure_stress(src)
                    if sum(stress.values()) / float(len(stress)) < baseval - 0.01:
                        found = True
                        msg(1024, "\tand it was okay!")
                        break
                    else:
                        msg(1024, "\tbut alas, a delibabe")
                        self.swapShowings(src, targsh, targ, srcsh)

            if found:
                break

        msg(1024,  "SEE AGAIN THE STRESS")
        msg(1024, "SHOWINGS: {0}".format(
            [x.id for x in self.auds[me].showings.values()]))
        a,b,c=self.mst(prio_index, csr)
        msg(1024, "\tMST: {0} - {1}".format(b,c))

        return found           


    def decreaseStress2(self, prio_index, csr):

        strs, base, baseval = self.mst(prio_index, csr)
        me = self.auds_by_prio[prio_index]

        sorter = [(self.auds[k].available_time, k) for k in strs.keys()]
        sorter = list(reversed(sorted(sorter)))
#        print sorter
        found = False

        src = self.auds[me]
        diffl = []

        for cands in sorter:

            out = []
            for k, v in base.items():
                if v > 0:
                    if self.auds[cands[1]].canHostShowing(self.showings[k]):
                        out.append((len(self.showings[k].time_constraint), k,v))
            out = list(reversed(sorted(out)))                        
            msg(1024, "\tRegarding move from {0} to {1}: {2}".format(
                me, cands[1], out))
            inc = []
            for k, v in strs[cands[1]].items():
                if v == 0:
                    if self.auds[me].canHostShowing(self.showings[k]):
                        inc.append((len(self.showings[k].time_constraint),k,v))
            inc = sorted(inc)                        
            msg(1024, "\t\tConverse dir: {0}".format(inc))

            mindiff = 999999
            minval = None
            targ = self.auds[cands[1]]

            for e1 in out:
                if found:
                    break

                srcsh = self.showings[e1[1]]
                for e2 in inc:
                    msg(2048, "\tTry: {0} vs {1}".format(e1[1], e2[1]))
#                    if e2[0] > 0:
#                        print "\t\tConstrained"
#                        break
                                            
                    targsh = self.showings[e2[1]]

                    diff = targsh.length - srcsh.length
                    msg(2048, "\t\tDiff={0}, AT1={1}, AT2={2}".format(diff,
                        src.available_time, targ.available_time))
                    if src.available_time - diff < 0:
                        continue
                    if targ.available_time + diff < 0:
                        continue
                    diffl.append((diff, srcsh.id, targ.ext_id, targsh.id))

        diffl = sorted(diffl)
        msg(1024, "diffl: {0}".format(diffl))

        for e in diffl:
            srcsh = self.showings[e[1]]
            targ = self.auds[e[2]]
            targsh = self.showings[e[3]]
            msg(1024, "\tMay swap: {0}({1}) <-> {2}({3})".format(srcsh.id,
                src.ext_id,
                targsh.id, targ.ext_id))

            self.swapShowings(src, srcsh, targ, targsh)

            stress = csr.measure_stress(src)
            if sum(stress.values()) / float(len(stress)) < baseval - 0.01:
                found = True
                msg(1024, "\tand it was okay!")
                break
            else:
                msg(1024, "\tbut alas, a delibabe")
                self.swapShowings(src, targsh, targ, srcsh)

            if found:
                break

        msg(1024,  "SEE AGAIN THE STRESS")
        msg(1024, "SHOWINGS: {0}".format(
            [x.id for x in self.auds[me].showings.values()]))
        a,b,c=self.mst(prio_index, csr)
        msg(1024, "\tMST: {0} - {1}".format(b,c))

        return found           

    def stats(self):
        ok = all([x.assigned_aud != None and x.assigned_time != -1 for x in
            self.showings.values()])

        latest = -1
        used_time = 0
        total_time = 0
        for k, v in self.auds.items():
            if len(v.showings) == 0:
                continue 

            for e in v.showings.values():
                used_time += e.length

            closing_time = max([x.assigned_time + x.length - v.clean_time  
                for x in v.showings.values()])

            total_time += closing_time - v.open_at 
            if closing_time > latest:
                latest = closing_time
#                print "late", k

        if latest == -1:
            ok = False

        return (ok, latest)




    ###DEFINED EXTERNALLY
    assignCleanBookings = assignCleanBookings
    distributeSplitShowings = distributeSplitShowings
    freezeAuditoriumAssignments = freezeAuditoriumAssignments
    combAuditoriums = combAuditoriums
    findCombableAuditoriumPair = findCombableAuditoriumPair
    mergeAuditoriums = mergeAuditoriums
#    improveAudAssignments = improveAudAssignments


def dhelper(csr):
    print "\n\n===============\nSCHEDULE STATE:"
    for k in sorted(csr.basic_clean_intervals.keys()):
        v = csr.basic_clean_intervals[k]
        print "\tMovie {0}".format(k)
        for e in sorted(v):
            t = (e[0] + e[1]) // 2 + csr.ad_time
            print "\t\tPlaying at {4}:{5} - {0} ({1}-{2}) in aud {3}".format(
                t, e[0], e[1], e[2],
                str(t // 60).zfill(2),
                str(t % 60).zfill(2))
#    for e in sdb.auds_by_prio:

def results_as_csv(sdb):
    res = []
    res.append("#aud_id, movie, tech, time, optional")
    for k in sorted(sdb.auds.keys()):
        v = sdb.auds[k]
        shl = [(x.assigned_time, x) for x in v.showings.values()]
        shl = [x[1] for x in sorted(shl)]

        for i2, e2 in enumerate(shl):
            ext = ''
            if e2.aud_constraint['scheme']:
                ext += ' / {0}'.format(e2.aud_constraint['scheme'])

            adt = sdb.ad_time
            res.append("{0},{1},{2},{3}:{4},{5}".format(v.ext_id,
                e2.title, (e2.format + ext).upper(), 
                str(((e2.assigned_time + adt) // 60 ) % 24).zfill(2),
                str(((e2.assigned_time + adt) % 60)).zfill(2),
                int(e2.optional_showing)))

    return '\n'.join(res)


def results_as_json(sdb):
    res = []
    for k in sorted(sdb.auds.keys()):
        v = sdb.auds[k]
        shl = [(x.assigned_time, x) for x in v.showings.values()]
        shl = [x[1] for x in sorted(shl)]

        for i2, e2 in enumerate(shl):
            ext = ''
            if e2.aud_constraint['scheme']:
                ext += ' / {0}'.format(e2.aud_constraint['scheme'])

            adt = sdb.ad_time
#            res.append("{0},{1},{2},{3}:{4},{5}".format(v.ext_id,
#                e2.title, (e2.format + ext).upper(), 
#                str(((e2.assigned_time + adt) // 60 ) % 24).zfill(2),
#                str(((e2.assigned_time + adt) % 60)).zfill(2),
#                int(e2.optional_showing)))
            res.append(e2.asDict(adtime=sdb.ad_time))

    return res


def perform_scheduling(inpdct):
    auds, books, maps = sdb_prepare_inputs(inpdct)

    bestresult = 10000
    ctr = 0
    totres = []

    while True:
        sdb = SDB([Auditorium(x, inpdct['globs']['cleaning'], 
            inpdct['globs']['adtime']) for x in auds], 
            [Showing(x) for x in books], maps, inpdct['globs'])

        msg(256, "Showings")
        for e in sdb.showings.values():
            msg(256, e)

        msg(256, "Aud priority order: {0}".format(sdb.auds_by_prio))

        sdb.doInitialAssignments(n_combs=ctr)

        for i in range(5):
            sdb.smoothenLoads_SingleSwaps()

        sdb.calculateFreezes()

#        sdb.improveAudAssignments()

        #now arrange:
        cinit = assemble_constrainter_init(inpdct['globs'], inpdct['bookings'])
        csr = Constrainter(cinit)

        for i, k in enumerate(sdb.auds_by_prio):
            v = sdb.auds[k]
#        dhelper(csr)

#        sdb.decreaseStress(i)
            msg(256, "ARRANGING {0}".format(v))
            msg(256, "\tplaying")
            for ee in v.showings.values():
                msg(256, "\t{0}".format(ee))

            for ii in range(5):
                v.arrangeShowings(csr)
                if len(v.showings) == 0:
                    break
                if v.showings.values()[0].assigned_time != -1:
                    break
                pp = sdb.decreaseStress(i, csr)
                if not pp:
                    sdb.decreaseStress2(i, csr)

                msg(256, "REARRANGING {0}".format(v))
                msg(256, "\tow tplaying")
                for ee in v.showings.values():
                    msg(256, "\t{0}".format(ee))

            msg(512, "NOW SWAP out of {0}, with {1}".format(k, 
                sdb.auds_by_prio[i+1:]))

            sdb.swapConstrainedPairs(k, sdb.auds_by_prio[i + 1:])

        bad_auds = []
        for e in sdb.auds_by_prio:
            if len(sdb.auds[e].showings) == 0:
                continue
#            if any([x.assigned_time == -1 
#                    for x in sdb.auds[e].showings.values()]):
            sh1 = sdb.auds[e].showings.values()[0]
            if sh1.assigned_time == -1:
                bad_auds.append(e)
        for e in bad_auds:
            msg(1, "BAD AUD: {0}, {1}".format(e, sdb.auds[e].available_time))
            for e2 in sdb.auds[e].showings:
                msg(1, sdb.showings[e2])

        for e in bad_auds:
            sdb.swapConstrainedPairs(e, [x for x in sdb.auds_by_prio if x != e])
        
        for k, v in csr.basic_clean_intervals.items():
            msg(16, "CSR entry: {0} - {1}".format(k, sorted(v)))

        sdb.mergeAuditoriums(csr)
        adtime = inpdct['globs']['adtime']

        stt, bst = sdb.stats()
        if not stt:
            break
        if bst >= bestresult:
            break
        bestresult = bst
        totres.append((ctr, bestresult, sdb, csr))
        ctr += 1

    return totres



def add_optionals(sdb, csr, inpdct):
    #0 set closing hour to latest existing
    sok, maxclose = sdb.stats()
    if maxclose < inpdct['globs']['earliest_closing_time']:
        maxclose = inpdct['globs']['earliest_closing_time']

    assert(sok)
    for k, v in sdb.auds.items():
        v.shrinkClosingHour(maxclose)

    sorter = sorted([(-v.capacity, v.ext_id) for v in sdb.auds.values()])

    sdb.auds_by_prio = [x[1] for x in sorter]

    clean_retries = 10
    itctr = 0
    while True:
        #1 generate 
        newshows, targauds = generate_single_random_bookings(inpdct['globs'],
            sdb.auds.values(), inpdct['movies'], inpdct['bookings'], itctr)
        added = []

        for i, e in enumerate(newshows):
            for e2id in [targauds[i]]:
#            for e2id in sdib.auds_by_prio:

                msg(8, "Trying to optio-push {0} into {1}".format(
                    e.title, e2id))
                e2 = sdb.auds[e2id]
                curlast = e2.lastShowEnding(with_cleaning = True)
#                nst = csr.next_showtime(e, e2, curlast)
                nst = csr.next_showtime(e, e2, e2.open_at)
                if nst > -1:
                    e2.assignShowing(e)
                    e.forceAssignAuditorium(e2)
                    e.fix_to_auditorium = True
                    msg(4, "SCHEDULING: {0}, {1}, {2}".format(e, e2.ext_id, 
                        nst))

                    e2.scheduleShowing(e.id, nst, csr)
                    added.append(e)
                    break
        
        if len(added) == 0:
            clean_retries -= 1
            if clean_retries <= 0:
                break
        itctr += 1


def apply_viper_setting(inpdct, setting):
    for e in ['clean_spacing', 'split_spacing']:
        if e in setting:
            inpdct['globs'][e] = setting[e]

def full_sched(inpdct, vipersetting):
    apply_viper_setting(inpdct, vipersetting)    

    totres = perform_scheduling(inpdct)

    if len(totres) > 0:
#        dhelper(totres[-1][-1])

#        print results_as_csv(totres[-1][3])
        add_optionals(totres[-1][2], totres[-1][-1], inpdct)

#        print results_as_csv(totres[-1][2])
#        dhelper(totres[-1][-1])
        result = {'status': 1, 'best_time': totres[-1][1],
            'best_index': totres[-1][0],
            'sdb': totres[-1][2],
            'csr': totres[-1][3]}
        return result

#
    return {'status': 0}

def generate_viper_tasks(dct, tasks):
    superlist = []
    if 'spacing' in tasks:
        ndiv = tasks['spacing'][1]
        assert(ndiv > 1)
        loc = [{} for i in range(ndiv)]

        for e in ['clean', 'split']:
            key = '{0}_spacing'.format(e)
            refval = dct['globs'][key]
            otherval = max(refval * tasks['spacing'][0], 0.08)
            step = (refval - otherval) / (ndiv - 1)
            for i in range(ndiv):
                loc[i][key] = otherval + i * step
        superlist.append(loc)


    lens = [len(x) for x in superlist]
    totvers = 1
    for e in lens:
        totvers *= e

    assert(totvers < 100)

    cursors = [lens[i] - 1 for i in range(len(superlist))]
    rls = range(len(superlist))
    maxind = len(superlist) - 1
    result = []

    for i in range(totvers):
        dct = {}
        for j in rls:
            actual = superlist[j][cursors[j]]
            for k, v in actual.items():
                dct[k] = v
        tail = maxind
        while True:
            cursors[tail] -= 1
            if cursors[tail] < 0:
                cursors[tail] = lens[tail] - 1
                tail -= 1

                if tail < 0:
                    break

            else:
                break

        result.append(dct)
    return result


if __name__ == "__main__":
#    from infra.auds_1_18 import *
#    from zs import *
#    from xxx18a import *
#    from yyy5 import *
    from a18 import *

    from utilities.utils import *
    set_verbosity(0)
    import sys
    f = open(sys.argv[1], 'rt')
    s = f.read()
    f.close()
    algo_input = eval(s)
    preliminary_prepare_input(algo_input)

#    print sdb.stats()

    todo = generate_viper_tasks(algo_input, {'spacing': (0.5, 30)})
    results = []

    for e in todo:
        rd = full_sched(algo_input, e)
        results.append(rd)

    minval = 100000000
    minloc = -1
    for i, e in enumerate(results):
        if e['status'] == 0:
            continue
        if e['best_time'] < minval:
            minval = e['best_time']
            minloc = i

    print "BEST RESULT: ", minval

    print results_as_csv(results[minloc]['sdb'])
    dhelper(results[minloc]['csr'])
    """        
        if rd['status'] == 0:
            print "NO LUCK"

        else:
            print results_as_csv(rd['sdb'])
            dhelper(rd['csr'])
    """
    """
    totres = perform_scheduling(algo_input)

    print totres
    if len(totres) > 0:
        dhelper(totres[-1][-1])

#        print results_as_csv(totres[-1][3])
        add_optionals(totres[-1][2], totres[-1][-1], algo_input)

        print results_as_csv(totres[-1][2])
        dhelper(totres[-1][-1])

#    for e in sdb.showings.values():
#        print e.title, e.assigned_time

    """
