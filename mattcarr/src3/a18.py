algo_input = {
    'globs': {
        'schedule': ('10:00', '03:05'),
        'cleaning': 10,
        'trailer': 20,
        'adtime': 25,
        'film_length_rounding': 5,
        'min_dist_splits': 120, #for constrained-time showing conflicts
        'earliest_closing_time': 1400,

        'max_concurrent_starts': 2,
        'showing_increments': 5,

        'clean_spacing': 0.8,
        'split_spacing': 0.15,

        'timedefs': {'early': (540, 720), 'matinee': (720, 900), 
            'afternoon': (900, 1080), 'evening': (1080, 1275), 
            'late': (1275, 1560), 'anytime': (540, 1560)}
    },


    'auditoriums': [
        {
            'id': 1,
            'capacity': 250, 
            'scheme': 'standard',
            'parameters': {'formats': ['2D', '3D']},
            'size_label': 'medium'
        },

        {
            'id': 2,
            'capacity': 225, 
            'scheme': 'standard',
            'parameters': {'formats': ['2D']},
            'size_label': 'medium'
        },

        {
            'id': 3,
            'capacity': 160, 
            'scheme': 'standard',
            'parameters': {'formats': ['2D', '3D']},
            'size_label': 'small'
        },
        {
            'id': 4,
            'capacity': 120, 
            'scheme': 'standard',
            'parameters': {'formats': ['2D']},
            'size_label': 'small'
        },
        {
            'id': 5,
            'capacity': 120, 
            'scheme': 'standard',
            'parameters': {'formats': ['2D']},
            'size_label': 'small'
        },
        {
            'id': 6,
            'capacity': 162, 
            'scheme': 'standard',
            'parameters': {'formats': ['2D']},
            'size_label': 'small'
        },
        {
            'id': 7,
            'capacity': 225, 
            'scheme': 'standard',
            'parameters': {'formats': ['2D']},
            'size_label': 'medium'
        },
        {
            'id': 8,
            'capacity': 300, 
            'scheme': 'standard',
            'parameters': {'formats': ['2D']},
            'size_label': 'large'
        },
        {
            'id': 9,
            'capacity': 400, 
            'scheme': 'RPX',
            'parameters': {'formats': ['2D']},
            'size_label': 'large'
        },
        {
            'id': 10,
            'capacity': 350, 
            'scheme': 'IMAX',
            'parameters': {'formats': ['2D', '3D']},
            'size_label': 'large'
        },
        {
            'id': 11,
            'capacity': 285,
            'scheme': 'standard',
            'parameters': {'formats': ['2D', '3D']},
            'size_label': 'large'
        },
        {
            'id': 12,
            'capacity': 225, 
            'scheme': 'standard',
            'parameters': {'formats': ['2D']},
            'size_label': 'medium'
        },
        {
            'id': 13,
            'capacity': 154, 
            'scheme': 'standard',
            'parameters': {'formats': ['2D']},
            'size_label': 'small'
        },
        {
            'id': 14,
            'capacity': 120, 
            'scheme': 'standard',
            'parameters': {'formats': ['2D']},
            'size_label': 'small'
        },
        {
            'id': 15,
            'capacity': 120, 
            'scheme': 'standard',
            'parameters': {'formats': ['2D']},
            'size_label': 'small'
        },
        {
            'id': 16,
            'capacity': 154, 
            'scheme': 'standard',
            'parameters': {'formats': ['2D']},
            'size_label': 'small'
        },
        {
            'id': 17,
            'capacity': 225, 
            'scheme': 'SCREENX',
            'parameters': {'formats': ['2D']},
            'size_label': 'medium'
        },
        {
            'id': 18,
            'capacity': 295, 
            'scheme': 'standard',
            'parameters': {'formats': ['2D']},
            'size_label': 'large'
        }


    ],


    'movies': [
        {
            'id': 'movie_bumble_bee',
            'title': 'Bumble Bee',
            'raw_length': 114,
            'demand': 5
        },

        {
            'id': 'movie_met_opera',
            'demand': 2,
            'raw_length': 135,
            'title': 'MET Opera'
        },
        {
            'id': 'movie_bohemian',
            'demand': 2,
            'raw_length': 135,
            'title': 'Bohemian'
        },

        {
            'id': 'movie_second_act',
            'demand': 2,
            'raw_length': 104,
            'title': 'Second Act'
        },

        {
            'id': 'movie_marwel',
            'demand': 2,
            'raw_length': 116,
            'title': 'Marwel'
        },

        {
            'id': 'movie_fant_beasts',
            'demand': 2,
            'raw_length': 134,
            'title': 'Fant Beasts'
        },
        {
            'id': 'movie_creed_2',
            'demand': 2,
            'raw_length': 130,
            'title': 'Creed 2'
        },
        {
            'id': 'movie_grinch',
            'demand': 2,
            'raw_length': 90,
            'title': 'Grinch'
        },
        {
            'id': 'movie_mortal_engine',
            'demand': 2,
            'raw_length': 128,
            'title': 'Mortal Engines'
        },
        {
            'id': 'movie_the_mule',
            'demand': 5,
            'raw_length': 116,
            'title': 'The Mule'
        },
        {
            'id': 'movie_holmes_watson',
            'demand': 2,
            'raw_length': 90,
            'title': 'Holmes Watson'
        },
        {
            'id': 'movie_ralph_breaks',
            'demand': 2,
            'raw_length': 112,
            'title': 'Ralph Breaks'
        },
        {
            'id': 'movie_mary_poppins',
            'demand': 8,
            'raw_length': 130,
            'title': 'Mary Poppins'
        },
        {
            'id': 'movie_insta_family',
            'demand': 2,
            'raw_length': 119,
            'title': 'Insta Family'
        },
        {
            'id': 'movie_aquaman',
            'demand': 8,
            'raw_length': 143,
            'title': 'Aquaman'
        },
        {
            'id': 'movie_spidermanu',
            'demand': 2,
            'raw_length': 117,
            'title': 'SpidermanU'
        },
        {
            'id': 'movie_vice',
            'demand': 5,
            'raw_length': 132,
            'title': 'Vice'
        },

    ],

    'bookings': [
        {
            'id': 'bk01',
            'playing': 'clean',
            'time_constraint': [],
            'auditorium_constraint': {'scheme': 'standard', 'capacity': []},
            'formats': ['2D', '2D', '2D', '2D', '3D'],
            'movie': 'movie_bumble_bee'
        },

        {
            'id': 'bk02',
            'playing': 'clean',
            'time_constraint': [],
            'auditorium_constraint': {'scheme': 'RPX', 'capacity': []},
            'formats': ['2D', '2D', '2D', '2D', '2D'],
            'movie': 'movie_bumble_bee'
        },

        {
            'id': 'bk03',
            'playing': 'clean',
            'time_constraint': [],
            'auditorium_constraint': {'scheme': 'standard', 'capacity': []},
            'formats': ['2D', '2D', '2D', '2D', '2D'],
            'movie': 'movie_vice'
        },
        {
            'id': 'bk04',
            'playing': 'clean',
            'time_constraint': [],
            'auditorium_constraint': {'scheme': 'standard', 'capacity': []},
            'formats': ['2D', '2D', '2D', '2D', '3D'],
            'movie': 'movie_spidermanu'
        },
        {
            'id': 'bk05',
            'playing': 'clean',
            'time_constraint': [],
            'auditorium_constraint': {'scheme': 'standard', 'capacity': []},
            'formats': ['2D', '2D', '2D', '2D'],
            'movie': 'movie_aquaman'
        },

        {
            'id': 'bk06',
            'playing': 'clean',
            'time_constraint': [],
            'auditorium_constraint': {'scheme': 'IMAX', 'capacity': []},
            'formats': ['2D', '2D', '2D', '3D'],
            'movie': 'movie_aquaman'
        },

        {
            'id': 'bk07',
            'playing': 'clean',
            'time_constraint': [],
            'auditorium_constraint': {'scheme': 'SCREENX', 'capacity': []},
            'formats': ['2D', '2D', '2D', '2D'],
            'movie': 'movie_aquaman'
        },

        {
            'id': 'bk08',
            'playing': 'clean',
            'time_constraint': [],
            'auditorium_constraint': {'scheme': 'standard', 'capacity': []},
            'formats': ['2D', '2D', '2D', '2D', '3D'],
            'movie': 'movie_aquaman'
        },
        {
            'id': 'bk09',
            'playing': 'clean',
            'time_constraint': [],
            'auditorium_constraint': {'scheme': 'standard', 'capacity': ["large"]},
            'formats': ['2D', '2D', '2D', '2D', '2D'],
            'movie': 'movie_mary_poppins'
        },

        {
            'id': 'bk10',
            'playing': 'clean',
            'time_constraint': [],
            'auditorium_constraint': {'scheme': 'standard', 'capacity': ["large"]},
            'formats': ['2D', '2D', '2D', '2D', '2D'],
            'movie': 'movie_mary_poppins'
        },

        {
            'id': 'bk11',
            'playing': 'split',
            'time_constraint': ['matinee', 'evening'],
            'auditorium_constraint': {'scheme': 'standard', 'capacity': []},
            'formats': ['2D', '2D'],
            'movie': 'movie_mary_poppins'
        },

        {
            'id': 'bk12',
            'playing': 'split',
            'time_constraint': ['matinee', 'evening'],
            'auditorium_constraint': {'scheme': 'standard', 'capacity': []},
            'formats': ['2D', '2D'],
            'movie': 'movie_insta_family'
        },
        {
            'id': 'bk13',
            'playing': 'split',
#        'time_constraint': [(540, 720), (720, 900), (900, 1080)],
            'time_constraint': ['early', 'matinee', 'afternoon'],
            'auditorium_constraint': {'scheme': 'standard', 'capacity': []},
            'formats': ['2D', '2D', '2D'],
            'movie': 'movie_ralph_breaks'
        },
        {
            'id': 'bk14',
            'playing': 'split',
#        'time_constraint': [(1080, 1275), (1275, 1455)],
            'time_constraint': ['evening', 'late'],
            'auditorium_constraint': {'scheme': 'standard', 'capacity': []},
            'formats': ['2D', '2D'],
            'movie': 'movie_mortal_engine'
        },
        {
            'id': 'bk15',
            'playing': 'clean',
            'time_constraint': [],
            'auditorium_constraint': {'scheme': 'standard', 'capacity': []},
            'formats': ['2D', '2D', '2D', '2D', '2D'],
            'movie': 'movie_the_mule'
        },

        {
            'id': 'bk16',
            'playing': 'clean',
            'time_constraint': [],
            'auditorium_constraint': {'scheme': 'standard', 'capacity': []},
            'formats': ['2D', '2D', '2D', '2D', '2D'],
            'movie': 'movie_holmes_watson'
        },
        {
            'id': 'bk17',
            'playing': 'split',
#        'time_constraint': [(540, 720), (900, 1080), (1275, 1455)],
            'time_constraint': ['early', 'afternoon', 'late'],
            'auditorium_constraint': {'scheme': 'standard', 'capacity': []},
            'formats': ['2D', '2D', '2D'],
            'movie': 'movie_grinch'
        },
        {
            'id': 'bk18',
            'playing': 'clean',
            'time_constraint': [],
            'auditorium_constraint': {'scheme': 'standard', 'capacity': []},
            'formats': ['2D', '2D', '2D', '2D', '2D'],
            'movie': 'movie_marwel'
        },
        {
            'id': 'bk19',
            'playing': 'split',
#        'time_constraint': [(900, 1080), (1275, 1455)],
            'time_constraint': ['afternoon', 'late'],
            'auditorium_constraint': {'scheme': 'standard', 'capacity': []},
            'formats': ['2D', '2D'],
            'movie': 'movie_fant_beasts',
        },

        {
            'id': 'bk20',
            'playing': 'split',
#        'time_constraint': [(900, 1080), (1275, 1455)],
            'time_constraint': ['afternoon', 'late'],
            'auditorium_constraint': {'scheme': 'standard', 'capacity': []},
            'formats': ['2D', '2D'],
            'movie': 'movie_creed_2',
        },
        {
            'id': 'bk21',
            'playing': 'split',
#        'time_constraint': [(540, 900), (1080, 1275)],
            'time_constraint': ['early'],
            'auditorium_constraint': {'scheme': 'standard', 'capacity': []},
            'formats': ['2D'],
            'movie': 'movie_bohemian'
        },
        {
            'id': 'bk22',
            'playing': 'clean',
            'time_constraint': [],
            'auditorium_constraint': {'scheme': 'standard', 'capacity': []},
            'formats': ['2D', '2D', '2D', '2D', '2D'],
            'movie': 'movie_second_act'
        },

        {
            'id': 'bk23',
            'playing': 'custom',
            'time_constraint': [(1110, 1111)],
            'auditorium_constraint': {'scheme': 'standard', 'capacity': [],
                'id': 15},

            'formats': ['2D'],
            'movie': 'movie_met_opera'
        },
    ]
}



#for e in auditoriums:
#    e['schedule'] = globs['schedule']
