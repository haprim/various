from utilities.utils import *
from infra.enforcer import *
from showing import *
import copy


def preliminary_prepare_input(inpdct):
    for e in ['globs', 'auditoriums', 'movies', 'bookings']:
        vassert(e in inpdct, "Need to have field '{0}' in input".format(e))

    validate_globs(inpdct['globs'])

    globs = inpdct['globs']
    auditoriums = inpdct['auditoriums']
    movies = inpdct['movies']
    bookings = inpdct['bookings']

    #enforce format
    for e in auditoriums:
        e['schedule'] = inpdct['globs']['schedule']
        validate_auditorium(e)
    for e in movies:
        validate_movie(e)
    for e in bookings:
        validate_booking(e)

    mba_cross_validate(movies, bookings, auditoriums)

    #patch up input with calculated values
    rectify_input(globs, auditoriums, movies, bookings)


#prepare inputs for algo work
def sdb_prepare_inputs(inpdct):
    globs = inpdct['globs']
    auditoriums = inpdct['auditoriums']
    movies = inpdct['movies']
    bookings = inpdct['bookings']




    """
    for e in ['globs', 'auditoriums', 'movies', 'bookings']:
        vassert(e in inpdct, "Need to have field '{0}' in input".format(e))

    validate_globs(inpdct['globs'])

    globs = inpdct['globs']
    auditoriums = inpdct['auditoriums']
    movies = inpdct['movies']
    bookings = inpdct['bookings']

    #enforce format
    for e in auditoriums:
        e['schedule'] = inpdct['globs']['schedule']
        validate_auditorium(e)
    for e in movies:
        validate_movie(e)
    for e in bookings:
        validate_booking(e)

    mba_cross_validate(movies, bookings, auditoriums)

    #patch up input with calculated values
    rectify_input(globs, auditoriums, movies, bookings)
    """
    import random
    import datetime
    random.seed(datetime.datetime.now())
    random.shuffle(bookings)

    #create constructor inputs for showings from the booking inputs
    sl = []
    for e in bookings:
        if e['playing'] == 'split':
            for i2, e2 in enumerate(e['time_constraint']):
                cpd = copy.deepcopy(e)
                cpd['time_constraint'] = [e2]
                cpd['format'] = e['formats'][i2]
                if 'optional' in cpd:
                    del cpd['optional']
                del cpd['formats']
                sl.append(cpd)
        else:
            for e2 in e['formats']:
                cpd = copy.deepcopy(e)
                cpd['format'] = e2
                if 'optional' in cpd:
                    del cpd['optional']
                del cpd['formats']
                sl.append(cpd)

    tempauds = []
    maxid = max([x['id'] for x in auditoriums])
    tempid = 0
    audsplits = {}

    #create auditorium constructor inputs, split custom auds
    for e in auditoriums:
        tempauds.append(e)
        if e['hasCust'] != -1:
            cll0 = globs['aud_custom_leftovers']
            assert(e['id'] in cll0)
            cll = cll0[e['id']]

            for i2, e2 in enumerate(cll):
                nofirst = True
                nolast = True
                if i2 == 0:
                    nofirst = False
                if i2 == len(cll) - 1:
                    nolast = False

                clone = copy.deepcopy(e)
                clone['schedule'] = e2
                clone['id'] = 'temp{0}'.format(tempid)
                clone['hasCust'] = -1
                if nolast:
                    clone['nolast'] = True
                if nofirst:
                    clone['nofirst'] = True
                audsplits[clone['id']] = e['id']
                tempauds.append(clone)

                tempid += 1

    return tempauds, sl, audsplits

#check if a clean booking can be assigned to auditorium cur_ass
#     that means whether cur_ass is an auditorium not in prev_ass
def isPossibleAssigment(prev_ass, cur_ass, poss):
    blocks = set([])
    for k, v in prev_ass.items():
        blocks.add(v)

    if cur_ass[1] in blocks:
        return False

    updposs = poss - blocks
    if len(updposs) == 0:
        return False
   
    return True

#test preliminary clean booking auditorium assignment and resort:
#    higher demand movies should go to higher auditoriums
def improve_clean_bookings(cleans, auds, bkidmem, showdct):
    schwts = {}
    for k, v in bkidmem.items():
        audid = cleans[k[3]]
        capacity = auds[audid].capacity
        val = sum([showdct[x].demand for x in v['ids']])
        schwts[k[3]] = val

    kz = sorted(bkidmem.keys())

    while True:
        toswap = []
        for i in range(len(kz) - 1):
            for j in range(i + 1, len(kz)):
                bk1id = kz[i][3]
                bk2id = kz[j][3]
                aud1id = cleans[bk1id]
                aud2id = cleans[bk2id]

                assert(aud1id != aud2id)
                if aud1id not in bkidmem[kz[j]]['poss_auds']:
                    continue

                if aud2id not in bkidmem[kz[i]]['poss_auds']:
                    continue

                if schwts[bk1id] > schwts[bk2id]:
                    if auds[aud2id].capacity > auds[aud1id].capacity:
                        toswap.append((bk1id, bk2id))

                if schwts[bk1id] < schwts[bk2id]:
                    if auds[aud2id].capacity < auds[aud1id].capacity:
                        toswap.append((bk1id, bk2id))

                #it seems okay so far
        for e in toswap:
            x = cleans[e[1]]
            cleans[e[1]] = cleans[e[0]]
            cleans[e[0]] = x

        if len(toswap) == 0:
            break
            
    return cleans            

#this function will create the clean bookings assignments
#   "congregating" showings of the same clean bookings into the same auditorium
def congregateCleanShowings(showingsdct, auds, cleantime, adtime):
    bkidmem = {}
    showings = showingsdct.values()

    #use dictionary bkidmem for maintaining a set of auditoriums the booking
    #can go into. This would be a subset of auditoriums its individual
    #showings can be hosted in.
    for e in showings:
        if e.type != 'clean':
            continue

        #capacity, demand and bkid are constant over a clean booking
        key = (-len(e.aud_constraint['capacity']), e.demand, e.bkid)
        possauds = [x for x in e.possible_auditoriums if (type(x) == int) or
            ((type(x) == str) and (x.find('temp') == -1))]
        if key not in bkidmem:
            bkidmem[key] = {'audslist': [possauds], 
                'poss_auds': set(possauds),
                'total_length': e.length - cleantime - adtime,
                'ids': [e.id]}
        else:
            bkidmem[key]['audslist'].append(possauds)
            bkidmem[key]['poss_auds'] = bkidmem[key]['poss_auds'].intersection(
                    possauds)
            bkidmem[key]['total_length'] += e.length
            bkidmem[key]['ids'].append(e.id)

    bkidmem = {(len(v['poss_auds']), k[0], k[1], k[2]): v for k, v in bkidmem.items()}            
    srtr = sorted(bkidmem.keys())

    clean_bookings = {}
    used_auditoriums = set([])

    #first assign uniques
    bkidmem2 = copy.deepcopy(bkidmem)

    #srtr now has bookings with fewer options first. Assign the more 
    #"constrained" bookings first.
    for k in srtr:
        v = bkidmem[k]
        for e in v['audslist'][0]:
            if e in v['poss_auds']: #found auditorium
                if isPossibleAssigment(clean_bookings,(k[3],e), v['poss_auds']):
                    if auds[e].available_time >= v['total_length']:
                        clean_bookings[k[3]] = e
                        used_auditoriums.add(e)
                        for k2, v2 in bkidmem.items():
                            if e in v2['poss_auds']:
                                v2['poss_auds'].remove(e)
                        del bkidmem[k]
                        break

    assert(len(clean_bookings.keys()) == len(srtr))
    assert(len(clean_bookings.keys()) == len(used_auditoriums))

    #we now have an assignment, but we're not sure every booking has the
    #auditorium most appropriate for its demand. improve it.
    clean_bookings = improve_clean_bookings(
        clean_bookings, auds, bkidmem2, showingsdct)

    return clean_bookings, used_auditoriums

def freezeAuditoriumAssignments(self, used_auds):
    for k, v in self.showings.items():
        v.restrictAuditoriums(used_auds)

    for e in used_auds:
        self.auds[e].can_addremove = False
        self.auds[e].setAuditoriumType('clean')


def assignCleanBookings(self, cleans, used_auds):
    prehandled_showings = set([])
    for k, v in self.showings.items():
        if v.bkid in cleans:
            audid = cleans[v.bkid]
            v.forceAssignAuditorium(self.auds[audid])
            v.fix_to_auditorium = True
            self.auds[audid].assignShowing(v)

            prehandled_showings.add(k)

    return prehandled_showings


def distributeSplitShowings(self, splits, auds):
    for e in sorted(splits):
        av_auds = list(reversed(sorted([(v.available_time, k) for k, v in 
            auds.items() if v.available_time > 0])))

        found = False
        for e2 in av_auds:
            aud = auds[e2[1]]
            if aud.canHostShowing(e[2]):
                if not aud.hasTimingConflict(e[2], self.min_dist_splits):
                    found = True
                    aud.assignShowing(e[2])
                    e[2].forceAssignAuditorium(aud)
                    break

        if not found:
            for e2 in av_auds:
                aud = auds[e2[1]]
                if aud.canHostShowing(e[2]):
                    found = True
                    aud.assignShowing(e[2])
                    e[2].forceAssignAuditorium(aud)
                    break
        assert(found)                    


def comb_order_showings(aud):

    dct = {}
    for k in sorted(aud.showings.keys()):
        v = aud.showings[k]
        if v.title not in dct:
            dct[v.title] = []
        dct[v.title].append(k)

    assert(len(dct.keys()) in [1, 2]) #will not allow too much input diversity

    sorter = [(len(v), aud.showings[v[0]].length, k) for k, v in dct.items()]
    sorter = list(reversed(sorted(sorter)))
    result = []

    while True:
        done_something = False
        for e in sorter:
            if e[2] in dct:
                done_something = True
                sid = dct[e[2]].pop(0)
                result.append(aud.showings[sid])
                if len(dct[e[2]]) == 0:
                    del dct[e[2]]

        if not done_something:
            break
                                
    return result

def assign_combed(cmb_titles, allshows, forcees, aud, orig):
    remain = []
    while True:
        if len(cmb_titles) == 0:
            break
        e = cmb_titles[0]
        ok = False
        if e in forcees:
            aid = forcees[e].pop(0)
            if len(forcees[e]) == 0:
                del forcees[e]
        else:
            aid = min(allshows[e])

        sh = allshows[e][aid]

        assert(aud.canHostShowing(sh))
        if (aud.available_time >= sh.length) and (aud.load + sh.length <= orig):
            aud.assignShowing(sh)
            sh.forceAssignAuditorium(aud)
            sh.fix_to_auditorium = True
            ok = True

        if not ok:
            break

        cmb_titles.pop(0)
        del allshows[e][aid]
        if len(allshows[e]) == 0:
            del allshows[e]
    return cmb_titles

def findCombableAuditoriumPair(self, cleanaudids, combtally, maxcombs,
    blocked_pairs):
    const = 0.7
    srtr = sorted([(self.auds[x].available_time, self.auds[x]) for x in 
        cleanaudids])

    filt = [x[1] for x in srtr if combtally[x[1].ext_id] < maxcombs]
    if len(filt) > 5:
        midpt = len(filt) // 2
    l1 = filt[:midpt]
    l2 = filt[midpt:]
    random.shuffle(l1)
    random.shuffle(l2)
    filt = l1 + l2

    found = False
    for i in range(len(filt)):

        for j in range(len(filt) - 1, i, -1):
            if j == i:
                break
            if (filt[i].ext_id, filt[j].ext_id) in blocked_pairs:
                continue

            ld12 = sum([x.length for x in filt[i].showings.values() if
                filt[j].canHostShowing(x, ignore_aud_fixes=True)])

            if ld12 < const * filt[i].load:
                continue

            ld21 = sum([x.length for x in filt[j].showings.values() if
                filt[i].canHostShowing(x, ignore_aud_fixes=True)])
            if ld21 < const * filt[j].load:
                continue

            found = True
            break

        if found:
            break

    if found:
        return (True, filt[i], filt[j])

    return (False, None, None)


def is_combed_array_OK(combed, aud1, aud2, allshows):
    loadmax = max(aud1.load, aud2.load)
    ld1 = 0
    ld2 = 0
    cmb2 = combed[:]

    while True:
        e = cmb2[0]
        sh = allshows[e][min(allshows[e])]
        if ld1 + sh.length > loadmax:
            break
        ld1 += sh.length
        cmb2.pop(0)
        if len(cmb2) == 0:
            break

    while True:
        e = cmb2[0]
        sh = allshows[e][min(allshows[e])]
        if ld2 + sh.length > loadmax:
            break
        ld2 += sh.length
        cmb2.pop(0)
        if len(cmb2) == 0:
            break

    if len(cmb2) > 0:
        return False
    return True

def combAuditoriums(self, cleans, ncnt):
    #get clean auds
    cleanaudids = cleans.values()
    if len(cleanaudids) < 2:
        return False

#    ncnt = 2
    loopctr = 0
    maxcombs = 1
    combtally = {k: 0 for k in cleanaudids}
    blocked_pairs = set([])

    while True:
        if loopctr >= ncnt:
            break

        #1 sort by available time
#        srtr = sorted([(self.auds[x].available_time, self.auds[x]) for x in 
#            cleanaudids])

        res, aud1, aud2 = self.findCombableAuditoriumPair(cleanaudids,
            combtally, maxcombs, blocked_pairs)

        if not res:
            break
        assert(aud1.ext_id != aud2.ext_id)

        co1 = comb_order_showings(aud1)
        co2 = comb_order_showings(aud2)
        moves = {}
        
        ht = [co1, co2]
        if len(co2) > len(co1):
            ht = [co2, co1]

        #create comb list
        combed = []
        allshows = {}
        i = 0
        while True:
            if len(ht[0]) + len(ht[1]) == 0:
                break
            ind = i % 2
            i += 1
            if len(ht[ind]) == 0:
                continue
            e = ht[ind].pop(0)
            combed.append(e.title)
            if e.title not in allshows:
                allshows[e.title] = {}
            allshows[e.title][e.id] = e

        #combed check
        if not is_combed_array_OK(combed, aud1, aud2, allshows):
            blocked_pairs.add((aud1.ext_id, aud2.ext_id))
            continue

        combtally[aud1.ext_id] += 1
        combtally[aud2.ext_id] += 1
        requires_aud1 = {}
        requires_aud2 = {}
        orig_loads1 = 0
        orig_loads2 = 0

        for k, v in aud1.showings.items():
            v.fix_to_auditorium = False
            orig_loads1 += v.length
            if not aud2.canHostShowing(v):
                if v.title not in requires_aud1:
                    requires_aud1[v.title] = []
                requires_aud1[v.title].append(k)

            aud1.unassignShowing(v)
            v.unassignAuditorium()
        for k, v in aud2.showings.items():
            v.fix_to_auditorium = False
            orig_loads2 += v.length
            if not aud1.canHostShowing(v):
                if v.title not in requires_aud2:
                    requires_aud2[v.title] = []
                requires_aud2[v.title].append(k)

            aud2.unassignShowing(v)
            v.unassignAuditorium()

        remain = assign_combed(combed, allshows, requires_aud1, aud1, 
            max(orig_loads1, orig_loads2))
        remain = assign_combed(remain, allshows, requires_aud2, aud2, 
            max(orig_loads1, orig_loads2))

        assert(len(remain) == 0)

        loopctr += 1

    return True


def mergeAuditoriums(self, csr):
    psd = {}
    for x in self.pre_solution:
        if x[0].ext_id not in self.auds:
            self.auds[x[0].ext_id] = x[0]
        if x[0].ext_id not in psd:
            psd[x[0].ext_id] = []
        psd[x[0].ext_id].append((x[1].assigned_time, x[1]))

    for k, v in psd.items():
        psd[k] = sorted(v)

    for k, v in self.audmappings.items():
        aud2kill = self.auds[k]
        aud2revert2 = self.auds[v]
        aud2revert2.slurpScheduledAuditorium(aud2kill, csr)

    for k, v in psd.items():
        for e in v:
            t = e[0]
            aud = self.auds[k]
        
            e[1].forceAssignAuditorium(aud)
            aud.assignShowing(e[1])
            aud.scheduleShowing(e[1].id, t, csr)

    for k in self.audmappings.keys():
        del self.auds[k]
        csr.drop_auditorium(k)

    self.auds_by_prio = [x for x in self.auds_by_prio if x in self.auds]

"""
def improveAudAssignments(self):
    kl = sorted(self.auds.keys())
    for i in range(len(kl) - 1):
        for j in range(i + 1, len(kl)):
            print "CS", kl[i], kl[j], self.auds[kl[i]].canSwapAuditorium(self.auds[kl[j]])
"""


def generate_single_random_bookings0(globs, auds, rect_movies, rect_bookings, 
    itnum):
    #1 find movies with >= 1 clean booking
    possmovs = {}
    movset = set([x['id'] for x in rect_movies])
    for e in rect_bookings:
        if e['playing'] == 'clean':
            if e['movie'] in movset and e['movie'] not in possmovs:
                possmovs[e['movie']] = e
    sorter = [(v['demand'], k) for k, v in possmovs.items()]
    dist = [x[0] for x in sorter]
    minlen = min([x['length'] for x in possmovs.values()])

    audcnt = 0
    audcnt = sum([1 for x in auds if x.available_time >= minlen])
    movs = [roll_from_distribution(dist) for _ in range(audcnt)]
    std = list(reversed(sorted([sorter[x] for x in movs])))
    result = []
    newbks = []

    for i, e in enumerate(std):
        movid = e[1]
        bk = copy.copy(possmovs[movid])
        bk['id'] = 'tempbk_{0}_{1}'.format(itnum, i)
        bk['time_constraint'] = [(globs['schedule'][0], globs['schedule'][1])]
        bk['format'] = '2D'.lower()
        bk['playing'] = 'split'
        del bk['formats']
        bk['auditorium_constraint'] = {'scheme': '', 'capacity': []}
        bk['optional'] = True
        newbks.append(bk)

    result = [Showing(x) for x in newbks]
    return result


def generate_single_random_bookings(globs, auds, rect_movies, rect_bookings, 
    itnum):
    #1 find movies with >= 1 clean booking
    possmovs = {}
    movset = set([x['id'] for x in rect_movies])
    for e in rect_bookings:
        mid = e['movie']
        if mid not in movset: #just for safety
            continue

        if e['playing'] != 'clean':
            continue

        sch = e['auditorium_constraint']['scheme']

        if sch not in possmovs:
            possmovs[sch] = {}
        if mid not in possmovs[sch]:
                possmovs[sch][mid] = e

    mem = {}
    for k, v in possmovs.items():
        mem[k] = {'distbase': [(v2['demand'], k2) for k2, v2 in v.items()],
            'minlen': min([x['length'] for x in v.values()])}
        mem[k]['dist'] = [x[0] for x in mem[k]['distbase']]

    newbks = []
    targauds = []
    mctr = 0
    audsorter = sorted([(x.available_time, x) for x in auds])
    for e0 in audsorter:
        e = e0[1]
        sch = e.scheme
        if sch not in mem:
            continue
        if e.available_time < mem[sch]['minlen']:
            continue
        #can roll a movie here
        while True:
            movindex = roll_from_distribution(mem[sch]['dist'])
            movid = mem[sch]['distbase'][movindex][1]
            mov = possmovs[sch][movid]
            if mov['length'] <= e.available_time:
                break

        bk = copy.copy(mov)
        bk['id'] = 'tempbk_{0}_{1}'.format(itnum, mctr)
        bk['time_constraint'] = [(globs['schedule'][0], globs['schedule'][1])]
        bk['format'] = '2D'.lower()
        bk['playing'] = 'split'
        del bk['formats']
        bk['auditorium_constraint'] = {'scheme': sch, 'capacity': []}
        bk['optional'] = True
        newbks.append(bk)
        targauds.append(e.ext_id)
        mctr += 1

    result = [Showing(x) for x in newbks]
    return result, targauds
