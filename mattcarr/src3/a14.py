algo_input = {
    "globs": {
        "schedule": ["10:00", "03:05"],
        "cleaning": 10,
        "trailer": 20,
        "adtime": 25,
        "min_shows": 4,
        "film_length_rounding": 5,
        "min_dist_splits": 120, #for constrained-time showing conflicts
        "earliest_closing_time": 0,

        "max_concurrent_starts": 2,
        "showing_increments": 5,

        "clean_spacing": 0.8,
        "split_spacing": 0.15,

        "timedefs": {"early": [540, 720], "matinee": [720, 900], 
            "afternoon": [900, 1080], "evening": [1080, 1275], 
            "late": [1275, 1560]}
    },


    "auditoriums": [
        {
            "id": 1,
            "capacity": 125, 
            "scheme": "standard",
            "parameters": {"formats": ["2D"]},
            "size_label": "small"
        },

        {
            "id": 2,
            "capacity": 290, 
            "scheme": "standard",
            "parameters": {"formats": ["2D"]},
            "size_label": "large"
        },

        {
            "id": 3,
            "capacity": 100, 
            "scheme": "standard",
            "parameters": {"formats": ["2D"]},
            "size_label": "small"
        },
        {
            "id": 4,
            "capacity": 260, 
            "scheme": "standard",
            "parameters": {"formats": ["2D"]},
            "size_label": "large"
        },
        {
            "id": 5,
            "capacity": 250, 
            "scheme": "standard",
            "parameters": {"formats": ["2D", "3D"]},
            "size_label": "large"
        },
        {
            "id": 6,
            "capacity": 180, 
            "scheme": "standard",
            "parameters": {"formats": ["2D", "3D"]},
            "size_label": "medium"
        },
        {
            "id": 7,
            "capacity": 90, 
            "scheme": "standard",
            "parameters": {"formats": ["2D", "3D"]},
            "size_label": "small"
        },
        {
            "id": 8,
            "capacity": 189, 
            "scheme": "standard",
            "parameters": {"formats": ["2D", "3D"]},
            "size_label": "medium"
        },
        {
            "id": 9,
            "capacity": 125, 
            "scheme": "standard",
            "parameters": {"formats": ["2D"]},
            "size_label": "small"
        },
        {
            "id": 10,
            "capacity": 125, 
            "scheme": "standard",
            "parameters": {"formats": ["2D"]},
            "size_label": "small"
        },
        {
            "id": 11,
            "capacity": 125,
            "scheme": "standard",
            "parameters": {"formats": ["2D"]},
            "size_label": "small"
        },
        {
            "id": 12,
            "capacity": 125, 
            "scheme": "standard",
            "parameters": {"formats": ["2D"]},
            "size_label": "small"
        },
        {
            "id": 13,
            "capacity": 297, 
            "scheme": "standard",
            "parameters": {"formats": ["2D", "3D"]},
            "size_label": "large"
        },
        {
            "id": 14,
            "capacity": 250, 
            "scheme": "standard",
            "parameters": {"formats": ["2D"]},
            "size_label": "large"
        }

    ],


    "movies": [
        {
            "id": "movie_captain_marvel",
            "demand": 5,
            "raw_length": 124,
            "title": "Captain Marvel",
        },

        {
            "id": "movie_alita",
            "demand": 5,
            "raw_length": 122,
            "title": "Alita"
        },
        {
            "id": "movie_train_dragon",
            "demand": 5,
            "raw_length": 104,
            "title": "Train Dragon"
        },

        {
            "id": "movie_five_feet_apart",
            "demand": 5,
            "raw_length": 116,
            "title": "Five Feet Apart"
        },

        {
            "id": "movie_madea_family",
            "demand": 5,
            "raw_length": 102,
            "title": "Madea Family"
        },

        {
            "id": "movie_us",
            "demand": 5,
            "raw_length": 116,
            "title": "Us"
        },
        {
            "id": "movie_wonder_park",
            "demand": 4,
            "raw_length": 85,
            "title": "Wonder Park"
        },
        {
            "id": "movie_dumbo",
            "demand": 5,
            "raw_length": 112,
            "title": "Dumbo"
        },
        {
            "id": "movie_gloria_bell",
            "demand": 5,
            "raw_length": 102,
            "title": "Gloria Bell"
        },
        {
            "id": "movie_run_the_race",
            "demand": 5,
            "raw_length": 101,
            "title": "Run the Race"
        },
        {
            "id": "movie_unplan",
            "demand": 5,
            "raw_length": 110,
            "title": "Unplan"
        }

    ],

    "bookings": [
        {
            "id": "bk11",
            "playing": "clean",
            "time_constraint": [],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["2D", "2D", "2D", "2D"],
            "movie": "movie_captain_marvel"
        },

        {
            "id": "bk12",
            "playing": "clean",
            "time_constraint": [],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["2D", "2D", "2D", "2D"],
            "movie": "movie_captain_marvel"
        },

        {
            "id": "bk13",
            "playing": "split",
            "time_constraint": ["afternoon", "late"],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["3D", "3D"],
            "movie": "movie_captain_marvel"
        },
        {
            "id": "bk26",
            "playing": "split",
            "time_constraint": ["late"],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["2D"],
            "movie": "movie_alita"
        },
        {
            "id": "bk14",
            "playing": "split",
            "time_constraint": ["matinee", "afternoon", "evening"],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["2D", "2D", "2D"],
            "movie": "movie_train_dragon"
        },

        {
            "id": "bk16",
            "playing": "clean",
            "time_constraint": [],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["2D", "2D", "2D", "2D"],
            "movie": "movie_five_feet_apart"
        },

        {
            "id": "bk17",
            "playing": "clean",
            "time_constraint": [],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["2D", "2D", "2D", "2D"],
            "movie": "movie_madea_family"
        },

        {
            "id": "bk18",
            "playing": "clean",
            "time_constraint": [],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["2D", "2D", "2D", "2D"],
            "movie": "movie_us"
        },
        {
            "id": "bk27",
            "playing": "clean",
            "time_constraint": [],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["2D", "2D", "2D", "2D"],
            "movie": "movie_us"
        },

        {
            "id": "bk19",
            "playing": "clean",
            "time_constraint": [],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["2D", "2D", "2D", "2D"],
            "movie": "movie_wonder_park"
        },

        {
            "id": "bk20",
            "playing": "clean",
            "time_constraint": [],
            "auditorium_constraint": {"scheme": "standard", "capacity": ["large"]},
            "formats": ["2D", "2D", "2D", "2D"],
            "movie": "movie_dumbo"
        },

        {
            "id": "bk21",
            "playing": "clean",
            "time_constraint": [],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["2D", "2D", "2D", "2D"],
            "movie": "movie_dumbo"
        },
        {
            "id": "bk22",
            "playing": "clean",
            "time_constraint": [],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["3D", "3D", "3D", "3D"],
            "movie": "movie_dumbo"
        },
        {
            "id": "bk23",
            "playing": "clean",
            "time_constraint": [],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["2D", "2D", "2D", "2D","2D"],
            "movie": "movie_gloria_bell"
        },
        {
            "id": "bk24",
            "playing": "split",
            "time_constraint": ["matinee", "evening"],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["2D", "2D"],
            "movie": "movie_run_the_race"
        },

        {
            "id": "bk25",
            "playing": "clean",
            "time_constraint": [],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["2D", "2D", "2D", "2D"],
            "movie": "movie_unplan"
        }
    ]
}



#for e in auditoriums:
#    e["schedule"] = globs["schedule"]
