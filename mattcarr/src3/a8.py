algo_input = {
    "globs": {
        "schedule": ["10:00", "03:05"],
        "cleaning": 10,
        "trailer": 20,
        "adtime": 25,
        "min_shows": 4,
        "film_length_rounding": 5,
        "min_dist_splits": 120, #for constrained-time showing conflicts
        "earliest_closing_time": 0,

        "max_concurrent_starts": 2,
        "showing_increments": 5,

        "clean_spacing": 0.8,
        "split_spacing": 0.15,

        "timedefs": {"early": [540, 720], "matinee": [720, 900], 
            "afternoon": [900, 1080], "evening": [1080, 1275], 
            "late": [1275, 1560]}
    },


    "auditoriums": [
        {
            "id": 1,
            "capacity": 125, 
            "scheme": "standard",
            "parameters": {"formats": ["2D"]},
            "size_label": "small"
        },

        {
            "id": 2,
            "capacity": 290, 
            "scheme": "standard",
            "parameters": {"formats": ["2D"]},
            "size_label": "large"
        },

        {
            "id": 3,
            "capacity": 100, 
            "scheme": "standard",
            "parameters": {"formats": ["2D", "3D"]},
            "size_label": "small"
        },
        {
            "id": 4,
            "capacity": 260, 
            "scheme": "standard",
            "parameters": {"formats": ["2D", "3D"]},
            "size_label": "large"
        },
        {
            "id": 5,
            "capacity": 250, 
            "scheme": "standard",
            "parameters": {"formats": ["2D", "3D"]},
            "size_label": "large"
        },
        {
            "id": 6,
            "capacity": 180, 
            "scheme": "standard",
            "parameters": {"formats": ["2D", "3D"]},
            "size_label": "medium"
        },
        {
            "id": 7,
            "capacity": 90, 
            "scheme": "standard",
            "parameters": {"formats": ["2D", "3D"]},
            "size_label": "small"
        },
        {
            "id": 8,
            "capacity": 189, 
            "scheme": "standard",
            "parameters": {"formats": ["2D"]},
            "size_label": "medium"
        }

    ],


    "movies": [
        {
            "id": "movie_captain_marvel",
            "demand": 8,
            "raw_length": 124,
            "title": "Captain Marvel",
        },

        {
            "id": "movie_train_dragon",
            "demand": 5,
            "raw_length": 104,
            "title": "Train Dragon"
        },
        {
            "id": "movie_captive_state",
            "demand": 2,
            "raw_length": 109,
            "title": "Captive State"
        },


        {
            "id": "movie_five_feet_apart",
            "demand": 2,
            "raw_length": 116,
            "title": "Five Feet Apart"
        },

        {
            "id": "movie_madea_family",
            "demand": 2,
            "raw_length": 102,
            "title": "Madea Family"
        },

        {
            "id": "movie_us",
            "demand": 5,
            "raw_length": 116,
            "title": "Us"
        },
        {
            "id": "movie_wonder_park",
            "demand": 4,
            "raw_length": 85,
            "title": "Wonder Park"
        }

    ],

    "bookings": [
        {
            "id": "bk11",
            "playing": "clean",
            "time_constraint": [],
            "auditorium_constraint": {"scheme": "standard", "capacity": 
                ["large"]},
            "formats": ["2D", "2D", "2D", "2D"],
            "movie": "movie_captain_marvel"
        },

        {
            "id": "bk12",
            "playing": "clean",
            "time_constraint": [],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["2D", "2D", "2D", "2D"],
            "movie": "movie_captain_marvel"
        },

        {
            "id": "bk13",
            "playing": "split",
            "time_constraint": ["evening"],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["2D"],
            "movie": "movie_captain_marvel"
        },
        {
            "id": "bk14",
            "playing": "split",
            "time_constraint": ["matinee", "afternoon", "late"],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["2D", "2D", "2D"],
            "movie": "movie_train_dragon"
        },
        {
            "id": "bk15",
            "playing": "clean",
            "time_constraint": [],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["2D", "2D", "2D", "2D"],
            "movie": "movie_captive_state"
        },

        {
            "id": "bk16",
            "playing": "clean",
            "time_constraint": [],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["2D", "2D", "2D", "2D"],
            "movie": "movie_five_feet_apart"
        },

        {
            "id": "bk17",
            "playing": "clean",
            "time_constraint": [],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["2D", "2D", "2D", "2D"],
            "movie": "movie_madea_family"
        },

        {
            "id": "bk18",
            "playing": "clean",
            "time_constraint": [],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["2D", "2D", "2D", "2D"],
            "movie": "movie_us"
        },

        {
            "id": "bk19",
            "playing": "clean",
            "time_constraint": [],
            "auditorium_constraint": {"scheme": "standard", "capacity": []},
            "formats": ["2D", "2D", "2D", "2D"],
            "movie": "movie_wonder_park"
        }

    ]
}

