from utilities.utils import *

def ms_timefactor(time):
    return 1
    if time < 900:
        return 1.2
    elif time < 1300:
        return 1.2 - 0.05 * ((time - 600) // 60)
    else:
        return 1.2

class Constrainter:
    def __init__(self, initdict):
        self.max_concurrent = initdict['max_concurrent']
        self.showing_increments = initdict['showing_increments']
        self.clean_min_spreads = initdict['clean_min_spreads']
        self.split_min_spreads = initdict['split_min_spreads']
        self.playtimes = {}
        self.basic_clean_intervals = {} 
        self.basic_split_intervals = {} 

        self.merged_clean_intervals = {}
        self.merged_split_intervals = {}
        self.showings = {}
        self.aud_occupancies = {}
        self.total_perf_count = initdict['total_perf_count']
        self.perf_count = {}
        for k in self.total_perf_count.keys():
            self.perf_count[k] = 0
        self.clean_time = initdict['clean_time']
        self.ad_time = initdict['ad_time']

    #return the next valid showtime
    def next_showtime(self, showing, aud, time):
        #find valid times and then check concurrency
        t = time
        ln = showing.length

        while True:
            rr = self.can_add_showing(showing, ln, aud, t)
            msg(16,"\tfor time {0} rr was {1}".format(t, rr))
            if not rr['status']:
                reason = rr['reason']
                if reason in [1,3,4,7,9]:
                    return -1
                elif reason == 2:
                    t += self.showing_increments
                    continue
                elif reason == 5 or reason == 6 or reason == 8:
#                    if showing.title in self.merged_clean_intervals:
#                        print self.merged_clean_intervals[showing.title]
#                        print self.basic_clean_intervals[showing.title]
                    dv = rr['retry']
                    if dv < 0:
                        return -1
                    if dv % self.showing_increments != 0:
                        dv = self.showing_increments * \
                        (1 + dv // self.showing_increments)
                    t = dv
                    continue
                else:
                    assert(0)

            return t

    def drop_auditorium(self, audid):
        lst = [self.basic_clean_intervals, self.basic_split_intervals]
#            self.merged_clean_intervals, self.merged_split_intervals]
        
        for e in lst:
            for k, v in e.items():
                e[k] = [x for x in v if x[2] != audid]

        if audid in self.aud_occupancies:
            del self.aud_occupancies[audid]

        #FIXME recalculate merged intervals for full consistency


    def add_intervals(self, aud, sh, lngt, time):
#        lngt = sh.length #FIXME adjust for first / last performance

        title = sh.title
#        interval = (time, 
#            time + 

        d = int(ms_timefactor(time)*self.clean_min_spreads[title])
        d2 = int(ms_timefactor(time)*self.split_min_spreads[title])
#        d2 = int(ms_timefactor(time + self.max_spreads[title]) * self.max_spreads[title])
        clean_interval = (time - d, time + d)
        split_interval = (time - d2, time + d2)
        assert(clean_interval[1] > clean_interval[0])
        assert(split_interval[1] > split_interval[0])

        if title not in self.basic_clean_intervals:
            self.basic_clean_intervals[title] = []
        if title not in self.basic_split_intervals:
            self.basic_split_intervals[title] = []

        self.basic_clean_intervals[title].append((clean_interval[0], 
            clean_interval[1], aud.ext_id))
        self.basic_split_intervals[title].append((split_interval[0], 
            split_interval[1], aud.ext_id))

        self.merged_clean_intervals[title] = merge_intervals(
            self.basic_clean_intervals[title])
        self.merged_split_intervals[title] = merge_intervals(
            self.basic_split_intervals[title])

        if aud.ext_id not in self.aud_occupancies:
            self.aud_occupancies[aud.ext_id] = []
        self.aud_occupancies[aud.ext_id].append((time, time + lngt))

        if title not in self.perf_count:
            self.perf_count[title] = 0
        self.perf_count[title] += 1

        msg(32, "\t\tADDINTERVALS: {0}, {1}".format(clean_interval, 
            split_interval))


    def can_add_showing(self, showing, ln, aud, time):
        #1 showing known?
        if showing.id in self.showings:
            return {'status': False, 'reason': 1}

        #2 concurrency constraint
        if time in self.playtimes:
            if self.playtimes[time] >= self.max_concurrent:
                return {'status': False, 'reason': 2}
        
        #3 can be hosted in aud
        if not aud.canHostShowing(showing, ignore_freezes = True):
            return {'status': False, 'reason': 3}

        #4 is within the open hours constraint
        if time < aud.open_at or time + ln - self.clean_time > aud.close_at:
#            print time, aud.open_at, aud.close_at, ln, time < aud.open_at, time + ln > aud.close_at
            return {'status': False, 'reason': 4}

        #5 violates spread
#        if aud.auditorium_type == 'clean':
        if showing.type == 'clean':
            mergedi = self.merged_clean_intervals
        else:
            mergedi = self.merged_split_intervals

        if showing.title in mergedi:
            min_error = 9999999
            diff = 0

            good = True
            for e in mergedi[showing.title]:
                start = time

                #FIXME below should go outside loop
                if aud.auditorium_type == 'clean':
                    mspr = self.clean_min_spreads[showing.title]
                else:
                    mspr = self.split_min_spreads[showing.title]

                end = time + int(ms_timefactor(time) * mspr)

             
                if (e[0] <= time < e[1]):
#                if intervals_intersect((start, end), e):
                    good = False
                    diff = e[1] - time
                    if diff > 0:
                        if diff < min_error:
                            min_error = diff
#                    print "AAA", diff, time, e
            if not good:
                if min_error > 100000:
                    min_error = -1
                return {'status': False, 'reason': 5, 'retry': time + min_error}

        #6 auditorium occupied: intersects with any interval
        if aud.ext_id in self.aud_occupancies:
            end = time + ln
#            print "XX", aud.ext_id, time, ln, self.aud_occupancies[aud.ext_id], end

            for e in self.aud_occupancies[aud.ext_id]:
#                print "\t", intervals_intersect((time,end), e)
#                if (e[0] <= time < e[1]) or \
#                    (e[0] < end < e[1]) or \
#                    ((time <= e[0]) and (end > e[1])):
                if intervals_intersect((time, end), e):
                        return {'status': False, 'reason': 6, 
                            'retry': e[1]}
        #7 showing's own time constraint
        if len(showing.time_constraint) > 0:
            if time >= showing.time_constraint[0][1]:
                return {'status': False, 'reason': 7} 

            elif time < showing.time_constraint[0][0]:
                return {'status': False, 'reason': 8, 
                    'retry': showing.time_constraint[0][0]}

        return {'status': True}

    def add_showing(self, showing, aud, time, force = False):
        ln = showing.length

        if not force:
            rr = self.can_add_showing(showing, ln, aud, time)
            if not rr['status']:
                return rr

        #now add
        self.add_intervals(aud, showing, ln, time)
        
#        .title, (time,
#            time + self.min_spreads[showing.title]))
        msg(32, "ADDING showing {0} at time {1}".format(showing.id, time))

        if time not in self.playtimes:
            self.playtimes[time] = 0
        self.playtimes[time] += 1

        return {'status': True}

    def fix_auditorium(self, aud):
        pass

    def measure_stress(self, aud):
        if len(aud.showings) == 0:
            return {}

        stress = {}
        for e in aud.showings.values():
            #time constraints
            #FIXME first and last false for now, but may need to fix this
            st = self.next_showtime(e, aud, aud.open_at)
            if st == -1:
                stress[e.id] = 1
                continue

            if st - aud.open_at > aud.available_time:
                stress[e.id] = 1
                continue
            if st - aud.open_at == aud.available_time:
                stress[e.id] = 0.5
                continue
            stress[e.id] = 0

        return stress

