globs = {
    'schedule': ('10:00', '03:05'),
    'cleaning': 10,
    'trailer': 20,
    'adtime': 25,
    'min_shows': 4,
    'film_length_rounding': 5,
    'min_dist_splits': 120, #for constrained-time showing conflicts

    'max_concurrent_starts': 2,
    'showing_increments': 5,

    'timedefs': {'early': (540, 720), 'matinee': (720, 900), 
        'afternoon': (900, 1080), 'evening': (1080, 1275), 
        'late': (1275, 1560), 'anytime': (540, 1560)}
}


auditoriums = [
    {
        'id': 1,
        'capacity': 250, 
        'scheme': 'standard',
        'parameters': {'formats': ['2D', '3D']},
    },

    {
        'id': 2,
        'capacity': 225, 
        'scheme': 'standard',
        'parameters': {'formats': ['2D']},
    },

    {
        'id': 3,
        'capacity': 160, 
        'scheme': 'standard',
        'parameters': {'formats': ['2D', '3D']},
    },
    {
        'id': 4,
        'capacity': 120, 
        'scheme': 'standard',
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 5,
        'capacity': 120, 
        'scheme': 'standard',
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 6,
        'capacity': 162, 
        'scheme': 'standard',
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 7,
        'capacity': 225, 
        'scheme': 'standard',
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 8,
        'capacity': 300, 
        'scheme': 'standard',
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 9,
        'capacity': 400, 
        'scheme': 'RPX',
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 10,
        'capacity': 350, 
        'scheme': 'IMAX',
        'parameters': {'formats': ['2D', '3D']},
    },
    {
        'id': 11,
        'capacity': 285,
        'scheme': 'standard',
        'parameters': {'formats': ['2D', '3D']},
    },
    {
        'id': 12,
        'capacity': 225, 
        'scheme': 'standard',
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 13,
        'capacity': 154, 
        'scheme': 'standard',
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 14,
        'capacity': 120, 
        'scheme': 'standard',
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 15,
        'capacity': 120, 
        'scheme': 'standard',
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 16,
        'capacity': 154, 
        'scheme': 'standard',
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 17,
        'capacity': 225, 
        'scheme': 'SCREENX',
        'parameters': {'formats': ['2D']},
    },
    {
        'id': 18,
        'capacity': 295, 
        'scheme': 'standard',
        'parameters': {'formats': ['2D']},
    }


]


movies = [
    {
        'id': 'unique_1',
        'title': 'Bumble Bee',
        'raw_length': 114,
        'demand': 5
    }
]

bookings = [
    {
        'id': 'bk01',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (1, 10000)},
        'formats': ['2D', '2D', '2D', '2D', '3D'],
        'demand': 5,
        'raw_length': 114,
        'title': 'Bumble Bee'
    },

    {
        'id': 'bk02',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': 'RPX', 'capacity': (0, 10000)},
        'formats': ['2D', '2D', '2D', '2D', '2D'],
        'demand': 5,
        'raw_length': 114,
        'title': 'Bumble Bee'
    },

    {
        'id': 'bk03',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (0, 10000)},
        'formats': ['2D', '2D', '2D', '2D', '2D'],
        'demand': 5,
        'raw_length': 132,
        'title': 'Vice'
    },

    {
        'id': 'bk04',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (1, 10000)},
        'formats': ['2D', '2D', '2D', '2D', '3D'],
        'demand': 2,
        'raw_length': 117,
        'title': 'SpidermanU'
    },

    {
        'id': 'bk05',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (280, 10000)},
        'formats': ['2D', '2D', '2D', '2D'],
        'demand': 8,
        'raw_length': 143,
        'title': 'Aquaman'
    },

    {
        'id': 'bk06',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': 'IMAX', 'capacity': (0, 10000)},
        'formats': ['2D', '2D', '2D', '3D'],
        'demand': 8,
        'raw_length': 143,
        'title': 'Aquaman'
    },

    {
        'id': 'bk07',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': 'SCREENX', 'capacity': (0, 10000)},
        'formats': ['2D', '2D', '2D', '2D'],
        'demand': 8,
        'raw_length': 143,
        'title': 'Aquaman'
    },

    {
        'id': 'bk08',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (1, 10000)},
        'formats': ['2D', '2D', '2D', '2D', '3D'],
        'demand': 8,
        'raw_length': 143,
        'title': 'Aquaman'
    },

    {
        'id': 'bk09',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (200, 10000)},
        'formats': ['2D', '2D', '2D', '2D', '2D'],
        'demand': 8,
        'raw_length': 130,
        'title': 'Mary Poppins'
    },

    {
        'id': 'bk10',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (200, 10000)},
        'formats': ['2D', '2D', '2D', '2D', '2D'],
        'demand': 8,
        'raw_length': 130,
        'title': 'Mary Poppins'
    },

    {
        'id': 'bk11',
        'playing': 'split',
        'time_constraint': ['matinee', 'evening'],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (0, 10000)},
        'formats': ['2D', '2D'],
        'demand': 8,
        'raw_length': 130,
        'title': 'Mary Poppins'
    },

    {
        'id': 'bk12',
        'playing': 'split',
        'time_constraint': ['matinee', 'evening'],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (0, 10000)},
        'formats': ['2D', '2D'],
        'demand': 2,
        'raw_length': 119,
        'title': 'Insta Family'
    },

    {
        'id': 'bk13',
        'playing': 'split',
#        'time_constraint': [(540, 720), (720, 900), (900, 1080)],
        'time_constraint': ['early', 'matinee', 'afternoon'],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (0, 10000)},
        'formats': ['2D', '2D', '2D'],
        'demand': 2,
        'raw_length': 112,
        'title': 'Ralph Breaks'
    },

    {
        'id': 'bk14',
        'playing': 'split',
#        'time_constraint': [(1080, 1275), (1275, 1455)],
        'time_constraint': ['evening', 'late'],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (0, 10000)},
        'formats': ['2D', '2D'],
        'demand': 2,
        'raw_length': 128,
        'title': 'Mortal Engines'
    },

    {
        'id': 'bk15',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (0, 10000)},
        'formats': ['2D', '2D', '2D', '2D', '2D'],
        'demand': 5,
        'raw_length': 116,
        'title': 'The Mule'
    },

    {
        'id': 'bk16',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (0, 10000)},
        'formats': ['2D', '2D', '2D', '2D', '2D'],
        'demand': 2,
        'raw_length': 90,
        'title': 'Holmes Watson'
    },

    {
        'id': 'bk17',
        'playing': 'split',
#        'time_constraint': [(540, 720), (900, 1080), (1275, 1455)],
        'time_constraint': ['early', 'afternoon', 'late'],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (0, 10000)},
        'formats': ['2D', '2D', '2D'],
        'demand': 2,
        'raw_length': 90,
        'title': 'Grinch'
    },

    {
        'id': 'bk18',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (0, 10000)},
        'formats': ['2D', '2D', '2D', '2D', '2D'],
        'demand': 2,
        'raw_length': 116,
        'title': 'Marwel'
    },

    {
        'id': 'bk19',
        'playing': 'split',
#        'time_constraint': [(900, 1080), (1275, 1455)],
        'time_constraint': ['afternoon', 'late'],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (0, 10000)},
        'formats': ['2D', '2D'],
        'demand': 2,
        'raw_length': 134,
        'title': 'Fant Beasts'
    },

    {
        'id': 'bk20',
        'playing': 'split',
#        'time_constraint': [(900, 1080), (1275, 1455)],
        'time_constraint': ['afternoon', 'late'],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (0, 10000)},
        'formats': ['2D', '2D'],
        'demand': 2,
        'raw_length': 130,
        'title': 'Creed 2'
    },

    {
        'id': 'bk21',
        'playing': 'split',
#        'time_constraint': [(540, 900), (1080, 1275)],
        'time_constraint': ['early'],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (0, 10000)},
        'formats': ['2D'],
        'demand': 2,
        'raw_length': 135,
        'title': 'Bohemian'
    },

    {
        'id': 'bk22',
        'playing': 'clean',
        'time_constraint': [],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (0, 10000)},
        'formats': ['2D', '2D', '2D', '2D', '2D'],
        'demand': 2,
        'raw_length': 104,
        'title': 'Second Act'
    },

    {
        'id': 'bk23',
        'playing': 'custom',
        'time_constraint': [(1110, 1111)],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (0, 10000),
            'id': 15},

        'formats': ['2D'],
        'demand': 2,
        'raw_length': 135,
        'title': 'MET Opera'
    },
]
"""
    {
        'id': 'bk24',
        'playing': 'custom',
        'time_constraint': [(720, 1001)],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (0, 10000),
            'id': 15},

        'formats': ['2D'],
        'demand': 2,
        'raw_length': 179,
        'title': 'MET Opera2'
    },

]
"""
"""
    {
        'id': 'bk24',
        'playing': 'split',
        'time_constraint': ['anytime'],
        'auditorium_constraint': {'scheme': 'standard', 'capacity': (0, 10000)},
        'formats': ['2D'],
        'demand': 8,
        'raw_length': 143,
        'title': 'Aquaman'
    },
    ]
"""




for e in auditoriums:
    e['schedule'] = globs['schedule']
