import itertools
import copy
from utilities.utils import *
from consts import *

def default_weightf(t):
    if t >= 540 and t < 720:
        return 1
    if t >= 720 and t < 900:
        return 2
    if t >= 900 and t < 1080:
        return 4
    if t >= 1080 and t < 1275:
        return 6
    if t >= 1275 and t < 1455:
        return 5
    if t >= 1455:
        return 3
    assert(0)

def iterlistlist(l, blocks, level=0):
    assert(len(l) > 0)
    if len(l) == 1:
        return [l[0]]

    result = []
    for car in l[0]:
        if car in blocks:
            continue

        bl = copy.deepcopy(blocks)
        bl.add(car)
#        print " " * 4 * level, car, " x ", l[1:]
        cdr = iterlistlist(l[1:], bl, level+1)
#        print " " * 4 * level, cdr
        
        for e in cdr:
            loc = [car]
            loc.extend(e)
            result.append(loc)
#        print " " * 4 * level, "R", result

        
    return result

def generate_title_list(titlefreqs, blueprint):
    tf2 = copy.deepcopy(titlefreqs)
    result = []
    while True:
        for e in blueprint:
            if e in tf2:
                result.append(e)
                tf2[e] -= 1
                if tf2[e] <= 0:
                    del tf2[e]
        if len(tf2) == 0:
            break
    return result



def generate_acc_sh_orders(titles):
    freqs = {}
    for e in titles:
        if e not in freqs:
            freqs[e] = 0
        freqs[e] += 1
    freqtally = {}
    for k, v in freqs.items():
        if v not in freqtally:
            freqtally[v] = []
        freqtally[v].append(k)
    schema = []
    for k in reversed(sorted(freqtally.keys())):
        for e in freqtally[k]:
            schema.append(freqtally[k])
    ll = iterlistlist(schema, set([]))
    result = set([])
    for e in ll:
        loc = generate_title_list(freqs, e)
        result.add(tuple(loc))
    return result

def generate_arrange_loop_expression(acceptable_shorders,
    shindices, showings):
    #1 gather by title
    bytitle = {}
    for k, v in showings.items():
        if v.title not in bytitle:
            bytitle[v.title] = []
        bytitle[v.title].append(k)

    #2 byid
    byid = {e: i for i, e in enumerate(shindices)}
    result = []
#    print bytitle, acceptable_shorders
    for e in acceptable_shorders:
        bt2 = copy.deepcopy(bytitle)
        loc = []
        for e2 in e:
            trid = bt2[e2].pop(0)
            loc.append(byid[trid])
        result.append(loc)
    return result

class Auditorium:
    aud_id_ctr = 1
    def __init__(self, aud, clean_time, ad_time):
        self.capacity = aud['capacity']
        self.size_label = aud['size_label']
        self.formats = aud['parameters']['formats']
        self.scheme = aud['scheme']
        self.ext_id = aud['id']
        self.id = Auditorium.aud_id_ctr
        self.has_custom_showing = (aud['hasCust'] != -1)

        if 'partial' in aud:
            self.is_partial = True
        else:
            self.is_partial = False

        Auditorium.aud_id_ctr += 1

        self.open_at = aud['schedule'][0]
        self.close_at= aud['schedule'][1]
        assert(self.close_at > self.open_at)
        self.available_time = self.close_at - self.open_at
        self.load = 0
        self.slots = [(self.open_at, self.close_at)]

        self.can_addremove = True
#        if self.has_custom_showing:
#            self.can_addremove = False

        self.showings = {}
        self.prio_showings = []
        self.schedule = {}
        self.clean_time = clean_time
        self.ad_time = ad_time
        self.nofirst = False
        if 'nofirst' in aud:
            if aud['nofirst']:
                self.nofirst = True
        self.nolast = False
        if 'nolast' in aud:
            if aud['nolast']:
                self.nolast = True

        self.is_sticky = False
        self.auditorium_type = 'split'

    def setAuditoriumType(self, s):
        self.auditorium_type = s

    def canHostShowing(self, showing, ignore_freezes = False, 
        ignore_aud_fixes = False):
        msg(AUD_HOSTING_MSGS, "\t\tcan host showing {0} in aud {1}?".format(showing.id,
            self.ext_id))
        #if already frozen, skip
        if not ignore_freezes:
            if not self.can_addremove:
                msg(AUD_HOSTING_MSGS, "\t\t\tNO - frozen aud")
                return False
    
        shcap = showing.aud_constraint['capacity']
   
        if not ignore_aud_fixes:
            if showing.fix_to_auditorium:
                if showing.assigned_aud.ext_id != self.ext_id:
                    msg(AUD_HOSTING_MSGS, "\t\t\tNO - aud fixed for showing")
                    return False


        #showing requires some other auditorium size
        if len(shcap) > 0:
            if self.size_label not in shcap:
                msg(AUD_HOSTING_MSGS, "\t\t\tNO - auditorium size violated")
                return False

        #showing requires different technology / scheme
        if len(showing.aud_constraint['scheme']) > 0:
            if showing.aud_constraint['scheme'] != self.scheme:
                msg(AUD_HOSTING_MSGS, "\t\t\tNO - scheme mismatch")
                return False

        #showing requires different format
        if showing.format not in self.formats:
            msg(AUD_HOSTING_MSGS, "\t\t\tNO - format mismatch")
            return False

        #showing requires a particular auditorium
        if 'id' in showing.aud_constraint:
            if showing.aud_constraint['id'] != self.ext_id:
                msg(AUD_HOSTING_MSGS, "\t\t\tNO - disallowed aud")
                return False

        if len(showing.time_constraint) > 0:
            if showing.time_constraint[0][1] < self.open_at:
                msg(AUD_HOSTING_MSGS, "\t\t\tNO - plays too early")
                return False
            if showing.time_constraint[0][0] + showing.length - \
                self.clean_time >= self.close_at:
                msg(AUD_HOSTING_MSGS, "\t\t\tNO - plays too late")
                return False
            #FIXME also need to take off adtime is first=last

        #otherwise it's good
        msg(AUD_HOSTING_MSGS, "\t\t\tYES")
        return True

    def hasTimingConflict(self, sh, diff):
        if len(sh.time_constraint) == 0:
            return False

        for e in self.showings.values():
            if len(e.time_constraint) > 0:
                if abs(e.time_constraint[0][0] - 
                    sh.time_constraint[0][0]) < diff:
                    return True
        return False

    def hasTitleClash(self, sh):
        for e in self.showings.values():
            if e.title == sh.title:
                return True
        return False

    def prioritizeShowings(self):
        sorter = [(-x.demand, x) for x in self.showings.values()]
        self.prio_showings = [x[1] for x in sorter]

    def getNFreezes(self):
        return sum([int(len(x.possible_auditoriums)==1) for x in self.showings.values()]), len(self.showings)

    def findReassignableShowings(self):
        return [x for x in self.showings.values() if 
            len(x.possible_auditoriums) > 1]

    def blockTime(self, begin, end):
        newints = []

        for e in self.slots:
            int1 = max(e[0], begin)
            int2 = min(e[1], end)
#            print "beg/end/e[0], e[1]/int1/int2", begin, end, e[0], e[1],int1, int2
            if int1 > int2:
                newints.append((e[0], e[1]))
                continue
            if e[0] < int1:
                newints.append((e[0], int1))
            if e[1] > int2:
                newints.append((int2, e[1]))
        self.slots = newints
        self.available_time = sum([x[1] - x[0] for x in self.slots])

    def assignShowing(self, showing):
        assert(showing.id not in self.showings)

        self.showings[showing.id] = showing
        self.available_time -= showing.length
        self.load += showing.length

    def next_showtime_in_aud(self, csr, showing):
        #need 3 things: fit into a valid interval, be valid for movie 
        #constraints, do not violate csr constraints
        ll = showing.length

        ct = self.slots[:]
        t = ct[0][0]

        while True:
            if len(ct) == 0:
#                print "EXIi1"
                return -1

            #find first valid interval
            if (t > ct[0][1]) or ((ct[0][1] - t) < ll):
                ct = ct[1:]
                if len(ct) == 0:
#                    print "EXIi2"
                    return -1

                t = ct[0][0]
                continue

            #else: does it meet film's constraints
            if len(showing.time_constraint) == 1:
                if t < showing.time_constraint[0][0]:
                    t = showing.time_constraint[0][0]
                    continue

                elif t >= showing.time_constraint[0][1]:

#                    print "EXIi3", t, showing.time_constraint
                    return -1

            #now system constraints
            t2 = csr.next_showtime(showing, self, t)
#FIXME the line above is called twice after it returns a valid result. should be accepted upon the first. probably the elif t2>t -> continue should be switched to "break"
            if t2 == -1:
                return -1
            elif t2 > t:
                t = t2
                continue
            elif t2 < t:
                assert(0) #just for safety FIXME

            #t == t2 case
            return t2


    def arrangeShowings(self, csr0, weightf = default_weightf):
        if len(self.showings) == 0:
            return True

        clean_aud = False
        acceptable_shorders = set([])
        seen_orders = set([])

        if self.auditorium_type == 'clean':
            acceptable_shorders = generate_acc_sh_orders([e.title for e in self.showings.values()])
            clean_aud = True
#            print acceptable_shorders

        shindices = self.showings.keys()
        rls = range(len(shindices))
        max_score = -10000
        best_loc = None
        best_times = []
        tot_time = sum([e[1] - e[0] for e in self.slots])
        verlen = sum([e.length for e in self.showings.values()]) - \
            self.clean_time - self.ad_time
        if verlen > tot_time:
            return False

#        assert(verlen <= tot_time)
        #FIXME how about chopped up schedules e.g. custom?
        if self.auditorium_type in ['split', 'custom']:
            evalexp = 'itertools.permutations(rls)'
        else: #clean
            genale = generate_arrange_loop_expression(acceptable_shorders,
                shindices, self.showings)
            evalexp = 'genale'


#        for perm in itertools.permutations(rls):
        for perm in eval(evalexp):
            msg(2, "STARTING WITH PERM: {0}".format(perm))
            ct = self.slots[:]

            score = -1
            times = []
            csr = copy.deepcopy(csr0)

            for index, i in enumerate(perm):
                sh = self.showings[shindices[i]]

                tm = self.next_showtime_in_aud(csr, sh)
                if tm == -1:
                    break
#                if ii == 0:
#                    if tm - self.open_at > self.available_time:
#                        print "IMPOSS!"
#                        break
                if any([tm <= x for x in times]):
                    msg(2, "NONLINEARITY")
                    break
#                print "\telt", i, tm
                times.append(tm)
                csr.add_showing(sh, self, tm, force = True)
            
            msg(2, "{0} - {1}".format(perm, times))
            if len(times) < len(rls):
                continue

            #now measure weight
#            if not clean_aud:
            if 0:
                score = 0
                for j in rls:
                    score += weightf(times[j]) * \
                        self.showings[shindices[perm[j]]].demand
            else:
#                score = -(times[-1] + self.showings[shindices[perm[-1]]].length - times[0])
                score = -(times[-1] + self.showings[shindices[perm[-1]]].length - self.open_at)


            if score > max_score:
                msg(2, "NEW MAX: {0} / {1}".format(score, perm))
                max_score = score
                best_loc = [shindices[x] for x in perm]
                best_times = times[:]

        if not best_loc:
            return False

        for i in range(len(best_loc)):
            self.scheduleShowing(best_loc[i], best_times[i], csr = csr0)

        return True

    def scheduleShowing(self, sh_index, t, csr):
        sh = self.showings[sh_index]
        msg(4, "\tasked to schedule for {0}, time {1}".format(sh, t))

        #verify it's valid for the intervals
        found_slot = False
        for e in self.slots:
            if e[0] <= t < e[1]:
                found_slot = True
                break
        assert(found_slot)

#        ln = actual_showing_length(sh, self.ad_time, self.clean_time)
        #FIXME remove actual_showing_length
        ln = sh.length

        self.available_time -= ln
        self.load += ln
        self.blockTime(t, t + ln)
        sh.assigned_time = t
        self.assigned_aud = self
#        csr.add_intervals(self, sh, t)
        csr.add_showing(sh, self, t, force=True)

    def unassignShowing(self, showing):
        del self.showings[showing.id]
        self.available_time += showing.length
        self.load -= showing.length

    def adjustOpenHours(self, optime, closetime):
        assert(closetime > optime)
        l1 = self.close_at - self.open_at
        l2 = closetime - optime
        self.available_time += l2 - l1
        self.open_at = optime
        self.close_at = closetime
        if self.slots:
            self.slots[0] = (optime, self.slots[0][1])
            self.slots[-1] = (self.slots[-1][0], closetime)

    def slurpScheduledAuditorium(self, aud2, csr):
#        print aud2.ext_id, aud2.showings.values()
        assert(all([x.assigned_time != -1 for x in aud2.showings.values()]))
        sh2s = [(x.assigned_time, x) for x in aud2.showings.values()]

        for i, e in enumerate(sh2s):
            t = e[1].assigned_time
            e[1].forceAssignAuditorium(self)
            self.assignShowing(e[1])
            self.scheduleShowing(e[1].id, t, csr)
            aud2.unassignShowing(e[1])

    def shrinkClosingHour(self, new_close):
        assert(new_close <= self.close_at)
        cldif = new_close - self.close_at
        self.available_time += cldif
        if new_close < self.close_at:
            self.blockTime(new_close, self.close_at)
        self.close_at = new_close

    def lastShowEnding(self, with_cleaning = False):
        last = -1
        for k, v in self.showings.items():
            if v.assigned_time > -1:
                rem = v.assigned_time + v.length
                if not with_cleaning:
                    rem -= self.clean_time
                if rem > last:
                    last = rem
        return last                    


    def getAssignmentWeight(self):
        result = self.capacity * sum([x.demand for x in self.showings.values()])
        if len(self.showings) > 0:
            result /= len(self.showings)
        return result

    def __str__(self):
        return "Aud {0} ({4}): shows: {1}, load: {2}, oi: {3}".format(self.ext_id,
            [x.id for x in self.showings.values()], self.load, self.slots,
            self.auditorium_type)




if __name__ == "__main__":
    from a18 import *
    from utilities.utils import *
    globs = algo_input['globs']
    auditoriums = algo_input['auditoriums']
    movies = algo_input['movies']
    bookings = algo_input['bookings']
    rectify_input(globs, auditoriums, movies, bookings)

    a1 = Auditorium(auditoriums[0], globs['cleaning'], globs['adtime'])
    a2 = Auditorium(auditoriums[1], globs['cleaning'], globs['adtime'])
    print a1, a1.available_time
    a1.shrinkClosingHour(a1.close_at - 100)
    print a1, a1.available_time
