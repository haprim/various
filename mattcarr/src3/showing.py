from auditorium import *

class Showing:
    showing_id_ctr = 1
    def __init__(self, initdct):
        self.type = initdct['playing']
        self.time_constraint = initdct['time_constraint']
        self.tctext = None

        if 'tctext' in initdct:
            self.tctext = initdct['tctext']

        self.aud_constraint = initdct['auditorium_constraint']
        self.aud_constraint['capacity'] = set(self.aud_constraint['capacity'])
        self.format = initdct['format']
        self.demand = initdct['demand']
        self.length = initdct['length']
        self.raw_length = initdct['raw_length']
        self.title = initdct['title']
        self.bkid = initdct['id']
        self.movid = initdct['movie']

        self.assigned_aud = None
        self.assigned_time = -1
        self.fix_to_auditorium = False
        self.id = Showing.showing_id_ctr
        Showing.showing_id_ctr += 1

        self.auditoriums = {}
        self.possible_auditoriums = []
        self.statevar1 = 0 #can use for greedy algos
        self.blocked_auditoriums = set([])
        self.optional_showing = False
        if 'optional' in initdct:
            if initdct['optional']:
                self.optional_showing = True
        self.custom = False
        if initdct['playing'] == 'custom':
            self.custom = True


    def calculatePossibleAuditoriums(self, auds):
        result = []

        for e in auds:
            if e.canHostShowing(self):
                self.auditoriums[e.ext_id] = e
                result.append((-e.capacity, e.ext_id))
        result = [x[1] for x in sorted(result)]
        self.possible_auditoriums = result
#        print "XXX", self, self.possible_auditoriums
        return result

    def restrictAuditoriums(self, restrict_set):
        if len(restrict_set) == 0:
            return

        #1 if assigned, require that the assigned aud is in the restrict list
        if self.assigned_aud != None:
            assert(self.assigned_aud.ext_id in restrict_set)
            self.possible_auditoriums = [self.assigned_aud.ext_id]
            return

        #2 not yet assigned
        newposs = []
        for e in self.possible_auditoriums:
            if e not in restrict_set:
                newposs.append(e)
        self.possible_auditoriums = newposs
        return

    def forceAssignAuditorium(self, aud):
        tasks = [('unlink', self.id, self.assigned_aud),
            ('link', self.id, aud.ext_id)]
        self.assigned_aud = aud

        return tasks
    
    def unassignAuditorium(self):
        self.assigned_aud = None
        self.assigned_time = -1

    def __str__(self):
        xid = ''
        if self.assigned_aud:
            xid = 'aud ' + str(self.assigned_aud.ext_id)
        return "Showing {0}, '{1}', fmt {2} {3}, {4} {5} {6}".format(self.id, 
            self.title, self.aud_constraint['scheme'],
            self.format, xid, self.assigned_time, self.length) + \
            str(self.possible_auditoriums) + " / " + str(self.time_constraint)

    def asDict(self, adtime):
        tm = "{0}:{1}".format(
            str(((self.assigned_time + adtime) // 60) % 24).zfill(2),
            str((self.assigned_time + adtime) % 60).zfill(2))
        result = {
            'aud_id': self.assigned_aud.ext_id,
            'film_id': self.movid,
            'film_title': self.title,
            'format': self.format.upper(),
            'time': tm,
            'optional': int(self.optional_showing),
            'scheme': self.aud_constraint['scheme'].upper(),
            'booking_id': self.bkid
        }
        return result


if __name__ == "__main__":
    from a18 import *
    from utilities.utils import *
    rectify_input(globs, auditoriums, bookings)

    for e in bookings:
        e['format'] = e['formats'][0]
    sg1 = Showing(bookings[0])
    sg2 = Showing(bookings[4])
    print sg1
    print sg2
    auds = []
    for e in auditoriums:
        auds.append(Auditorium(e))

    aa=sg2.calculatePossibleAuditoriums(auds)
#    if len(aa) == 1:
#        aa.forceAssignAuditorium
    print aa
