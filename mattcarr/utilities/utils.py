import random

verbosity=0

def set_verbosity(v):
    global verbosity
    verbosity = v

def msg(verb, s):
    global verbosity
    if verbosity & verb > 0:
        print s

def is_int(x):
    try:
        int(x)
    except:
        return False
    return True        

def vassert(stmt, msg = 'Err'):
    try:
        assert(stmt)
    except:
        raise Exception(msg)

def time_to_mins(tms):
    t = tms.split(':')
    vassert(len(t) == 2, 'Time {0} is wrong'.format(tms))

    hour = int(t[0])
    mins = int(t[1])

    return 60 * hour + mins

def actual_showing_length(sh, first, last, ad_time, clean_time):
    result = sh.length
    if first:
        result -= ad_time
    if last:
        result -= clean_time
    return result

def calc_minspread(bookings, maxtime, min_incr):
    result = {}
    
    mem = {}
    lmem = {}
    for e in bookings:
        if e['title'] not in mem:
            mem[e['title']] = 0
        mem[e['title']] += len(e['formats'])
        if e['title'] not in lmem:
            lmem[e['title']] = e['length']
    clean_factors = {1: 1, 2: 0.6, 3: 0.55, 4: 0.5, 5: 0.5, 6: 0.4,
        7: 0.35, -1: 0.7, 9: 0.5, 17:0.7, 10: 0.7, 12: 0.7}
    clean_factors = {-1: 0.7}
#    clean_factors = {-1: 0.2}
#    factors= {-1:0.8}
#    factors = {-1: 0.2, 12: 0.2, 17: 0.8}
    split_factor = 0.15


    for k, v in mem.items():
        if v in clean_factors:
            key = v
        else:
            key = -1

        if v == 1:
            val1 = 2 * min_incr
            val2 = 2 * min_incr
        else:
            val1 = int(round(clean_factors[key] * (maxtime - lmem[k]) // (v)))
            val2 = int(round(split_factor * (maxtime - lmem[k]) // (v)))
        result[k] = (val1, val2)
    
    for e in bookings:
        e['clean_min_spread'] = result[e['title']][0]
        e['split_min_spread'] = result[e['title']][1]
#    print "MIN_SPREADS", result


def calc_minspread2(bookings, globs):
    bksum = {}
    camaxlen = -1
    for e in bookings:
        if e['playing'] != 'clean':
            continue
            
        totlen = e['length'] * len(e['formats'])            

        if totlen > camaxlen:
            camaxlen = totlen

        if e['title'] not in bksum:
            bksum[e['title']] = (0, 0, e['length'])

        bksum[e['title']] = (bksum[e['title']][0] + len(e['formats']),
            bksum[e['title']][1] + 1, bksum[e['title']][2])

            
    result = {}
    
    splitmem = {}
    splitlmem = {}
    for e in bookings:
        if e['playing'] not in  ['split', 'custom']:
            continue
        if e['title'] not in splitmem:
            splitmem[e['title']] = 0
        splitmem[e['title']] += len(e['formats'])
        if e['title'] not in splitlmem:
            splitlmem[e['title']] = e['length']

    clean_factor = globs['clean_spacing']
    split_factor = globs['split_spacing']

    for k, v in bksum.items():
        v = int(clean_factor * v[2] / float(v[1]))
        result[k] = (v, int(0.33 * v))

    for k, v in splitmem.items():
        if k in result:
            continue
        v2 = (camaxlen - splitlmem[k])/ v
        result[k] = (int(0.5 * v2), int(split_factor * v2))

    for e in bookings:
        e['clean_min_spread'] = result[e['title']][0]
        e['split_min_spread'] = result[e['title']][1]
#    print "MIN_SPREADS", result



def preprocess_custom_performances(auditoriums, bookings, camap, globs):
    custom_auds = [x for x in auditoriums if x['id'] in camap]
    busier = {}
    for k, v in camap.items():
        if k not in busier:
            busier[k] = []
        for e in v:
            interval = (bookings[e]['time_constraint'][0][0] - globs['adtime'], 
                    bookings[e]['time_constraint'][0][0] + 
                    bookings[e]['length'] - globs['adtime'])
            if any([intervals_intersect(interval, x) for x in busier[k]]):
                vassert(0, 
                    "Time for booking {0} clashes with other custom bookings".format(
                    bookings[e]['id']))
            busier[k].append(interval)

    result = {}
    for k, v in busier.items():
        if k not in result:
            result[k] = []
        interval = (globs['schedule'][0], globs['schedule'][1])            

        for e in sorted(v):
            tomem = (interval[0], e[0])
            interval = (e[1], interval[1])
            if tomem[1] > tomem[0]:
                result[k].append(tomem)
        result[k].append(interval)

    return result

def rectify_input(globs, auditoriums, movies, bookings):

    #convert schedule values to ints in auditoriums
    op = time_to_mins(globs['schedule'][0])
    cl = time_to_mins(globs['schedule'][1])
    cl = op - 10
    if cl < op:
        cl += 1440
    globs['schedule'] = (op - globs['adtime'], cl - globs['adtime'])
    for k, v in globs['timedefs'].items():
        x1 = max(op, v[0]) - globs['adtime']
        x2 = min(cl, v[1]) - globs['adtime']
        globs['timedefs'][k] = (x1, x2)

#    if 'anytime' not in globs['timedefs']:
    globs['timedefs']['anytime'] = (op - globs['adtime'], cl - globs['adtime'])

    #copy movies info into bookings
    mvdct = {e['id']: e for e in movies}
    for e in bookings:
        for e2 in ['demand', 'raw_length', 'title']:
            e[e2] = mvdct[e['movie']][e2]


    bkids = set([])
    cust_auds = {}

    for i, e in enumerate(bookings):
        vassert(e['id'] not in bkids, "Booking ID {0} not unique!".format(
            e['id']))
        #calc total length
        if e['auditorium_constraint']['scheme'] == None:
            e['auditorium_constraint']['scheme'] = ''

        e['auditorium_constraint']['scheme'] = \
            e['auditorium_constraint']['scheme'].lower()

        if e['playing'] == 'custom':
            targaud = e['auditorium_constraint']['id']
#            assert(targaud not in cust_auds)
            if targaud not in cust_auds:
                cust_auds[targaud] = []
            cust_auds[targaud].append(i)
            

        for ii, tc in enumerate(e['time_constraint']):
            e['tctext'] = None
            if e['playing'] != 'custom':
                assert(type(tc) in [str, unicode])
                if type(tc) == unicode:
                    tc = str(tc)

            if type(tc) == str:
                tc = globs['timedefs'][tc]
                e['tctext'] = tc

            v1 = max(globs['schedule'][0], tc[0])
            v2 = min(globs['schedule'][1], tc[1])
            e['time_constraint'][ii] = (v1, v2)


        e['formats'] = [x.lower() for x in e['formats']]
        totlen = e['raw_length'] + globs['cleaning'] + globs['trailer'] + \
            globs['adtime']
        flr = globs['film_length_rounding']
        if totlen % flr > 0:
            totlen = flr * (1 + (totlen // flr))

        e['length'] = totlen
        bkids.add(e['id'])
    #end loop over bookings

    #now check custom performance collisions and pre-process them
    leftovers = preprocess_custom_performances(auditoriums, bookings, 
        cust_auds, globs)

    globs['aud_custom_leftovers'] = leftovers

    for e in auditoriums:
        e['scheme'] = e['scheme'].lower()
        e['parameters']['formats'] = set([x.lower() for x in 
            e['parameters']['formats']])
        if e['id'] in cust_auds:
            e['hasCust'] = cust_auds[e['id']]
        else:
            e['hasCust'] = -1
#        print e['id'],e['hasCust']

    #now fix aud starts
    sorter = [(auditoriums[i]['capacity'], i) for i in range(len(auditoriums))]
    sorter = list(reversed(sorted(sorter)))
    conc_ctr = 1
    delay = 0

#    print sorter
    for e in sorter:
        if conc_ctr > globs['max_concurrent_starts']:
            conc_ctr = 1
            delay += globs['showing_increments']
        auditoriums[e[1]]['schedule'] = (globs['schedule'][0] + 0*delay,
            globs['schedule'][1]) #want to close at the same time!!
        conc_ctr += 1
#    for e in auditoriums:
#        print e['id'], e['capacity'], e['schedule']

#    calc_minspread(bookings, (cl - op), globs['showing_increments'])
#    calc_minspread2(bookings, globs)
#    for e in bookings:
#        print e['title'], e['clean_min_spread'], e['split_min_spread']

def assemble_constrainter_init(globs, bookings):
    result = {}
    calc_minspread2(bookings, globs)
        #FIXME this was in rectify_input before viper!
    result['max_concurrent'] = globs['max_concurrent_starts']
    result['showing_increments'] = globs['showing_increments']
    result['clean_min_spreads'] = {}
    result['split_min_spreads'] = {}
    result['total_perf_count'] = {}
    for e in bookings:
        if e['title'] not in result['clean_min_spreads']:
            result['clean_min_spreads'][e['title']] = e['clean_min_spread']
        if e['title'] not in result['split_min_spreads']:
            result['split_min_spreads'][e['title']] = e['split_min_spread']
        if e['title'] not in result['total_perf_count']:
            result['total_perf_count'][e['title']] = 0
        result['total_perf_count'][e['title']] += len(e['formats'])

    result['clean_time'] = globs['cleaning']
    result['ad_time'] = globs['adtime']

    return result


#merge a list of intervals (integer pairs)
def merge_intervals(intervals0, ln = 2):
    intervals = [(x[0], x[1]) for x in intervals0]

    while True:
        intervals = sorted(intervals)
        merged = False

        for i in range(len(intervals) - 1):
            for j in range(i + 1, len(intervals)):
                if intervals[j][0] <= intervals[i][1] <= intervals[j][1]:
                    merged = True
                    intervals[i] = (intervals[i][0], 
                        max(intervals[i][1], intervals[j][1]))
                    del intervals[j]
                    break
            if merged:
                break

        if not merged:
            break

    return intervals

#determine if two intervals have an intersection
def intervals_intersect(i1, i2):
    if i1[0] <= i2[0] < i1[1]:
        return True
    if i1[0] < i2[1] <= i1[1]:
        return True

    if i2[0] <= i1[0] < i2[1]:
        return True
    if i2[0] < i1[1] <= i2[1]:
        return True

    return False

def roll_from_distribution(d):
    mx = sum(d)
    r = mx * random.random()
    tot = 0
    for i, e in enumerate(d):
        tot += e
        if tot > r:
            return i
    assert(0)            


