%\input{amstex}
\documentclass{article}
%\documentclass[12pt]{report}
\usepackage{verbatim}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amsthm}
%\usepackage{url}
\usepackage{graphicx}

%xxx: in the graph results subsection, we have minll instead of maxll!!! maybe recalc ll-s better...

\begin{document}

\title{$\;$ Movie scheduling system}
\author{Peter Hussami}

\maketitle

\tableofcontents
\newpage

\section{The purpose of this document}

The system's task is to process a request containing movie bookings and auditorium data, and provide an efficient schedule in response. This document describes the algorithm implemented to that end, and touches on the basic features of the software implementation.

\section{The algorithm}

\subsection{Problem overview}

The program's task is to build a movie performance schedule for a theater for one day.

An input generally contain the following information:
\begin{itemize}
\item Global variables related to the theater and/or the request in general, such as number of concurrent performances allowed, theater opening time etc.
\item A list of auditoriums, each with a particular size, a single technological scheme as well as a list of supported formats. Technological schemes can take values such as "standard", "imax" etc., while format is generally '2D' or '3D'.
\item A list of bookings, each referring to 1+ performances of a single movie. The formats can vary within the booking, but the auditorium scheme cannot. Bookings can be 'clean' (playing nose to tail the whole day), 'split' (not playing in every set of the day), or 'custom' (playing in a particular auditorium at a particular time).
\end{itemize}

\subsection{Definitions}

We will now venture to define a set of terms that will help us throughout the document. Some of these terms may appear obvious, but by defining them in strict terms, we are able to structure the problem better.

\begin{itemize}
\item A \underline{set} is a time interval encompassing ca. 3 hours of time. The sets used in the industry are 
\begin{itemize}
\item "very early" (6AM - 9AM),
\item "early" (9AM - noon), 
\item "matinee" (noon - 3PM),
\item "afternoon" (3PM - 6PM),
\item "evening" (6PM - 9:15PM),
\item "late" (9:15PM - 12:05AM),
\item "very late" (12:05AM - 6AM).
\end{itemize}
All these intervals are inclusive on the lower bound and not on the upper bound, e.g. matinee includes 12PM, but 3PM sharp is already not part of it.

\item A time block reserved for a movie consists of advertising time, trailer time, showtime and cleaning time, in this order.
\item For each auditorium, the first performance of the day is not required to include ad time prior to the performance, and the last performance of the day in an auditorium expressly excludes cleaning time (the program is not concerned about the day's last cleanup of an auditorium).
\item A \underline{clean booking} is a booking that has performances in every set during the relevant business hours of the theater.
\item A \underline{split booking} is a booking that has performance in specified sets only.
\item A \underline{custom booking} is a booking assigned to an auditorium and a time slot.
\item A \underline{showing} is a close synonym to the term "performance", it refers to a single performance of a particular movie sampled from a booking. A showing has technological requirements related to auditoriums. Showing is the term used for the programmatic representation of a performance, even if it does not have an auditorium or a showtime assigned -- as it will not at the beginning of the program's run. This terms will be used interchangebly with "performance" all across the document.
\item A \underline{mandatory showing} is a showing required by the input. An \underline{optional showing} is a showing supplied by the system.
\end{itemize}
\newpage
\section{Algorithm overview}

\subsection{Introduction}
The problem at hand is an NP-hard problem without constraints. One could find approximate solutions and hope that they are close enough. In any case, writing an approximation algorithm that beats a human in compactness of the schedule should not be difficult.

Here, however, we cannot take this approach. The problem has constraints that most likely escape modeling attempts -- at list they did escape ours. Assigning showings to auditoriums is one thing, but then they need to be scheduled in a way that all constraints are upheld: the limit on concurrent showtime starts, the spacing gap between two consecutive showings of the same movie should be "nice" (i.e. semi-even), movies from clean bookings should be represented in each set, etc.

Instead, we will take a greedy approach. We will break the problem down into various subproblems, optimize them in a greedy manner, and -- where necessary -- calculate relevant statistics on the fly. In particular, rather than trying to come up with a construction that meets all constraints, we will try scheduling movies one by one, while making sure that they remain consistent with all constraints spanned out by previously scheduled performances.

In our greedy approach, we will perform each step as if no subsequent steps existed, and hope that we get nice enough results. This means we first assign showings to auditoriums, and when we're satisfied with the assignments, we schedule the showings in each auditorium, one at a time.


\subsection{First subproblem: assgining clean bookings to auditoriums}

Various attempts at mathematically sound scheduling algorithms lead to schedules with very uneven spacing, as well as clean bookings unevenly distributed across sets. That lead to the simplification of the problem:
\begin{enumerate}
\item Clean bookings will initially be assigned to a single auditorium, the higher demand movies will go to higher capacity auditoriums.
\item Subsequent to this, we will try to improve auditorium load balances by "combing" auditoriums, i.e. combing together the contents of the auditorium pair with the longest and shortest total playtime (the total playtime should converge to the average for both auditoriums), and see if the overall scheduling improves).
\end{enumerate}


\subsection{Second subproblem: assigning split bookings to auditoriums}

Scheduling split bookings is an entirely different matter. Here the spacing requirements are less stringent, but movie lengths are generally quite varied.

The approach taken here is as follows:
\begin{enumerate}
\item We reverse sort auditoriums by the amount of available time
\item We assign every split booking to the largest auditorium that can host it and that has no other split booking assigned to with similar time constraints (e.g. same set). If no such auditorium is found, the list of auditoriums is tested again, however, this time the set conflict is allowed. If there is still no solution, scheduling fails.
\item We balance the loads: an auditorium's load can be decreased in two different ways, either by removing one of its showings or by swapping one its showings for a shorter one. Here we go through the list of auditoriums, starting with the most constrained (=least available time), and
\begin{itemize}
\item If we cannot remove any of the showings from this auditorium (e.g. no other auditoriums match the showing's technical requirements), then we skip this auditorium.
\item Otherwise if there is an auditorium that can host one of the current auditorium's showings without a swap, then we reassign the longest such showing to that auditorium, and
\item If that's not possible, we try to swap the showing for a shorter one assigned to an auditorium with more available time.
\end{itemize}
\item We keep iterating the above until the load for the auditoriums converge as much as possible.
\end{enumerate}

\subsection{Third subproblem: scheduling the showings assigned to an auditorium}

Once an auditorium has its showings assigned, scheduling can start. In a greedy model where we schedule one auditorium at a time, it is easy to see that the scheduling of each auditorium becomes more constrained gradually. 

For the implementation, we will use a constraint management module that will keep track of all necessary constraints.

Algorithm description:
\begin{enumerate}
\item Algorithms are sorted in priority order. Special auditoriums are handled first (IMAX etc.). The for each auditorium:
\item All possible orderings of the auditorium's showings is generated. Note that this is greatly simplified for clean bookings, where we will typically see several copies of the same movie, or perhaps two movies after combing, rarely more.
\item Assuming this particular order of the showings, they are test-scheduled, each at the earliest possible time slot available. This availability is tested against all constraints (concurrency, spacing etc.). Then the scheduling is scored -- currently the best score is awarded to the ordering that finishes earliest.
\item The best-scoring scheduling is kept, and is assigned.
\end{enumerate}

That more or less settles the problem. There will be edge cases, such as: a split booking has two matinee movies assigned (the load balancer didn't do a great job). There are ways to decrease the "stress" within the auditorium, but in practice, once we invoke the stress relief algorithm, we are mostly going to see a failed schedule anyway, so there is no need to discuss stress relief here. It's there in the code.

\subsection{Putting together the whole algorithm}

Now that we've covered the main subproblems in detail, let us sketch up the algorithm.
\begin{enumerate}
\item Input is received and tested against strict formal rules (Return error upon failure).
\item Auditoriums and showings are generated based on the input.
\item For each showing, we create a priority list of auditoriums -- this will be the list of auditoriums that can play the showing, reversed ordered by size.
\item A big loop begins here: looping over the number of combings with loop variable $n$.
\item Clean bookings are assigned: they tak the largest auditoriums, each with the proper technological constraint. This is checked post mortem: if the auditorium assignments can be improved (=movie with lower demand assigned to larger auditorium), it is improved.
\item Upon assignment, $n$ combings are performed ($n$ is the loop variable).
\item Split bookings are given their initial assignments, and then load balancing is performed.
\item All auditoriums are scheduled, starting with the special ones, and then continuing with the larger ones. In practice that will give us the schedules for the clean bookings first (as those go to the larger auditoriums).
\item If the scheduling was successful, we see if the closing hour is greater than the best closing hour for the schedules generated by previous iterations in the scheduling loop (over $n$). If not (or there was an error) we stop scheduling. If yes, we continue with $n$ := $n+1$. This way, the last successful schedule will always contain the best result.
\item End loop.
\item If no schedules were generated, we return an error (if we haven't yet).
\item Now we proceed to adding optional showings: we iterate over each auditorium, and random-generate a performance that fits into the time available for that auditorium. The weights for the generation are the demand values for each movie. Then we assign these movies to the earliest possible slot. We keep iterating this process until no more movies can be assigned.
\item We return the result.
\end{enumerate}

That concludes the discussion of the algorithm.

\end{document}
