drop table if exists articles;

create table articles(
    id int primary key auto_increment,
    title varchar(200) not null default '',
    text varchar(50000) not null default '',
    full_geo varchar(100) not null default '',
    town varchar(60) not null default '',
    art_id int not null default -1,
    url varchar(200) not null default ''
);

create index artindex1 on articles(art_id);

