drop table if exists geonames;

create table geonames(
    id int primary key auto_increment,
    town varchar(50) not null default '',
    state varchar(3) not null default '',
    country varchar(5) not null default '',
    postal varchar(12) not null default ''
);

create index geoindex1 on geonames(town, state, country, postal);

