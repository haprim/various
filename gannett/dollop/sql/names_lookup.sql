drop table if exists names_lookup;

create table names_lookup(
    id int primary key auto_increment,
    geoname_id int not null default -1,
    token varchar(50) not null default '',
    ordinal int not null default -1,
    name_length int not null default -1
);

create index names_lookupindex1 on names_lookup(token, ordinal);

