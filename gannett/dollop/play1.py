import MySQLdb
#from process.names2 import headed_names
import re
from classes.utilities import *
from classes.splitter import *
#print headed_names

sfield1 = 'title'

def process_article(article, field_name, names_lookup):
    rework = article[field_name].strip().replace("'s", '').replace(
        '"','').replace("'",'').replace('  ', ' ').replace(
        '(',' ').replace(')',' ').replace(' -', ' _ ').replace(
        '- ', ' _ ').replace('/', ' ').replace('-', ' ')
    #FIXME watch out with the dash!

    st = split_text(rework, names_lookup)
    result = []
    for e0 in st:
        locres = {}
        towns = find_geonames_in_list(e0, names_lookup, require_cap=True)
        for e in towns:
            head = e[1]
            tail = e[2] - 1
            while head > 0:
                if e0[head - 1][0].isupper():
                    head -= 1
                else:
                    break
            while tail < len(e0) - 1:
                if e0[tail + 1][0].isupper():
                    tail += 1
                else:
                    break

            mul = 1.0
            if head < e[1]:
                mul *= 0.3
            if tail >= e[2]:
                mul *= 0.75 ** abs(tail - e[2] + 1)
            
            locres[e[1]] = (e[0], e[1], e[2], mul)
        result.append(locres)            
                                
    return result        

def identify_hits(summer):
    if len(summer) == 0:
        return []
#    elif len(summer) > 10:
#        return []
    nitems = len(summer)
    smval = sum(summer.values())
    smmax = max(summer.values())
    avg = smval / float(nitems)
    thr1 = 0.7
    thr2 = 0.1

    result = []
    for k, v in summer.items():
        if (v >= thr2 * smval) and (v >= thr1 * smmax):
            result.append((k, v))
    return result


if __name__ == "__main__":
    conn = MySQLdb.connect('localhost', 'root', '', 'dollop')
    cursor = conn.cursor(MySQLdb.cursors.DictCursor)
    lcursor = conn.cursor()
    tally = {}

    q = "select * from articles where id < 100000"
    cursor.execute(q)

    geodict = get_geotokens(lcursor)

    artl = cursor.fetchall()
#print artl[0]
    good = 0
    actbase = 0
    field = 'text'
    for e in artl:

        if e['town'].lower() not in geodict['nameset']:
            continue
        if e['town'].lower() not in e[field].lower():
#can be refined
            continue
        actbase += 1        
            
        raw1 = process_article(e, field, geodict['lookup'])
#    print raw1
        summer = {}
        for it in raw1:
            for k, v in it.items():
                nmindex = geodict['id2plain'][v[0]]
                nm = geodict['plain'][nmindex][0]
                if nm not in summer:
                    summer[nm] = 0.0
                summer[nm] += v[3]
#                print nm

        bests = identify_hits(summer)
        bestnames = set([x[0] for x in bests])
        
        srt = tuple(reversed(sorted([int(100*x) for x in summer.values()])))
        srt = len(srt)
        if srt not in tally:
            tally[srt] = (0,0)

        okay = False
        if e['town'].lower() in bestnames:
            okay = True
            good += 1
        if okay:
            pref = "\033[0m"
        else:        
            pref = "\033[91m"
        tally[srt] = (tally[srt][0] + int(okay), tally[srt][1] + 1)
            
        print pref + "SUMM", srt, e['id'], summer, bestnames, e['town'], good, actbase
        if srt == 0:
            print e['town'], e[sfield1]


    print "\033[92mEND: {0}, {1}, {2}".format(good, actbase,
        good / float(actbase))

conn.close()

for k in sorted(tally.keys()):
    print "\t", k, tally[k]
