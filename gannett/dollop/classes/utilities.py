def get_geotokens(cursor):
    q1 = "select lower(town), lower(state), lower(country), lower(postal), id from geonames"
    cursor.execute(q1)
    res = {'plain': [], 'id2plain': {}, 'lookup': {}, 'nameset': set([])}
    for i, e in enumerate(cursor.fetchall()):
        res['plain'].append(e)
        res['id2plain'][e[4]] = i
        res['nameset'].add(e[0])

    q2 = "select geoname_id, lower(token), ordinal, name_length from names_lookup"
    cursor.execute(q2)
    for e in cursor.fetchall():
        if e[1] not in res['lookup']:
            res['lookup'][e[1]] = []
        res['lookup'][e[1]].append((e[0], e[2], e[3]))            

    return res        

def get_modifiers(cursor):
    q1 = "select lower(text), row_type, score from modifiers"
    cursor.execute(q1)

    res = {0: {}, 1: {}}
    for e in cursor.fetchall():
        if abs(e[2]) > 0.5:
            res[e[1]][e[0]] = (1 + e[2]) / 2.0

    return res        
