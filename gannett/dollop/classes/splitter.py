import re

def resolve_intervals(intervals):
    #rule: if two overlap, longer wins
    #paraphrase: add in descending length order, skip if there is an overlap
    sorter = []
    for k, v in intervals.items():
        for e in v:
            sorter.append((e[1], e[0], k))
    sorter = sorted(sorter)            
    mem = {}
    result = []
    for e in reversed(sorter):
        indices = range(e[2], e[2] + e[0])
        if any([x in mem for x in indices]):
            continue
        for x in indices:
            mem[x] = e[1]
        result.append((e[1], e[2], e[2] + e[0]))
                    
    return result



def find_geonames_in_list(lst, geotokens, require_cap=False):
    assert(not any(x=='' for x in lst))

    cands = {}
    hits = {}
    #iterate over list
    for i, e in enumerate(lst):
        newcands = {}
        if require_cap:
            if not e[0].isupper():
                cands = {}
                continue
        key = e.lower().replace('.', '')
        #if list element might be a geo token
        if (key in geotokens):
            #retrieve ordinal and length info for all possible matches
            ll = geotokens[key]
            #and for all of those matches
            for e2 in ll:
                base = i - e2[1]
                if e2[0] not in cands:
                    if e2[1] != 0:
                        continue
                    if e2[2] == 1:
                        if base not in hits:
                            hits[base] = set([])
                        hits[base].add((e2[0], 1))
                    else:                        
                        newcands[e2[0]] = {i: e2[1]}
                elif base in cands[e2[0]]:
                    if base + e2[2] - 1 == i:
                        if base not in hits:
                            hits[base] = set([])
                        hits[base].add((e2[0], e2[2]))
                    else:                        
                        newcands[e2[0]] = cands[e2[0]]
        cands = newcands                        

    #now disambiguate if need to
    hits = resolve_intervals(hits)

    return hits                

def split_part_using_geoname_mask(lst, intervals):
    result = []
    locres = []

    mask = [0 for _ in lst]
    for e in intervals:
        for ii in range(e[1], e[2]):
            mask[ii] = 1

    for i, e in enumerate(lst):
        if mask[i] == 1:
            locres.append(e.replace('.', ''))
        else:
            x = e.find('.')
            if x == -1:
                locres.append(e)
            else:                
                assert(x == len(e) - 1)
                s = e[:-1]
                if s != '':
                    locres.append(e[:-1])
                result.append(locres)
                locres = []
    if locres:                
        result.append(locres)                
    return result


def split_text(text, geotokens):
    #step 1: split by the obvious part separators
    tl = re.split(',|:|!|\?|;', text.replace('.', '. '))
#    print tl
    result = []
    for e in tl:
        lst = re.split(' |\t', e.strip())
        lst = [x for x in lst if x]

        hits = find_geonames_in_list(lst, geotokens)
        ll = split_part_using_geoname_mask(lst, hits)
        for e2 in ll:
            result.append(e2)
    return result            
        

if __name__ == "__main__":
    import MySQLdb
    from utilities import *
    conn = MySQLdb.connect('localhost', 'root', '', 'dollop')
    cursor = conn.cursor()

    geodict = get_geotokens(cursor)

    dum = {5: set([(100,3)]), 6: set([(101,4),(102,2)]), 8: set([(103,2)]),
        2: set([(104,2)])}
#    print resolve_intervals(dum)
#    quit()


    txt = "10Best: Meet me in St. Louis, you st., louis man"
    st = split_text(txt, geodict['lookup'])
    print st

    conn.close()
