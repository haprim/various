from splitter import *
from utilities import *

def defoconds(text, town, mem, geodict, cap=True):
    rework = text.strip().replace("'s", '').replace(
        '"','').replace("'",'').replace('  ', ' ').replace(
        '(',' ').replace(')',' ').replace(' -', ' _ ').replace(
        '- ', ' _ ').replace('/', ' ').replace('-', ' ')

    lookup = geodict['lookup']        

    st = split_text(rework, lookup)
    pre = 1
    post = 1
    for e0 in st:
        hits = find_geonames_in_list(e0, lookup)
        for e in hits:
            idp = geodict['id2plain'][e[0]]
            cname = geodict['plain'][idp][0]
            if town.lower() == cname.lower():
                cat = 1
            else:
                cat = 0

            worklist = []
            if e[1] >= 1:
                worklist.append(("R", e[1] - 1))
            if e[2] < len(e0):
                worklist.append(("S", e[2]))

            for ind in worklist:                    
                word = ind[0] + ':' + e0[ind[1]].lower()
                if word not in mem[cat]:
                    mem[cat][word] = {'cnt': 0, 'heads': {}}
                mem[cat][word]['cnt'] += 1
                if cname not in mem[cat][word]['heads']:
                    mem[cat][word]['heads'][cname] = 0
                mem[cat][word]['heads'][cname] += 1                    
                mem['c{0}'.format(cat)] += 1

    return mem

if __name__ == "__main__":
    import MySQLdb
    conn = MySQLdb.connect('localhost', 'root', '', 'dollop')
    cursor = conn.cursor(MySQLdb.cursors.DictCursor)
    lcursor = conn.cursor()
    tally = {}
    mem = {0: {}, 1: {}, 'c0': 0, 'c1': 0}
    sign = 5

    q = "select * from articles where id < 100000"
    cursor.execute(q)

    geodict = get_geotokens(lcursor)

    artl = cursor.fetchall()
#print artl[0]
    good = 0
    actbase = 0
    field = 'text'
    for e in artl:

        if e['town'].lower() not in geodict['nameset']:
            continue
        if e['town'].lower() not in e[field].lower():
            continue
        actbase += 1        
            
        defoconds(e[field], e['town'], mem, geodict)

    conn.close()

    copy = {0: {}, 1: {}}
    allkeys = set(mem[0].keys() + mem[1].keys())

    norm0 = float(mem['c0'])
    norm1 = float(mem['c1'])
    norm = norm0 + norm1
    comp = {}

    for e in allkeys:
        sgn = sum([mem[i][e]['cnt'] for i in [0,1] if e in mem[i]])
        assert(sgn != 0)

        if sgn < sign:
            continue

        assert(e in mem[0] or e in mem[1])

        if e not in mem[0]:
#            comp[e] = (100000, mem[1][e]['heads'])
            comp[e] = (1, mem[1][e]['heads'])
        elif e not in mem[1]:            
            comp[e] = (-1, mem[0][e]['heads'])
        else:
#            comp[e] = (mem[1][e]['cnt'] / float(mem[0][e]['cnt']),
#                mem[0][e]['heads'].keys() + 
#                ["O:{0}".format(x) for x in mem[1][e]['heads'].keys()])
            dd = {}
            for k, v in mem[0][e]['heads'].items():
                dd[k] = v
            for k, v in mem[1][e]['heads'].items():
                if k not in dd:
                    dd[k] = 0
                dd[k] += v

            comp[e] = (
                mem[1][e]['cnt'] / norm1 - mem[0][e]['cnt'] / norm0, dd)
#                mem[0][e]['heads'].keys() + 
#                ["O:{0}".format(x) for x in mem[1][e]['heads'].keys()])

    sorter = [(v[0], k, v[1]) for k, v in comp.items()]

    conn = MySQLdb.connect('localhost', 'root', '', 'dollop')
    cursor = conn.cursor()

    cursor.execute("delete from modifiers")

    for e in sorted(sorter):
        print e
        if (e[1][0:2] == 'R:'):
            t = 0
        elif (e[1][0:2] == 'S:'):
            t = 1
        else:
            assert(0)
        text = e[1][2:]
        significance = sum(e[2].values())
        score = e[0]
        query = "insert into modifiers(row_type, text, significance, score) values ({0}, '{1}', {2}, {3})".format(t, text, significance, score)
        cursor.execute(query)

    conn.commit()
    conn.close()
