import MySQLdb

import traceback
from bbnames1 import base_names
#from names1 import base_names

conn = MySQLdb.connect('localhost', 'root', '', 'dollop')
cursor = conn.cursor()
q = "delete from geonames"
cursor.execute(q)
q = "delete from names_lookup"
cursor.execute(q)

try:
    for e in base_names:
        e = (e[0], e[1].replace('.', '').replace('-', ' '))
        e1l = e[1].split(' ')
        ll = len(e1l)
        e1l = [conn.escape_string(x).title() for x in e1l]

        q = "insert into geonames(town, state) values ('{0}', '{1}')".format(
            conn.escape_string(e[1]), e[0])
        cursor.execute(q)
        qid = cursor.lastrowid
        for ii, e2 in enumerate(e1l):
            q = "insert into names_lookup(geoname_id, token, ordinal, name_length) values({0}, '{1}', {2}, {3})".format(
                qid, e2, ii, len(e1l))
            cursor.execute(q)
except:
    traceback.print_exc()

conn.commit()
conn.close()
