from bbnames1 import base_names

store = {}
seen = set([])

for e in base_names:
    if e[1] in seen:
        continue

    seen.add(e[1])
    nl = e[1].lower().split(' ')
    key = nl[0]

    if key not in store:
        store[key] = []

    store[key].append(nl)        

print store    
