import MySQLdb
#print headed_names

cap = 50000

def submit_data(cursor, x):
    if 'text' not in x:
        x['text'] = x['title']

    for k, v in x.items():
        if type(v) == str:
            x[k] = conn.escape_string(v)
    x['text'] = x['text'][:cap]
    x['url'] = x['url'][:200]

    q = "insert into articles(title, text, full_geo, town, art_id, url) values ('{0}', '{1}', '{2}', '{3}', {4}, '{5}')".format(
        x['title'][:200], x['text'], x['geo'][:100], x['town'][:60], 
        x['id'], x['url'])
    cursor.execute(q)

f = open('../data/articles.txt', 'rt')
sl = f.readlines()

conn = MySQLdb.connect('localhost', 'root', '', 'dollop')
cursor = conn.cursor()

art = {}
for ctr, ee in enumerate(sl):
    if ctr % 100 == 0:
        conn.commit()
    state = 0
    line = ee
    if len(line) == 0 or line[0] in (' ', '\n'):
        submit_data(cursor, art)
        art = {}

    beg = ee[0]
    if beg == ':':
        art['id'] = int(ee[1:])
    elif beg == '>':
        art['url'] = ee[1:].strip()
    elif beg == '@':
        art['geo'] = ee[1:].strip()
        art['town'] = ee.split('/')[-1].strip()
    elif beg == '#':
        art['title'] = ee[1:].strip()
    elif beg == '.':            
        art['text'] = ee[1:].strip()

conn.commit()
conn.close()
