 
"""
Created on Mon Oct  8 20:19:00 2018

@author: BH
"""
import requests
from bs4 import BeautifulSoup
import sys
import codecs
import csv
from process.lemmatizer import *
import re

class SynAntDictionary(object):
    @staticmethod
    def synonym(Word):
        if len(Word.split()) > 1:
            assert(0)
            print("Error: A Term must be only a single word")
        else:
            try:
                data = requests.get("http://www.thesaurus.com/browse/" + Word)
                Selection=BeautifulSoup(data.text,"lxml")
                Synterms=Selection.find(class_="postab-container css-1nq2eka e9i53te1").find_all("strong")
                "Filter and format Syn Terms into Comma Delineated String"
                t=0
                while t < len(Synterms):
                    if t == 0:
                        SynList=Synterms[t]
                    else:
                        SynList= str(SynList) + "," + str(Synterms[t])
                    t=t+1
                SynList = SynList.replace("<strong>", "")
                SynList = SynList.replace("</strong>", "")
                SynList = SynList.replace(", ", ",")
#                print(SynList)
                
                return SynList
            except:
                return None
                print(str(Word) +" has no Synonyms in the API")
                
    @staticmethod
    def antonym(Word):
        if len(Word.split()) > 1:
            assert(0)
            print("Error: A Term must be only a single word")
        else:
            try:
                data = requests.get("http://www.thesaurus.com/browse/" + Word)
                SelectionAnt=BeautifulSoup(data.text,"lxml")
                Antterms=SelectionAnt.find(class_="antonyms-container css-7jbvqm e1991neq0").find(class_="css-1lc0dpe et6tpn80").find_all("a")
                "Filter and format Ant Terms into Comma Delineated String"
                t=0
                while t < len(Antterms):
                    if t == 0:
                        AntList=Antterms[t].get_text()
                    else:
                        AntList= str(AntList) + "," + str(Antterms[t].get_text())
                    if t==10:
                        t=len(Antterms)
                    t=t+1
#                print(AntList)
                return AntList
                
            except:
                return None
#                print(str(Word) + " has no Antonyms in the API")


def lemm_action(tabular):
    lm = Lemmatizer()
    resl = []
    for e in tabular:
        if e[5] == '':
            continue
        l = e[5].strip().lower().split(' ')
        tkl = [lm.lemmatize(x) for x in l]
#        print sorted(tkl)
        resl.extend([x[1] for x in tkl if (x and x[2] in ['n', 'v'])])

    return set(resl)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Usage: action_csv chat_csv".format(sys.argv[0])
        quit()

    #1 convert action file to lemmatized form
    with codecs.open(sys.argv[1]) as acf:
        acf_reader = csv.reader(acf, delimiter=',')
        bag1 = lemm_action(acf_reader)
        
    #2 read chat file
    with codecs.open(sys.argv[2]) as chf:
        chf_reader = csv.reader(chf, delimiter=',')
        bag2 = []
        for e in chf_reader:
            ll = e[2].split(' ')
            if len(ll) > 0:
                for e2 in ll:
                    index = e2.find('+')
                    if index == -1:
                        continue
                    bag2.append(e2[:index])
        bag2 = set(bag2)

    bag1 = bag1.union(bag2)

    aa=SynAntDictionary()
    synos = {}
    for i, e in enumerate(sorted(bag1)):
#    for i, e in [(0, "milk")]:
        sns = aa.synonym(e)
        if sns:
            vals = re.split(',|;', sns)
            synos[e] = [x.lower().strip().replace(' ', '_') for x in vals]
            sys.stderr.write("{0} - {1}: {2}\n".format(i, e, synos[e]))
    print "synos={0}".format(synos)


