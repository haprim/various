import MySQLdb


"""
Created on Mon Oct  8 20:19:00 2018

@author: BH
"""
import requests
from bs4 import BeautifulSoup
import sys
import codecs
import csv
from process.lemmatizer import *
import re


def synonymlist(cursor, word, pos):
    assert(pos in ['n', 'v'])
    q1 = "select words.wordid, synsets.synsetid, senseid, tagcount, pos, lemma from senses, synsets, words where senses.synsetid=synsets.synsetid and senses.wordid=words.wordid and lemma='{0}' and pos='{1}'".format(word, pos)
    cursor.execute(q1)
    synsetids = set([x['synsetid'] for x in cursor.fetchall()])
    if len(synsetids) == 0:
        return []

    q2 = "select words.wordid, synsets.synsetid, senseid, tagcount, pos, lemma from senses, synsets, words where senses.synsetid=synsets.synsetid and senses.wordid=words.wordid and synsets.synsetid in ({0}) and pos='{1}'".format(
        ','.join([str(x) for x in synsetids]), pos)
    cursor.execute(q2)
    res = [(x['tagcount'], x['lemma']) for x in cursor.fetchall() if x['lemma'] != word]
    return res





def lemm_action(tabular):
    lm = Lemmatizer()
    resl = []
    for e in tabular:
        if e[5] == '':
            continue
        l = e[5].strip().lower().split(' ')
        tkl = [lm.lemmatize(x) for x in l]
#        print sorted(tkl)
        resl.extend([(x[1], x[2]) for x in tkl if (x and x[2] in ['n', 'v'])])

    return set(resl)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Usage: action_csv chat_csv".format(sys.argv[0])
        quit()

    #1 convert action file to lemmatized form
    with codecs.open(sys.argv[1]) as acf:
        acf_reader = csv.reader(acf, delimiter=',')
        bag1 = lemm_action(acf_reader)
        
    #2 read chat file
    with codecs.open(sys.argv[2]) as chf:
        chf_reader = csv.reader(chf, delimiter=',')
        bag2 = []
        for e in chf_reader:
            ll = e[2].split(' ')
            if len(ll) > 0:
                for e2 in ll:
                    index = e2.find('+')
                    if index == -1:
                        continue
                    bag2.append((e2[:index], e2[index+1:]))
        bag2 = set(bag2)

    bag1 = bag1.union(bag2)

    conn = MySQLdb.connect('localhost', 'root', '', 'wordnet')
    cursor = conn.cursor(MySQLdb.cursors.DictCursor)

    synos = {}
    for i, e in enumerate(sorted(bag1)):
        if e[0].isdigit():
            continue
#    for i, e in [(0, "milk")]:
        sns = synonymlist(cursor, e[0], e[1])
        if sns:
            synos[e[0]] = [x[1].lower().strip().replace(' ', '_') for x in sns]
            sys.stderr.write("{0} - {1}: {2}\n".format(i, e, synos[e[0]]))
    print "synos={0}".format(synos)

    conn.close()


