import csv
import codecs
import sys
from lemmatizer import *

from tokenizer import *

removables = ['there we go', 'there you go', 'here we go', 'here you go',
    'is', 'are', 'will', 'oh']

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Usage: {0} CSV_name".format(sys.argv[0])
        quit()

    tk = Tokenizer()
    lm = Lemmatizer()

    csv_reader = csv.reader(codecs.open(sys.argv[1], 'rt'))
    f = codecs.open('out.csv', 'wt')
    csv_writer = csv.writer(f, delimiter=',', quotechar='"', 
        quoting=csv.QUOTE_MINIMAL)

    for e in csv_reader:
        blah = e[2].strip().lower()
        for e2 in removables:
            blah = blah.replace(e2, '')
        tkl = tk.tokenize(blah)
#        if e[2] == '__none__':
#            continue

        lemmed = []
        for ii, e2 in enumerate(tkl):
            if len(e2) < 2:
                continue
            ret = lm.lemmatize(e2)

            if ret == None:
                continue
            if ret[2] not in ('n', 'v'):
                continue
            lemmed.append(ret[1] + '+' + ret[2])
        if len(lemmed) == 0:
            continue
        csv_writer.writerow([e[0], e[1], ' '.join(lemmed)])
        

    f.close()        
