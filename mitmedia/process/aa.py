import nltk
from nltk.stem import WordNetLemmatizer
import sys
import csv
import codecs

csv_reader = csv.reader(codecs.open(sys.argv[1], 'rt'))

wordnet_lemmatizer = WordNetLemmatizer()
punctuations="?:!.,;"

for e in csv_reader:
    sentence_words = nltk.word_tokenize(e[2])
    for word in sentence_words:
        if word in punctuations:
            sentence_words.remove(word)
    print             

sentence_words
print("{0:20}{1:20}".format("Word","Lemma"))
for word in sentence_words:
    print ("{0:20}{1:20}".format(word,wordnet_lemmatizer.lemmatize(word)))
