import codecs
import sys
import csv

f = codecs.open(sys.argv[1], 'rt')

state = 0
row = []
total = []

for e0 in f.readlines():
    e = e0.strip()
    if state == 0:
        row.append(int(e))
        state = 1
    elif state == 1:
        fromto = e.split('-->')
        row.append(fromto[0].strip())
        row.append(fromto[1].strip())
        state = 2
    elif state == 2:
        row.append(e)
        state = 3
    else:
        if len(e) > 0:
            row[3] += ' // ' + e
        else:
            state = 0
            total.append(row)
            row = []

opw = csv.writer(sys.stdout, delimiter=',', quotechar='"', 
    quoting=csv.QUOTE_MINIMAL)

for e in total:
    opw.writerow(e)

f.close()
