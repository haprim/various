import codecs
import csv
import sys

from process.lemmatizer import *
from testcalcs import *
from data.synonyms_wn import *

import matplotlib.pyplot as plt
from base import *
from segstatter import *

chatevals = {
    'winter': {
        'good': set([0, 1, 3, 5, 6, 8, 10, 12, 13, 14, 18, 19, 32, 34, 42, 43,
    45, 47, 48, 49, 50, 52, 54, 64, 66, 67, 72, 74, 75, 76, 83, 84, 85]),

        'notsure': set([2, 7, 9, 11, 17, 21, 26,38, 51, 53, 78, 79, 90]),

        'bad': set([4, 15, 16, 22, 23, 25, 27, 28, 29, 30, 31, 33, 35, 36, 37, 39,
            40, 44, 46, 55, 56, 57, 58, 59, 60, 61, 62, 63, 65, 68, 69, 70, 71, 73,
            77, 80, 81, 82, 86, 87, 88, 89, 91])
    }
}

filter_out = ['bad', 'notsure']

def segment_ok(seg, fltr):
    for e in fltr:
        if seg.command.ctr in chatevals['winter'][e]:
            return False
    return True

sctests = [('full', full_test), ('loose_full', loose_full_test),
    ('b_o_w', bagofwords1), ('indicator', indicator_test),
    ('moment', moment_test)]

operator_delay = 1000

if __name__ == "__main__":

    if len(sys.argv) < 3:
        print "Usage: {0} str_csv chat_csv".format(sys.argv[0])
        quit()

    timeline = read_sources(sys.argv[1],sys.argv[2])
#    for e in timeline:
#        print e

    segments = build_segments(timeline, operator_delay)
    lm = Lemmatizer()

    scores = {}
    memvec = []
    statvec = []
    

    for i1, e in enumerate(segments):
#        if not segment_ok(e, filter_out):
#            continue

        e.filter((0.4, 1))
        if len(e.entries) == 0:
            continue
        mem = stat_segment(e, synos)
        rr = calcsegstats(e, mem)
        memvec.append(mem)
        statvec.append(rr)
        print "sss", e
#        if segment_ok(e, filter_out):
#            e.mlclass = 1

    for i1, e in enumerate(segments):
        if not segment_ok(e, filter_out):
            continue

#        e.filter((0.4, 1))
        #FIXME quoted stuff!!
        tkl = [lm.lemmatize(x) for x in 
            e.command.command.lower().split(' ')]
        oktoks = [x[1] for x in tkl if (x and x[2] in ['n', 'v'])]
        proc_cmd = ' '.join(oktoks)

        print "\n=================\nSEGMENT", e.command, '|', \
            proc_cmd
        print "\t", memvec[i1]['n'][:5] 
        print "\t", memvec[i1]['v'][:5] 
        print "\t", memvec[i1]['t'][:5] 
        calc_scores(proc_cmd, sctests, memvec[i1], scores)
        if moment_test(proc_cmd, memvec[i1]):
            e.mlclass = 1
        for i2, e2 in enumerate(memvec[i1]['t'][:5]):
            print "\t", e2
            plt.plot(e2[2], range(len(e2[2])), label='kax{0}'.format(i2))
#            plt.scatter(e2[2], [1+i2 for _ in e2[2]], label='kax{0}'.format(i2))
#        plt.show()
#        plt.savefig('figs/fig{0}.png'.format(i1 + 1))

#        break
#        print 

    print scores
#    quit()

    reskeys = statvec[0].keys()
    for e in reskeys:
        print "PLOT FOR KEY {0}".format(e)
        good = [x[e] for i, x in enumerate(statvec) if segments[i].mlclass==1]
#        bad = [x[e] for i, x in enumerate(statvec) if segments[i].mlclass==0 and segment_ok(segments[i], filter_out) ] #use for good vs bad filteres
        bad = [x[e] for i, x in enumerate(statvec) if segments[i].mlclass==0] #use for unfiltered
        ax1=plt.subplot(2,1,1)
        plt.xlabel('xx')
#        plt.xticks([rangie(10)]), plt.yticks([])
        plt.grid=True
        plt.title('good for {0}'.format(e))
        plt.hist(good, bins=30)

        ax2=plt.subplot(2,1,2, sharex=ax1)
#        plt.xticks([]), plt.yticks([])
        plt.title('bad for {0}'.format(e))
        plt.grid=True
        plt.hist(bad, bins=30)
#        plt.show()
        
    """    
    print segments
    mem = {'n': {}, 'v': {}, 't': {}}
    for i in range(segments[0][0], segments[0][1]):
        print timeline[i]
        if timeline[i].command not in mem['t']:
            mem['t'][timeline[i].command] = 0
        mem['t'][timeline[i].command] += 1
        for e in timeline[i].command.split(' '):
            xl = e.split('+')
            key = xl[1]
            k2 = xl[0]
            if k2 not in mem[key]:
                mem[key][k2] = 0
            mem[key][k2] += 1
#        print timeline[i].command
    for e in ['n', 'v', 't']:
        print "For {0}".format(e)
        sorter = [(v, k) for k, v in mem[e].items()]
        for e2 in reversed(sorted(sorter)):
            print "\t", e2
    print mem            
    """
