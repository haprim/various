import MySQLdb
import sys

if len(sys.argv) < 1:
    print "Usage: {0} word".format(sys.argv[0])
    quit()

conn = MySQLdb.connect('localhost', 'root', '', 'wordnet')
cursor = conn.cursor(MySQLdb.cursors.DictCursor)
s = sys.argv[1]
posterm = ''
if s.find('+') != -1:
    ind = s.split('+')
    pos = ind[1]
    s = ind[0]
    posterm = " and pos='{0}'".format(pos)
    

q1 = "select words.wordid, synsets.synsetid, senseid, tagcount, pos, lemma from senses, synsets, words where senses.synsetid=synsets.synsetid and senses.wordid=words.wordid and lemma='{0}' {1}".format(s, posterm)
print q1
cursor.execute(q1)
collect = {}
for e in cursor.fetchall():
    collect[e['synsetid']] = e

skc = set(collect.keys())

print skc

q2 = "select words.wordid, synsets.synsetid, senseid, tagcount, pos, lemma from senses, synsets, words where senses.synsetid=synsets.synsetid and senses.wordid=words.wordid and synsets.synsetid in ({0}) {1}".format(
    ','.join([str(x) for x in skc]), posterm)
cursor.execute(q2)
res = [(x['tagcount'], x['lemma']) for x in cursor.fetchall() if x['lemma'] != s]
print res
"""
#filter for pos
if posterm:
    q2a = "select synsetid from synsets where synsetid in ({0}) and pos='{1}'".format(
        ','.join([str(x) for x in skc]), pos
        )
    print q2a
    cursor.execute(q2a)
    skc = [e['synsetid'] for e in cursor.fetchall()]


q2 = "select * from lexlinks where (synset1id in ({0}) or synset2id in ({0})) and linkid not in ({1})".format(
    ','.join([str(x) for x in skc]),
    "30,80"
    )
print q2
cursor.execute(q2)

cands = {}
for e in cursor.fetchall():
    if e['synset1id'] in skc:
        key = 'word2id'
    else:
        assert(e['synset2id'] in skc)
        key = 'word1id'
    cands[e[key]] = e['linkid']

print cands
q3 = "select * from words where wordid in ({0})".format(
    ','.join([str(x) for x in cands.keys()])
    )
cursor.execute(q3)
for e in cursor.fetchall():
    print e
"""
conn.close()
