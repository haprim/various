import codecs
import csv
import sys

from process.lemmatizer import *
from testcalcs import *
from data.synonyms_wn import *

sctests = [('full', full_test), ('loose_full', loose_full_test),
    ('b_o_w', bagofwords1)]

operator_delay = 2000

class Event:
    eventctr = 0
    def __init__(self, time, typ, actor, command):
        if type(time) == tuple:
            self.timestamp = time
        elif type(time) == str:
            self.timestamp = clock2tuple(time)
        else:
            assert(0)
        
        self.actor = actor
        self.type = typ.lower()
        self.ctr = Event.eventctr
        self.command = command
        Event.eventctr += +1

    def __str__(self):
        return "{0}: {1}, {2}, type {3}, '{4}'".format(self.ctr,
            self.actor, self.timestamp, self.type, self.command)


def clock2tuple(stamp):
    xl = stamp.split(':')
    assert(len(xl) == 3)
    xl2 = xl[2].split(',')
    assert(len(xl2) == 2)

    return (int(xl[0]), int(xl[1]), int(xl2[0]), int(xl2[1]))

def read_sources(srt, chat):
    res_unso = []
    with codecs.open(srt, 'rt') as srtf:
        srt_reader = csv.reader(srtf, delimiter = ',')
        for e in srt_reader:
            if e[5] != '':
                ee = Event(e[1], 'r', '_ACTOR_', e[5].lower())
                res_unso.append(ee)

    with codecs.open(chat, 'rt') as chf:
        ch_reader = csv.reader(chf, delimiter = ',')
        for e in ch_reader:
            assert(len(e) == 3)
            ee = Event(e[0], '_', e[1], e[2].lower())
            res_unso.append(ee)
           

    #now sort
    sorter = [(x.timestamp, x) for x in res_unso]
    return [x[1] for x in sorted(sorter)]

def segment_timeline(timeline):
    result = []
    start_index = 0
    state = 0 # 0 neutral, 1 waiting

    for i, e in enumerate(timeline):
        if e.actor == '_ACTOR_':
            if i - start_index > 1:
                result.append((start_index, i))
            start_index = i + 1

#FIXME leftover at the end
#    if state == 1:
#        if start_index < len(timeline):
#            result.append((start_index + 1, len(timeline)))

    return result

def twocombos(l):
    if len(l) == 0:
        return []
    elif len(l) == 1:
        return [[l[0][:-2]]]

    result = []
    for i in range(len(l) - 1):
        for j in range(i + 1, len(l)):
            result.append([l[i][:-2], l[j][:-2]])
    return result

def nvkeys(words, syns):
    nval = 0.7
    vval = 0.5

    assert(len(words) > 0)

    initcands = twocombos(words)

    result = [(' '.join(x), 1.0) for x in initcands]

    if len(initcands[0]) == 1:
        key = initcands[0][0]
#        if key in syns:
        if 0:
            for e in syns[key]:
                ln = len(e.split('_'))
                result.append((e, 0.5 ** ln))
        return result

    for e in initcands:
        if (e[0] not in syns) or (e[1] not in syns):
            continue
        continue

        for v1 in syns[e[0]]:
            for v2 in syns[e[1]]:
                ln1 = len(v1.split('_'))
                ln2 = len(v2.split('_'))
                result.append(("{0} {1}".format(v1, v2), 0.5 ** (ln1 * ln2)))

    #first, originals
    return result

def calc_scores(command, statted, mem):
    for e in sctests:
        if e[0] not in mem:
            mem[e[0]] = (0, 0)
        sc = e[1](command, statted)
        mem[e[0]] = (mem[e[0]][0] + sc, mem[e[0]][1] + 1)


def stat_segment(timeline, start, end, syns):
    mem = {'n': {}, 'v': {}, 't': {}}
    for i in range(start, end):
        if timeline[i].actor == '_ACTOR_':
            continue
#        print timeline[i]
        wlist = timeline[i].command.split(' ')
        tkeys = nvkeys(wlist, syns)

        for e in tkeys:
            if e[0] not in mem['t']:
                mem['t'][e[0]] = 0
            mem['t'][e[0]] += e[1]

#        print i, timeline[i]
        for e in wlist:
            xl = e.split('+')
            key = xl[1]
            k2 = xl[0]
            if k2 not in mem[key]:
                mem[key][k2] = 0
            mem[key][k2] += 1
#        print timeline[i].command
    for e in ['n', 'v', 't']:
#        print "For {0}".format(e)
        sorter = list(reversed(sorted([(v, k) for k, v in mem[e].items()])))
        mem[e] = sorter
#        for e2 in sorter:
#            print "\t", e2
    return mem

if __name__ == "__main__":

    if len(sys.argv) < 3:
        print "Usage: {0} str_csv chat_csv".format(sys.argv[0])
        quit()

    timeline = read_sources(sys.argv[1],sys.argv[2])
#    for e in timeline:
#        print e

    segments = segment_timeline(timeline)
    lm = Lemmatizer()

    scores = {}

    for e in segments:
        mem = stat_segment(timeline, e[0], e[1], synos)

        tkl = [lm.lemmatize(x) for x in 
            timeline[e[1]].command.lower().split(' ')]
        oktoks = [x[1] for x in tkl if (x and x[2] in ['n', 'v'])]
        proc_cmd = ' '.join(oktoks)

        print "\n=================\nSEGMENT", e, timeline[e[1]].command, '|', \
            proc_cmd
        print "\t", mem['n'][:5] 
        print "\t", mem['v'][:5] 
        print "\t", mem['t'][:5] 
        calc_scores(proc_cmd, mem, scores)
#        break
#        print 

    print scores

    """    
    print segments
    mem = {'n': {}, 'v': {}, 't': {}}
    for i in range(segments[0][0], segments[0][1]):
        print timeline[i]
        if timeline[i].command not in mem['t']:
            mem['t'][timeline[i].command] = 0
        mem['t'][timeline[i].command] += 1
        for e in timeline[i].command.split(' '):
            xl = e.split('+')
            key = xl[1]
            k2 = xl[0]
            if k2 not in mem[key]:
                mem[key][k2] = 0
            mem[key][k2] += 1
#        print timeline[i].command
    for e in ['n', 'v', 't']:
        print "For {0}".format(e)
        sorter = [(v, k) for k, v in mem[e].items()]
        for e2 in reversed(sorted(sorter)):
            print "\t", e2
    print mem            
    """
