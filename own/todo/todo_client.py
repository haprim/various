import xmlrpclib
import todo
import sys
import json

if __name__ == "__main__":
    passed, dat = todo.parse_args(sys.argv)
    if not passed:
        print "Usage: {0} [[command arg] ...] text".format(sys.argv[0])
        quit()

#    server = xmlrpclib.ServerProxy('http://localhost:9000')
    server = xmlrpclib.ServerProxy('http://hussami.com:9200')
    s = server.test(dat)
    dct = json.loads(s)

    if 'success' not in dct:
        print "Error: responso incomprehensiblo"
    elif dct['success'] == False:
        print "Fail: {0}".format(dct['reason'])

    else:
        if dat['command'] != 'query':
            print "OK"
        else:
            if len(dct['data']) == 0:
                print "Nothing to do!"
            else:
                ml = max([len(v) for v in [v0['text'] for v0 in 
                    dct['data']]])

                print "Id      | Prio    | Text"
                print "-" * (max(ml + 20, 24))
                for e in dct['data']:
                    print "{0}\t| {1}\t  | {2}".format(e['id'],
                        e['priority'], e['text'])

