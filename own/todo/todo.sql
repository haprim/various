drop database if exists hig_todo;

create database hig_todo;
use hig_todo;

create table todo_entries(
    id int primary key auto_increment,
    text varchar(150),
    priority int,
    status int,
    updated timestamp,
    date1 date,
    date2 date,
    userid varchar(25),
    category varchar(30)
);

