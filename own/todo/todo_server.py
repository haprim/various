from SimpleXMLRPCServer import SimpleXMLRPCServer
from SimpleXMLRPCServer import SimpleXMLRPCRequestHandler
import socket
import sys
import MySQLdb
import todo
import json

def servfn(dct):
    conn = MySQLdb.connect('localhost', 'root', '', 'hig_todo')
    res = todo.process(dct, conn)
    conn.close()
    return json.dumps(res)


# Restrict to a particular path.
class RequestHandler(SimpleXMLRPCRequestHandler):
   rpc_paths = ('/RPC2',)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Usage: {0} portno".format(sys.argv[0])
        quit()
    hostname = 'localhost'
    portno = int(sys.argv[1])        


    # Create server
    server = SimpleXMLRPCServer((hostname, portno),
              requestHandler=RequestHandler)
    server.register_introspection_functions()
       
    # Register pow() function; this will use the value of
    # pow.__name__ as the name, which is just 'pow'.
#    server.register_function(adder_function, 'add')
          
    server.register_function(servfn, 'test')

                   
    # Run the server's main loop
    server.serve_forever()




