import sys
import datetime
import getpass
import MySQLdb

def date2str(d):
    return '{0}-{1}-{2}'.format(d.year, str(d.month).zfill(2), 
        str(d.day).zfill(2))

commands = set(['query', 'done', 'cancel', 'add', 'modify'])
short_commands = {x[0]: x for x in commands}

args = {'priority': ('priority', int), 'start_date': ('date1', None),
    'end_date': ('date2', None), 'user': ('user', None),
}

short_args = {'p': 'priority', 's': 'start_date', 'e': 'end_date',
    'u': 'user', 'd': 'done', 'c': 'cancel'}

def assign_fields(dct, arglist):
    work = [('s', 'start_date', None), ('e', 'end_date', None),
        ('i', 'id', int), ('q', 'query_date', None),
        ('c', 'category', None), ('u', 'user', None),
        ('p', 'priority', int), ('t', 'text', None)]

    if len(arglist) < 2:
        return

    assert(len(arglist) % 2 == 0)

    ii = 2
    argdct = {arglist[2 * ii]: arglist[2 * ii + 1] for ii in 
        range(1, len(arglist) / 2)}

    work1 = []
    for e in work:
        val = None
        if '-' + e[0] in argdct:
            val = argdct['-' + e[0]]
        elif '--' + e[1] in argdct:
            val = argdct['--' + e[1]]

        if val != None:
            if e[2] != None:
                val = e[2](val)
            work1.append((e[1], val))

    for e in work1:
        dct[e[0]] = e[1]

def process(dct, conn):
    if dct['command'] == 'add':
        query = "insert into todo_entries(priority, status, date1, date2, " +\
            "userid, text, category) values({0}, 1, '{1}', '{2}', '{3}', '{4}', '{5}')".format(
            dct['priority'], dct['start_date'], dct['end_date'],
            dct['user'], conn.escape_string(dct['text']), dct['category']
            )
    elif dct['command'] == 'modify':
        query = "update todo_entries set priority={0}, date1='{1}', date2='{2}', userid='{3}', text='{4}', category='{5}' where status=1 and id={6}".format(
            dct['priority'], dct['start_date'], dct['end_date'],
            dct['user'], dct['text'], dct['category'], dct['id']
        )
    elif dct['command'] == 'done':
        query = "update todo_entries set status=2 where id={0}".format(
            dct['id'])
    elif dct['command'] == 'cancel':        
        query = "update todo_entries set status=0 where id={0}".format(
            dct['id'])
    elif dct['command'] == 'query':
        restrict = "category='{0}' and ".format(dct['category'])
        
        if dct['category'] == 'all':
            restrict = ''
        query = "select id, priority, text from todo_entries where {0} userid='{1}' and date1 <= '{2}' and status=1 order by priority desc".format(
            restrict, dct['user'], dct['query_date'])
    else:
        return {"success": False, "reason": "invalid command"}

    print query
    try:
        cursor = conn.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute(query)        
        if dct['command'] == 'query':
            res = []
            for e in cursor.fetchall():
                res.append(e)
            return {"success": True, "data": res}
        else:
            conn.commit()
            return {"success": True}
    except:
        return {"success": False, "reason": "sql error"}
    
def parse_args(lst):
    passed = True
    dat = {'user': getpass.getuser(), 
        'query_date': date2str(datetime.datetime.now()),
        'start_date': '2000-01-01', 'end_date': '3999-12-31',
        'command': 'query', 'category': 'main', 'priority': 5, 'id': 0}

    if len(lst) == 1:
        passed = True

    elif len(lst) >= 2:
        key = lst[1].lower()
        if key in short_commands:
            dat['command'] = short_commands[key]
        elif key in commands:
            dat['command'] = key
        else:
            passed = False

    if len(lst) > 2:
        assign_fields(dat, lst)

    if dat['command'] == 'add' and 'text' not in dat:
        passed = False

    if not passed:
        return (False, None)

    return (True, dat)

if __name__ == "__main__":
    passed, dat = parse_args(sys.argv)
    if not passed:
        print "Usage: {0} [[command arg] ...] text".format(sys.argv[0])
        quit()
    print dat
    conn = MySQLdb.connect('localhost', 'root', '', 'hig_todo')
    a = process(dat, conn)
    print a
