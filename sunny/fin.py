import math
import numpy as np

__EPS__ = 0.00001
ER = 6378000
NS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

"""
    The class the does it all.
"""
class Geoconv:
    """
        Constructor, input a resolution.
        Calculates X and Y resolutions. x_res divides a full equator perimeter
            as dictated by the input, y_res divides just a quarter circle.
    """
    def __init__(self, resolution = 14):
        self.ER = 6378000 #earth radius
        self.reso2way = resolution

        self.x_res = int(math.ceil(2 * ER * math.pi / resolution))
        self.y_res = int(math.ceil(ER * math.pi / (2.0 * resolution)))
        #print self.internals()

    """
        Return internal settings.
    """
    def internals(self):
        return "XRES={0}, YRES={1}".format(self.x_res, self.y_res)

    """
        Fit a point (x,y) on a cosine-transformed grid.

        The resulting dict will have a lot of vars (useful for debugging).
        Notable ones: xgrid, ygrid - new x values after grid fitting
                      xcnt: the number of x ticks from the left-hand end
                        of the mapping given ygrid.
    """
    def gridize(self, point):
        result = {'x': point['x'], 'y': point['y']}

        yunit = 90.0 / float(self.y_res)
        xunit = 360.0 / float(self.x_res)
        result['xunit'] = xunit
        result['yunit'] = yunit

        result['ycnt'] = point['y'] / yunit
        result['ygrid'] = round(result['ycnt']) * result['yunit']

        latshrink = math.cos(math.pi * result['ygrid'] / 180.0)
        perim = self.x_res * latshrink

        result['perim'] = perim
        result['newx'] = latshrink * (result['x'] + 180.0)
        result['xcnt'] = result['newx'] / xunit

        result['xgrid'] = round(result['xcnt']) * result['xunit']
        return result

    """
        Return the zero band, i.e. the y coordinate such that if 
            abs(degree) < y, then we consider y = 0.
    """
    def zeroband(self):
        return 0.5 * 90.0 / float(self.y_res)

    """
        Calculate the offset for the latitude.
        Input is a latitude. IT MUST BE ON THE GRID!!!

        One of the key functions. Essentially it is a cosine integral,
        plus a modification term to make sure the various bands do not overlap,
            which is required for invertability, binary search, etc.
    """
    def x_offset(self, y):
        if (y > 90) or (y <= -90):
            raise Exception("NO")

        const = 10 #modification factor

        zb = self.zeroband()
        #in zero band? return 0-s
        if abs(y) < zb:
            return 0, 0

        #southern hemisphere? the offset is the offset for the entire
        #northern hemisphere plus whatever needs to be used on the southern
        #side
        if y <= -zb:
            bo = int(2 * self.x_res * self.y_res / math.pi + 
                const * self.y_res  + 1)
            i = round(y / (-90.0 / float(self.y_res)))
            offset = int(math.ceil((2 * self.x_res * self.y_res / math.pi) * 
                    (math.sin(0.5 * math.pi * (i+1) / self.y_res)) + 
                    const * (i + 1)))
            return bo + offset, int(i)

        #northern hemisphere: simple integral
        i = y / (90.0 / float(self.y_res))
        offset = int(math.ceil((2 * self.x_res * self.y_res / math.pi) * 
                (math.sin(0.5 * math.pi * (i+1) / self.y_res)) + 
                const * (i + 1)))

        return offset, int(i)
               
    """
        Produce an 8-long string from a pair of geo coords.
        1. For point on a grid
        2. Calculate offset for gridded latitude
        3. Use xcnt for computing # of ticks along latitude.
        4. Convert offset + ticks into result.
    """
    def geo_2_str(self, point):
        gridded = self.gridize(point)
        xo, i = self.x_offset(gridded['ygrid'])
        kk = int(round(gridded['xcnt']))

        return baseN(xo + kk, 36).zfill(8)

    """
        Binary search: find the latitude that contains the search value.
        The latitude is equivalent to an offset, so we actually search for the
        offset such that the search value is greater than it, but 
        offset + latitude perimeter is already greater than offset. 
        In short, find the latitude that contains the value...

        Once found, compute x from it, and return x, y.
    """
    def binsearch(self, val, mn0, mx0):
        mn = round((mn0 / 90.0) * self.y_res)
        mx = round((mx0 / 90.0) * self.y_res)
        itctr = 0
        is_repeat = False

        while True:
            mid1 = round((mn + mx) / 2)

            #(mx - mn) == 1, mid1 would always stay mx because of the rounding.
            #avoid this by making sure that we take mn on second attempts.
            if is_repeat:
                mid1 = int((mn + mx) / 2)

            #the latitude corresponding to an integer grid point
            mid = 90.0 * mid1 / self.y_res

            #find offset and calculate perimeter for (hypothetical) latitude
            fv, yy = self.x_offset(mid)
            prm = self.x_res * math.cos(math.pi * mid / 180.0)

#            print "\t\t", mid, fv, yy
#            print mn, mx, mid1, mid, val - fv, val - prm - fv, fv, prm

            #if the value is on the latitude, we are done
            if (fv <= val) and (fv + prm >= val):
                break

            #the rest is management
            if mid1 == mx:
                is_repeat = True

            elif fv < val:
                mn = mid1
            else:
                mx = mid1

            itctr += 1
            if itctr > 100:
                print "OOO:", itctr, mid1, mid
                return None, None
                break


        #ok, now found x, convert back into original coordinate
        xr = round(val - fv)
        xd = (360 * xr / prm) - 180.0
        return xd, mid

    """
        Number base conversion.
    """
    def str_2_int(self, s):
        if s == '':
            return 0
        v = NS.find(s[-1]) + 36 * self.str_2_int(s[:-1])
        return v

    """
        Inverse direction: convert string into geo codes.
        First, convert to number base 10, and then find latitude and 
        longitude using binary search.
    """
    def str_2_geo(self, s):
        val = self.str_2_int(s)

        zb = self.zeroband()
        halver, yy = self.x_offset(-zb)

        if val < halver:
            x, y = self.binsearch(val, 0, 90.0)

        else:
            x, y = self.binsearch(val, -zb, -90.0)
        return x, y        

"""
    Function to compute distance between two 2-D points.
    Note that the shortest path between (x1,y1) and (x2,y2) is attained by
    first making moves along the x axis at the greater latitude (i.e.
    further away from the equator). This greater latitude is 
        max(abs(y1), abs(y2)).
    
    If x1 and x2 are more than 180 degrees apart, walking in the negative
    direction is the way to do it (will matter around the date line).

    With the above, it's a plain Euclidian distance..
"""
def dist(x1, y1, x2, y2):
    yy = max(abs(y1), abs(y2))

    xdiff = x2 - x1
    #walking east is too tough? ok, we walk west
    if abs(xdiff) > 180:
        xdiff = 360.0 - abs(xdiff)

    #x1p = ER * math.pi * 2 * math.cos(yy * math.pi / 180.0) * x1 / 360.0
    #x2p = ER * math.pi * 2 * math.cos(yy * math.pi / 180.0) * x2 / 360.0
    xdp = ER * math.pi * 2 * math.cos(yy * math.pi / 180.0) * xdiff / 360.0
    y1p = ER * math.pi / 2 * y1 / 90.0
    y2p = ER * math.pi / 2 * y2 / 90.0

#    print "\tdd", x1p, y1p, x2p, y2p, "DS:", x2p-x1p, y2p-y1p

    return ((xdp)**2 + (y2p - y1p)**2)**0.5

"""
    Convert to a numerical base based on a dictionary.
"""
def baseN(num,b,numerals=NS):
    return ((num == 0) and numerals[0]) or \
        (baseN(num // b, b, numerals).lstrip(numerals[0]) + numerals[num % b])


if __name__ == "__main__":
    reso = 14

    aaa = Geoconv(reso)

    import random
    for i in range(500000):
        if i % 10000 == 0:
            print i

        x = random.random() * 360.0 - 180.0
        y = random.random() * 180.0 - 90.0

        s1 = aaa.geo_2_str({'x': x, 'y': y})

        if len(s1) > 8:
            print "ERR:", s1, x, y

        x2, y2 = aaa.str_2_geo(s1)

        if x2 == None:
            print "CATAST:", x, y, x2, y2

        dd = dist(x, y, x2, y2)

        if dd > 10:
            print x, y, x2, y2, dd

