import math
import numpy as np
import sys

__EPS__ = 0.00001
ER = 6378000
NS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

class Geoconv:
    def __init__(self, resolution = 14):
        self.ER = 6378000 #earth radius
        self.reso2way = resolution

        self.x_res = int(math.ceil(2 * ER * math.pi / resolution))
        self.y_res = int(math.ceil(ER * math.pi / (2.0 * resolution)))
        print self.internals()

    def internals(self):
        return "XRES={0}, YRES={1}".format(self.x_res, self.y_res)

    def gridize(self, point):
        result = {'x': point['x'], 'y': point['y']}

        yunit = 90.0 / float(self.y_res)
        xunit = 360.0 / float(self.x_res)
        result['xunit'] = xunit
        result['yunit'] = yunit

        result['ycnt'] = point['y'] / yunit
        result['ygrid'] = round(result['ycnt']) * result['yunit']

        latshrink = math.cos(math.pi * result['ygrid'] / 180.0)
        perim = self.x_res * latshrink

        result['perim'] = perim
        result['newx'] = latshrink * (result['x'] + 180.0)
        result['xcnt'] = result['newx'] / xunit

        result['xgrid'] = round(result['xcnt']) * result['xunit']
        print result
        return result

    def x_offset(self, y):
        if (y > 90) or (y <= -90):
            raise Exception("NO")
        const = 1            

        if y < 0:
            bo = int(2 * self.x_res * self.y_res / math.pi + 
                const * self.y_res  + 1)
            i = round(y / (-90.0 / float(self.y_res)))
#        print "i=", i, y
            offset = int(math.ceil((2 * self.x_res * self.y_res / math.pi) * 
                    (math.sin(0.5 * math.pi * (i+1) / self.y_res)) + 
                    const * (i + 1)))
            return bo + offset, int(i)

        if y < __EPS__:
            return 0, 0

        i = y / (90.0 / float(self.y_res))
        offset = int(math.ceil((2 * self.x_res * self.y_res / math.pi) * 
                (math.sin(0.5 * math.pi * (i+1) / self.y_res)) + 
                const * (i + 1)))
        return offset, int(i)
               

    def calcto(self, point):
        gridded = self.gridize(point)
        xo, i = self.x_offset(gridded['ygrid'])
#        print gridded, xo, i
        kk = int(round(gridded['xcnt']))
        print "RES=", xo+kk
        return baseN(xo + kk, 36).zfill(8)

    def binsearch(self, val, mn0, mx0):
        mn = round((mn0 / 90.0) * self.y_res)
        mx = round((mx0 / 90.0) * self.y_res)
        itctr = 0
        while True:
#        if mx - mn < 0.0001:

#            break
            mid1 = round((mn + mx) / 2)
            mid = 90.0 * mid1 / self.y_res

            fv, yy = self.x_offset(mid)
            prm = self.x_res * math.cos(math.pi * mid / 180.0)
#            print "\t\t", mid, fv, yy
#        print mn, mx, mid1, mid, val - fv, val - prm - fv, fv, prm
            if (fv <= val) and (fv + prm >= val):
                break

            elif fv < val:
                mn = mid1
            else:
                mx = mid1
            itctr += 1
            if itctr > 100:
                print "OOO:", itctr, mid1, mid
                break


        #ok, now found x
        xr = round(val - fv)
        xd = (360 * xr / prm) - 180.0
#        print "mid=", mid, xd, xr, prm
        return xd, mid

    def str_2_int(self, s):
#    print "\t", s
        if s == '':
            return 0
        v = NS.find(s[-1]) + 36 * str_2_int(s[:-1])
#    print "\t\t", v
        return v

    def str_2_geo(self, s):
        val = str_2_int(s)

        halver, yy = self.x_offset(-0.00001)
#    print "H=", halver

        if val < halver:
            x, y = self.binsearch(val, 0, 90.0)

        else:
            x, y = self.binsearch(val, -0.0001, -90.0)
        return x, y        

def dist(x1, y1, x2, y2):
    yy = max(abs(y1), abs(y2))
#    if x2 - x1 > 180.0:
#        x1 += 360
#    elif x1 - x2 > 180.0:
#        x2 += 360
    xdiff = x2 - x1
    if abs(xdiff) > 180:
        xdiff = 360.0 - abs(xdiff)
    x1p = ER * math.pi * 2 * math.cos(yy * math.pi / 180.0) * x1 / 360.0
    x2p = ER * math.pi * 2 * math.cos(yy * math.pi / 180.0) * x2 / 360.0
    xdp = ER * math.pi * 2 * math.cos(yy * math.pi / 180.0) * xdiff / 360.0
    y1p = ER * math.pi / 2 * y1 / 90.0
    y2p = ER * math.pi / 2 * y2 / 90.0
#    print "\tdd", x1p, y1p, x2p, y2p, "DS:", x2p-x1p, y2p-y1p
    return ((xdp)**2 + (y2p - y1p)**2)**0.5
    xd = (x2 - x1) * math.pi / 180.0
    yd = (y2 - y1) * math.pi / 180.0
    print xd, yd, ER
    dd = ER * (xd**2 + yd**2)**0.5
    print "\tAA", dd
    return dd



def baseN(num,b,numerals=NS):
#    print "\t", num, b
    return ((num == 0) and numerals[0]) or \
        (baseN(num // b, b, numerals).lstrip(numerals[0]) + numerals[num % b])

def x_offset(y, xreso, yreso):
    if (y > 90) or (y < -90):
        raise Exception("NO")

    if y < 0:
        bo = int(2 * xreso * yreso / math.pi + 2 * yreso + 1)
        i = round(y / (-90.0 / float(yreso)))
#        print "i=", i, y
        offset = int(math.ceil((2 * xreso * yreso / math.pi) * 
                (math.sin(0.5 * math.pi * (i+1) / yreso)) + 2 * (i + 1)))
        return bo + offset, int(i)


    if y < __EPS__:
        return 0, 0

    i = round(y / (90.0 / float(yreso)))
#    print "i=", i, y
    offset = int(math.ceil((2 * xreso * yreso / math.pi) * 
            (math.sin(0.5 * math.pi * (i+1) / yreso)) + 2 * (i + 1)))
    return offset, int(i)

def geo_2_str(x0, y, xreso, yreso):
    if (x0 <= -180) or (x0 > 180):
        raise Exception("NO")

    perim = int(xreso * math.cos(y * math.pi / 180.0))

    xo, ygrid = x_offset(y, xreso, yreso)

    x = 180 + x0
    kk = int(round(perim * x / 360.0))
    print "OFF", xo, perim, ygrid, kk
    
    val = xo + kk
    print "val=", val
    return baseN(val, 36).zfill(8)

def binsearch(val, mn0, mx0, xreso, yreso):
    mn = round((mn0 / 90.0) * yreso)
    mx = round((mx0 / 90.0) * yreso)
    while True:
#        if mx - mn < 0.0001:

#            break
        mid1 = round((mn + mx) / 2)
        mid = 90.0 * mid1 / yreso

        fv, yy = x_offset(mid, xreso, yreso)
        prm = xreso * math.cos(math.pi * mid / 180.0)
        print "\t\t", mid, fv, yy
#        print mn, mx, mid1, mid, val - fv, val - prm - fv, fv, prm
        if (fv <= val) and (fv + prm >= val):
            break

        elif fv < val:
            mn = mid1
        else:
            mx = mid1


    #ok, now found x
    xr = round(val - fv)
    xd = (360 * xr / prm) - 180.0
    print "mid=", mid, xd, xr, prm
    return xd, mid

def str_2_int(s):
#    print "\t", s
    if s == '':
        return 0
    v = NS.find(s[-1]) + 36 * str_2_int(s[:-1])
#    print "\t\t", v
    return v

def str_2_geo(s, xreso, yreso):
    val = str_2_int(s)

    halver, yy = x_offset(-0.00001, xreso, yreso)
#    print "H=", halver

    if val < halver:
        x, y = binsearch(val, 0, 90.0, xreso, yreso)

    else:
        x, y = binsearch(val, -0.0001, -90.0, xreso, yreso)
    return x, y        


#ER = 1000

def xo_test(ER, x_res, y_res):
    const = 10   
    lastoffset = 0
    for i in range(0, y_res):
        ydeg = i * (90.0 / float(y_res))
#        print i, ydeg
        offset = int(math.ceil((2 * x_res * y_res / math.pi) * 
                (math.sin(0.5 * math.pi * (i+1) / y_res)) + 
                const * (i + 1)))
        perim = x_res * math.cos(ydeg * math.pi/ 180.0)
        print "AT:", i, offset, perim, offset - lastoffset, \
            offset - lastoffset <= perim
        lastoffset = offset


    """
    if y < 0:
        bo = int(2 * self.x_res * self.y_res / math.pi + 
            const * self.y_res  + 1)
        i = round(y / (-90.0 / float(self.y_res)))
#        print "i=", i, y
        offset = int(math.ceil((2 * self.x_res * self.y_res / math.pi) * 
                (math.sin(0.5 * math.pi * (i+1) / self.y_res)) + 
                const * (i + 1)))
        return bo + offset, int(i)

    if y < __EPS__:
        return 0, 0
    """        

    """
    i = y / (90.0 / float(self.y_res))
#        print "i=", i, y
    offset = int(math.ceil((2 * self.x_res * self.y_res / math.pi) * 
            (math.sin(0.5 * math.pi * (i+1) / self.y_res)) + 
            const * (i + 1)))
    """    
    return
    return offset, int(i)



reso = 14

aaa = Geoconv(reso)

x = 19.9655
y = 47.7204

x = 19.0269
y = 47.5203

x = -119.7823
y = 46.8982

if len(sys.argv) < 3:
    print "Usage: {0} x y".format(sys.argv[0])
    quit()

x = float(sys.argv[1])
y = float(sys.argv[2])

a1 = aaa.calcto({'x': x, 'y': y})
print a1

x2, y2 = aaa.str_2_geo(a1)

dd = dist(x, y, x2, y2)
print x2, y2
print "error=", dd

quit()
xo_test(ER, aaa.x_res, aaa.y_res)

quit()

a1=aaa.calcto({'x': 19.15, 'y': 47.529})
print a1
a2=aaa.calcto({'x': 179.901915, 'y': 0.000529})
print a2
a3=aaa.calcto({'x': -179.999999, 'y': 47.529})
print a3
a4= aaa.calcto({'x': -179.999999, 'y': -89.529})
print aaa.str_2_geo(a1)

import random
import matplotlib.pyplot as plt
dl = []
for i in range(5000):
    x = random.random() * 360.0 - 180.0
    y = random.random() * 180.0 - 90.0
    s1 = aaa.calcto({'x': x, 'y': y})

    if len(s1) > 8:
        raise Exception("NO!")

    x2, y2 = aaa.str_2_geo(s1)
    dd = dist(x, y, x2, y2)
    dl.append(dd)
    print s1

n, bins, patches = plt.hist(dl, 50)
plt.show()
for i in range(500000):
    if i %1000 == 0:
        print i
    x = random.random() * 360.0 - 180.0
    y = random.random() * 180.0 - 90.0
    s1 = aaa.calcto({'x': x, 'y': y})
    if len(s1) > 8:
        print "ERR:", s1, x, y
    x2, y2 = aaa.str_2_geo(s1)
    dd = dist(x, y, x2, y2)
    if dd > 10:
        print x, y, x2, y2, dd
#    print s1
#    print "D=({0}, {1}) VS ({2}, {3}), {4}".format(x, y, x2, y2, dd)

#print baseN(105, 36)
#print baseN(123235245, 36)
quit()
aa=np.float64(0)
i = 0
while True:

    kk = int(xreso / 2 *math.cos(0.5 * (float(i) / yreso) *math.pi))
    aa += 2 * kk + 1
    if i % 10000 == 0:
        print i, int(aa), kk, int(math.ceil((4 * xreso * yreso / math.pi) * 
            (math.sin(0.5 * math.pi * (i+1) / yreso)) + 2 * (i + 1)))
    i += 1        
    if i >= yreso:
#    if i >= 100:
        break

print "FINAL:", aa        
print "VS", 36**8, 36**8/float(2*aa)
print "integral:", 4 * xreso * yreso / math.pi + 2 * yreso
