import math
import numpy as np

__EPS__ = 0.00001
ER = 6378000
NS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def baseN(num,b,numerals=NS):
#    print "\t", num, b
    return ((num == 0) and numerals[0]) or \
        (baseN(num // b, b, numerals).lstrip(numerals[0]) + numerals[num % b])

def x_offset(y, xreso, yreso):
    if (y > 90) or (y < -90):
        raise Exception("NO")

    if y < 0:
        bo = int(2 * xreso * yreso / math.pi + 2 * yreso + 1)
        i = round(y / (-90.0 / float(yreso)))
#        print "i=", i, y
        offset = int(math.ceil((2 * xreso * yreso / math.pi) * 
                (math.sin(0.5 * math.pi * (i+1) / yreso)) + 2 * (i + 1)))
        return bo + offset, int(i)


    if y < __EPS__:
        return 0, 0

    i = round(y / (90.0 / float(yreso)))
#    print "i=", i, y
    offset = int(math.ceil((2 * xreso * yreso / math.pi) * 
            (math.sin(0.5 * math.pi * (i+1) / yreso)) + 2 * (i + 1)))
    return offset, int(i)

def geo_2_str(x0, y, xreso, yreso):
    if (x0 <= -180) or (x0 > 180):
        raise Exception("NO")

    perim = int(xreso * math.cos(y * math.pi / 180.0))

    xo, ygrid = x_offset(y, xreso, yreso)

    x = 180 + x0
    kk = int(round(perim * x / 360.0))
    print "OFF", xo, perim, ygrid, kk
    
    val = xo + kk
    print "val=", val
    return baseN(val, 36).zfill(8)

def binsearch(val, mn0, mx0, xreso, yreso):
    mn = round((mn0 / 90.0) * yreso)
    mx = round((mx0 / 90.0) * yreso)
    while True:
#        if mx - mn < 0.0001:

#            break
        mid1 = round((mn + mx) / 2)
        mid = 90.0 * mid1 / yreso

        fv, yy = x_offset(mid, xreso, yreso)
        prm = xreso * math.cos(math.pi * mid / 180.0)
        print "\t\t", mid, fv, yy
#        print mn, mx, mid1, mid, val - fv, val - prm - fv, fv, prm
        if (fv <= val) and (fv + prm >= val):
            break

        elif fv < val:
            mn = mid1
        else:
            mx = mid1


    #ok, now found x
    xr = round(val - fv)
    xd = (360 * xr / prm) - 180.0
    print "mid=", mid, xd, xr, prm
    return xd, mid

def str_2_int(s):
#    print "\t", s
    if s == '':
        return 0
    v = NS.find(s[-1]) + 36 * str_2_int(s[:-1])
#    print "\t\t", v
    return v

def str_2_geo(s, xreso, yreso):
    val = str_2_int(s)

    halver, yy = x_offset(-0.00001, xreso, yreso)
#    print "H=", halver

    if val < halver:
        x, y = binsearch(val, 0, 90.0, xreso, yreso)

    else:
        x, y = binsearch(val, -0.0001, -90.0, xreso, yreso)
    return x, y        

def dist(x1, y1, x2, y2):
    x1p = ER * math.pi * 2 * math.cos(y1 * math.pi / 180.0) * x1 / 360.0
    x2p = ER * math.pi * 2 * math.cos(y2 * math.pi / 180.0) * x2 / 360.0
    y1p = ER * math.pi / 2 * y1 / 90.0
    y2p = ER * math.pi / 2 * y2 / 90.0
    print "\tdd", x1p, y1p, x2p, y2p, "DS:", x2p-x1p, y2p-y1p
    return ((x2p - x1p)**2 + (y2p - y1p)**2)**0.5

#ER = 1000
reso = 14
yreso = int(math.ceil((ER * math.pi / 2.0)/ reso))
xreso = int(math.ceil(ER * 2 * math.pi / reso))
print yreso, xreso
#print geo_2_str(19.15, 47.529, xreso, yreso)
print "XX"
#print geo_2_str(47.129, 89.91915, xreso, yreso)
#print geo_2_str(179.999999, 89.99999, xreso, yreso)
#print geo_2_str(-179.999999, -0.00000001, xreso, yreso)
#last = geo_2_str(-144.999999, -23.00001, xreso, yreso)
#last = geo_2_str(169.999999, -79.900000001, xreso, yreso)
#last = geo_2_str(169.999999, 49.900000001, xreso, yreso)
#print last
#print str_2_geo(last, xreso, yreso)

import random
for i in range(1):
    x = random.random() * 360.0 - 180.0
    y = random.random() * 180.0 - 90.0
    s1 = geo_2_str(x, y, xreso, yreso)
    x2, y2 = str_2_geo(s1, xreso, yreso)
    print "D=({0}, {1}) VS ({2}, {3}), {4}".format(x, y, x2, y2,
        dist(x, y, x2, y2))

#print baseN(105, 36)
#print baseN(123235245, 36)
quit()
aa=np.float64(0)
i = 0
while True:

    kk = int(xreso / 2 *math.cos(0.5 * (float(i) / yreso) *math.pi))
    aa += 2 * kk + 1
    if i % 10000 == 0:
        print i, int(aa), kk, int(math.ceil((4 * xreso * yreso / math.pi) * 
            (math.sin(0.5 * math.pi * (i+1) / yreso)) + 2 * (i + 1)))
    i += 1        
    if i >= yreso:
#    if i >= 100:
        break

print "FINAL:", aa        
print "VS", 36**8, 36**8/float(2*aa)
print "integral:", 4 * xreso * yreso / math.pi + 2 * yreso
