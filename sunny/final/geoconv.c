#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "geoconv.h"

using namespace std;

/*
 * Place points on grid
 */
gridpoint gridize(pt2d point, int nx, int ny)
{
    ld yunit = 90.0 / (ld) ny;
    ld xunit = 360.0 / (ld) nx;
    ld ycnt = point.y / yunit;
    ld ygrid = round(ycnt) * yunit;
    if (ygrid <= -90.0)
    {
        ygrid = -89.9999999;
    }
    else if (ygrid >= 90.0)
    {
        ygrid = 89.9999999;
    }

    ld latshrink = cos(M_PI * ygrid / 180.0);
    ld perim = nx * latshrink;

    ld newx = latshrink * (point.x + 180.0);
    ld xcnt = newx / xunit;

    ld xgrid = round(xcnt) * xunit;
    gridpoint result;
    result.x = xgrid;
    result.y = ygrid;
    result.xcnt = xcnt;
    return result;
}

/*
 * Calculate offset corresponding to latitude
 */
ll x_offset(ld y, int nx, int ny)
{
    if ((y > 90) || (y <= -90))
    {
        throw 100;
    }
    int cnst = 10;

    if (y < 0)
    {
        ld amp = nx * (ll) ny;
        ll bo = (ll) (2 * amp / M_PI + cnst * ny + 1);
        ll i = round(y / (-90.0 / (ld) ny));
        ll offset = (ll)(ceil((2 * amp / M_PI) * 
            sin(0.5 * M_PI * (i+1) / (ld) ny)) + cnst * (i+1));
        return bo + offset;
    }
    ll i = round(y / (90.0 / (ld) ny));
    ld amp = nx * (ll) ny;
    ll offset = (ll)(ceil((2 * amp / M_PI) * 
        sin(0.5 * M_PI * (i+1) / (ld) ny)) + cnst * (i+1));
    return offset;
}

/*
 * Represent integer in base 36
 */
string base36(ll num, char *buf, int cursor)
{
    if (num == 0)
    {
        return string("");
    }
    char c = NS[num % (ll) 36];
    return base36(num / (ll) 36, buf, cursor - 1) + c;
}

/*
 * Produce string from geo coordinates
 */
string geo_to_str(pt2d point, int nx, int ny)
{
    gridpoint gridded = gridize(point, nx, ny);
    ll xo = x_offset(gridded.y, nx, ny);
    ll kk = (ll)round(gridded.xcnt);
    char b36str[9];
    memset(b36str, 0, 9);
    return base36(xo + kk, b36str, 8);
}

/*
 * Binary search
 * val is the target value, mna/mxa define the range.
 */
pt2d binsearch(ll val, ld mna, ld mxa, int nx, int ny)
{
    int mn = round((mna / 90.0) * ny);
    int mx = round((mxa / 90.0) * ny);

    int mid1;
    ld mid;
    ll offs;

    bool is_repeat = false;
    int itctr = 0;
    ld perim;

    while ( 1 )
    {
        mid1 = round((mn + mx) / 2.0);
        if ( is_repeat )
        {
            mid1 = (mn + mx) / 2;
        }
        mid = 90.0 * mid1 / (ld) ny;

        offs = x_offset(mid, nx, ny);
        perim = nx * cos(M_PI * mid / (ld) 180.0);
        if ((offs <= val) && (offs + perim >= val))
        {
            break;
        }

        if (mid1 == mx)
        {
            is_repeat = true;
        }
        else if (offs < val)
        {
            mn = mid1;
        }
        else
        {
            mx = mid1;
        }

        itctr += 1;
        if (itctr > 100)
        {
            printf("BAD\n");
            break;
        }

    }

    ll xr = round(val - offs);
    if (xr > perim)
    {
        printf("STRANGE!!!\n");
    }
    ld xd = (360.0 * xr / perim) - 180.0;
    pt2d result;
    result.x = xd;
    result.y = mid;
    return result;

}

/*
 * Base 36 to base 10 conversion
 */
ll str_to_int(const string &s, const string &chrmap, int ctr)
{
    if ( ctr < 0 )
    {
        return 0;
    }
    return chrmap.find(s[ctr]) + 36 * str_to_int(s, chrmap, ctr - 1);
}

/*
 * Convert string to a pair of geo coordinates
 */
pt2d str_to_geo(const string &s, int nx, int ny)
{
    ll val = str_to_int(s, string(NS), s.size() - 1);
    ll halver = x_offset(-0.00000001, nx, ny);

    pt2d result;
    if (val < halver)
    {
        result = binsearch(val, (ld) 0.0, (ld) 90.0, nx, ny);
    }
    else
    {
        result = binsearch(val, (ld) -0.00000001, (ld) -90.0, nx, ny);
    }
    return result;
}

/*
 * Measure geo distance between two points
 */
ld geo_dist(pt2d pt1, pt2d pt2)
{
    ld x1 = pt1.x;
    ld x2 = pt2.x;
    ld y1 = pt1.y;
    ld y2 = pt2.y;

    ld y12 = fabs(y1);
    ld y22 = fabs(y2);

    ld mx = y12;
    if (y22 > mx)
    {
        mx = y22;
    }
    ld xd = fabs(x2 - x1);
    if (xd > 180)
    {
        xd = 360.0 - xd;
    }
    ld xdp = M_PI * 2 * cos(mx * M_PI / 180.0) * xd / 360.0;
    ld y1p = M_PI * y1 / 180.0;
    ld y2p = M_PI * y2 / 180.0;
    return ER * sqrt(xdp*xdp + (y2p-y1p)*(y2p-y1p));
}

void unit_test(void)
{
    ld x, y;
    for ( int ii = 0; ii < 500000; ++ii )
    {
        x = (ld) ((rand() / (float) RAND_MAX)*360.0 - 180.0);
        y = (ld) ((rand() / (float) RAND_MAX)*180.0 - 90.0);
        pt2d pt;
        pt.x = x;
        pt.y = y;
        gridpoint gr = gridize(pt, NX, NY);

        string b36 = geo_to_str(pt, NX, NY);
        pt2d rev = str_to_geo(b36, NX, NY);
        ld dd = geo_dist(pt, rev);
        if (dd > 9.99)
        {
            printf("Point1: %Lf/%Lf, Reco: %Lf/%Lf, dist=%Lf\n",
                pt.x, pt.y, rev.x, rev.y, dd);
        }
    }
}

int main(int argc, char **argv)
{
    if (argc < 3)
    {
        printf("Usage: %s x y\n", argv[0]);
        return 1;
    }

    pt2d point;
    point.x = (ld) atof(argv[1]);
    point.y = (ld) atof(argv[2]);

    printf("Input was: X=%Lf, Y=%Lf\n", point.x, point.y);
    string code = geo_to_str(point, NX, NY);
    printf("Code: %s\n", code.c_str());

    pt2d rev = str_to_geo(code, NX, NY);

    printf("Revert from code: X=%Lf, Y=%Lf\n", rev.x, rev.y);

    ld d = geo_dist(point, rev);
    printf("The distance is %Lf (m)\n", d);

    /* Switch this on for unit testing */
//    unit_test();

    return 0;
}
