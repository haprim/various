#include <string>

#define RESOLUTION      14      //meters, changing this will trash old codes
#define NX 2862440
#define NY 715610

#define ER              6378000
#define NS              "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

typedef long double ld;
typedef unsigned long long ll;

struct pt2d { ld x; ld y; };

typedef struct
{
    ld x;
    ld y;
    ld xcnt;
} gridpoint;

/* function to convert geo coordinates to string */
std::string geo_to_str(pt2d point, int nx, int ny);

/* function to convert string to geo coordinates */
pt2d str_to_geo(const std::string &s, int nx, int ny);

/* function to measure distances between two geo coordinate pairs */
ld geo_dist(pt2d pt1, pt2d pt2);

