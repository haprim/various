#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <ctype.h>

#include "geoconv.h"

//FIXME: areas won't work on the dateline for now

#define L4LEN 3002
#define L3LEN 808

const char *l3[L3LEN] = {"ace", "act", "aft", "age", "aim", "air", "ale", "alf",
    "all", "alm", "alp", "alt", "amp", "and", "ant", "any",
    "ape", "apt", "are", "ark", "arm", "art", "ash", "ask",
    "ate", "bab", "bad", "bag", "bal", "bam", "ban", "bap",
    "bar", "bat", "bax", "bay", "bed", "bee", "beg", "ben",
    "bep", "bet", "bex", "bib", "bid", "big", "bin", "bip",
    "bit", "bix", "boa", "bob", "bod", "bog", "bom", "bon",
    "boo", "bot", "bow", "box", "boy", "bro", "bub", "bud",
    "bug", "bun", "bup", "bus", "but", "bux", "cab", "cad",
    "cag", "cam", "can", "cap", "car", "cat", "cax", "cly",
    "cob", "cod", "cog", "com", "con", "cop", "cos", "cot",
    "cow", "cox", "cry", "cub", "cud", "cup", "cut", "dab",
    "dad", "dag", "dam", "dan", "dap", "dat", "dax", "day",
    "deb", "deg", "del", "dem", "den", "dep", "dew", "dib",
    "did", "dig", "dim", "din", "dip", "dit", "dob", "dod",
    "dog", "dom", "don", "dop", "dot", "dox", "dry", "dub",
    "dud", "dug", "dum", "ear", "eat", "eco", "eel", "ego",
    "elf", "elk", "elm", "emo", "end", "fab", "fad", "fam",
    "fan", "fap", "far", "fat", "fax", "feb", "fed", "fee",
    "feg", "fem", "fen", "few", "fib", "fid", "fig", "fim",
    "fin", "fip", "fir", "fit", "fix", "fly", "fob", "fod",
    "fog", "foo", "fop", "for", "fot", "fox", "fry", "fub",
    "fud", "fum", "fun", "fur", "fut", "fux", "gab", "gad",
    "gag", "gal", "gam", "gan", "gap", "gar", "gas", "gat",
    "gax", "gel", "gem", "gen", "get", "gex", "gib", "gid",
    "gig", "gin", "gix", "gly", "gob", "gog", "gom", "gon",
    "goo", "gop", "got", "gox", "gub", "gug", "gum", "gun",
    "gup", "gur", "gus", "gut", "gux", "hab", "had", "hag",
    "ham", "han", "hap", "has", "hat", "hax", "hem", "hen",
    "hep", "her", "het", "hex", "hey", "hib", "hid", "hig",
    "him", "hin", "hip", "his", "hit", "hix", "hob", "hod",
    "hog", "hom", "hon", "hop", "hor", "hot", "hox", "hub",
    "hud", "hug", "hum", "hun", "hup", "hut", "hux", "imp",
    "ink", "int", "jab", "jad", "jag", "jam", "jan", "jap",
    "jar", "jas", "jat", "jaw", "jax", "jed", "jen", "jep",
    "jer", "jet", "jex", "jib", "jid", "jig", "jim", "jin",
    "jip", "jir", "jit", "jix", "job", "jod", "jog", "jom",
    "jon", "jop", "jor", "jot", "jox", "joy", "jub", "jud",
    "jug", "jum", "jun", "jup", "jur", "jut", "jux", "ken",
    "key", "kib", "kid", "kig", "kim", "kin", "kip", "kit",
    "kix", "lab", "lad", "lag", "lam", "lan", "lap", "lar",
    "lat", "lax", "lay", "leb", "led", "leg", "lem", "len",
    "lep", "ler", "let", "lex", "lib", "lid", "lie", "lig",
    "lim", "lin", "lip", "lir", "lit", "lix", "lob", "lod",
    "log", "lom", "lon", "lop", "lor", "los", "lot", "low",
    "lox", "lub", "lud", "lug", "lum", "lup", "lur", "lux",
    "mab", "mad", "mag", "mal", "mam", "man", "map", "mar",
    "mas", "mat", "max", "may", "meb", "med", "meg", "mel",
    "mem", "men", "met", "mex", "mib", "mic", "mid", "mig",
    "mim", "min", "mip", "mir", "mis", "mit", "mix", "mob",
    "mod", "mog", "mom", "mon", "mop", "mor", "mos", "mot",
    "mow", "mox", "mud", "mug", "mum", "mun", "mup", "mut",
    "mux", "nab", "nad", "nag", "nam", "nan", "nap", "nar",
    "nat", "nax", "nay", "neb", "ned", "neg", "nem", "nen",
    "ner", "net", "new", "nex", "nic", "nil", "nim", "nin",
    "nip", "nit", "nix", "nod", "nog", "nom", "non", "nor",
    "not", "now", "nox", "nub", "nud", "nug", "num", "nun",
    "nup", "nur", "nut", "nux", "oak", "oar", "oat", "oct",
    "off", "oil", "old", "one", "opt", "orb", "ork", "our",
    "out", "owl", "own", "pab", "pad", "pag", "pal", "pam",
    "pan", "pap", "par", "pas", "pat", "paw", "pax", "pay",
    "pea", "peb", "ped", "peg", "pem", "pen", "pep", "per",
    "pes", "pet", "pex", "pib", "pic", "pid", "pie", "pig",
    "pin", "pit", "pix", "poa", "pod", "pom", "pop", "pot",
    "pow", "pox", "poy", "pry", "pub", "pud", "pug", "pum",
    "pun", "pup", "pur", "pus", "put", "pux", "rab", "rad",
    "rag", "ram", "ran", "rap", "rar", "ras", "rat", "raw",
    "rax", "ray", "rea", "reb", "red", "reg", "rem", "ren",
    "rep", "rex", "rib", "rid", "rig", "rim", "rin", "rip",
    "rit", "rix", "roa", "rob", "rod", "rog", "rom", "ron",
    "rop", "ror", "ros", "rot", "row", "rox", "roy", "rub",
    "rud", "rug", "rum", "run", "rup", "rur", "rus", "rut",
    "rux", "sab", "sad", "sag", "sam", "san", "sap", "sar",
    "sat", "saw", "sax", "say", "sea", "seb", "sed", "see",
    "seg", "sep", "ses", "set", "she", "shy", "sib", "sic",
    "sid", "sig", "sim", "sip", "sir", "sis", "sit", "six",
    "sky", "sly", "sob", "sod", "sog", "sol", "som", "son",
    "soo", "sop", "sor", "sos", "sot", "sox", "soy", "spy",
    "sub", "sud", "sum", "sun", "sup", "sus", "sut", "tab",
    "tad", "tag", "tam", "tan", "tap", "tar", "tat", "tax",
    "tea", "teb", "ted", "teg", "tel", "tem", "ten", "tep",
    "tet", "tex", "the", "tib", "tid", "tie", "tig", "tim",
    "tin", "tip", "tir", "tix", "tob", "tod", "tog", "tom",
    "ton", "too", "top", "tor", "tos", "tot", "tow", "tox",
    "toy", "try", "tud", "tug", "tum", "tun", "tup", "tur",
    "tut", "tux", "two", "ufo", "ups", "use", "vab", "vad",
    "vag", "vam", "van", "vap", "vat", "vax", "veb", "ved",
    "veg", "vep", "vet", "vex", "vib", "vid", "vig", "vin",
    "vip", "vit", "vix", "vob", "vod", "vog", "vol", "vom",
    "von", "vop", "vot", "vox", "vub", "vud", "vug", "vup",
    "vux", "wab", "wad", "wag", "wam", "wan", "wap", "war",
    "was", "wat", "waw", "wax", "way", "web", "wed", "weg",
    "wep", "wet", "wex", "who", "why", "wid", "wig", "wim",
    "win", "wip", "wit", "wix", "wob", "wod", "wok", "wom",
    "won", "wop", "wow", "wox", "xab", "xad", "xag", "xam",
    "xan", "xap", "xar", "xat", "yab", "yad", "yag", "yam",
    "yan", "yap", "yar", "yat", "yep", "yes", "yet", "yex",
    "yib", "yid", "yim", "yin", "yip", "yit", "yob", "yod",
    "yom", "yon", "yot", "you", "yub", "yud", "zab", "zad",
    "zag", "zan", "zap", "zat", "zax", "zed", "zen", "zep",
    "zig", "zip", "zit", "zix", "zoo", "zot", "zub", "zud"};

const char *l4[L4LEN] = 
    {"babe", "babs", "baby", "bace", "back", "bade", "bads", "bady",
    "bags", "bagy", "bake", "balb", "bald", "bale", "balf", "balk",
    "ball", "balm", "balp", "balt", "bame", "bamp", "bams", "band",
    "bane", "bang", "bank", "bans", "bape", "baps", "barb", "bard",
    "bare", "barf", "bark", "barm", "barn", "barp", "bars", "bart",
    "base", "bash", "bask", "basp", "bass", "bast", "bate", "bath",
    "bats", "baud", "baum", "baut", "baux", "bave", "bawd", "bawk",
    "bawl", "bawn", "baws", "bays", "bead", "beak", "beal", "beam",
    "bean", "bear", "beat", "beax", "bebe", "bebs", "beck", "beds",
    "beed", "beek", "beep", "beer", "bees", "beet", "beex", "begs",
    "belb", "beld", "belf", "bell", "belm", "belp", "belt", "bend",
    "beng", "bens", "bent", "beps", "bern", "besk", "best", "beth",
    "bets", "bibe", "bibs", "bice", "bide", "bids", "biff", "bigs",
    "bike", "bilb", "bile", "bilf", "bill", "bilm", "bilp", "bilt",
    "bime", "bimp", "bims", "bind", "bing", "bink", "bins", "bint",
    "bipe", "bips", "bird", "birk", "birs", "bite", "bits", "bizz",
    "blad", "blag", "blan", "blap", "blat", "blax", "bled", "bleg",
    "blep", "blex", "blid", "blin", "blip", "blir", "blit", "blix",
    "blod", "blog", "blom", "blop", "blot", "blox", "blug", "blum",
    "blup", "blur", "blux", "boam", "boar", "boas", "boat", "boax",
    "bobe", "bobs", "boby", "bode", "bods", "body", "boff", "bogs",
    "boil", "boin", "bold", "bolt", "bomp", "boms", "bond", "bone",
    "bong", "bonk", "bons", "bont", "bony", "book", "bool", "boom",
    "boon", "boop", "boot", "bope", "bops", "bore", "born", "bors",
    "bose", "boss", "bots", "bowl", "bows", "boxy", "boys", "bozz",
    "brad", "brag", "brat", "bubs", "buby", "buck", "buct", "buds",
    "budy", "buff", "bugs", "buke", "bulb", "buld", "bulf", "bulk",
    "bull", "bulm", "buln", "bulp", "bult", "bump", "bums", "bumy",
    "bund", "bung", "bunk", "buns", "bunt", "buny", "bups", "burn",
    "burs", "burt", "bury", "buse", "bush", "busk", "busp", "bust",
    "busy", "bute", "buts", "buzz", "cabe", "cabs", "cade", "cads",
    "cafe", "caft", "cage", "cags", "cagy", "cake", "calb", "cald",
    "cale", "calf", "call", "calm", "calp", "calt", "came", "camp",
    "cams", "cane", "cans", "cant", "cape", "caps", "capt", "card",
    "care", "cark", "carn", "carp", "cars", "cart", "case", "cash",
    "cask", "casp", "cast", "cats", "caty", "caul", "cave", "cede",
    "cent", "chap", "chat", "cite", "cive", "clab", "clad", "clag",
    "clan", "clap", "clat", "clax", "cled", "clep", "cler", "clex",
    "clid", "clim", "clip", "clob", "clod", "clog", "clom", "clop",
    "clot", "clox", "club", "clud", "clug", "clup", "clur", "clut",
    "clux", "coal", "coat", "coax", "cobs", "code", "cods", "coft",
    "cogs", "coil", "coin", "coke", "colb", "cold", "cole", "colk",
    "coln", "colt", "come", "comp", "coms", "cone", "cong", "conk",
    "cons", "cont", "cony", "cook", "cool", "coon", "coop", "coor",
    "coos", "coot", "cope", "cops", "copt", "copy", "corb", "cord",
    "core", "cork", "corm", "corn", "corp", "cort", "cory", "cosh",
    "cosk", "cost", "cosy", "coth", "cots", "coup", "cove", "covy",
    "cowk", "cowl", "cows", "cozy", "crab", "cram", "crib", "crip",
    "crop", "crud", "cube", "cubs", "cuby", "cude", "cuds", "cuff",
    "cugs", "cult", "cump", "cups", "curb", "cure", "cust", "cute",
    "cuts", "cuve", "dabe", "dabs", "dace", "dack", "dade", "dads",
    "daft", "dail", "daim", "dain", "daix", "dake", "dald", "dale",
    "dalf", "dalp", "dalt", "dame", "damp", "dams", "dand", "dane",
    "dank", "dans", "dant", "dape", "daps", "dard", "dare", "darf",
    "dark", "darm", "darn", "darp", "dart", "dash", "dask", "dasp",
    "dast", "date", "dats", "daux", "dave", "dawk", "dawl", "dawn",
    "days", "daze", "dead", "deal", "dean", "dear", "debs", "deck",
    "dede", "deed", "deem", "deep", "deer", "deet", "deff", "deft",
    "degs", "delk", "delt", "dems", "dens", "dent", "deps", "dept",
    "desk", "dess", "dets", "deum", "dews", "dibs", "dice", "dict",
    "dide", "dids", "died", "diel", "diem", "dies", "diet", "diex",
    "diff", "dige", "digs", "dike", "dild", "dill", "dilp", "dilt",
    "dime", "dims", "dine", "dink", "dins", "dint", "dipe", "dips",
    "dire", "dirk", "dirt", "dish", "disk", "disp", "diss", "dist",
    "dits", "dive", "doab", "doal", "dobs", "dock", "docs", "doct",
    "dode", "dods", "dogs", "doke", "dole", "doll", "dome", "doms",
    "done", "dong", "dons", "doom", "doop", "door", "dops", "dore",
    "dorm", "dorn", "dort", "dose", "dote", "dots", "dove", "down",
    "doze", "drag", "drap", "drip", "drop", "drum", "dubs", "duck",
    "duct", "duds", "dugs", "duke", "dule", "dulf", "dulp", "dume",
    "dums", "dune", "dunk", "duns", "dunt", "dups", "dush", "dusk",
    "dust", "duth", "duts", "fabe", "fabs", "face", "fact", "fade",
    "fads", "fage", "fags", "faid", "fail", "fair", "fake", "fale",
    "fall", "falt", "fame", "fams", "fane", "fang", "fans", "fant",
    "fape", "faps", "fare", "farm", "fars", "fast", "fats", "faun",
    "fave", "fear", "feat", "febe", "febs", "feds", "feed", "feel",
    "fees", "feet", "fegs", "fell", "felt", "fems", "fend", "fens",
    "feps", "fets", "fibe", "fibs", "fice", "fick", "fide", "fids",
    "fiff", "fift", "figs", "file", "filk", "fill", "film", "filt",
    "fimp", "fims", "find", "fine", "fink", "fins", "fipe", "fips",
    "fird", "fire", "firm", "firn", "firp", "firs", "firt", "fise",
    "fish", "fisk", "fisp", "fiss", "fist", "fite", "fith", "fits",
    "five", "flad", "flag", "flam", "flan", "flap", "flat", "flax",
    "fled", "fleg", "flep", "flet", "flex", "flid", "flip", "flir",
    "flit", "flix", "flod", "flog", "flop", "flot", "flox", "flub",
    "flug", "flur", "flut", "flux", "foal", "foam", "fobs", "fock",
    "fods", "fogs", "foil", "foim", "foin", "foir", "fold", "folk",
    "folt", "foms", "fond", "fonk", "fons", "font", "food", "fool",
    "foot", "fops", "ford", "fore", "forf", "fork", "form", "forn",
    "fors", "fort", "fosh", "fost", "fots", "foul", "four", "fowl",
    "fret", "frog", "from", "fubs", "fuct", "fuds", "fuff", "fuft",
    "fugs", "fuld", "full", "fulm", "fult", "fume", "fump", "fums",
    "fund", "fune", "fung", "funk", "funs", "funt", "fups", "furs",
    "furt", "fuse", "fush", "fusk", "fust", "futs", "fuzz", "gabe",
    "gabs", "gace", "gact", "gade", "gads", "gafe", "gaff", "gaft",
    "gags", "gail", "gain", "gake", "gale", "galf", "gall", "galp",
    "galt", "game", "gamp", "gams", "gand", "gang", "gans", "gant",
    "gape", "gaps", "garb", "gard", "garf", "garm", "garp", "gars",
    "gart", "gase", "gash", "gask", "gasp", "gass", "gast", "gate",
    "gats", "gaul", "gaum", "gaun", "gave", "gawk", "gawl", "gawn",
    "gaze", "gear", "geat", "gebs", "geds", "geek", "geel", "geep",
    "geff", "gegs", "gelb", "geld", "gell", "geln", "gelt", "gems",
    "gend", "gene", "gens", "gent", "geps", "germ", "gern", "gert",
    "gesk", "gesp", "gest", "geth", "gets", "gezz", "gibs", "gice",
    "gids", "giff", "gift", "gigs", "gilb", "gild", "gilf", "gilk",
    "gill", "giln", "gilp", "gimp", "gims", "gink", "gins", "gint",
    "gips", "gisp", "giss", "gist", "gits", "glad", "glam", "glap",
    "glat", "glax", "gled", "glem", "glen", "glet", "glex", "glid",
    "glig", "glip", "glog", "glop", "glot", "glox", "glum", "glur",
    "glut", "goak", "goal", "goat", "gobs", "gock", "goct", "gogs",
    "gold", "golf", "goll", "golp", "goms", "gone", "gong", "gonk",
    "gons", "good", "gook", "goon", "gope", "gops", "gopt", "gorb",
    "gord", "gore", "gorf", "gork", "gorn", "gorp", "gort", "gose",
    "gosk", "gosp", "goth", "gots", "goup", "gove", "gown", "gows",
    "grab", "grad", "gral", "gran", "grap", "grat", "greg", "grid",
    "grim", "grin", "grip", "grit", "grog", "grub", "gubs", "guck",
    "guct", "guds", "guft", "gugs", "gulb", "gulf", "gull", "gulm",
    "gulp", "gums", "gund", "gung", "gunk", "guns", "gunt", "gups",
    "gupt", "gurb", "gurk", "gush", "gusk", "guss", "gust", "guts",
    "hact", "hads", "haft", "hags", "hail", "haim", "hain", "hair",
    "halb", "hald", "half", "hall", "halm", "haln", "halt", "hamp",
    "hams", "hand", "hang", "hank", "hans", "hant", "haps", "hapt",
    "harb", "hard", "hare", "harf", "hark", "harm", "harn", "harp",
    "hart", "hash", "hask", "hasp", "hass", "hast", "hate", "hats",
    "have", "haze", "hebs", "hect", "heds", "heft", "hegs", "helb",
    "held", "helf", "helm", "help", "helt", "hemp", "hems", "hens",
    "hent", "heps", "hept", "herb", "herd", "here", "herk", "herm",
    "hern", "herp", "hers", "hesh", "hesk", "hesp", "hess", "hest",
    "hets", "hezz", "hibs", "hick", "hict", "hide", "hids", "hiff",
    "hift", "high", "higs", "hike", "hild", "hilf", "hill", "hilp",
    "hilt", "hims", "hind", "hing", "hink", "hins", "hint", "hips",
    "hipt", "hire", "hirs", "hise", "hish", "hisk", "hisp", "hiss",
    "hist", "hite", "hits", "hive", "hobs", "hock", "hode", "hods",
    "hoft", "hogs", "hold", "hole", "holk", "holl", "holm", "holn",
    "holp", "holt", "home", "homs", "hone", "hong", "honk", "hons",
    "hope", "hops", "hord", "hork", "horm", "horn", "horp", "hors",
    "hort", "hose", "hosh", "hosk", "host", "hote", "hots", "hove",
    "hube", "hubs", "huck", "huct", "huds", "huff", "huge", "hugs",
    "hulb", "hulk", "hull", "hulp", "hume", "hump", "hums", "hund",
    "hung", "hunk", "huns", "hunt", "hups", "hurd", "hurn", "hurt",
    "hush", "husk", "huss", "hust", "huts", "huzz", "jabe", "jabs",
    "jace", "jack", "jact", "jade", "jads", "jags", "jake", "jalb",
    "jalf", "jalm", "jalp", "jalt", "jams", "jane", "jang", "jank",
    "jans", "jarf", "jars", "jats", "jave", "jays", "jazz", "jebs",
    "jeds", "jegs", "jems", "jens", "jeps", "jets", "jibe", "jigs",
    "jive", "joab", "joan", "jobe", "jobs", "jock", "jods", "jogs",
    "joil", "join", "joir", "joke", "jolt", "joms", "jone", "jops",
    "josh", "jots", "jove", "joys", "jube", "jubs", "juce", "juct",
    "jude", "juds", "jufe", "juff", "jugs", "juke", "jule", "jume",
    "jump", "jums", "june", "junt", "jupe", "jups", "jure", "jusk",
    "juss", "just", "juts", "juve", "kale", "kate", "keel", "keen",
    "keep", "kibe", "kick", "kide", "kids", "kife", "kiff", "kilb",
    "kild", "kilf", "kilk", "kill", "kilm", "kiln", "kilp", "kilt",
    "kimp", "kims", "kind", "king", "kink", "kins", "kint", "kipe",
    "kips", "kire", "kirk", "kirp", "kiss", "kite", "kits", "kobe",
    "labe", "labs", "lace", "lack", "lact", "lade", "lads", "lafe",
    "laff", "lage", "lags", "laid", "lain", "lake", "lame", "lamp",
    "lams", "land", "lane", "lang", "lank", "lans", "lant", "lape",
    "laps", "lard", "lare", "lark", "larn", "lase", "lash", "lasp",
    "lass", "last", "late", "lave", "lawn", "laws", "lays", "laze",
    "lazz", "leab", "lead", "leak", "leal", "leam", "lean", "leap",
    "lear", "leas", "leat", "leax", "lebs", "leck", "lect", "leds",
    "lees", "leff", "left", "legs", "lemp", "lems", "lend", "lenk",
    "lens", "lent", "leps", "lept", "lerk", "lesk", "lesp", "less",
    "lest", "lets", "lewd", "lewn", "lezz", "libe", "libs", "lice",
    "lick", "lict", "lids", "lied", "lien", "lier", "lies", "life",
    "liff", "lift", "ligs", "like", "lill", "lime", "limp", "lims",
    "lind", "line", "ling", "link", "lins", "lint", "lipe", "lips",
    "lipt", "lire", "lise", "lisk", "lisp", "liss", "list", "lite",
    "lits", "live", "lizz", "load", "loam", "loan", "loap", "lobe",
    "lobs", "loch", "lock", "loct", "lode", "lods", "loff", "loft",
    "loge", "logs", "loin", "loip", "loir", "loit", "loke", "loll",
    "lols", "lome", "lomp", "loms", "lond", "lone", "long", "lonk",
    "lons", "lont", "look", "loom", "loon", "loop", "loot", "lope",
    "lops", "lopt", "lorb", "lord", "lore", "lorf", "lork", "lorm",
    "lorn", "lorp", "lort", "lose", "losh", "losk", "losp", "loss",
    "lost", "lote", "loth", "lots", "loud", "lout", "loux", "love",
    "lozz", "lube", "lubs", "luce", "luck", "lude", "luds", "lufe",
    "luft", "luge", "lugs", "luke", "lull", "lume", "lump", "lums",
    "lund", "lune", "lung", "lunk", "luns", "lunt", "lupe", "lups",
    "lure", "lurk", "lush", "lusk", "lusp", "lust", "lute", "luts",
    "luxe", "mabe", "mabs", "mace", "mach", "made", "mads", "mafe",
    "maff", "maft", "mage", "mags", "maid", "mail", "maim", "main",
    "make", "mald", "male", "malf", "malk", "mall", "maln", "malp",
    "mals", "malt", "mame", "mams", "mane", "mang", "mank", "mant",
    "mape", "maps", "mapt", "marb", "mare", "marf", "mark", "marp",
    "mars", "mart", "mase", "mash", "mask", "masp", "mass", "mast",
    "mate", "math", "mats", "maur", "maut", "maws", "maze", "mead",
    "meak", "meal", "mean", "meap", "mear", "meat", "mebs", "meck",
    "mect", "meds", "meed", "meek", "meff", "meft", "megs", "meld",
    "melk", "mell", "mels", "melt", "meme", "mems", "mend", "mene",
    "menk", "mens", "ment", "mept", "mere", "merk", "mern", "mert",
    "mesh", "mess", "meth", "mets", "mezz", "mibs", "mice", "mick",
    "mict", "mide", "mids", "miff", "mift", "migs", "mike", "mild",
    "mile", "milk", "mill", "miln", "milp", "milt", "mime", "mims",
    "mind", "mine", "ming", "mink", "mins", "mint", "mipe", "mipt",
    "mird", "mire", "mirp", "mish", "misk", "miss", "mist", "mite",
    "mith", "mits", "mizz", "moab", "moan", "mobe", "mobs", "mock",
    "mode", "mods", "moff", "moft", "moge", "mogs", "mold", "mole",
    "molk", "moll", "moln", "molp", "molt", "moms", "mond", "mong",
    "monk", "mont", "mood", "moon", "moor", "moos", "moot", "mope",
    "mops", "morb", "mord", "more", "morf", "mork", "morm", "morn",
    "morp", "mort", "mosh", "mosk", "mosp", "moss", "most", "mote",
    "moth", "mots", "move", "mozz", "mubs", "much", "muck", "mude",
    "muds", "mufe", "muff", "muft", "muge", "mugs", "muke", "mulb",
    "mule", "mulf", "mulk", "mull", "mulp", "muls", "mump", "mums",
    "mund", "munk", "muns", "munt", "mups", "mupt", "murb", "murd",
    "murf", "murk", "murm", "murp", "murt", "muse", "mush", "musk",
    "musp", "must", "mute", "muts", "nabe", "nabs", "nace", "nade",
    "nads", "nafe", "nage", "nags", "nail", "nake", "nale", "name",
    "nane", "nans", "nape", "naps", "nare", "nase", "nate", "nats",
    "nave", "near", "neat", "nebs", "neck", "nect", "neds", "need",
    "neff", "neft", "negs", "nell", "nems", "nens", "nepe", "neps",
    "nept", "nesh", "nesk", "nesp", "ness", "nest", "nets", "nezz",
    "nibs", "nice", "nick", "nide", "nids", "nigs", "nike", "nile",
    "nill", "nils", "nims", "nine", "nins", "nipe", "nips", "nire",
    "nish", "nite", "noam", "nobe", "nobs", "noce", "nock", "noct",
    "node", "nods", "noft", "noge", "nogs", "noke", "nold", "nolk",
    "noll", "nolm", "nolt", "nomp", "noms", "none", "nons", "nook",
    "noon", "nope", "nops", "nopt", "norb", "nord", "nore", "norf",
    "nork", "norm", "norn", "norp", "nors", "nort", "nose", "nosk",
    "nosp", "noss", "nost", "note", "noth", "nots", "noun", "nove",
    "nube", "nubs", "nuce", "nuds", "nuff", "nugs", "nuke", "nule",
    "null", "nume", "nums", "nune", "nuns", "nupe", "nups", "nurb",
    "nuss", "nuts", "nuzz", "pabs", "pace", "pack", "pact", "pade",
    "pads", "paff", "paft", "page", "pags", "paid", "pain", "pair",
    "pake", "pald", "pale", "pall", "palm", "palp", "pals", "palt",
    "pame", "pamp", "pams", "pand", "pane", "pang", "pank", "pans",
    "pant", "paps", "papt", "pard", "park", "parm", "parn", "pars",
    "part", "pase", "pask", "pasp", "pass", "past", "pate", "path",
    "pats", "paul", "pave", "pawn", "peak", "pear", "peas", "peat",
    "pebs", "peck", "peds", "peek", "peel", "peen", "peep", "peer",
    "pegs", "peld", "pelf", "pelk", "pell", "pelp", "pelt", "pemp",
    "pems", "pend", "peng", "penk", "pens", "pent", "peps", "perk",
    "perm", "perp", "pert", "pesp", "pete", "peth", "pets", "pibs",
    "pick", "pict", "pids", "pier", "pies", "piff", "pigs", "pike",
    "pile", "pill", "pilp", "pilt", "pime", "pims", "pind", "pine",
    "ping", "pink", "pins", "pint", "pipe", "pips", "pire", "pirm",
    "pite", "pith", "pits", "pive", "pizz", "plad", "plag", "plam",
    "plan", "plat", "plax", "pled", "pleg", "plen", "plep", "plet",
    "plex", "plim", "plin", "plit", "plix", "plod", "plog", "plop",
    "plot", "plox", "plub", "plud", "plug", "plum", "plun", "plup",
    "plur", "plus", "plut", "plux", "poas", "poat", "pobs", "pock",
    "poct", "pods", "poft", "pogs", "poid", "poke", "pold", "pole",
    "polk", "poll", "polm", "polp", "polt", "pome", "pomp", "poms",
    "pond", "pone", "pong", "ponk", "pons", "pont", "pool", "poon",
    "poor", "pope", "pops", "popt", "pord", "pore", "pork", "porp",
    "port", "pose", "posh", "posk", "posp", "poss", "post", "pote",
    "pots", "pour", "pout", "pram", "prep", "prop", "pube", "pubs",
    "puce", "puck", "puct", "pude", "puds", "puff", "pugs", "puke",
    "pulb", "pule", "pull", "puln", "pulp", "pult", "pume", "pump",
    "pums", "pund", "pune", "punk", "puns", "punt", "pupe", "pups",
    "pure", "purm", "purn", "purp", "purs", "purt", "puse", "push",
    "pute", "puth", "puts", "puve", "puzz", "quad", "quid", "quit",
    "rabe", "rabs", "race", "rack", "ract", "rade", "rads", "raff",
    "raft", "rage", "rags", "raid", "rail", "rain", "rake", "rald",
    "rale", "ralf", "ralk", "rall", "ralm", "ralp", "ralt", "rame",
    "ramp", "rams", "rand", "rane", "rang", "rank", "rans", "rant",
    "raps", "rapt", "rare", "rase", "rash", "rass", "rate", "rath",
    "rats", "raul", "rave", "rays", "raze", "razz", "read", "real",
    "ream", "reap", "rear", "rebe", "rebs", "reck", "rect", "rede",
    "reds", "reed", "reek", "reel", "reff", "reft", "rege", "rein",
    "reir", "rell", "remp", "rems", "rend", "renk", "rent", "reps",
    "rept", "resh", "resk", "resp", "ress", "rest", "rets", "rezz",
    "ribe", "ribs", "rice", "rich", "rick", "ride", "rids", "rife",
    "riff", "rift", "rigs", "rill", "rimp", "rims", "rind", "ring",
    "rink", "rins", "rint", "ripe", "rips", "rise", "rish", "risk",
    "risp", "riss", "rist", "rite", "rive", "rizz", "road", "roal",
    "roam", "roar", "robe", "robs", "roce", "rock", "roct", "rode",
    "rods", "roff", "roil", "role", "roll", "rome", "romp", "roms",
    "rond", "rone", "ronk", "rons", "ront", "rood", "rook", "rool",
    "room", "roon", "roos", "root", "rope", "ropt", "rose", "rosh",
    "ross", "rost", "rote", "roth", "rots", "rout", "rows", "rube",
    "rubs", "ruce", "ruck", "ruct", "rude", "ruds", "ruff", "ruft",
    "ruge", "rugs", "ruke", "rule", "rull", "rume", "rump", "rums",
    "rund", "rune", "rung", "runk", "runs", "runt", "rupe", "rups",
    "rupt", "rure", "ruse", "rush", "rusk", "rusp", "russ", "rust",
    "rute", "ruth", "ruts", "ruzz", "sabe", "sabs", "sack", "sact",
    "sade", "sads", "safe", "saff", "saft", "sage", "sags", "said",
    "sail", "sair", "sait", "sake", "sale", "salf", "sall", "salm",
    "salp", "salt", "same", "samp", "sams", "sand", "sane", "sang",
    "sank", "sans", "sant", "sape", "saps", "sard", "sare", "sarf",
    "sark", "sarm", "sarn", "sart", "sash", "sate", "sath", "sats",
    "saul", "save", "sawn", "saws", "says", "scab", "scad", "scag",
    "scam", "scan", "scap", "scar", "scat", "scax", "scon", "scot",
    "scub", "scud", "scun", "scur", "scut", "scux", "sead", "seal",
    "seam", "sean", "seap", "sear", "seas", "seat", "sebs", "seck",
    "secs", "sect", "seds", "seed", "seek", "seem", "seen", "seep",
    "seer", "sees", "seet", "seff", "seft", "segs", "self", "selk",
    "sell", "selm", "selp", "selt", "semp", "sems", "send", "seng",
    "senk", "sens", "sent", "seps", "sept", "serf", "serk", "serm",
    "serp", "seth", "sets", "sewn", "sews", "shed", "shin", "ship",
    "shop", "shot", "shut", "sibs", "sick", "sict", "side", "sids",
    "siff", "sift", "sigh", "sigs", "sild", "silk", "sill", "silm",
    "siln", "silp", "silt", "sims", "sind", "sing", "sink", "sint",
    "sips", "sipt", "sird", "sire", "sirk", "sirt", "siss", "site",
    "sith", "sits", "size", "sizz", "skid", "skim", "skin", "skip",
    "skis", "skit", "skix", "slab", "slad", "slag", "slam", "slan",
    "slap", "slar", "slat", "slax", "sled", "sleg", "slem", "slet",
    "slex", "slid", "slim", "slin", "slip", "slir", "slit", "slix",
    "slob", "slod", "slog", "slop", "slot", "slox", "slub", "slud",
    "slug", "slun", "slup", "slur", "slux", "snab", "snad", "snap",
    "snat", "snax", "sned", "snep", "sner", "snet", "snib", "snig",
    "snip", "snir", "snit", "snix", "snod", "snop", "snox", "snub",
    "snud", "snug", "snup", "snur", "snut", "snux", "soak", "soan",
    "soap", "soar", "soat", "sobs", "sock", "soct", "sods", "soft",
    "sogs", "soid", "soil", "soin", "soir", "sold", "sole", "solk",
    "soll", "solm", "some", "somp", "soms", "sone", "song", "sonk",
    "sons", "soon", "sope", "sops", "sopt", "sorb", "sore", "sorf",
    "sork", "sorm", "sorn", "sorp", "sort", "soss", "sots", "soul",
    "soup", "sour", "sout", "sowd", "sown", "sows", "spad", "spam",
    "span", "spar", "spas", "sped", "spem", "spen", "spin", "spop",
    "spot", "spox", "spud", "spun", "spur", "stab", "stag", "stan",
    "stap", "star", "sted", "steg", "stem", "sten", "step", "stet",
    "stex", "stid", "stin", "stir", "stop", "stox", "stub", "stud",
    "stun", "subs", "such", "suct", "suds", "suff", "suft", "sugs",
    "sulf", "sulk", "sull", "sulp", "sult", "sume", "sump", "sums",
    "sund", "sung", "sunk", "suns", "sunt", "sups", "sure", "surf",
    "surm", "surp", "sush", "susk", "suts", "swab", "swad", "swam",
    "swan", "swap", "swar", "swat", "swax", "swet", "swex", "swim",
    "swin", "swip", "swir", "swit", "swix", "swob", "swod", "swom",
    "swon", "swop", "swot", "swox", "swud", "swum", "swun", "swur",
    "swux", "tabe", "tabs", "tace", "tack", "tact", "tade", "tads",
    "taft", "tage", "tags", "taid", "tail", "take", "tald", "tale",
    "talk", "tall", "talp", "tame", "tamp", "tams", "tang", "tank",
    "tans", "tape", "taps", "tapt", "tarb", "tard", "tarf", "tark",
    "tarm", "tarn", "tarp", "tars", "tart", "tase", "task", "tasp",
    "tass", "tast", "tate", "tats", "tave", "teak", "teal", "team",
    "tear", "teas", "teat", "tebs", "tech", "teck", "teds", "teek",
    "teel", "teen", "teep", "teer", "tees", "teet", "teff", "tegs",
    "teld", "tele", "telk", "tell", "telp", "telt", "temp", "tems",
    "tend", "teng", "tenk", "tens", "tent", "teps", "terd", "terk",
    "term", "tern", "terp", "tert", "tesk", "tesp", "tess", "test",
    "tets", "than", "that", "them", "then", "thin", "this", "tibe",
    "tibs", "tice", "tick", "tict", "tide", "tids", "tied", "tiel",
    "tier", "ties", "tigs", "tike", "tile", "tilk", "till", "tilp",
    "tilt", "time", "timp", "tims", "tind", "ting", "tink", "tins",
    "tint", "tips", "tire", "tisk", "toad", "toal", "toat", "tobe",
    "tobs", "tock", "toct", "tode", "tods", "toff", "toft", "togs",
    "toil", "toin", "toke", "tolb", "told", "tole", "tolf", "tolk",
    "toll", "tolm", "toln", "tolp", "tolt", "tomp", "toms", "tond",
    "tone", "tong", "tonk", "tons", "tont", "took", "tool", "tops",
    "topt", "torb", "tore", "torm", "torn", "torp", "tort", "tosk",
    "toss", "tost", "tote", "toum", "tour", "town", "tows", "toys",
    "tram", "trap", "trim", "trip", "trod", "trot", "tube", "tubs",
    "tuck", "tude", "tuds", "tuft", "tugs", "tuld", "tulk", "tull",
    "tulp", "tult", "tump", "tums", "tune", "tung", "tunk", "tunt",
    "turb", "turd", "turf", "turk", "turn", "turp", "turt", "tusk",
    "tusp", "tust", "tuth", "tuts", "twig", "twin", "twix", "vabe",
    "vade", "vaid", "vail", "vain", "vale", "vane", "vape", "vase",
    "vast", "vate", "vaze", "veal", "vend", "vent", "verb", "verf",
    "vest", "vibe", "vice", "vick", "vile", "vill", "void", "volt",
    "vote", "wabe", "wack", "wact", "wade", "wads", "waff", "waft",
    "wage", "wait", "wake", "wald", "wale", "walf", "walk", "wall",
    "walm", "waln", "walp", "walt", "wamp", "wand", "want", "wapt",
    "ward", "ware", "warf", "wark", "warm", "warn", "warp", "wart",
    "wash", "wask", "wasp", "wass", "wast", "wave", "ways", "wazz",
    "weak", "wear", "weck", "wect", "weed", "week", "weep", "weff",
    "weft", "welb", "weld", "welf", "welk", "well", "welm", "weln",
    "welp", "welt", "wemp", "wend", "weng", "went", "wept", "werb",
    "were", "werf", "werk", "werm", "werp", "wert", "wesh", "wesk",
    "wesp", "wess", "west", "weth", "wezz", "what", "when", "whim",
    "whom", "wibe", "wick", "wict", "wide", "wife", "wiff", "wift",
    "wigs", "wilb", "wild", "wilf", "wilk", "will", "wilm", "wiln",
    "wilp", "wilt", "wimp", "wind", "wine", "wing", "wink", "wins",
    "wint", "wipe", "wipt", "wirb", "wird", "wire", "wirf", "wirk",
    "wirm", "wirp", "wirt", "wise", "wish", "wisk", "wisp", "wiss",
    "wist", "with", "wits", "wizz", "wock", "woct", "woff", "woft",
    "woke", "wolb", "wold", "wolf", "wolk", "woll", "wolm", "wolp",
    "wolt", "womp", "wond", "wong", "wont", "wood", "wool", "wopt",
    "worb", "word", "wore", "worf", "work", "worn", "worp", "wort",
    "wosk", "wosp", "woss", "wost", "woth", "wove", "wows", "wozz",
    "wrap", "wuct", "wuff", "wuft", "wulb", "wulf", "wulk", "wull",
    "wulm", "wulp", "wult", "wump", "wund", "wunk", "wunt", "wupt",
    "wurf", "wurm", "wurp", "wusk", "wuss", "wust", "wuth", "wuzz",
    "yoke", "yope"};

/*
 * Base 36 to base 10 conversion
 */
ll str_to_int(char *s, const char *chrmap, int ctr, int first_time)
{
    if ( s[ctr] == '.' )
    {
        return str_to_int(s, chrmap, ctr - 1, first_time);
    }

    if ( first_time )
    {
        unsigned int ii;
        for ( ii = 0; ii < strlen(s); ++ii )
        {

            if ( (! isalnum(toupper(s[ii]))) && (s[ii] != '.') )
            {
                return -1;
            }
        }
    }

    if ( ctr < 0 )
    {
        return 0;
    }
    ll diff = strchr(chrmap, toupper(s[ctr])) - chrmap;
    return diff + 36 * str_to_int(s, chrmap, ctr - 1, 0);
}

/*
 * Represent integer in base 36
 */
void base36(char *result, ll num, int cursor)
{
    if (num == 0)
    {
        memset(result, 0, cursor);
    }
    char c = NS[num % (ll) 36];
    memset(result, '0', cursor);

    result[cursor - 1] = c;
//    printf("MID: %s %d\n", result, strlen(result));
    if (cursor > 0)
    {
        base36(result, num / (ll) 36, cursor - 1);
    }
    return;
}



pt2d fit_point_on_uni_grid(pt2d p)
{
    char uni[_UNI_SIZE];
    geo_to_unistr(uni, p, NX, NY);
    p = unistr_to_geo(uni, NX, NY);
    return p;
}


area_desc_array read_config_file(char *filename)
{
    area_desc_array result;
    char * line = NULL;
    size_t len = 0;
    ssize_t read = 0;
    int ctr = 0;

    result.n = 0;

    FILE *f = fopen(filename, "r");
    if ( f == NULL )
    {
        return result;
    }

    while ( (read = getline(&line, &len, f)) != -1 )
    {
        char *mobile = line;
        char *token;
        int locctr = 0;
        area_desc kax;
        memset(kax.area_code, 0, 6);

        while ( (token = strsep(&mobile, ",")) )
        {
            switch (locctr)
            {
                case 0:
                    memset(kax.area_code, 0, 5);
                    snprintf(kax.area_code, 5, "%s", token);
                    break;
                case 1:
                    kax.xtopleft = atof(token);
                    break;
                case 2:
                    kax.ytopleft = atof(token);
                    break;
                case 3:
                    kax.xbottomright = atof(token);
                    break;
                case 4:                    
                    kax.ybottomright = atof(token);
                    break;
                case 5:
                    kax.ndigits = atoi(token);
                    break;
                default:
                    break;
            }
            locctr += 1;
            if (locctr >= 6)
            {
                pt2d p1;
                p1.x = kax.xtopleft;
                p1.y = kax.ytopleft;
                p1 = fit_point_on_uni_grid(p1);
                kax.xtopleft = p1.x;
                kax.ytopleft = p1.y;

                p1.x = kax.xbottomright;
                p1.y = kax.ybottomright;
                p1 = fit_point_on_uni_grid(p1);
                kax.xbottomright = p1.x;
                kax.ybottomright = p1.y;

                result.cities[result.n] = kax;
                result.n += 1;
                break;
            }
        }
        ctr += 1;
        if ( ctr >= MAX_NUM_AREAS )
        {
            break;
        }
    }
    fclose(f);

    /*
    for (int ii = 0; ii < result.n; ++ii )
    {
        printf("%s %Lf\n", result.cities[ii].area_code, result.cities[ii].xtopleft);
    }
    */
    return result;
}

/*
area_desc areas[NUM_AREAS] =
{
    {"BUD", 19, 48, 20, 47, 4},
    {"MIL", 16, 42, 17, 41, 5},
    {"LON", -0.5, 53, 0.6, 52.4, 5}
};
*/

int is_geo_in_area(pt2d point, area_desc *as)
{
    if ((point.x >= as -> xtopleft) && (point.x <= as -> xbottomright) &&
        (point.y <= as -> ytopleft) && (point.y >= as -> ybottomright))
    {
        return 1;
    }
    return 0;
}

/*
 * Test if a pair of coordinates belongs to a known area
 */
int is_geo_in_areas(pt2d point, area_desc *as, int nareas)
{
    int ii;
    for ( ii = 0; ii < nareas; ++ii )
    {
        if ( is_geo_in_area(point, as + ii) )
        {
            return ii;
        }
    }
    return -1;
}

/*
 * Test if a string is an area string
 */
int is_str_in_areas(const char *s, area_desc *as, int nareas)
{
    int ii;
    unsigned int jj;
    for ( ii = 0; ii < nareas; ++ii )
    {
        int fail = 0;
        for ( jj = 0; jj < strlen(as[ii].area_code); ++jj )
        {
            if (toupper(as[ii].area_code[jj]) != toupper(s[jj]))
            {
                fail = 1;
                break;
            }
        }
        if (!fail) { return ii; }
    }
    return -1;
}


/*
* Separate unistr from areastr
*/
int classify_str(const char *s)
{
    int state = 0; //0 only letters, 1 also dash, 2 alphanum, 3 invalid

    unsigned int ii;

    if ( strlen(s) < 8 )
    {
        return 3;
    }

    for ( ii = 0; ii < strlen(s); ++ii )
    {
        if (s[ii] == '.')
        {
            if ( state == 3 )
            {
                return 3;
            }
            if ( ii == 3 )
            {
                state = 1;
            }
            else if ( ii == 4 )
            {
                if ( state == 1 )
                {
                    return 3;
                }
                state = 2;
            }
            else if ( (ii > 5) && (state == 1) )
            {
                state = 3;
            }
            else
            {
                return 3;
            }
        }
        else if (! isalnum(s[ii]) )
        {
            return 3;
        }
    }

    if (state == 1) 
    { 
        int sl = strlen(s);
        if (sl > 10)
        {
            if ( sl <= _WS_SIZE )
            {
                return 4;
            }
            return 3;
        }
        return 1;
    }
    else if (state == 2)
    {
        return 0;
    }
    else if (state == 3)
    {
        int sl = strlen(s);
        if ( (sl >= 9) and (sl <= _WS_SIZE) )
        {
            return 4;
        }
        return 3;
    }
    //err if no dot in position 3 or 4
    return 3;
}

int d6_to_word(char *result, char *input, int lim)
{
    ll intval = str_to_int(input, NS, (int) strlen(input) - 1, 1);
    snprintf(result, lim, "%lld", intval);
    ll divor = L3LEN * L4LEN;
    int last3 = intval % 900;
    ll i2 = intval / 900;
    if (i2 >= divor)
    {
//        printf("i2=%d %s %lld %lld\n", last3, input, intval, divor);
        memset(result, 0, lim);
        return -1;
    }
//    ll i2 = intval % divor;

    int w2 = i2 % L3LEN;
    int w1 = i2 / L3LEN;
//    printf("%s %d %d %lld\n", input, w1, w2, intval);

    snprintf(result, lim, "%s%s.%03d", l4[w1], l3[w2], last3 + 100);
    return 0;
}

int local_code_to_word(char *result, char *input, int lim, area_desc *cities,
    int nares)
{
    memset(result, 0, lim);
    int ac = is_str_in_areas(input, cities, nares);
    if (ac == -1)
    {
        return -1;

    }
    int clen = strlen(cities[ac].area_code);
    char res2[lim];
    int status = d6_to_word(res2, input + clen + 1, lim);
    if (status == 0)
    {
        snprintf(result, lim, "%s.%s", cities[ac].area_code, res2);
    }
    int ii;
    for (ii = 0; ii < strlen(result); ++ii )
    {
        result[ii] = toupper(result[ii]);
    }
    return status;
}

int word_bin_search(char *input, int matchlen, const char **array, int arrlen)
{
    int result = -1;
    int loind = 0;
    int hiind = arrlen - 1;
    int mid1, mid2;
    int repeat = 0;

    while (hiind > loind)
    {
        mid1 = round((hiind + loind) / 2.0);
        if ( repeat > 0)
        {
            return -1;
        }
        else if ( repeat )
        {
            mid1 = (hiind + loind) / 2;
        }

        int cmpval = strncasecmp(input, array[mid1], matchlen);
        if (cmpval == 0)
        {
            break;
        }

        if (mid1 == hiind)
        {
            repeat = 1;
        }
        else if (cmpval < 0)
        {
            hiind = mid1;
        }
        else
        {
            loind = mid1;
        }
    }
    return mid1;
}

//result must be char[7] or longer
int word_to_d6(char *result, char *data, char *ref)
{
    memset(result, 0, 7);
    if (strlen(data) != 11)
    {
        return -1;
    }
    char w1[5], w2[4];
    memset(w1, 0, 5);
    memset(w2, 0, 4);
    memcpy(w1, data, 4);
    memcpy(w2, data + 4, 3);
    int last3 = atoi(data + 8);
    if ( (last3 < 100) || (last3 > 999) )
    {
        return -1;
    }
    int ii;
    for (ii = 0; ii < 4; ++ii)
    {
        w1[ii] = tolower(w1[ii]);
        w2[ii] = tolower(w2[ii]);
    }
    int wbs1 = word_bin_search(w1, 4, l4, L4LEN);
    int wbs2 = word_bin_search(w2, 3, l3, L3LEN);

//    ll prod = (wbs1 * L3LEN + wbs2) + last3 * L3LEN * L4LEN;
    ll prod = (wbs1 * L3LEN + (ll) wbs2) * 900 + (last3 - 100);
//    printf("PROD %lld\n", prod);
    //manual base36 conversion, base36 gives memory error for some reason
    int cursor = 5;
    int toput;
    while (cursor >= 0)
    {
        toput = prod % (ll) 36;
        prod = prod / (ll) 36;
        result[cursor] = NS[toput];
        cursor -= 1;
    }
    return 0;
}

int word_to_local_code(char *result, char *input, int lim, area_desc *cities,
    int nares)
{
    memset(result, 0, lim);
    int ac = is_str_in_areas(input, cities, nares);
    if (ac == -1)
    {
        return -1;

    }
    int clen = strlen(cities[ac].area_code);
    char res2[7];
    int status = word_to_d6(res2, input + clen + 1, result);
    if (status == 0)
    {
//        snprintf(result,lim, "%s", cities[ac].area_code);
//        snprintf(result,lim, "%s", res2);
        snprintf(result, lim, "%s.%s", cities[ac].area_code, res2);
    }
    return status;
}


int geo_to_areastr(char *result, pt2d point0, area_desc *ar, int nares, 
    int nx, int ny, int fit_to_uni_grid)
{
    if ((point0.y < -90) || (point0.y > 90) || (point0.x < -180) || 
        (point0.x > 180))
    {
        snprintf(result, _UNI_SIZE, "%s", "");
        return -1;
    }

    int ac = is_geo_in_areas(point0, ar, nares);

    if (ac == -1)
    {
        return ac;
    }

    area_desc fnd = ar[ac];

    pt2d point = point0;
    if ( fit_to_uni_grid )
    {
        point = fit_point_on_uni_grid(point0);
    }

//    ld medlat = (fnd.ytopleft + fnd.ybottomright) / 2.0;
    ld avg_ang_width = (fnd.xbottomright - fnd.xtopleft);

    ld ang_height = (fnd.ytopleft - fnd.ybottomright);

//    printf("ml / aw, aw %Lf %Lf %Lf \n", medlat, avg_ang_width, 
//        ang_height);

    ld ratio = ang_height / avg_ang_width;

    ll multitude = (ll)pow(36, fnd.ndigits);
    ll xnum = trunc(sqrt(0.99999 * multitude / ratio)); //FIXME 0.99 for safety
    ll ynum = trunc(ratio * xnum); //FIXME 0.99 for safety

    ld xstep = avg_ang_width / xnum;
    ld ystep = ang_height / ynum;

    ll xco = round((point.x - fnd.xtopleft) / xstep);
    ll yco = round((fnd.ytopleft - point.y) / ystep);

    /*
    printf("\tG2A:xnum/ynum: %lld / %lld, xco / yco: %lld / %lld\n", 
        xnum, ynum, xco, yco);
    printf("MUL222=%lld, %lld, %lld, %lld %.12Lf %.12Lf\n", 
        multitude, xnum, xco, yco, xstep, ystep);
    printf("FND: x/y %Lf %Lf\n", fnd.xtopleft, fnd.ytopleft);
    */

    char b36str[_UNI_SIZE];
    memset(b36str, 0, _UNI_SIZE);

    base36(b36str, yco * xnum + xco, fnd.ndigits);
//    printf("\t\t -> %lld, .. %s\n", yco * ynum + xco, b36str);
    snprintf(result, _LOC_SIZE, "%s.%s", fnd.area_code, b36str);
    return 0;
}

/*
 * Convert area string to geo coordinates
 */
pt2d areastr_to_geo(char *s, area_desc *ar, int nares, int nx, int ny,
    int fit_to_uni_grid)
{
    int cs = classify_str(s);
    if (cs == 3)
    {
        pt2d badres = {DEFAULT_BAD_LATLONG, DEFAULT_BAD_LATLONG};
        return badres;
    }

    int ac = is_str_in_areas(s, ar, nares);

    if (ac == -1)
    {
        pt2d badres = {DEFAULT_BAD_LATLONG, DEFAULT_BAD_LATLONG};
        return badres;
    }

//    printf("ENTERING A2G: %s\n", s);

    area_desc fnd = ar[ac];

//    ld medlat = (fnd.ytopleft + fnd.ybottomright) / 2.0;
    ld avg_ang_width = (fnd.xbottomright - fnd.xtopleft);

    ld ang_height = (fnd.ytopleft - fnd.ybottomright);

    ld ratio = ang_height / avg_ang_width;

    ll multitude = (ll)pow(36, fnd.ndigits);
    ll xnum = trunc(sqrt(0.99999 * multitude / ratio)); //FIXME 0.99 for safety
    ll ynum = trunc(ratio * xnum); //FIXME 0.99 for safety

    ld xstep = avg_ang_width / xnum;
    ld ystep = ang_height / ynum;

    char *s2 = s + strlen(fnd.area_code) + 1;
//    string loc = s.substr(strlen(fnd.area_code) + 1, string::npos);
//    printf("LOCC=%s\n", s2);
    
    ll intval = str_to_int(s2, NS, (int) strlen(s2) - 1, 1);
    if ( intval < 0 )
    {
        pt2d ret = {DEFAULT_BAD_LATLONG, DEFAULT_BAD_LATLONG};
        return ret;
    }

    ll xco = intval % xnum;
    ll yco = intval / xnum;

    /*
    printf("\t xnum/ynum: %lld / %lld, iv=%lld, xco / yco: %lld / %lld\n", 
        xnum, ynum, intval, xco, yco);

    printf("MUL222=%lld, %lld, %lld, %lld %.12Lf %.12Lf\n", 
        multitude, xnum, xco, yco, xstep, ystep);
    */    

    pt2d result;

    result.x = xco * xstep + fnd.xtopleft;
    result.y = fnd.ytopleft - yco * ystep;

//    printf("TEMP: %Lf %Lf\n", result.x, result.y);
    if ( fit_to_uni_grid )
    {
        result = fit_point_on_uni_grid(result);
    }

//    printf("MUL=%Lf %Lf\n", result.x, result.y);
    return result;
}


int unistr_to_areastr(char *result, char *s, area_desc *ars, int nares, 
    int nx, int ny)
{
    int cs = classify_str(s);
    if (cs == 3)
    {
        return -1;
    }

    pt2d point;

    point = unistr_to_geo(s, nx, ny);
//    printf("GEOMAN: %Lf %Lf\n", point.x, point.y);

    if ( is_geo_in_areas(point, ars, nares) == -1 )
    {
        strcpy(result, s);
        return 1;
    }

    geo_to_areastr(result, point, ars, nares, nx, ny, 1);
    return 0;
    
}


int areastr_to_unistr(char *result, char *s, area_desc *ars, int nares,
    int nx, int ny)
{
    int cs = classify_str(s);
    if (cs == 3)
    {
        return -1;
    }

    if ( is_str_in_areas(s, ars, nares) == -1 )
    {
        strcpy(result, s);
        return 1; //#FIXME we assume it's a unistr
    }

    pt2d point;

    point = areastr_to_geo(s, ars, nares, nx, ny, 0);

    return geo_to_unistr(result, point, nx, ny);
}

taggy_results geo_to_taggy(pt2d point, area_desc *ars, int nares,
    int nx, int ny, int ndecimal)
{
    ndecimal = 20;
    taggy_results result;

    memset(result.unistr, 0, _UNI_SIZE);
    memset(result.areastr, 0, _LOC_SIZE);
    memset(result.wordstr, 0, _WS_SIZE);
    result.status = 0;

    long double  rounder = pow(10, ndecimal);
    point.x = round(point.x * rounder) / rounder;
    point.y = round(point.y * rounder) / rounder;

    int r2 = geo_to_unistr(result.unistr, point, nx, ny);

    if ( r2 == -1 )
    {
        result.status = 0;
        return result;
    }

    pt2d point2 = point;
//    if ( fit_to_uni_grid )
    if ( 0 )
    {
        point2 = fit_point_on_uni_grid(point);
    }
    int r1 = geo_to_areastr(result.areastr, point2, ars, nares, nx, ny, 0);
    if ( r1 == -1 )
    {
        result.status = 1;
        return result;
    }

    int r3 = local_code_to_word(result.wordstr, result.areastr, _WS_SIZE, 
        ars, nares);
    if ( r3 == -1 )
    {
        result.status = 2;
        return result;
    }

    result.status = 3;
    return result;
}


taggy_results2 taggy_decode(char *s0, area_desc *ars, int nares,
    int nx, int ny, int ndecimal) 
        // status: 0 bad, 1 input was uni, but no corresponding
        //local, 2: all is well (input either uni or geo)
{
    ndecimal = 20;
    taggy_results2 result;

    memset(result.unistr, 0, _UNI_SIZE);
    memset(result.areastr, 0, _LOC_SIZE);
    memset(result.wordstr, 0, _WS_SIZE);
    result.status = 0;
    result.lat = DEFAULT_BAD_LATLONG;
    result.lng = DEFAULT_BAD_LATLONG;

    int cs = classify_str(s0);
    if ( cs == 3 )
    {
        return result;
    }

    pt2d point;
    if ( (cs == 1) || (cs == 4) ) //it's a local code
    {
        char s[_LOC_SIZE];
        snprintf(s, _LOC_SIZE, "%s", s0);
        if ( cs == 4 )
        {
            int st = word_to_local_code(s, s0, _LOC_SIZE, ars, nares);
            if ( st == -1 )
            {
                result.status = 0;
                return result;
            }
            snprintf(result.wordstr, _WS_SIZE, "%s", s0);
        }
        point = areastr_to_geo(s, ars, nares, nx, ny, 0);
        if ( (point.x < -180) || (point.x > 180) || (point.y < -90) || 
                (point.y > 90) ) //could not convert, invalid area code
        {
            return result; //return bad
        }
        long double rounder = pow(10, ndecimal);
//        printf("BEF %Lf %Lf\n", point.x, point.y);
        point.x = round(rounder * point.x) / rounder;
        point.y = round(rounder * point.y) / rounder;
        result.lat = point.y;
        result.lng = point.x;
        
        snprintf(result.areastr, _LOC_SIZE, "%s", s);

        geo_to_unistr(result.unistr, point, nx, ny);

        if ( cs != 4 )
        {
            int rs = local_code_to_word(result.wordstr, s, _WS_SIZE, ars, 
                nares);
            if ( rs == -1 )
            {
                result.status = 0;
                return result;
            }
            result.status = 2;
        }
        else
        {
            result.status = 3;
        }

        return result;
    }
    else // cs == 0, global string
    {
        point = unistr_to_geo(s0, nx, ny);
        if ( (point.x < -180) || (point.x > 180) || (point.y < -90) || 
                (point.y > 90) ) //could not convert, invalid uni code
        {
            return result;
        }
        long double rounder = pow(10, ndecimal);
        point.x = round(rounder * point.x) / rounder;
        point.y = round(rounder * point.y) / rounder;

        result.lat = point.y;
        result.lng = point.x;

        snprintf(result.unistr, _UNI_SIZE, "%s", s0);

        int r = geo_to_areastr(result.areastr, point, ars, nares, nx, ny, 1);
        result.status = 1;
        if ( r == -1 )
        {
            snprintf(result.areastr, _LOC_SIZE, "%s", "");
        }
        else
        {
            r = word_to_local_code(result.wordstr, result.areastr, _WS_SIZE,
                ars, nares);

            if ( r == -1 )
            {
                result.status = 0;
            }
        }
        return result;
    }
}




/*
 * Place points on grid
 */
gridpoint gridize(pt2d point, int nx, int ny)
{
    ld yunit = 90.0 / (ld) ny;
    ld xunit = 360.0 / (ld) nx;
    ld ycnt = point.y / yunit;
    ld ygrid = round(ycnt) * yunit;
    if (ygrid <= -90.0)
    {
        ygrid = -89.9999999;
    }
    else if (ygrid >= 90.0)
    {
        ygrid = 89.9999999;
    }

    ld latshrink = cos(M_PI * ygrid / 180.0);
//    ld perim = nx * latshrink;

    ld newx = latshrink * (point.x + 180.0);
    ld xcnt = newx / xunit;

    ld xgrid = round(xcnt) * xunit;
    gridpoint result;
    result.x = xgrid;
    result.y = ygrid;
    result.xcnt = xcnt;
    return result;
}

/*
 * Calculate offset corresponding to latitude
 */
ll x_offset(ld y, int nx, int ny)
{
    if ((y > 90) || (y <= -90))
    {
        return 0;
    }
    int cnst = 10;

    if (y < 0)
    {
        ld amp = nx * (ll) ny;
        ll bo = (ll) (2 * amp / M_PI + cnst * ny + 1);
        ll i = round(y / (-90.0 / (ld) ny));
        ll offset = (ll)(ceil((2 * amp / M_PI) * 
            sin(0.5 * M_PI * (i+1) / (ld) ny)) + cnst * (i+1));
        return bo + offset;
    }
    ll i = round(y / (90.0 / (ld) ny));
    ld amp = nx * (ll) ny;
    ll offset = (ll)(ceil((2 * amp / M_PI) * 
        sin(0.5 * M_PI * (i+1) / (ld) ny)) + cnst * (i+1));
    return offset;
}

/*
 * Produce string from geo coordinates
 */
int geo_to_unistr(char *result, pt2d point, int nx, int ny)
{
    if ((point.y < -90) || (point.y > 90) || (point.x < -180) || 
        (point.x > 180))
    {
        snprintf(result, _UNI_SIZE, "%s", "");
        return -1;
    }

    gridpoint gridded = gridize(point, nx, ny);
    ll xo = x_offset(gridded.y, nx, ny);
    ll kk = (ll)round(gridded.xcnt);
    char b36str[_UNI_SIZE];
    memset(b36str, 0, _UNI_SIZE);
    memset(result, '0', _UNI_SIZE - 1);
    base36(b36str, xo + kk, _UNI_SIZE - 2);
//    printf("GOFOR %ld %s\n", xo+kk, b36str);
    snprintf(result, _UNI_SIZE, "%c%c%c%c.%c%c%c%c", b36str[0], b36str[1],
        b36str[2], b36str[3], b36str[4], b36str[5], b36str[6], b36str[7]);
    return 0;
}

/*
 * Binary search
 * val is the target value, mna/mxa define the range.
 */
pt2d binsearch(ll val, ld mna, ld mxa, int nx, int ny)
{
    int mn = round((mna / 90.0) * ny);
    int mx = round((mxa / 90.0) * ny);

    int mid1;
    ld mid;
    ll offs;

    int is_repeat = 0;
    int itctr = 0;
    ld perim;

    while ( 1 )
    {
        mid1 = round((mn + mx) / 2.0);
        if ( is_repeat )
        {
            mid1 = (mn + mx) / 2;
        }
        mid = 90.0 * mid1 / (ld) ny;

        offs = x_offset(mid, nx, ny);
        perim = nx * cos(M_PI * mid / (ld) 180.0);
        if ((offs <= val) && (offs + perim >= val))
        {
            break;
        }

        if (mid1 == mx)
        {
            is_repeat = 1;
        }
        else if (offs < val)
        {
            mn = mid1;
        }
        else
        {
            mx = mid1;
        }

        itctr += 1;
        if (itctr > 100)
        {
//            printf("BAD\n");
            break;
        }

    }

    ll xr = round(val - offs);
    if (xr > perim)
    {
//        printf("STRANGE!!!\n");
    }
    ld xd = (360.0 * xr / perim) - 180.0;
    pt2d result;
    result.x = xd;
    result.y = mid;
    return result;

}

/*
 * Convert string to a pair of geo coordinates
 */
pt2d unistr_to_geo(char *s, int nx, int ny)
{
    int cs = classify_str(s);
//    printf("str=%s, cs=%d\n", s, cs);
    if ((cs != 0) && (cs != 2))
    {
        pt2d result = {DEFAULT_BAD_LATLONG, DEFAULT_BAD_LATLONG};
        return result;
    }

    ll val = str_to_int(s, NS, (int) strlen(s) - 1, 1);
//    printf("VAL=%ld\n", val);
    if ( val < 0 )
    {
        pt2d ret = {DEFAULT_BAD_LATLONG, DEFAULT_BAD_LATLONG};
        return ret;
    }

    ll halver = x_offset(-0.00000001, nx, ny);

    pt2d result;
    if (val < halver)
    {
        result = binsearch(val, (ld) 0.0, (ld) 90.0, nx, ny);
    }
    else
    {
        result = binsearch(val, (ld) -0.00000001, (ld) -90.0, nx, ny);
    }
    return result;
}

/*
 * Measure geo distance between two points
 */
ld geo_dist(pt2d pt1, pt2d pt2)
{
    ld x1 = pt1.x;
    ld x2 = pt2.x;
    ld y1 = pt1.y;
    ld y2 = pt2.y;

    ld y12 = fabsl(y1);
    ld y22 = fabsl(y2);

    ld mx = y12;
    if (y22 > mx)
    {
        mx = y22;
    }
    ld xd = fabsl(x2 - x1);
    if (xd > 180)
    {
        xd = 360.0 - xd;
    }
    ld xdp = M_PI * 2 * cos(mx * M_PI / 180.0) * xd / 360.0;
    ld y1p = M_PI * y1 / 180.0;
    ld y2p = M_PI * y2 / 180.0;
    return ER * sqrt(xdp*xdp + (y2p-y1p)*(y2p-y1p));
}

void unit_test(void)
{
    ld x, y;
    int ii;
    for ( ii = 0; ii < 500000; ++ii )
    {
        x = (ld) ((rand() / (float) RAND_MAX)*360.0 - 180.0);
        y = (ld) ((rand() / (float) RAND_MAX)*180.0 - 90.0);
        pt2d pt;
        pt.x = x;
        pt.y = y;
//        gridpoint gr = gridize(pt, NX, NY);

        char b36[_UNI_SIZE];
        geo_to_unistr(b36, pt, NX, NY);
        pt2d rev = unistr_to_geo(b36, NX, NY);
        ld dd = geo_dist(pt, rev);
        if (dd > 9.99)
        {
            printf("Point1: %Lf/%Lf, Reco: %Lf/%Lf, dist=%Lf\n",
                pt.x, pt.y, rev.x, rev.y, dd);
        }
    }
}

void unit_test2(void)
{
    ld x, y;
    int ii;
    float x0 = 18.910989;
    float y0 = 47.3545011;
    float x1 = 19.3184702;
    float y1 = 47.6147324;
    char confname[] = "conf.txt";
    area_desc_array ars = read_config_file(confname);
    int skips = 0;
    for ( ii = 0; ii < 500000; ++ii )
    {
        x = (ld) (x0 + (rand() / (float) RAND_MAX)*(x1 - x0));
        y = (ld) (y0 + (rand() / (float) RAND_MAX)*(y1 - y0));
        pt2d pt;
        pt.x = x;
        pt.y = y;
//        gridpoint gr = gridize(pt, NX, NY);
        taggy_results tr1 = geo_to_taggy(pt, ars.cities, ars.n, NX, NY, 6);
        if (tr1.status != 3)
        {
            skips += 1;
            continue;
        }
//        printf("local Taggy: %s\n", tr1.areastr);

        taggy_results2 tr2 = taggy_decode(tr1.areastr, ars.cities, ars.n, 
                NX, NY, 6);
//        printf("R2 lat/long %Lf / %Lf\n", tr2.lat, tr2.lng);
        ld dy = tr2.lat - pt.y;
        ld dx = tr2.lng - pt.x;
        if ((fabs(dx) > 0.00001) || (fabs(dy) > 0.00001))
        {
            printf("CACK: %Lf %Lf %s %d\n", dx, dy, tr1.areastr, tr1.status);

            printf("%Lf %Lf %Lf %Lf\n", pt.x, pt.y, tr2.lng, tr2.lat);
            exit(1);
        }
    }
    printf("SKIPS %d\n", skips);
}


int main2(int argc, char **argv)
{
	char confname[] = "conf.txt";
	area_desc_array ars = read_config_file(confname);

	// the input is a string
	// eg: ./geoconv "5FOL.ON5R"
	// eg: ./geoconv "BUD.ON5R"
	// eg: ./geoconv "ZZZZ.ZZZZ" // should result a not-valid Taggy Code result
	// eg: ./geoconv "Budapest, Kelemen utca 57." // should result a not-valid Taggy Code result
	if (argc == 2) 
        {
            unsigned int ii;
            char *inpstr = argv[1];
            for ( ii = 0; ii < strlen(inpstr); ++ii )
            {
                inpstr[ii] = toupper(inpstr[ii]);
            }

            taggy_results2 res2;

            printf("\t...working from input str\n");
            res2 = taggy_decode(inpstr, ars.cities, ars.n, 
                NX, NY, 6);
            printf("\tRes status: %d", res2.status);
            switch (res2.status)
            {
                case 0: printf(" -- ERROR!\n"); exit(1);

                case 2: printf(" -- area code\n"); break;
                
                case 3: printf(" -- word code\n"); break;

                case 1: printf(" -- universal code\n"); break;
            }


            printf("\tInferred GEO codes: lat=%Lf, lng=%Lf\n", 
                res2.lat, res2.lng);
            printf("\tInferred universal code: %s\n", res2.unistr);
            printf("\tInferred local code: %s\n", res2.areastr);
            printf("\tInferred word code: %s\n", res2.wordstr);

            pt2d point;
            point.x = res2.lng;
            point.y = res2.lat;

            printf("input lat: %Lf\ninput long: %Lf\n\n", point.x, point.y);

            taggy_results result;
            result = geo_to_taggy(point, ars.cities, ars.n, NX, NY, 16);
            printf("result INT: %i\n", result.status);
            printf("local Taggy: %s\n", result.areastr);
            printf("Word code: %s\n", result.wordstr);

            int lclen = 20;
            char backd6res[lclen];
            int locnum = word_to_local_code(backd6res, result.wordstr, lclen, 
                ars.cities, ars.n); 
            printf("\tverifying local/word conversion: ");

            if (locnum < 0)
            {
                printf("FAILED\n");
            }
            else
            {
                printf("OK, result is %s\n", backd6res);
            }

	}

	// the input is a lat and long
	// eg: ./geoconv 47.4797411 19.0650528 // should result a valid universal and local Taggy Code
	// eg: ./geoconv 37.334912 -122.033057 // should result a valid universal Taggy Code
	// eg: ./geoconv -7500.353871, -1700.222269 // should result a not-valid Taggy Code result
	else if (argc == 3) {
		pt2d point;
		point.x = (ld) atof(argv[1]);
		point.y = (ld) atof(argv[2]);

		printf("input lat: %Lf\ninput long: %Lf\n\n", point.x, point.y);
//		char code[15];
//		geo_to_unistr(code, point, NX, NY);
//		printf("result Taggy: %s\n", code);

		taggy_results result;
		result = geo_to_taggy(point, ars.cities, ars.n, NX, NY, 6);
		printf("result INT: %i\n", result.status);
		printf("local Taggy: %s\n", result.areastr);
		printf("universal Taggy: %s\n", result.unistr);
                printf("Word form: %s\n", result.wordstr);

/*
                int lclen = 20;
                char d6res[lclen];
                int locst = local_code_to_word(d6res, result.areastr, lclen, 
                    ars.cities, ars.n);
    //            int d6st = d6_to_word(d6res, result.areastr, 10);
    //            printf("\tin words: %s\n", d6st);
                if (locst < 0)
                {
                    printf("Could not convert to word!\n");
                }
                else
                {
                    printf("\tin words: %s\n", d6res);
                    char backd6res[lclen];
                    int locnum = word_to_local_code(backd6res, d6res, 15, ars); 
                    if (locnum < 0)
                    {
                        printf("\tcould not convert back\n");
                    }
                    else
                    {
                        printf("and back: %s\n", backd6res);
                    }
                }
 */

                printf("\nNow reverting:\n");

                taggy_results2 res2;

                if (result.status == 1) //only universal code available
                {
                    printf("\t...working from universal code\n");
                    res2 = taggy_decode(result.unistr, ars.cities, ars.n, 
                        NX, NY, 6);
                    printf("\tRes status: %d\n", res2.status);
                    printf("\tInferred GEO codes: lat=%Lf, lng=%Lf\n", 
                        res2.lat, res2.lng);
                }
                else if ((result.status == 2) || (result.status == 3))
                    //area also available
                {
                    printf("\t...working from area code (to test)\n");
                    res2 = taggy_decode(result.areastr, ars.cities, ars.n, 
                        NX, NY, 6);
                    printf("\tRes status: %d\n", res2.status);
                    printf("\tInferred GEO codes: lat=%Lf, lng=%Lf\n", 
                        res2.lat, res2.lng);
                    printf("\tInferred universal code: %s\n", res2.unistr);
                    printf("\tBackward word code: %s\n", res2.wordstr);

                    if ( result.status == 3 )
                    {
                        printf("\n===\n\tNOW THE SAME FROM WORD CODE\n");
                        res2 = taggy_decode(result.wordstr, ars.cities, ars.n, 
                            NX, NY, 6);
                        printf("\tRes status: %d\n", res2.status);
                        printf("\tInferred GEO codes: lat=%Lf, lng=%Lf\n", 
                            res2.lat, res2.lng);
                        printf("\tInferred universal code: %s\n", res2.unistr);
                        printf("\tBackward word code: %s\n", res2.wordstr);


                    }
//                    printf("\tInferred local code: %s\n", res2.areastr);
                }
                else
                {
                    printf("\t...changed my mind, bad ststus, sorry\n");
                }

	}

//    /* Switch this on for unit testing */
//    unit_test();

    return 0;
}


int main(int argc, char **argv)
{
    return main2(argc, argv);
//    unit_test2();
    exit(0);

    char confname[] = "conf.txt";
    area_desc_array ars = read_config_file(confname);
    printf("%s %Lf %Lf %Lf %Lf", ars.cities[0].area_code,
        ars.cities[0].xtopleft,
        ars.cities[0].ytopleft,
        ars.cities[0].xbottomright,
        ars.cities[0].ybottomright);

    pt2d ad[3] = {{19.02702, 47.5204}, {29.5, 47.5},{16.6, 41.5}};
    printf("\n%d kax\n", is_geo_in_areas(ad[0], ars.cities, ars.n));
    printf("\n%d kax\n", is_geo_in_areas(ad[0], ars.cities, ars.n));
    printf("%d k2\n", is_geo_in_areas(ad[1], ars.cities, ars.n));
    printf("%d k2\n", is_geo_in_areas(ad[2], ars.cities, ars.n));

    char aad1[_LOC_SIZE], aad2[_LOC_SIZE];
    printf("1 %p\n", aad1);
    geo_to_areastr(aad1, ad[0], ars.cities, ars.n, NX, NY, 1);
    printf("2\n");
    geo_to_areastr(aad2, ad[1], ars.cities, ars.n, NX, NY, 1);
    printf("%s\n", aad1);
    printf("%s\n\n\n", aad2);

    int ii;

    for (ii = 0; ii < 3; ++ii )
    {
        printf("ORIG: LAT=%Lf, LON=%Lf\n", ad[ii].x, ad[ii].y);
        printf("\t%d\n", is_geo_in_areas(ad[ii], ars.cities, ars.n));
        char ast[_LOC_SIZE];

        geo_to_areastr(ast, ad[ii], ars.cities, ars.n, NX, NY, 1);
        printf("AST=%s\n", ast);
        printf("BACK AREA: %d\n", is_str_in_areas(ast, ars.cities, ars.n));
        pt2d a2g = areastr_to_geo(ast, ars.cities, ars.n, NX, NY, 1);
        printf("A2G: %Lf, %Lf\n", a2g.x, a2g.y);
        char ust[12], bust[12];
        areastr_to_unistr(ust, ast, ars.cities, ars.n, NX, NY);
        printf("KAX1 %s\n", ust);
        unistr_to_areastr(bust, ust, ars.cities, ars.n, NX, NY);

        printf("two more strings: %s, %s\n", ust, bust);
    }

    printf("%d\n", classify_str("geo"));
    printf("%d\n", classify_str("geo.d389a"));
    printf("%d\n", classify_str("ge.4d389a"));
    printf("%d\n", classify_str("geo.d38.9a"));
    printf("%d\n", classify_str("geod389a"));
    printf("%d\n", classify_str("geo"));


    if (argc < 3)
    {
        printf("Usage: %s x y\n", argv[0]);
        return 1;
    }

    pt2d point;
    point.x = (ld) atof(argv[1]);
    point.y = (ld) atof(argv[2]);

    printf("Input was: X=%Lf, Y=%Lf\n", point.x, point.y);
    char code[_LOC_SIZE];
    printf("B36=%p\n", code);
    geo_to_unistr(code, point, NX, NY);
    printf("Code: %s\n", code);

    pt2d rev = unistr_to_geo(code, NX, NY);

    printf("Revert from code: X=%Lf, Y=%Lf\n", rev.x, rev.y);

    ld d = geo_dist(point, rev);
    printf("The distance is %Lf (m)\n", d);

    /* Switch this on for unit testing */
    unit_test();

    return 0;
}
