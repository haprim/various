sets = {
    'v': 'aeiou',
    'D': ['ai', 'ay', 'au', 'aw', 'ea', 'ee', 'ei', 'ey', 'eu', 'ew',
            'ie', 'oa', 'oi', 'oy', 'oo', 'ou', 'ow'],

    'c': 'bcdfghjklmnprstvwxyz', 
    'y': 'bcdfghklmnprstvwxz', 

    'K': ['sp', 'st', 'sk', 'sc', 'sm', 'sn', 'sl', 'sw', 'pl', 'bl', 'fl', 'cl', 'gl', 'pr', 'br', 'fr', 'tr', 'dr', 'cr', 'gr', 'tw', 'dw', 'qu', 'ch', 'sh', 'th', 'gh', 'ph', 'wh', 'wr'],

    'z': 'bdgklmnprstx',

    'G': ['mp', 'nt', 'nd', 'nk', 'ng', 'lp', 'lb', 'lf', 'lm', 'lt', 'ld', 'ln', 'lk', 'rp', 'rb', 'rf', 'rm', 'rt', 'rd', 'rn', 'rk', 'sp', 'st', 'sk', 'ft', 'pt', 'ct', 'ck', 'th', 'ph', 'ch', 'sh', 'gh', 'll', 'ss', 'ff', 'zz'],

    'L': ['bs', 'ds', 'gs', 'ks', 'ls', 'ms', 'ns', 'ps', 'rs', 'ts', 'cs', 'fs', 'ks']
}


def gen(combo):
    res = []

    rl = len(combo[0])
    mem = [[] for _ in range(rl)]

    for i in range(rl):
        if combo[1][i] == '.':
            st = sets[combo[0][i]]
            mem[i] = [x for x in st]
        else:
            mem[i] = [combo[1][i]]

    if rl == 3:
        for e in mem[0]:
            for e2 in mem[1]:
                for e3 in mem[2]:
                    res.append('{0}{1}{2}'.format(e, e2, e3))
    elif rl == 2:
        for e in mem[0]:
            for e2 in mem[1]:
                    res.append('{0}{1}'.format(e, e2))
    elif rl == 4:                    
        for e in mem[0]:
            for e2 in mem[1]:
                for e3 in mem[2]:
                    for e4 in mem[3]:
                        res.append('{0}{1}{2}{3}'.format(e, e2, e3, e4))

    else:
        assert(0)
    return res            


combos3 = [('cvz', '...'), ('c..', '.ic'), ('y.z', '.y.'), ('..z', 'wy.'),
    ('cD', '..'), ('Dz', '..'), ('K.', '.y'), ('K.', '.e'), ('K.', '.o'),
    ('vG', '..'), ('vL', '..'), ('vy.', '..e'), ('vy.', '..o'), ('vy.', '..y'),
    ('v..', '.we'), ('v..', '.wo'), ('v..', '.wy')]

combos4 = [('cvy.', '...e'), ('cvy.', '...o'), ('cvy.', '...y'),
    ('y.y.', '.y.e'), ('y.y.', '.y.o'), ('y.y.', '.y.y'),
    ('cvG', '...'), ('cvL', '...'), ('y.G', '.y.'), ('..G', 'wy.'),
    ('Kvc', '...'),
    ('y.L', '.y.'), ('..L', 'wy.'),
    ('cDz', '...'), ('Kvz', '...'), ('K.z', '.y.'), ('cvG', '...')
    ]


if __name__ == "__main__":
    totcombs = []
#    for e in combos3:
    for e in combos4:
        totcombs.append('"NEW CLASS",{0}'.format(e[0]))
        totcombs.extend(gen(e))

#    totcombs.extend(['eau', 'aye', 'eye', 'oye', 'awe', 'ewe', 'owe'])        
    for e in totcombs:
        print "{0},".format(e)
