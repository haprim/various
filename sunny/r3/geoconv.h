//#include <string>

#define RESOLUTION      14      //meters, changing this will trash old codes
#define NX 2862440
#define NY 715610

#define ER              6378000
#define NS              "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

#define NUM_AREAS       3
#define MAX_NUM_AREAS   100

#define DEFAULT_BAD_LATLONG -1000

#define _UNI_SIZE 10
#define _LOC_SIZE 15
#define _WS_SIZE 22

typedef long double ld;
typedef signed long long ll;

typedef struct { ld x; ld y; } pt2d;

typedef struct
{
    char area_code[6];
    ld xtopleft;
    ld ytopleft;
    ld xbottomright;
    ld ybottomright;
    int ndigits;
} area_desc;

typedef struct 
{
    int n;
    area_desc cities[MAX_NUM_AREAS];
} area_desc_array;

typedef struct
{
    ld x;
    ld y;
    ld xcnt;
} gridpoint;

typedef struct
{
    char unistr[_UNI_SIZE];
    char areastr[_LOC_SIZE];
    char wordstr[_WS_SIZE];
    int status;
} taggy_results;


typedef struct
{
    char unistr[_UNI_SIZE];
    char areastr[_LOC_SIZE];
    char wordstr[_WS_SIZE];
    int status;
    ld lat;
    ld lng;
} taggy_results2;

/* read config */
area_desc_array read_config_file(char *);

/* Represent integer in base 36 */
void base36(char *result, ll num, int cursor);

/* function to convert geo coordinates to string */
int geo_to_unistr(char *result, pt2d point, int nx, int ny);

/* function to convert string to geo coordinates */
pt2d unistr_to_geo(char *s, int nx, int ny);

/* function to measure distances between two geo coordinate pairs */
ld geo_dist(pt2d pt1, pt2d pt2);

/* function to convert geo coordinates to area string, if possible */
int geo_to_areastr(char *result, pt2d point, area_desc *ars, 
    int nares, int nx, int ny, int fit_to_uni_grid);

/* function to convert area string to geo coordinates */
//pt2d areastr_to_geo(char *s, area_desc *ars = NULL, int nares = NUM_AREAS,
//    int nx = NX, int ny = NY);
pt2d areastr_to_geo(char *s, area_desc *ars, int nares, int nx, int ny,
    int fit_to_uni_grid);

/* function to convert unistr to area string, if possible */
int unistr_to_areastr(char *result, char *uni, area_desc *ars,
    int nares, int nx, int ny);

/* function to convert area string to unistr */
int areastr_to_unistr(char *result, char *areastr, area_desc *ars,
    int nares, int nx, int ny);

/* function to convert geo to unistr and areastr */
taggy_results geo_to_taggy(pt2d point, area_desc *ars, int nares,
    int nx, int ny, int ndecimal);

/* function to convert unistr or areastr into geo code AND unistr / areastr */
taggy_results2 taggy_decode(char *, area_desc *, int, int, int, int ndecimal);

/* function to convert location in local code format to area code + 
 * 7-letter word */
int local_code_to_word(char *result, char *input, int maxlen, 
    area_desc *ars, int nares);

/* function to convert area code + 7-letter word into local code format */
int word_to_local_code(char *result, char *input, int maxlen,
    area_desc *ars, int nares);


