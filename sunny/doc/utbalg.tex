%\input{amstex}
\documentclass{article}
\usepackage{verbatim}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amsthm}

\begin{document}

\title{Geocode conversion method}
\author{Peter Hussami}

\tableofcontents

\pagenumbering{arabic}

\pagebreak
\section{Introduction}

The purpose of this document is to provide the theoretical background for
a method to convert 2-D geo codes into alphanumerical sequences and back.

The exact specifications are:
\begin{itemize}
\item Geo codes must provide a 10-meter resolution anywhere on the planet.
\item The alphanumerical sequence must be case-insensitive and be at most
    8 characters long.
\end{itemize}

\pagebreak
\section{Feasibility}

\subsection{Set cardinalities}

Since the inputs to the algorithm (both directions) is short, compression
algorithms are out of the question: they would require storage of the
compression table, and that is too much overhead under the specifications.

Therefore, we have to find a reversible mapping between the set of possible
geo codes and their string representations.

The cardinality of the string set is easy to compute:
$$C_s = 36^{8} = 2821109907456$$,
approximately $1.2829 * 2^{41}$.

As for the geo codes, a simple calculation can show us the approximate
cardinality of this set. At the equator, 4 significant fractional digits
will give us a resolution of approximately 10 meters (further away from the
equator, this resolution will be finer).
Using this for an estimate, we get:
$$C = 360 * 180 * 10^{4} * 10^{4} = 6480000000000$$,
approximately $2.9468 * 2^{41}$.

While this latter number is more than the double of the first, it is close
enough that we should be able to solve the problem.

\subsection{Approach - geo location to string}

The approach is as follows: given that we wish to have constant spatial
resolution on the Earth's surface, rather than in geo code terms, we should
map the Earth's surface on a grid whose point density is constant.

To do this, let us note that the for any latitude $y$, the length of the
corresponding latitude circle is
$$L(y) = 2 * R * \pi * cos(y)$$,
where $R$ is the Earth's radius.

This means that the 360-degree longitude resolution would give us finer and
finer spatial resolution towards the poles.

Now, to have equal point density, we will represent each latitude by a number
of points that is proportional to its length. So, if the equator is represented
by $k$ points, than any latitude $y$ is represented by $k * cos(y)$ points.

Moreover, we want to count all points with a single variable, so we need to
identify the point where we start counting on each latitude. For easier
computation, we chose this to be the point representing $x = -180$ degrees
everywhere.

In sum, we construct the mapping:
$$F(x, y) = ( (x + 180) * cos(y), y ) $$.

This is already a constant-density space, so rounding here will give us
constant spatial resolution.

Naturally, we cannot round to integers, rather, to some other value that will
keep us within the error bounds.

\subsection{Error and resolution}

Error on a constant spatial resolution grid can be calculated as follows:

Consider grid points $A=(x1, y1), B=(x1, y2), C=(x2, y2), D=(x2, y1)$. Without
loss of generality we may assume $x1 < y1$ and $x2 < y2$.
Any point within this rectangle is at most 
$d_{max}=\frac{\sqrt{(x2-x1)^{2} + (y2-y1)^{2}}}{2}$ from one of the grid
points.

In our case, $x2-x1 = y2-y1$, so we operate on squares. This means
$d_{max}=\frac{\sqrt{2 \delta^{2}}}{2}$.

If we set $d_{max}=10$, we will get
$$\delta = 10 \sqrt{2} \approx 14.1421$$.

So an approximately 14-meter resolution should keep us within a 10-meter error.

\subsection{Rounding}

To continue with where we left off two sections above, we will round to
this $\delta$-resolution grid. In other words, for any latitude $y$, we
split up the latitude with a $\delta$ resolution, and simply count how many
$\delta$-s correspond to our mapped $x$ value.

This will determine the location of the longitudinal point within that latitude.

Now we just need to assign a unique value to the latitudes, and we are done.

\subsection{Latitude offsets}

As noted above, latitude lengths at angle $y$ are proportional to $cos y$.
Therefore, the offset pertaining to each latitude should be approximately
$const + \int{cos y dy}$.

Since the cosine function is not kind enough to have all integer values,
we need to add a term to make absolutely sure that the transformation remains
reversible, i.e.
$$\forall y_1 < y_2: val(+180, y_1) < offset(y_2)$$.

A linear term with a high enough coefficient will do.

Some notes:
\begin{itemize}
\item By arbitrary choice, we start counting on the equator, cover the
northern hemisphere first, going northward, and then proceed to the south, 
going south.
\item Any points "close enough" to the equator (i.e. within 0.5 * 14 meters)
is considered to be on the equator.
\end{itemize}

\subsection{Finally}

We have now counted all $x, y$ points with constant resolution.
We mapped a pair of points to an integer. This integer's representation in the
36-base number system is according to our specifications.

An integral of the offset function will confirm that this first into the
output range.

\section{Approach - string to geo location}

In the previous chapter we defined all the terms and showed a method for
assigning unique number / string to a pair of geo coordinates. The 
terminology will help us explain this direction much more concisely:

\begin{enumerate}
\item Memorize the first offset corresponding to the southern hemisphere 
(this is the latitude just outside the zero band southwards).
\item Determine from the input value whether we should look on the north or
south hemisphere or the equator.
\item Start a binary search on that hemisphere. In each step, we hypothesize
a latitude, and check if the target value falls on that latitude -- i.e.
the value is between the latitude's start offset and it's other end. If yes,
we are done, otherwise we go lower or higher, depending on the numbers.
\item Once the correct latitude is found, the number of $x$ ticks on the
latitude is just a simple subtraction. $x$ can then be mapped back to
a proper longitude by dividing with the latitude length and subtracting
180 degrees.
\end{enumerate}


\end{document}

