#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

#include "geoconv.h"

//FIXME: areas won't work on the dateline for now

pt2d fit_point_on_uni_grid(pt2d p)
{
    char uni[_UNI_SIZE];
    geo_to_unistr(uni, p, NX, NY);
    p = unistr_to_geo(uni, NX, NY);
    return p;
}


area_desc_array read_config_file(char *filename)
{
    area_desc_array result;
    char * line = NULL;
    size_t len = 0;
    ssize_t read = 0;
    int ctr = 0;

    result.n = 0;

    FILE *f = fopen(filename, "r");
    if ( f == NULL )
    {
        return result;
    }

    while ( (read = getline(&line, &len, f)) != -1 )
    {
        char *mobile = line;
        char *token;
        int locctr = 0;
        area_desc kax;
        memset(kax.area_code, 0, 6);

        while ( (token = strsep(&mobile, ",")) )
        {
            switch (locctr)
            {
                case 0:
                    snprintf(kax.area_code, 5, "%s", token);
                    break;
                case 1:
                    kax.xtopleft = atof(token);
                    break;
                case 2:
                    kax.ytopleft = atof(token);
                    break;
                case 3:
                    kax.xbottomright = atof(token);
                    break;
                case 4:                    
                    kax.ybottomright = atof(token);
                    break;
                case 5:
                    kax.ndigits = atoi(token);
                    break;
                default:
                    break;
            }
            locctr += 1;
            if (locctr >= 6)
            {
                pt2d p1;
                p1.x = kax.xtopleft;
                p1.y = kax.ytopleft;
                p1 = fit_point_on_uni_grid(p1);
                kax.xtopleft = p1.x;
                kax.ytopleft = p1.y;

                p1.x = kax.xbottomright;
                p1.y = kax.ybottomright;
                p1 = fit_point_on_uni_grid(p1);
                kax.xbottomright = p1.x;
                kax.ybottomright = p1.y;

                result.cities[result.n] = kax;
                result.n += 1;
                break;
            }
        }
        ctr += 1;
        if ( ctr >= MAX_NUM_AREAS )
        {
            break;
        }
    }
    fclose(f);

    /*
    for (int ii = 0; ii < result.n; ++ii )
    {
        printf("%s %Lf\n", result.cities[ii].area_code, result.cities[ii].xtopleft);
    }
    */
    return result;
}

/*
area_desc areas[NUM_AREAS] =
{
    {"BUD", 19, 48, 20, 47, 4},
    {"MIL", 16, 42, 17, 41, 5},
    {"LON", -0.5, 53, 0.6, 52.4, 5}
};
*/

int is_geo_in_area(pt2d point, area_desc *as)
{
    if ((point.x >= as -> xtopleft) && (point.x <= as -> xbottomright) &&
        (point.y <= as -> ytopleft) && (point.y >= as -> ybottomright))
    {
        return 1;
    }
    return 0;
}

/*
 * Test if a pair of coordinates belongs to a known area
 */
int is_geo_in_areas(pt2d point, area_desc *as, int nareas)
{
    int ii;
    for ( ii = 0; ii < nareas; ++ii )
    {
        if ( is_geo_in_area(point, as + ii) )
        {
            return ii;
        }
    }
    return -1;
}

/*
 * Test if a string is an area string
 */
int is_str_in_areas(const char *s, area_desc *as, int nareas)
{
    int ii;
    unsigned int jj;
    for ( ii = 0; ii < nareas; ++ii )
    {
        int fail = 0;
        for ( jj = 0; jj < strlen(as[ii].area_code); ++jj )
        {
            if (toupper(as[ii].area_code[jj]) != toupper(s[jj]))
            {
                fail = 1;
                break;
            }
        }
        if (!fail) { return ii; }
    }
    return -1;
}

/*
 * Base 36 to base 10 conversion
 */
ll str_to_int(char *s, const char *chrmap, int ctr, int first_time)
{
    if ( s[ctr] == '.' )
    {
        return str_to_int(s, chrmap, ctr - 1, first_time);
    }

    if ( first_time )
    {
        unsigned int ii;
        for ( ii = 0; ii < strlen(s); ++ii )
        {

            if ( (! isalnum(toupper(s[ii]))) && (s[ii] != '.') )
            {
                return -1;
            }
        }
    }

    if ( ctr < 0 )
    {
        return 0;
    }
    ll diff = strchr(chrmap, toupper(s[ctr])) - chrmap;
    return diff + 36 * str_to_int(s, chrmap, ctr - 1, 0);
}


/*
* Separate unistr from areastr
*/
int classify_str(const char *s)
{
    int state = 0; //0 only letters, 1 also dash, 2 alphanum, 3 invalid

    unsigned int ii;

    if ( strlen(s) < 8 )
    {
        return 3;
    }

    for ( ii = 0; ii < strlen(s); ++ii )
    {
        if (s[ii] == '.')
        {
            if ( ii == 3 )
            {
                state = 1;
            }
            else if ( ii == 4 )
            {
                if ( state == 1 )
                {
                    return 3;
                }
                state = 2;
            }
            else
            {
                return 3;
            }
        }
        else if (! isalnum(s[ii]) )
        {
            return 3;
        }
    }

    if (state == 1) 
    { 
        return 1;
    }
    else if (state == 2)
    {
        return 0;
    }
    //err if no dot in position 3 or 4
    return 3;
}

int geo_to_areastr(char *result, pt2d point0, area_desc *ar, int nares, 
    int nx, int ny)
{
    if ((point0.y < -90) || (point0.y > 90) || (point0.x < -180) || 
        (point0.x > 180))
    {
        snprintf(result, _UNI_SIZE, "%s", "");
        return -1;
    }
//    printf("ENTERING G2A, X=%Lf, Y=%Lf\n", point.x, point.y);

    int ac = is_geo_in_areas(point0, ar, nares);

    if (ac == -1)
    {
        return ac;
    }

    area_desc fnd = ar[ac];

    pt2d point = fit_point_on_uni_grid(point0);
//    pt2d point = point0;

//    ld medlat = (fnd.ytopleft + fnd.ybottomright) / 2.0;
    ld avg_ang_width = (fnd.xbottomright - fnd.xtopleft);

    ld ang_height = (fnd.ytopleft - fnd.ybottomright);

//    printf("ml / aw, aw %Lf %Lf %Lf \n", medlat, avg_ang_width, 
//        ang_height);

    ld ratio = ang_height / avg_ang_width;

    ll multitude = (ll)pow(36, fnd.ndigits);
    ll xnum = trunc(sqrt(0.99 * multitude / ratio)); //FIXME 0.99 for safety
    ll ynum = trunc(ratio * xnum); //FIXME 0.99 for safety

    ld xstep = avg_ang_width / xnum;
    ld ystep = ang_height / ynum;

    ll xco = trunc((point.x - fnd.xtopleft) / xstep);
    ll yco = trunc((fnd.ytopleft - point.y) / ystep);

    /*
    printf("\txnum/ynum: %lld / %lld, xco / yco: %lld / %lld\n", 
        xnum, ynum, xco, yco);
    printf("MUL222=%lld, %lld, %lld, %lld %.12Lf %.12Lf\n", 
        multitude, xnum, xco, yco, xstep, ystep);
    */  

    char b36str[_UNI_SIZE];
    memset(b36str, 0, _UNI_SIZE);

    base36(b36str, yco * xnum + xco, fnd.ndigits);
//    printf("\t\t -> %lld, .. %s\n", yco * ynum + xco, b36str);
    snprintf(result, _LOC_SIZE, "%s.%s", fnd.area_code, b36str);
    return 0;
}

/*
 * Convert area string to geo coordinates
 */
pt2d areastr_to_geo(char *s, area_desc *ar, int nares, int nx, int ny)
{
    int cs = classify_str(s);
    if (cs == 3)
    {
        pt2d badres = {DEFAULT_BAD_LATLONG, DEFAULT_BAD_LATLONG};
        return badres;
    }

    int ac = is_str_in_areas(s, ar, nares);

    if (ac == -1)
    {
        pt2d badres = {DEFAULT_BAD_LATLONG, DEFAULT_BAD_LATLONG};
        return badres;
    }

//    printf("ENTERING A2G: %s\n", s);

    area_desc fnd = ar[ac];

//    ld medlat = (fnd.ytopleft + fnd.ybottomright) / 2.0;
    ld avg_ang_width = (fnd.xbottomright - fnd.xtopleft);

    ld ang_height = (fnd.ytopleft - fnd.ybottomright);

    ld ratio = ang_height / avg_ang_width;

    ll multitude = (ll)pow(36, fnd.ndigits);
    ll xnum = trunc(sqrt(0.99 * multitude / ratio)); //FIXME 0.99 for safety
    ll ynum = trunc(ratio * xnum); //FIXME 0.99 for safety

    ld xstep = avg_ang_width / xnum;
    ld ystep = ang_height / ynum;

    char *s2 = s + strlen(fnd.area_code) + 1;
//    string loc = s.substr(strlen(fnd.area_code) + 1, string::npos);
//    printf("LOCC=%s\n", s2);
    
    ll intval = str_to_int(s2, NS, (int) strlen(s2) - 1, 1);
    if ( intval < 0 )
    {
        pt2d ret = {DEFAULT_BAD_LATLONG, DEFAULT_BAD_LATLONG};
        return ret;
    }

    ll xco = intval % xnum;
    ll yco = intval / xnum;

    /*
    printf("\t xnum/ynum: %lld / %lld, iv=%lld, xco / yco: %lld / %lld\n", 
        xnum, ynum, intval, xco, yco);

    printf("MUL222=%lld, %lld, %lld, %lld %.12Lf %.12Lf\n", 
        multitude, xnum, xco, yco, xstep, ystep);
    */  

    pt2d result;

    result.x = xco * xstep + fnd.xtopleft;
    result.y = fnd.ytopleft - yco * ystep;

//    printf("TEMP: %Lf %Lf\n", result.x, result.y);
    result = fit_point_on_uni_grid(result);

//    printf("MUL=%Lf %Lf\n", result.x, result.y);
    return result;
}


int unistr_to_areastr(char *result, char *s, area_desc *ars, int nares, 
    int nx, int ny)
{
    int cs = classify_str(s);
    if (cs == 3)
    {
        return -1;
    }

    pt2d point;

    point = unistr_to_geo(s, nx, ny);
//    printf("GEOMAN: %Lf %Lf\n", point.x, point.y);

    if ( is_geo_in_areas(point, ars, nares) == -1 )
    {
        strcpy(result, s);
        return 1;
    }

    geo_to_areastr(result, point, ars, nares, nx, ny);
    return 0;
    
}


int areastr_to_unistr(char *result, char *s, area_desc *ars, int nares,
    int nx, int ny)
{
    int cs = classify_str(s);
    if (cs == 3)
    {
        return -1;
    }

    if ( is_str_in_areas(s, ars, nares) == -1 )
    {
        strcpy(result, s);
        return 1; //#FIXME we assume it's a unistr
    }

    pt2d point;

    point = areastr_to_geo(s, ars, nares, nx, ny);

    return geo_to_unistr(result, point, nx, ny);
}

taggy_results geo_to_2strings(pt2d point, area_desc *ars, int nares,
    int nx, int ny)
{
    taggy_results result;

    memset(result.unistr, 0, _UNI_SIZE);
    memset(result.areastr, 0, _LOC_SIZE);
    result.status = 0;

    int r2 = geo_to_unistr(result.unistr, point, nx, ny);

    if ( r2 == -1 )
    {
        result.status = 0;
        return result;
    }

    pt2d point2 = fit_point_on_uni_grid(point);
//    printf("XXX GEO %Lf %Lf\n", point.x, point.y);
    int r1 = geo_to_areastr(result.areastr, point2, ars, nares, nx, ny);
    if ( r1 == -1 )
    {
        result.status = 1;
        return result;
    }

    result.status = 2;
    return result;
}


taggy_results2 taggy_decode(char *s, area_desc *ars, int nares,
    int nx, int ny) // status: 0 bad, 1 input was uni, but no corresponding
                    //local, 2: all is well (input either uni or geo)
{
    taggy_results2 result;

    memset(result.unistr, 0, _UNI_SIZE);
    memset(result.areastr, 0, _LOC_SIZE);
    result.status = 0;
    result.lat = DEFAULT_BAD_LATLONG;
    result.lng = DEFAULT_BAD_LATLONG;

    int cs = classify_str(s);
    if ( cs == 3 )
    {
        return result;
    }

    pt2d point;
    if ( cs == 1 ) //it's a local code
    {
        point = areastr_to_geo(s, ars, nares, nx, ny);
        if ( (point.x < -180) || (point.x > 180) || (point.y < -90) || 
                (point.y > 90) ) //could not convert, invalid area code
        {
            return result; //return bad
        }
        result.lat = point.y;
        result.lng = point.x;
        
        snprintf(result.areastr, _LOC_SIZE, "%s", s);

        geo_to_unistr(result.unistr, point, nx, ny);

        result.status = 2;
        return result;
    }
    else // cs == 0, global string
    {
        point = unistr_to_geo(s, nx, ny);
        if ( (point.x < -180) || (point.x > 180) || (point.y < -90) || 
                (point.y > 90) ) //could not convert, invalid uni code
        {
            return result;
        }
        result.lat = point.y;
        result.lng = point.x;

        snprintf(result.unistr, _UNI_SIZE, "%s", s);

        int r = geo_to_areastr(result.areastr, point, ars, nares, nx, ny);
        if ( r == -1 )
        {
            snprintf(result.areastr, _LOC_SIZE, "%s", "");
            result.status = 1;
        }
        else
        {
            result.status = 2;
        }
        return result;
    }
}




/*
 * Place points on grid
 */
gridpoint gridize(pt2d point, int nx, int ny)
{
    ld yunit = 90.0 / (ld) ny;
    ld xunit = 360.0 / (ld) nx;
    ld ycnt = point.y / yunit;
    ld ygrid = round(ycnt) * yunit;
    if (ygrid <= -90.0)
    {
        ygrid = -89.9999999;
    }
    else if (ygrid >= 90.0)
    {
        ygrid = 89.9999999;
    }

    ld latshrink = cos(M_PI * ygrid / 180.0);
//    ld perim = nx * latshrink;

    ld newx = latshrink * (point.x + 180.0);
    ld xcnt = newx / xunit;

    ld xgrid = round(xcnt) * xunit;
    gridpoint result;
    result.x = xgrid;
    result.y = ygrid;
    result.xcnt = xcnt;
    return result;
}

/*
 * Calculate offset corresponding to latitude
 */
ll x_offset(ld y, int nx, int ny)
{
    if ((y > 90) || (y <= -90))
    {
        return 0;
    }
    int cnst = 10;

    if (y < 0)
    {
        ld amp = nx * (ll) ny;
        ll bo = (ll) (2 * amp / M_PI + cnst * ny + 1);
        ll i = round(y / (-90.0 / (ld) ny));
        ll offset = (ll)(ceil((2 * amp / M_PI) * 
            sin(0.5 * M_PI * (i+1) / (ld) ny)) + cnst * (i+1));
        return bo + offset;
    }
    ll i = round(y / (90.0 / (ld) ny));
    ld amp = nx * (ll) ny;
    ll offset = (ll)(ceil((2 * amp / M_PI) * 
        sin(0.5 * M_PI * (i+1) / (ld) ny)) + cnst * (i+1));
    return offset;
}

/*
 * Represent integer in base 36
 */
void base36(char *result, ll num, int cursor)
{
//    printf("\tenter %s %d %ld\n", result, cursor, num);
    if (num == 0)
    {
        memset(result, 0, cursor);
    }
    char c = NS[num % (ll) 36];
    memset(result, '0', cursor);

    result[cursor - 1] = c;
//    printf("MID: %s %d\n", result, strlen(result));
    if (cursor > 0)
    {
        base36(result, num / (ll) 36, cursor - 1);
    }
    return;
}

/*
 * Produce string from geo coordinates
 */
int geo_to_unistr(char *result, pt2d point, int nx, int ny)
{
    if ((point.y < -90) || (point.y > 90) || (point.x < -180) || 
        (point.x > 180))
    {
        snprintf(result, _UNI_SIZE, "%s", "");
        return -1;
    }

    gridpoint gridded = gridize(point, nx, ny);
    ll xo = x_offset(gridded.y, nx, ny);
    ll kk = (ll)round(gridded.xcnt);
    char b36str[_UNI_SIZE];
    memset(b36str, 0, _UNI_SIZE);
    memset(result, '0', _UNI_SIZE - 1);
    base36(b36str, xo + kk, _UNI_SIZE - 2);
//    printf("GOFOR %ld %s\n", xo+kk, b36str);
    snprintf(result, _UNI_SIZE, "%c%c%c%c.%c%c%c%c", b36str[0], b36str[1],
        b36str[2], b36str[3], b36str[4], b36str[5], b36str[6], b36str[7]);
    return 0;
}

/*
 * Binary search
 * val is the target value, mna/mxa define the range.
 */
pt2d binsearch(ll val, ld mna, ld mxa, int nx, int ny)
{
    int mn = round((mna / 90.0) * ny);
    int mx = round((mxa / 90.0) * ny);

    int mid1;
    ld mid;
    ll offs;

    int is_repeat = 0;
    int itctr = 0;
    ld perim;

    while ( 1 )
    {
        mid1 = round((mn + mx) / 2.0);
        if ( is_repeat )
        {
            mid1 = (mn + mx) / 2;
        }
        mid = 90.0 * mid1 / (ld) ny;

        offs = x_offset(mid, nx, ny);
        perim = nx * cos(M_PI * mid / (ld) 180.0);
        if ((offs <= val) && (offs + perim >= val))
        {
            break;
        }

        if (mid1 == mx)
        {
            is_repeat = 1;
        }
        else if (offs < val)
        {
            mn = mid1;
        }
        else
        {
            mx = mid1;
        }

        itctr += 1;
        if (itctr > 100)
        {
//            printf("BAD\n");
            break;
        }

    }

    ll xr = round(val - offs);
    if (xr > perim)
    {
//        printf("STRANGE!!!\n");
    }
    ld xd = (360.0 * xr / perim) - 180.0;
    pt2d result;
    result.x = xd;
    result.y = mid;
    return result;

}

/*
 * Convert string to a pair of geo coordinates
 */
pt2d unistr_to_geo(char *s, int nx, int ny)
{
    int cs = classify_str(s);
//    printf("str=%s, cs=%d\n", s, cs);
    if ((cs != 0) && (cs != 2))
    {
        pt2d result = {DEFAULT_BAD_LATLONG, DEFAULT_BAD_LATLONG};
        return result;
    }

    ll val = str_to_int(s, NS, (int) strlen(s) - 1, 1);
//    printf("VAL=%ld\n", val);
    if ( val < 0 )
    {
        pt2d ret = {DEFAULT_BAD_LATLONG, DEFAULT_BAD_LATLONG};
        return ret;
    }

    ll halver = x_offset(-0.00000001, nx, ny);

    pt2d result;
    if (val < halver)
    {
        result = binsearch(val, (ld) 0.0, (ld) 90.0, nx, ny);
    }
    else
    {
        result = binsearch(val, (ld) -0.00000001, (ld) -90.0, nx, ny);
    }
    return result;
}

/*
 * Measure geo distance between two points
 */
ld geo_dist(pt2d pt1, pt2d pt2)
{
    ld x1 = pt1.x;
    ld x2 = pt2.x;
    ld y1 = pt1.y;
    ld y2 = pt2.y;

    ld y12 = fabsl(y1);
    ld y22 = fabsl(y2);

    ld mx = y12;
    if (y22 > mx)
    {
        mx = y22;
    }
    ld xd = fabsl(x2 - x1);
    if (xd > 180)
    {
        xd = 360.0 - xd;
    }
    ld xdp = M_PI * 2 * cos(mx * M_PI / 180.0) * xd / 360.0;
    ld y1p = M_PI * y1 / 180.0;
    ld y2p = M_PI * y2 / 180.0;
    return ER * sqrt(xdp*xdp + (y2p-y1p)*(y2p-y1p));
}

void unit_test(void)
{
    ld x, y;
    int ii;
    for ( ii = 0; ii < 500000; ++ii )
    {
        x = (ld) ((rand() / (float) RAND_MAX)*360.0 - 180.0);
        y = (ld) ((rand() / (float) RAND_MAX)*180.0 - 90.0);
        pt2d pt;
        pt.x = x;
        pt.y = y;
//        gridpoint gr = gridize(pt, NX, NY);

        char b36[_UNI_SIZE];
        geo_to_unistr(b36, pt, NX, NY);
        pt2d rev = unistr_to_geo(b36, NX, NY);
        ld dd = geo_dist(pt, rev);
        if (dd > 9.99)
        {
            printf("Point1: %Lf/%Lf, Reco: %Lf/%Lf, dist=%Lf\n",
                pt.x, pt.y, rev.x, rev.y, dd);
        }
    }
}

int main2(int argc, char **argv)
{
	char confname[] = "conf.txt";
	area_desc_array ars = read_config_file(confname);

	// the input is a string
	// eg: ./geoconv "5FOL.ON5R"
	// eg: ./geoconv "BUD.ON5R"
	// eg: ./geoconv "ZZZZ.ZZZZ" // should result a not-valid Taggy Code result
	// eg: ./geoconv "Budapest, Kelemen utca 57." // should result a not-valid Taggy Code result
	if (argc == 2) 
        {
            unsigned int ii;
            char *inpstr = argv[1];
            for ( ii = 0; ii < strlen(inpstr); ++ii )
            {
                inpstr[ii] = toupper(inpstr[ii]);
            }

            taggy_results2 res2;

            printf("\t...working from input str\n");
            res2 = taggy_decode(inpstr, ars.cities, ars.n, 
                NX, NY);
            printf("\tRes status: %d", res2.status);
            switch (res2.status)
            {
                case 0: printf(" -- ERROR!\n"); exit(1);

                case 2: printf(" -- area code\n"); break;

                case 1: printf(" -- universal code\n"); break;
            }


            printf("\tInferred GEO codes: lat=%Lf, lng=%Lf\n", 
                res2.lat, res2.lng);
            printf("\tInferred universal code: %s\n", res2.unistr);
            printf("\tInferred local code: %s\n", res2.areastr);

            pt2d point;
            point.x = res2.lng;
            point.y = res2.lat;

            printf("input lat: %Lf\ninput long: %Lf\n\n", point.x, point.y);

            taggy_results result;
            result = geo_to_2strings(point, ars.cities, ars.n, NX, NY);
            printf("result INT: %i\n", result.status);
            printf("local Taggy: %s\n", result.areastr);
            printf("universal Taggy: %s\n", result.unistr);

	}

	// the input is a lat and long
	// eg: ./geoconv 47.4797411 19.0650528 // should result a valid universal and local Taggy Code
	// eg: ./geoconv 37.334912 -122.033057 // should result a valid universal Taggy Code
	// eg: ./geoconv -7500.353871, -1700.222269 // should result a not-valid Taggy Code result
	else if (argc == 3) {
		pt2d point;
		point.x = (ld) atof(argv[1]);
		point.y = (ld) atof(argv[2]);

		printf("input lat: %Lf\ninput long: %Lf\n\n", point.x, point.y);
//		char code[15];
//		geo_to_unistr(code, point, NX, NY);
//		printf("result Taggy: %s\n", code);

		taggy_results result;
		result = geo_to_2strings(point, ars.cities, ars.n, NX, NY);
		printf("result INT: %i\n", result.status);
		printf("local Taggy: %s\n", result.areastr);
		printf("universal Taggy: %s\n", result.unistr);

                printf("\nNow reverting:\n");

                taggy_results2 res2;

                if (result.status == 1) //only universal code available
                {
                    printf("\t...working from universal code\n");
                    res2 = taggy_decode(result.unistr, ars.cities, ars.n, 
                        NX, NY);
                    printf("\tRes status: %d\n", res2.status);
                    printf("\tInferred GEO codes: lat=%Lf, lng=%Lf\n", 
                        res2.lat, res2.lng);
                }
                else if (result.status == 2) //area also available
                {
                    printf("\t...working from area code (to test)\n");
                    res2 = taggy_decode(result.areastr, ars.cities, ars.n, 
                        NX, NY);
                    printf("\tRes status: %d\n", res2.status);
                    printf("\tInferred GEO codes: lat=%Lf, lng=%Lf\n", 
                        res2.lat, res2.lng);
                    printf("\tInferred universal code: %s\n", res2.unistr);
//                    printf("\tInferred local code: %s\n", res2.areastr);
                }
                else
                {
                    printf("\t...changed my mind, bad ststus, sorry\n");
                }

	}

//    /* Switch this on for unit testing */
//    unit_test();

    return 0;
}


int main(int argc, char **argv)
{
    return main2(argc, argv);
    exit(0);

    char confname[] = "conf.txt";
    area_desc_array ars = read_config_file(confname);
    printf("%s %Lf %Lf %Lf %Lf", ars.cities[0].area_code,
        ars.cities[0].xtopleft,
        ars.cities[0].ytopleft,
        ars.cities[0].xbottomright,
        ars.cities[0].ybottomright);

    pt2d ad[3] = {{19.02702, 47.5204}, {29.5, 47.5},{16.6, 41.5}};
    printf("\n%d kax\n", is_geo_in_areas(ad[0], ars.cities, ars.n));
    printf("\n%d kax\n", is_geo_in_areas(ad[0], ars.cities, ars.n));
    printf("%d k2\n", is_geo_in_areas(ad[1], ars.cities, ars.n));
    printf("%d k2\n", is_geo_in_areas(ad[2], ars.cities, ars.n));

    char aad1[_LOC_SIZE], aad2[_LOC_SIZE];
    printf("1 %p\n", aad1);
    geo_to_areastr(aad1, ad[0], ars.cities, ars.n, NX, NY);
    printf("2\n");
    geo_to_areastr(aad2, ad[1], ars.cities, ars.n, NX, NY);
    printf("%s\n", aad1);
    printf("%s\n\n\n", aad2);

    int ii;

    for (ii = 0; ii < 3; ++ii )
    {
        printf("ORIG: LAT=%Lf, LON=%Lf\n", ad[ii].x, ad[ii].y);
        printf("\t%d\n", is_geo_in_areas(ad[ii], ars.cities, ars.n));
        char ast[_LOC_SIZE];

        geo_to_areastr(ast, ad[ii], ars.cities, ars.n, NX, NY);
        printf("AST=%s\n", ast);
        printf("BACK AREA: %d\n", is_str_in_areas(ast, ars.cities, ars.n));
        pt2d a2g = areastr_to_geo(ast, ars.cities, ars.n, NX, NY);
        printf("A2G: %Lf, %Lf\n", a2g.x, a2g.y);
        char ust[12], bust[12];
        areastr_to_unistr(ust, ast, ars.cities, ars.n, NX, NY);
        printf("KAX1 %s\n", ust);
        unistr_to_areastr(bust, ust, ars.cities, ars.n, NX, NY);

        printf("two more strings: %s, %s\n", ust, bust);
    }

    printf("%d\n", classify_str("geo"));
    printf("%d\n", classify_str("geo.d389a"));
    printf("%d\n", classify_str("ge.4d389a"));
    printf("%d\n", classify_str("geo.d38.9a"));
    printf("%d\n", classify_str("geod389a"));
    printf("%d\n", classify_str("geo"));


    if (argc < 3)
    {
        printf("Usage: %s x y\n", argv[0]);
        return 1;
    }

    pt2d point;
    point.x = (ld) atof(argv[1]);
    point.y = (ld) atof(argv[2]);

    printf("Input was: X=%Lf, Y=%Lf\n", point.x, point.y);
    char code[_LOC_SIZE];
    printf("B36=%p\n", code);
    geo_to_unistr(code, point, NX, NY);
    printf("Code: %s\n", code);

    pt2d rev = unistr_to_geo(code, NX, NY);

    printf("Revert from code: X=%Lf, Y=%Lf\n", rev.x, rev.y);

    ld d = geo_dist(point, rev);
    printf("The distance is %Lf (m)\n", d);

    /* Switch this on for unit testing */
    unit_test();

    return 0;
}
