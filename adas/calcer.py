import math
import numpy as np
EPSILON = 0.05

### begin reference data
ref_z0 = 430.0 #mm
ref_z0p = 1571.14

ref_w0=176.0
ref_h0=178.0
ref_diag0 = (ref_w0**2+ref_h0**2)**0.5

ref_pxw0 = 620.0
ref_pxh0 = 620.0

photo_ref_coords =\
{0: [(0, 1158, -645), (0, 1000, -726), (0, 1069, -487), (0, 919, -575)],
 1: [(-338, 0, -608), (-188, 0, -699), (-430, 0, -760), (-279, 0, -848)],
 3: [(-980, 594, 0), (-844, 702, 0), (-874, 462, 0), (-734, 580, 0)],
 2: [(-720, 228, 0), (-536, 228, 0), (-720, 66, 0), (-536, 66, 0)]}

cam_ref_coords = [(-1605, 1405, -1905), (-1795, 1405, 2210)]
### end reference data

#calculate the angle enclosed by two vectors
def calc_angle(pcenter, pn1, pn2):
    v1 = (pn1[0] - pcenter[0], pn1[1] - pcenter[1])
    v2 = (pn2[0] - pcenter[0], pn2[1] - pcenter[1])
    prd = v1[0]*v2[0] + v1[1]*v2[1]

    sz1 = (v1[0]**2 + v1[1]**2)**0.5
    sz2 = (v2[0]**2 + v2[1]**2)**0.5
    ac = prd / (sz1 * sz2)
    phi = math.acos(ac)
    return phi

#heuristic algorithm: get the vertex order by quadrilateral angles
def get_vertex_order(pts, imgx, imgy):

    angles = []
    angles.append((180*calc_angle(pts[0], pts[1], pts[2])    / math.pi, 0))
    angles.append((180*calc_angle(pts[1], pts[0], pts[3])    / math.pi, 1))
    angles.append((180*calc_angle(pts[2], pts[0], pts[3])    / math.pi, 2))
    angles.append((180*calc_angle(pts[3], pts[1], pts[2])    / math.pi, 3))
    angles = sorted(angles)

    vertex_order = {angles[k][1]: i for i, k in enumerate([3, 1, 0, 2])}
    return vertex_order

#calculate coefficients for the magnification equations
def calc_coeffs(p, imgcx, imgcy):
    rp = [(x[0] * ref_w0 / ref_pxw0, x[1] * ref_h0 / ref_pxh0) for x in p]

    va = [rp[i][0] / ref_z0 for i in range(4)]
    vb = [rp[i][1] / ref_z0 for i in range(4)]
    
    a1 = rp[0][0] / ref_z0
    a2 = rp[1][0] / ref_z0
    a3 = rp[2][0] / ref_z0
    b1 = rp[0][1] / ref_z0
    b2 = rp[1][1] / ref_z0
    b3 = rp[2][1] / ref_z0 

    cs = [va[i]**2 + vb[i]**2 + 1 for i in range(4)]
    c1s = [-2 * (va[0] * va[i] + vb[0] * vb[i] + 1) for i in range(4)]
    
    c_1 = a1**2+b1**2+1
    c_2 = a2**2+b2**2+1
    c_3 = a3**2+b3**2+1
    c_12 = -2*(a1*a2 + b1*b2 + 1)
    c_13 = -2*(a1*a3 + b1*b3 + 1)
    c_23 = -2*(a2*a3 + b2*b3 + 1)
    
    c_23 = -2*(va[1]*va[2] + vb[1]*vb[2] + 1)
    c_34 = -2*(va[2]*va[3] + vb[2]*vb[3] + 1)

    result = {'va': va, 'vb': vb, 'cs': cs, 'c1s': c1s, 'c_23': c_23,
        'c_34': c_34, 'rp': rp}

    return result        

#evaluate a substitution value for the magnification equations
def zvcalc(subs, c1, c12, c2, w0, direction):
    discr0 = ((subs * c12)**2 - 4 * c2 * (subs**2 * c1 - w0**2))
    if discr0 < 0:
        raise "A"
    discriminant = discr0 ** 0.5
    return (-subs*c12 + direction * discriminant) / (2 * c2)

#evaluate the total discrepancy for the magnification eq. system at a given
#substitution value
def finner(subs, c1, c2, c3, c12, c13, c23, d12, d13, d23, dr1, dr2):
    z2 = zvcalc(subs, c1, c12, c2, d12, dr1)
    z3 = zvcalc(subs, c1, c13, c3, d13, dr2)

    discrep = z2**2 * c2 + z2*z3 * c23 + z3**2 * c3 - d23**2
    return discrep

#solve the magnification equations
def solvef(f, lo, hi, args, maxstep = 50):
    lo0 = lo
    hi0 = hi
    ctr = 0
    left = True
    while True:
        ctr += 1
        if ctr > maxstep:
            if not left:
                return (lo + hi) / 2.0
                assert(0)
            lo = lo0
            hi = hi0
            args[10] = -args[10]
            ctr = 0
            left = False
            continue

        val = (hi + lo) / 2.0

        try:
            xv = finner(val, args[0], args[1], args[2], args[3], args[4], 
                args[5], args[6], args[7], args[8], args[9], args[10])
            if abs(xv) < EPSILON:
                return val

            xvhi = finner(val+1, args[0], args[1], args[2], args[3], args[4], 
                args[5], args[6], args[7], args[8], args[9], args[10])

            xvlo = finner(val-1, args[0], args[1], args[2], args[3], args[4], 
                args[5], args[6], args[7], args[8], args[9], args[10])

            if abs(xvlo) < abs(xvhi):
                hi = val
            else:
                lo = val
        except:
            hi = (val + hi) / 2.0
            continue
            
#get (camera) 3D coordinates from 2D image coordinates
def convertTo3D(pts0, imgx, imgy):
    pts = [(x[0]-imgx, x[1]-imgy) for x in pts0]

    drs = [-1, -1, -1, -1]
    vertex_order = get_vertex_order(pts, imgx, imgy)

    for i in [1, 2, 3]:
        if vertex_order[0] > vertex_order[i]:
            drs[i] = 1

    coeffs = calc_coeffs(pts, imgx, imgy)

    cs = coeffs['cs']
    c1s = coeffs['c1s']
    c_23 = coeffs['c_23']
    c_34 = coeffs['c_34']

    args = [cs[0], cs[1], cs[2], c1s[1], c1s[2], c_23, ref_w0, ref_h0, ref_diag0, drs[1], drs[2]] 
    soln = solvef(finner, 300, 12000, args)

    z2 = zvcalc(soln, cs[0], c1s[1], cs[1], ref_w0, drs[1])
    z3 = zvcalc(soln, cs[0], c1s[2], cs[2], ref_h0, drs[2])

    dd = -1
    if vertex_order[0] > vertex_order[3]:
        dd = 1
    z4 = zvcalc(soln, cs[2], c_34, cs[3], ref_diag0, dd)

    endpts = []
    for i, e in enumerate([soln, z2, z3, z4]):
        x = coeffs['rp'][i][0] * e / ref_z0
        y = coeffs['rp'][i][1] * e / ref_z0
        endpts.append((x, y, e))

    result = {'3dps': endpts}

    return result

#generic solver for n-th order polynomials of a restricted type
def solver2(M, T, lmmx, n = 4, eps = EPSILON):
    assert(len(T) == len(lmmx))
    assert(len(M) == len(T))
    for e in M:
        assert(len(e) == len(T))

    nv = len(T)

    dminmax = lmmx[:]
    masks = [(n+1)**i for i in reversed(range(len(lmmx)))]
    maxiter = (n+1) ** len(lmmx)
    ctr = 500
    
    while True:
        steps = [((e[1] - e[0]) / float(n)) for e in lmmx]
        evals = []
        for i in range(maxiter):
            sts = [(i // masks[t]) % (n + 1)  for t in range(len(lmmx))]
            vs = [lmmx[t][0] + sts[t] * steps[t] for t in range(len(lmmx))]
            sumerr = 0.0
            for ii, ee in enumerate(vs):
                lineval = sum([(M[ii][t][0] * vs[t] + M[ii][t][1])**M[ii][t][2] 
                    for t in range(len(lmmx))]) - T[ii]
                sumerr += lineval**2                        
                
            evals.append((sumerr, sts))

        minn = sorted(evals)[0]
        if minn[0] < eps:
            return vs

        for i, e in enumerate(minn[1]):
            mini = e - 1
            maxi = e + 1

            if e == 0:
                mini = 0
            elif e == n:
                maxi = e

            lmmx[i] = (lmmx[i][0] + mini * steps[i],
                lmmx[i][0] + maxi * steps[i])
        ctr -= 1
        if ctr <= 0:
            return None
            
#get camera world coordinates from 3D point list
def p3d_to_camera_coords(CAM, W):
    import sympy
    from sympy import Symbol
    import mpmath

    cm = CAM['3dps']

    ds = [(e[0]**2 + e[1]**2 + e[2]**2)**0.5 for e in CAM['3dps']]

    c1 = Symbol('c1')
    c2 = Symbol('c2')
    c3 = Symbol('c3')
    norms2 = [(x[0]**2 + x[1]**2 + x[2]**2) for x in cm]
    wnorms2 = [(float(x[0]**2 + x[1]**2 + x[2]**2)) for x in W]
    f1 = (2 * c1 * (W[1][0] - W[0][0]) + 2 * c2 * (W[1][1] - W[0][1]) +
        2 * c3 * (W[1][2] - W[0][2]) + wnorms2[0] - wnorms2[1] +
            norms2[0] - norms2[1])

    f2 = (2 * c1 * (W[2][0] - W[0][0]) + 2 * c2 * (W[2][1] - W[0][1]) +
        2 * c3 * (W[2][2] - W[0][2]) + wnorms2[0] - wnorms2[2] +
            norms2[0] - norms2[2])

    f3 = (2 * c1 * (W[2][0] - W[1][0]) + 2 * c2 * (W[2][1] - W[1][1]) +
        2 * c3 * (W[2][2] - W[1][2]) + wnorms2[1] - wnorms2[2] +
            norms2[1] - norms2[2])

    w2 = np.array(W[:3])
    C = [np.average(w2[:,0]), np.average(w2[:,1]), np.average(w2[:,2])]

    if (W[0][0] == 0) or (W[0][1] == 0) or (W[0][2] == 0):
        aa = sympy.solve((f1, f2), (c1, c2, c3))

        if c1 not in aa:
            c1 = (norms2[0] - (W[0][1] - aa[c2])**2 - (W[0][2] - aa[c3])**2)**0.5
        else:
            c1 = aa[c1]

        if c2 not in aa:
            c2 = (norms2[0] - (W[0][0] - c1)**2 - (W[0][2] - aa[c3])**2)**0.5
        else:
            c2 = aa[c2]

        if c3 not in aa:
            c3 = (norms2[0] - (W[0][0] - c1)**2 - (W[0][1] - c2)**2)**0.5
        else:
            c3 = aa[c3]
        if c1 > -500:
            c1 = 2 * C[0] - c1
        if c2 < 700:
            c2 = 2 * C[1] - c2
        if c3 > -500:
            c3 = 2 * C[2] - c3

    else:        
        aa = sympy.solve((f1, f2, f3), (c1, c2, c3))
        c1 = aa[c1]
        c2 = aa[c2]
        c3 = aa[c3]

    return c1, c2, c3

if __name__ == "__main__":
    p0=[(1144,533), (1294, 430), (1292, 675), (1452, 556)]
    #p0=[(552, 314), (458,448), (717, 358), (626, 506)]

    imgd = (1920, 1088)
    print convertTo3D(p0, imgd[0] / 2, imgd[1] / 2)

