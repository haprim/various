import cv2
import sys
import numpy as np

EPSILON = 0.0005

def d_euclid(x1, y1, x2, y2):
    return ((x1 - x2)**2 + (y1 - y2)**2) ** 0.5

def findQRMarkers(img0):
    img = cv2.bilateralFilter(img0, 11, 17, 17)
    img = cv2.Canny(img, 30, 400)

    #find contours
    (cont, hier) = cv2.findContours(img, cv2.RETR_TREE, 
        cv2.CHAIN_APPROX_TC89_KCOS)

    areas = {}
    ratok = (1.3, 2.0)
    cands = []

    #for hierarchic contour pairs, check area ratios and center proximities
    #those that pass are candidates
    for i, e in enumerate(hier[0]):
        ar = cv2.contourArea(cont[i])
        areas[i] = ar
        if e[3] == -1:
            continue

        
        if (areas[e[3]] >= ratok[0] * ar) and (areas[e[3]] <= ratok[1] * ar):
            mc = cv2.moments(cont[i])
            mp = cv2.moments(cont[e[3]])
            if abs(mc['m00']) < EPSILON:
                continue
            cxc = mc['m10'] / mc['m00']
            cyc = mc['m01'] / mc['m00']
            cxp = mp['m10'] / mp['m00']
            cyp = mp['m01'] / mp['m00']
            if (abs(cxc - cxp) > 3) and (abs(cyc - cyp) > 3):
                continue

            cands.append((i, e[3], cxc, cyc))

    result = []
    #of all candidates, those that are or can be easily approximated with
    #quadrilaterals are accepted
    for e in cands:
        peri = cv2.arcLength(cont[e[1]], True)
        appr = cv2.approxPolyDP(cont[e[1]], 0.03 * peri, True)

        if len(appr) != 4:
            continue
        result.append({'pts': appr, 'cx': e[2], 'cy': e[3]})

#    for e in result:
#        print e['cx'], e['cy']
    return result

def groupMarkers(mkl):
    sorter = []
    for i, e in enumerate(mkl[:-1]):
        for j in range(i + 1, len(mkl)):
#            d1 = ((mkl[j]['cy'] - e['cy'])**2 + (mkl[j]['cx'] - e['cx'])**2)**0.5
            d = d_euclid(mkl[j]['cx'], mkl[j]['cy'], e['cx'], e['cy'])
            sorter.append((d, i, j))

    result = []
    incomps = {}
    comps = set([])
    blocked = set([])
    
    for e in sorted(sorter):
        if (e[1] in comps) or (e[2] in comps) or (e[1] in blocked) or \
                (e[2] in blocked):
            continue

        if (e[1] not in incomps) and (e[2] not in incomps):
            incomps[e[1]] = e[2]
            incomps[e[2]] = e[1]
            continue

        elif (e[1] in incomps) and (e[2] in incomps):
            continue
            assert(0)

        for i in [1, 2]:
            if e[i] in incomps:
                comps.add(tuple(sorted((e[i], incomps[e[i]], e[3 - i]))))
                blocked.add(e[i])
                blocked.add(incomps[e[i]])
                blocked.add(e[3-i])
                del incomps[incomps[e[i]]]
                del incomps[e[i]]
                    
    return comps        

def getOrientation(mkl, triplets):
    result = []
    for e in triplets:
        #1 determine diagonal
        p = (mkl[e[0]], mkl[e[1]], mkl[e[2]])
#        ds = sorted([ (((p[i]['cy'] - p[(i + 1) % 3]['cy'])**2 +
#                (p[i]['cx'] - p[(i + 1) % 3]['cx'])**2) ** 0.5, i) for
#                i in [0, 1, 2] ])
        ds = sorted([(d_euclid(p[i]['cx'], p[i]['cy'], p[(i + 1) % 3]['cx'],
                p[(i + 1) % 3]['cy']), i) for i in [0, 1, 2]])

        diag = (ds[-1][1], (ds[-1][1] + 1) % 3)
        if p[diag[0]]['cx'] > p[diag[1]]['cx']:
#            print "SWITCHIN"
            diag = (diag[1], diag[0])

        p3 = 3 - diag[0] - diag[1]

        if abs(p[diag[0]]['cx'] - p[diag[1]]['cx']) < EPSILON:
            if p[diag[1]]['cy'] > p[diag[0]]['cy']:
                if p[p3]['cx'] < p[diag[0]]['cx']:
                    orient = 1
                else:
                    orient = 0

        else:                    
            slope = -(p[diag[1]]['cy'] - p[diag[0]]['cy']) / \
                (p[diag[1]]['cx'] - p[diag[0]]['cx'])
            dc = (-slope * p[p3]['cx'] + p[p3]['cy'] + \
                (slope * p[diag[1]]['cx']) - p[diag[1]]['cy']) / \
                (slope**2 + 1)**0.5
            if (slope / dc) > 0:
                orient = 1
            else:
                orient = -1

#            print "TOP", p[p3], orient, slope, dc

        if orient == -1:
            result.append({'top': e[p3], 'right': e[diag[0]], 
                'bottom': e[diag[1]]})
        else:
            result.append({'top': e[p3], 'right': e[diag[1]], 
                'bottom': e[diag[0]]})
#        print "ORIENT", result
#    print result
#    print [(i, e['cx'], e['cy']) for i, e in enumerate(mkl)]
    return result

def fourPoints(mkl, triplets):
    result = []
    for e in triplets:
        extrapol = []

        yr0 = mkl[e['top']]['cy']
        xr0 = mkl[e['top']]['cx']

        for key in ['right', 'bottom']:
            locl = []
#            dcr0_2 = (mkl[e[key]]['cy'] - yr0)**2 + \
#                (mkl[e[key]]['cx'] - xr0)**2
            dcr0_2 = d_euclid(mkl[e[key]]['cx'], mkl[e[key]]['cy'], xr0, yr0)**2

            for i, e2 in enumerate(mkl[e[key]]['pts']):
                d2 = (e2[0][0] - xr0)**2 + (e2[0][1] - yr0)**2
                if d2 > dcr0_2:
                    locl.append((d2, i))

            assert(len(locl) >= 2)
            locl = sorted(locl)
            extrapol.append([locl[-2][1], locl[-1][1]])

        l1 = (mkl[e['right']]['pts'][extrapol[0][0]] - \
            mkl[e['right']]['pts'][extrapol[0][1]])[0]
        l2 = (mkl[e['bottom']]['pts'][extrapol[1][0]] - \
            mkl[e['bottom']]['pts'][extrapol[1][1]])[0]

        l1 = [l1[1], -l1[0]]
        l2 = [l2[1], -l2[0]]

        c1 = -l1[0] * mkl[e['right']]['pts'][extrapol[0][0]][0][0] - \
            l1[1] * mkl[e['right']]['pts'][extrapol[0][0]][0][1]

        c2 = -l2[0] * mkl[e['bottom']]['pts'][extrapol[1][0]][0][0] - \
            l2[1] * mkl[e['bottom']]['pts'][extrapol[1][0]][0][1]

        yy = (l1[0] * c2 - l2[0] * c1) / (l2[0] * l1[1] - l1[0] * l2[1])
        xx = (l1[1] * c2 - l2[1] * c1) / (l1[0] * l2[1] - l2[0] * l1[1])

        locpts = [(xx, yy)]
        for ii, akey in enumerate(['right', 'bottom']):
            anchor = mkl[e[akey]]['pts']
            #we have the intersection point, now finish smoothly:
            #from right
#        d1 = (mkl[e['right']]['pts'][extrapol[0][0]][0][0] - xx)**2 + \
#            (mkl[e['right']]['pts'][extrapol[0][0]][0][1] - yy)**2
#        d2 = (mkl[e['right']]['pts'][extrapol[0][1]][0][0] - xx)**2 + \
#            (mkl[e['right']]['pts'][extrapol[0][1]][0][1] - yy)**2
            
            d1 = d_euclid(anchor[extrapol[ii][0]][0][0],
                anchor[extrapol[ii][0]][0][1], xx, yy)
            d2 = d_euclid(anchor[extrapol[ii][1]][0][0],
                anchor[extrapol[ii][1]][0][1], xx, yy)

            if d1 < d2:
                p = anchor[extrapol[ii][1]][0]
            else:            
                p = anchor[extrapol[ii][0]][0]
            locpts.append((p[0], p[1]))            

        """
        d1 = (mkl[e['bottom']]['pts'][extrapol[1][0]][0][0] - xx)**2 + \
            (mkl[e['bottom']]['pts'][extrapol[1][0]][0][1] - yy)**2
        d2 = (mkl[e['bottom']]['pts'][extrapol[1][1]][0][0] - xx)**2 + \
            (mkl[e['bottom']]['pts'][extrapol[1][1]][0][1] - yy)**2
        if d1 < d2:
            p = mkl[e['bottom']]['pts'][extrapol[1][1]][0]
        else:            
            p = mkl[e['bottom']]['pts'][extrapol[1][0]][0]
        locpts.append((p[0], p[1]))            
        """

        dmax = 0
        dloc = None
        for e2 in mkl[e['top']]['pts']:
            d = d_euclid(xx, yy, e2[0][0], e2[0][1])
            if d > dmax:
                dmax = d
                dloc = (e2[0][0], e2[0][1])
        locpts.append((dloc[0], dloc[1]))
        l2 = [locpts[3], locpts[1], locpts[2], locpts[0]]
        result.append(l2)

    return result

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Usage: {0} image [show_normalized_qr_codes]".format(sys.argv[0])
        print "To display the extracted QR codes, use 1 as the second argument"
        print "The first window closes at key press, the QR normalized windows need to have the window closed"
        quit()

    fn = sys.argv[1]

    img = cv2.imread(fn, cv2.IMREAD_GRAYSCALE)
    imgy, imgx = img.shape

    #find all markers in the image. this would be closed quadrilateral
    #curves with roughly approximable area ratios
    markers = findQRMarkers(img)

    #group markers. produces groups of 3. primitive approach: everyone to 
    #their closest match(es), greedy algorithm
    m3s = groupMarkers(markers)

    #identify the 'top', 'right', and 'bottom' markers
    trb = getOrientation(markers, m3s)

    #extrapolate the fourth corner based on the 'right' and 'bottom' 
    #marker edges
    fpl0 = fourPoints(markers, trb)
    std = sorted([(min([e[0] for e in x]), i) for i, x in enumerate(fpl0)])
    fpl = []
    for e in reversed(std):
        fpl.append(fpl0[e[1]])

    print "Found {0} QR images".format(len(fpl))

    im2 = img.copy()
    im2 = cv2.cvtColor(im2, cv2.COLOR_GRAY2RGB)
    for e in fpl:
        ee = np.array([[e[0], e[1], e[3], e[2]]])
        cv2.polylines(im2, ee, True, color=(0, 255, 255))
    cv2.imshow('image', im2)
#    cv2.imwrite('op.jpg', im2)
#    cv2.waitKey()

    if len(sys.argv) > 2:
        if sys.argv[2] == '1':
            import matplotlib.pyplot as plt

            for i, e in enumerate(fpl):
                pp1 = np.float32([[e[0][0], e[0][1]], [e[1][0], e[1][1]],
                            [e[2][0], e[2][1]], [e[3][0], e[3][1]]])
                pp2 = np.float32([[0, 0], [200, 0], [0, 200], [200, 200]])
                M = cv2.getPerspectiveTransform(pp1, pp2)
                dst = cv2.warpPerspective(img, M, (200, 200))
                plt.imshow(dst)
                plt.savefig('kax{0}.png'.format(i))
                plt.subplot(121), plt.imshow(img)
                plt.subplot(122), plt.imshow(dst)
                plt.show()

    import calcer
    tally = {0: [], 1: [], 2:[]}
    for i, e in enumerate(fpl):
        print '\t{0}: {1}'.format(i + 1, e)
        pts = calcer.convertTo3D(e, imgx / 2, imgy / 2)
        soln = calcer.p3d_to_camera_coords(pts, calcer.photo_ref_coords[i])
        print "\t\t yields: X={0}, Y={1}, Z={2}\n".format("%.4f" % soln[0],
            "%.4f" % soln[1], "%.4f" % soln[2])
        for j in range(3):
            tally[j].append(float(soln[j]))

    coordnames = ['X', 'Y', 'Z']
    stats = []
    for j in range(3):
        stats.append((np.average(tally[j]), np.std(tally[j])))
        print "Camera {0}: M={1}, SD={2}".format(coordnames[j], 
            stats[-1][0], stats[-1][1])

    print "\nCompare: actual camera coordinates: X={0}, Y={1}, Z={2}".format(
        "%.4f" % calcer.cam_ref_coords[0][0], 
        "%.4f" % calcer.cam_ref_coords[0][1],
        "%.4f" % calcer.cam_ref_coords[0][2])

#    cv2.waitKey(0)
