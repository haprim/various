import math
import numpy as np
from numpy.linalg import inv

EPSILON = 0.05

imgd = (1920, 1088)
imcr = (imgd[0]/2, imgd[1]/2)

p0=[(1144,533), (1294, 430), (1292, 675), (1452, 556)]
p0=[(552, 314), (458,448), (717, 358), (626, 506)]

#1 find the ellipse center at the intersection of the diagonals
#N1[1] should not be 0
N1 = (p0[3][1] - p0[0][1], p0[0][0] - p0[3][0])
N2 = (p0[2][1] - p0[1][1], p0[1][0] - p0[2][0])

C1 = (N1[0] * p0[0][0] + N1[1] * p0[0][1])
C2 = (N2[0] * p0[2][0] + N2[1] * p0[2][1])

xs = (C2 - N2[1] * C1 / N1[1]) / (N2[0] - N2[1] * N1[0] / N2[0])
ys = (C1 - N1[0] * xs) / N1[1]
print N1, N2, C1, C2, xs, ys

def subser(subs, cx, cy, x1, y1, x2, y2, x3, y3):
    rotma = [[math.cos(subs), -math.sin(subs)], 
             [math.sin(subs), math.cos(subs)]]

    xp1 = (x1 - cx) * rotma[0][0] + (y1 - cy) * rotma[0][1]
    yp1 = (x1 - cx) * rotma[1][0] + (y1 - cy) * rotma[1][1]

    xp2 = (x2 - cx) * rotma[0][0] + (y2 - cy) * rotma[0][1]
    yp2 = (x2 - cx) * rotma[1][0] + (y2 - cy) * rotma[1][1]

    xp3 = (x3 - cx) * rotma[0][0] + (y3 - cy) * rotma[0][1]
    yp3 = (x3 - cx) * rotma[1][0] + (y3 - cy) * rotma[1][1]

    """
    print xp1, yp1
    print xp2, yp2
    print xp3, yp3
    quit()
    """

    b2 = (yp2**2 - (xp2 / xp1)**2 * yp1**2) / (1 - (xp2 / xp1)**2)
    b2 = ((xp1 * yp2)**2 - (xp2 * yp1)**2) / (xp1**2 - xp2**2)
    a2 = b2 * xp1**2 / (b2 - yp1**2)
    a2 = b2 * (xp1**2 - xp2**2) / (yp2**2 - yp1**2)

    rv = ((xp3**2 / a2) + (yp3**2 / b2) - 1, a2, b2)

    print "xp1={0}, yp1={1}, xp2={2}, yp2={3}, xp3={4}, yp3={5}".format(
        xp1, yp1, xp2, yp2, xp3, yp3)
    print "\ta2={0}, b2={1}, rv={2}".format(a2, b2, rv[0])
#    print xp1, yp1, a2, b2, xp3, yp3, xp3**2 / a2, yp3**2 / b2, rv, '\n'
#    quit()

    return rv


def solvef(f, lo, hi, args):
    ctr = 0
    while True:
        if ctr > 20:
            break
        val = (hi + lo) / 2.0
        val = -ctr * 0.05
        print "VAL=", val

        try:
            xv, a2, b2 = subser(val, args[3], args[4], args[0][0], args[0][1],
                args[1][0], args[1][1], args[2][0], args[2][1])

            if abs(xv) < EPSILON:
                return val

            """
            xvhi, _, _ = subser(val + 0.001, args[3], args[4], args[0][0], args[0][1],
                args[1][0], args[1][1], args[2][0], args[2][1])

            xvlo, _, _ = subser(val - 0.001, args[3], args[4], args[0][0], args[0][1],
                args[1][0], args[1][1], args[2][0], args[2][1])
            """

            if xv > 0:
                hi = val
                print "L"
            else:
                lo = val
                print "R"
            """
#            if xv < -EPSILON:
            if abs(xvlo) < abs(xvhi):
                hi = val
            else:
                lo = val
            """                
        except:
            hi = val
            

#        print val, xv, a2, b2, hi, lo, xvhi, xvlo
#        print val, xv, a2, b2, hi, lo
        print xv, a2, b2
        ctr += 1

def convert_to_3D(p, imgcx, imgcy):
    args = [p[0], p[1], p[2], imgcx, imgcy]

    ff=solvef(subser, -math.pi, math.pi, args)

if __name__ == "__main__":

    args = [p0[0], p0[1], p0[2], xs, ys]
#ff=solvef(subser, -math.pi / 2.0, math.pi / 2.0, args)
    ff=solvef(subser, -math.pi, math.pi, args)
    print "FINAL", ff
#c12 = [a2**2+b2**2+1, -2*(a1*a2_b1*b2+1),

    M = np.array([[(p0[k][0] - xs)**2, (p0[k][1] - ys)**2, 
        2 * (p0[k][0] - xs) * (p0[k][1] - ys)] for k in range(3)])


    xx = [(3**0.5 + 1, -1), (1, 3 ** 0.5 - 1), (-1, (5 * (3**0.5) + 7) / 13.0)]


    xx = [(p0[0][0]-xs, p0[0][1] - ys), (p0[1][0]-xs, p0[1][1]-ys),
        (p0[2][0]-xs, p0[2][1]-ys)]

#M = np.array([[(3**0.5 + 1)**2, 1, -2 * (3**0.5 + 1)],
#    [1, (3**0.5 - 1)**2, 2 * (3**0.5 - 1)],
#    [1, ((5*3**0.5 + 7) / 13.0)**2, -2 * (5*3**0.5 + 7) / 13.0]])
    M = np.array([[xx[0][0]**2, xx[0][1]**2, 2*xx[0][0]*xx[0][1]],
        [xx[1][0]**2, xx[1][1]**2, 2 * xx[1][0]*xx[1][1]],
        [xx[2][0]**2, xx[2][1]**2, 2 * xx[2][0]*xx[2][1]]])

    print xx
    print M
    D = np.transpose(np.array([1,1,1]))
    D2 = np.transpose(D)
    D.shape = (1,3)
    print D2[:, np.newaxis]
    C = inv(M)

    C11 = sum(C[0])
    C22 = sum(C[1])
    C12 = sum(C[2])
    print C11, C22, C12
    print C

    theta = -0.5 * np.arctan(2 * C12 / (C22 - C11))

    print "th", 180*theta/math.pi
    e1 = C11 + C22
    n1 = (C22 - C11) / np.cos(2 * theta)

    print n1, e1, n1 - e1


    sma = (2 / abs(n1 - e1)) ** 0.5
    smb = (2 / abs(n1 + e1)) ** 0.5
    print theta, sma, smb

    R=np.arange(0, 2*np.pi, 0.01)
    xps = sma*np.cos(R)*np.cos(theta) - smb*np.sin(R)*np.sin(theta)
    yps = sma*np.cos(R)*np.sin(theta) + smb*np.sin(R)*np.cos(theta)
    from pylab import *
    plot([xx[k][0] for k in range(3)], [xx[k][1] for k in range(3)])
    plot(xps, yps, color='red')
    show()
#coeffs

