import cv2
import sys
import numpy as np

EPSILON = 0.0005

def findQRMarkers(img0):
    img = cv2.bilateralFilter(img0, 11, 17, 17)
    img = cv2.Canny(img, 30, 400)

    (cont, hier) = cv2.findContours(img, cv2.RETR_TREE, 
        cv2.CHAIN_APPROX_TC89_KCOS)
#        cv2.CHAIN_APPROX_NONE)

    areas = {}
    ratok = (1.3, 2.0)
    cands = []

    for i, e in enumerate(hier[0]):
        ar = cv2.contourArea(cont[i])
        areas[i] = ar
        if e[3] == -1:
            continue

        
        if (areas[e[3]] >= ratok[0] * ar) and (areas[e[3]] <= ratok[1] * ar):
            mc = cv2.moments(cont[i])
            mp = cv2.moments(cont[e[3]])
            if abs(mc['m00']) < EPSILON:
                continue
            cxc = mc['m10'] / mc['m00']
            cyc = mc['m01'] / mc['m00']
            cxp = mp['m10'] / mp['m00']
            cyp = mp['m01'] / mp['m00']
            if (abs(cxc - cxp) > 3) and (abs(cyc - cyp) > 3):
                continue

            cands.append((i, e[3], cxc, cyc))

    result = []
    for e in cands:
        peri = cv2.arcLength(cont[e[1]], True)
        appr = cv2.approxPolyDP(cont[e[1]], 0.03 * peri, True)

        if len(appr) != 4:
            continue
        result.append({'pts': appr, 'cx': e[2], 'cy': e[3]})
    return result

def groupMarkers(mkl):
    print "ENTERING GM"
    for i, e in enumerate(mkl):
        print i, e
    sorter = []
    for i, e in enumerate(mkl[:-1]):
        for j in range(i + 1, len(mkl)):
            d = ((mkl[j]['cy'] - e['cy'])**2 + (mkl[j]['cx'] - e['cx'])**2)**0.5
            sorter.append((d, i, j))

    result = []
    incomps = {}
    comps = set([])
    blocked = set([])
    
    print sorted(sorter)            
    for e in sorted(sorter):
        if (e[1] in comps) or (e[2] in comps) or (e[1] in blocked) or \
                (e[2] in blocked):
            continue

        if (e[1] not in incomps) and (e[2] not in incomps):
            incomps[e[1]] = e[2]
            incomps[e[2]] = e[1]
            continue

        elif (e[1] in incomps) and (e[2] in incomps):
            assert(0)

        for i in [1, 2]:
            if e[i] in incomps:
                comps.add(tuple(sorted((e[i], incomps[e[i]], e[3 - i]))))
                blocked.add(e[i])
                blocked.add(incomps[e[i]])
                blocked.add(e[3-i])
                del incomps[incomps[e[i]]]
                del incomps[e[i]]
                    
        print comps
    return comps        

def getOrientation(mkl, triplets):
    result = []
    for e in triplets:
        #1 determine diagonal
        p = (mkl[e[0]], mkl[e[1]], mkl[e[2]])
        ds = sorted([ (((p[i]['cy'] - p[(i + 1) % 3]['cy'])**2 +
                (p[i]['cx'] - p[(i + 1) % 3]['cx'])**2) ** 0.5, i) for
                i in [0, 1, 2] ])
        diag = (ds[-1][1], (ds[-1][1] + 1) % 3)

        p3 = 3 - diag[0] - diag[1]

        if abs(p[diag[0]]['cx'] - p[diag[1]]['cx']) < EPSILON:
            if p[diag[1]]['cy'] > p[diag[0]]['cy']:
                if p[p3]['cx'] < p[diag[0]]['cx']:
                    orient = 1
                else:
                    orient = 0

        else:                    
            slope = (p[diag[1]]['cy'] - p[diag[0]]['cy']) / \
                (p[diag[1]]['cx'] - p[diag[0]]['cx'])
            dc = (-slope * p[p3]['cx'] + p[p3]['cy'] + \
                (slope * p[diag[1]]['cx']) - p[diag[1]]['cy']) / \
                (slope**2 + 1)**0.5

            if (slope / dc) > 0:
                orient = 1
            else:
                orient = -1

        if orient == -1:
            result.append({'top': e[p3], 'right': e[diag[1]], 
                'bottom': e[diag[0]]})
        else:
            result.append({'top': e[p3], 'right': e[diag[0]], 
                'bottom': e[diag[1]]})

    return result

def fourPoints(mkl, triplets):
    result = []
    for e in triplets:
        print "TRIP", e
        extrapol = []

        yr0 = mkl[e['top']]['cy']
        xr0 = mkl[e['top']]['cx']

        for key in ['right', 'bottom']:
            locl = []
            dcr0_2 = (mkl[e[key]]['cy'] - yr0)**2 + \
                (mkl[e[key]]['cx'] - xr0)**2

            for i, e2 in enumerate(mkl[e[key]]['pts']):
                d2 = (e2[0][0] - xr0)**2 + (e2[0][1] - yr0)**2
                print i, d2, e2[0][0], e2[0][1]
                if d2 > dcr0_2:
                    locl.append((d2, i))

            print locl, dcr0_2, xr0, yr0, key
            assert(len(locl) >= 2)
            locl = sorted(locl)
            extrapol.append([locl[-2][1], locl[-1][1]])

        l1 = (mkl[e['right']]['pts'][extrapol[0][0]] - \
            mkl[e['right']]['pts'][extrapol[0][1]])[0]
        l2 = (mkl[e['bottom']]['pts'][extrapol[1][0]] - \
            mkl[e['bottom']]['pts'][extrapol[1][1]])[0]

        l1 = [l1[1], -l1[0]]
        l2 = [l2[1], -l2[0]]

        c1 = -l1[0] * mkl[e['right']]['pts'][extrapol[0][0]][0][0] - \
            l1[1] * mkl[e['right']]['pts'][extrapol[0][0]][0][1]

        c2 = -l2[0] * mkl[e['bottom']]['pts'][extrapol[1][0]][0][0] - \
            l2[1] * mkl[e['bottom']]['pts'][extrapol[1][0]][0][1]

        yy = (l1[0] * c2 - l2[0] * c1) / (l2[0] * l1[1] - l1[0] * l2[1])
        xx = (l1[1] * c2 - l2[1] * c1) / (l1[0] * l2[1] - l2[0] * l1[1])

        locpts = [(xx, yy)]
        #we have the intersection point, now finish smoothly:
        #from right
        d1 = (mkl[e['right']]['pts'][extrapol[0][0]][0][0] - xx)**2 + \
            (mkl[e['right']]['pts'][extrapol[0][0]][0][1] - yy)**2
        d2 = (mkl[e['right']]['pts'][extrapol[0][1]][0][0] - xx)**2 + \
            (mkl[e['right']]['pts'][extrapol[0][1]][0][1] - yy)**2
        if d1 < d2:
            p = mkl[e['right']]['pts'][extrapol[0][1]][0]
        else:            
            p = mkl[e['right']]['pts'][extrapol[0][0]][0]
        locpts.append((p[0], p[1]))            

        d1 = (mkl[e['bottom']]['pts'][extrapol[1][0]][0][0] - xx)**2 + \
            (mkl[e['bottom']]['pts'][extrapol[1][0]][0][1] - yy)**2
        d2 = (mkl[e['bottom']]['pts'][extrapol[1][1]][0][0] - xx)**2 + \
            (mkl[e['bottom']]['pts'][extrapol[1][1]][0][1] - yy)**2
        if d1 < d2:
            p = mkl[e['bottom']]['pts'][extrapol[1][1]][0]
        else:            
            p = mkl[e['bottom']]['pts'][extrapol[1][0]][0]
        locpts.append((p[0], p[1]))            

        dmax = 0
        dloc = None
        for e2 in mkl[e['top']]['pts']:
            d = (e2[0][0] - xx)**2 + (e2[0][1] - yy)**2
            if d > dmax:
                dmax = d
                dloc = (e2[0][0], e2[0][1])
        locpts.append((dloc[0], dloc[1]))
        l2 = [locpts[3], locpts[1], locpts[2], locpts[0]]
        result.append(l2)

    return result

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Usage: {0} image".format(sys.argv[0])
        quit()

    fn = sys.argv[1]

    img = cv2.imread(fn, cv2.IMREAD_GRAYSCALE)

    markers = findQRMarkers(img)
    m3s = groupMarkers(markers)

    trb = getOrientation(markers, m3s)

    fpl = fourPoints(markers, trb)

    ctrx = img.shape[1] / 2.0
    ctry = img.shape[0] / 2.0
    for e in fpl:
        x1 = e[0][0]
        x2 = e[1][0]
        x3 = e[2][0]
        y1 = e[0][1]
        y2 = e[1][1]
        y3 = e[2][1]

        AB = -(x2-x1)*(x3-x1)+(y2-y1)*(y3-y1)
        A2_B2 = (x3-x1)**2+(y3-y1)**2 - (x2-x1)**2 - (y2-y1)**2
        print AB, A2_B2
        a1 = (A2_B2 - (A2_B2**2 + 4 * AB)**0.5) / 2.0
        a2 = (A2_B2 + (A2_B2**2 + 4 * AB)**0.5) / 2.0
        print "\t", a1, a2
        print img.shape
        print "D", ((e[1][0]-e[0][0])**2+(e[1][1]-e[0][1])**2)**0.5, \
            ((e[2][0]-e[0][0])**2+(e[2][1]-e[0][1])**2)**0.5
    print fpl
    quit()
    import matplotlib.pyplot as plt
    for i, e in enumerate(fpl):
        pp1 = np.float32([[e[0][0], e[0][1]], [e[1][0], e[1][1]],
            [e[2][0], e[2][1]], [e[3][0], e[3][1]]])
        pp2 = np.float32([[0, 0], [200, 0], [0, 200], [200, 200]])
        M = cv2.getPerspectiveTransform(pp1, pp2)
        dst = cv2.warpPerspective(img, M, (200, 200))
        plt.imshow(dst)
        plt.savefig('kax{0}.png'.format(i))
        plt.subplot(121), plt.imshow(img)
        plt.subplot(122), plt.imshow(dst)
        plt.show()


    quit()        

#    print hier[0]
    
    for i, e in enumerate(cont):
        M = cv2.moments(e)
        peri = cv2.arcLength(e, True)

        appr = cv2.approxPolyDP(e, 0.02 * peri, True)
        if (len(appr) == 4) or (int(M['m01'] / M['m00']) == 15):
            print i, e[0], cv2.contourArea(e), M['m10'] / M['m00'], M['m01'] / M['m00'], len(appr)
    cv2.drawContours(img, cont, -1, (0,255,0), 2)
    cv2.imshow('image', img)

#    cv2.waitKey(0)
