%\input{amstex}
%\documentclass{article}
\documentclass[10pt]{report}
\usepackage{verbatim}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amsthm}
%\usepackage{url}
%\usepackage{graphicx}

\begin{document}

\title{$\;$ Computer vision project summary}
\author{Peter Hussami}

\maketitle

\tableofcontents
\chapter{Task outline}

The tasks was as follows. I was supposed to:
\begin{itemize}
\item Create some QR code printouts of known size and place them at locations is various, known positions (w.r.t. some absolute reference point),
\item Take a photo of the setup,
\item Identify the QR codes in the photo, also read them perhaps,
\item Infer 3D coordinates from the image,
\item From the various 3D coordinate sets, infer the camera position,
\item Examine how some of the variables -- number of images, camera distance -- affect the outcome.
\end{itemize}

\chapter{Solution ideas, the selected approach}

After some investigation and brainstorming I found various ways to the solution.

\section{QR code detection}

As for detecting the QR code: while there are Python libraries for handling 
this, the ones I found are apparently oldish and not maintained. I did not
manage to install them. Fair enough, I use Fedora, and these were for Ubuntu,
but the result is the same. Therefore, I had to implement my own QR detection
algorithm.

The approach is as follows:
\begin{itemize}
\item Find the markers, i.e. hierarchically related closed contour pairs in the image, such that
    \begin{enumerate}
    \item their centers are closely aligned,
    \item their area ratio is within set bounds, and
    \item they are quadrilateral (or if not, they can be approximated as such with small error.
    \end{enumerate}

\item Group markers into sets of 3. Primitive algorithm: compute pairwise distances and then greedily cluster them into clusters of 3 points. This will only work well if the input QR samples are distant enough from each other.

\item Identify 'top', 'right' and 'bottom' markers within each 3-set.

\item Infer the fourth corner of the sample by intersecting the line extensions
to the appropriate boundaries of the 'right' and 'bottom' markers.
\end{itemize}

\section{3D reconstruction from the QR quadrilaterals}

Several approaches here would qualify.

\begin{itemize}
\item \textbf{Direct "arithmetic" solution} 
    \begin{itemize}
    \item The camera is assumed to look in the direction of the image 
        center, with 
        unknown depth. 
    \item Depth parameters are assigned to each image point (=vertex of the 
        quadrilateral). We can define a parametric notion of "magnification" by 
        measuring a "reference" image, and taking its depth coordinate as 
        reference.
    \item The real-life object dimensions are known, and the 
        horizontal/vertical 
        coordinates of the object vertices can be expressed in terms of their 
        image coordinates and the depth parameter. This yields a system of 
        quadratic equations with only the depth parameters unknown.
    \item The solution allows us to reconstruct the 3D coordinates w.r.t to the
        camera's coordinate system.
    \end{itemize}

\item \textbf{Nicer geometric solution}
    \begin{itemize}
    \item We view the problem as two rotations: one in an unknown plane around 
        and unknown center, and the second perpendicular to this plane, around
        the object center. We use a reference object (in a known plane, at a 
        known distance).
    \item The circle touching all 4 corners of the reference rectangle (square)
        will turn into an ellipse under the first of the above-mentioned 
        rotations. The major axis of this ellipse will only depend on the 
        object's new distance from the camera.
    \item Thus, the length of the major axis (relative to the reference radius)
        determines the distance, the direction determines the first plane of
        rotation, and the minor to major axis ratio determines the angle in the
        second plane of rotation.
    \item Having reconstructed the transformation operations, we can now apply
        the transformations to the reference object to get the target object's
        3D coordinates.
    \end{itemize}

    Note: objects that are close to the camera will not actually be on the 
    ellipse, but an A4 sheet from 2-3 meters is already far enough that the
    method should work.

\item \textbf{Using OpenCV's built-in functionality}
    This approach involves the use of OpenCV functions to get the job done.
    While it is reasonable that OpenCV can do most or all that is is 
    supposed to do, I did not find the documentation verbose enough to
    go down this path. Especially, I had difficulty getting the extrinsic
    parameters.
\end{itemize}


\section{Reconstructing the world coordinates of the camera from the object 
    world and camera coordinates}

Using the camera 3D coordinates obtained by one of the methods above, we can
obtain the camera coordinates as well.

I can quote at least two ways for doing this:
\begin{itemize}
\item Finding a matrix transformation between the coordinate systems. This
    involves a rotation as well as a translation, so it should have a degree
    of freedom of 12, i.e. it requires all 4 object vertices.

\item Since the existence of a reference object allowed us to determine the
    camera 3D coordinates in real-life metric already (not pixels), we can
    think of the camera / object combination as a pyramid whose base is
    fully known, as is the apex' distance to all base vertices. This leads to
    a quadratic equation system for the camera's center in world coordinates.
\end{itemize}

\chapter{The selected methods, more details about the specific approach}

While there were various ideas for solving the problem, I was seriously 
constrained by the lack of a professional solution to the QR detecting problem.

\medskip

The reason is quite simple: as much as I have learnt a lot building a QR
detection function from scratch, it is nowhere nearly as good as an
established solution. Some of the object were not found, the boundaries that
allowed the inference of the fourth (unmarked) point were quite short, so
the points are estimated with some error.

\medskip

This led me to conclude that I should rely on methods that require 3 object
points only, to the extent possible. E.g. I tried the ellipse fitting method,
setting as the center of the ellipse the intersection of the diagonals as would
be appropriate -- but the ellipse would not fit, likely because of the error in
the fourth point's coordinates.

\medskip
Thus, by exclusion I went with the plain "arithmetic" approach for the
camera coordinate reconstruction using the object's 3 marker points, and then
solved the 3 equations for 3 spheres around each of the marker vertices, 
with the constraint that the camera center is on the spheres' surface.

\section{Details of the approach}

\subsection{Object 3D coordinates reconstruction}

\begin{itemize}
\item I measured a reference object in world space. Its world and image 
dimensions allowed me to define a reference projection plane for the image.
This is of course not the real projection plane, but that hardly matters. I 
then used this plane's depth coordinates for calculating all magnifications.

\item The solver for the quadratic equation system for the 3 object points'
depth coordinates was quite sensitive: a.) to negative discriminants, b.)
the greater / smaller relations between the roots. The discriminant's
negativity was due to actually impossible projections, stumbling upon which
was not possible during a binary search. As for the greater / smaller
relation between the root, a simple heuristic worked well: I was able to
determine the 'top' object vertex' relation to its two neighbors from the
angles of the quadrilateral.

\item I did not calibrate the camera or adjust the image for camera distortion.
This might seem as unholy to a computer vision expert, but I was already
beyond the deadline and did not have a chance both to study and implement 
something in a field new to me.

\end{itemize}

\section{Camera coordinates reconstruction}

The chosen method was to intersect 3 spheres. Technically, it is a system of
quadratic equations. Due to various symmetries, there are several possible
solutions over 3 vertices -- still, I did not include the fourth vertex due
to its uncertainty.

\medskip
I used the sympy package as well as wrote my own 
numerical solver for finding the roots, and found by experiment that even the
system's solver-friendly form is not solver-friendly.

\medskip
By subtracting the equations from each other, I could reduce the system into
linear equations, but this was cheating in the sense that I lost roots.
Nonetheless, this is exactly what I did, and then mirrored the inferred
camera coordinates around the object to get the final solution.

\chapter{Results}

Given the uncertainty of all the above, the results for the main image were
as follows.

\begin{enumerate}
\item 3 out of 4 QR codes were found in the image (this is \textbf{not} nice).
\item 2 out of the 3 recognized QR codes have the fourth vertex estimated
correctly (this is also not too nice). But the fourth vertex did not interfere
with the results (this is why I chose the inference methods I did).
\item For an actual camera placed at (-1605 mm, 1405 mm, -1905 mm) from a 
reference point, I got (-1708.3 mm, 1190.2 mm, -2008 mm) plus a standard 
deviation of 136-448 mm. The error of the mean was thus (133 mm, 214.8 mm,
103 mm), around 27 cm at a distance of 2.86 m, less than 10\%. Given the
erroneous nature of the QR measurements and everything that followed from it,
I think these results are not bad.
\end{enumerate}

\medskip

I also wanted to check what happens if we a.) change the number of objects, and
b.) if we change the camera position / distance to the objects.

\medskip

For changing the objects, the answer suggested by the results is that more
objects might give us better measurements, if they are placed in a way that
their errors can cancel each other's, i.e. their orientation angles are
uniformly distributed along the unit sphere. I believe if they are placed
at similar angles, their errors will do more to reinforce each other than to
cancel each other. But this is just intuition, further data would be needed
to conclude this.

\medskip

As for playing with the camera distance, I did do that, but the QR detection
failed on the second image. It is add, as the QR function was tested on
various images, however, close to the deadline, and having destroyed the setup
after taking the photo, I cannot run this experiment. Intuition says that
the QR detection might be less accurate due to pixel size, but distortion, 
especially for slanted objects will be smaller.

\chapter{How to do a better job in more time}

\begin{enumerate}
\item Better QR detection. Ideally an professional external solution, rather 
than my own hack.
\item Inclusion of camera calibration data.
\item Getting the coordinates through computing and applying the object's 
transformation matrix.
\item Getting 3D coordinates by a matrix operation, rather than the spherical
approach.
\end{enumerate}

Note that the above (save the first entry) require 4 points (IMHO), and this
is part of the reason why I could not use them at present.

\end{document}
